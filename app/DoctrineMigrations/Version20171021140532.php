<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171021140532 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ps_product_ebay_message (id BIGINT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, title VARCHAR(400) NOT NULL, seriesPage TINYINT(1) DEFAULT NULL, created DATETIME NOT NULL, id_product INT DEFAULT NULL, store INT NOT NULL, test INT NOT NULL, INDEX IDX_D1AC6E55A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ps_product_ebay_message ADD CONSTRAINT FK_D1AC6E55A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user CHANGE store store INT DEFAULT NULL, CHANGE status status INT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ps_product_ebay_message');
        $this->addSql('ALTER TABLE user CHANGE store store INT NOT NULL, CHANGE status status INT DEFAULT NULL');
    }
}
