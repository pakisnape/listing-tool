<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180107203525 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bug_report_message DROP FOREIGN KEY FK_C282884BF396750');
        $this->addSql('ALTER TABLE bug_report_message ADD bugReport INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bug_report_message ADD CONSTRAINT FK_C2828844C7C8302 FOREIGN KEY (bugReport) REFERENCES bug_report (id)');
        $this->addSql('CREATE INDEX IDX_C2828844C7C8302 ON bug_report_message (bugReport)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bug_report_message DROP FOREIGN KEY FK_C2828844C7C8302');
        $this->addSql('DROP INDEX IDX_C2828844C7C8302 ON bug_report_message');
        $this->addSql('ALTER TABLE bug_report_message DROP bugReport');
        $this->addSql('ALTER TABLE bug_report_message ADD CONSTRAINT FK_C282884BF396750 FOREIGN KEY (id) REFERENCES bug_report (id)');
    }
}
