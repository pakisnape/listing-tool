<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180103211631 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_activation (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, created DATETIME NOT NULL, id_product INT DEFAULT NULL, productData LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\', store INT DEFAULT NULL, INDEX IDX_8F0F3F5CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_activation ADD CONSTRAINT FK_8F0F3F5CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        //$this->addSql('ALTER TABLE ps_product_ebay_message DROP FOREIGN KEY FK_D1AC6E55A76ED395');
        //$this->addSql('DROP INDEX IDX_D1AC6E55A76ED395 ON ps_product_ebay_message');
        //$this->addSql('ALTER TABLE ps_product_ebay_message ADD user_id INT NOT NULL');
        //$this->addSql('ALTER TABLE ps_product_ebay_message ADD CONSTRAINT FK_D1AC6E55A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        //$this->addSql('CREATE INDEX IDX_D1AC6E55A76ED395 ON ps_product_ebay_message (user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE product_activation');
        //$this->addSql('ALTER TABLE ps_product_ebay_message DROP FOREIGN KEY FK_D1AC6E55A76ED395');
        //$this->addSql('DROP INDEX IDX_D1AC6E55A76ED395 ON ps_product_ebay_message');
        //$this->addSql('ALTER TABLE ps_product_ebay_message DROP user_id');
        //$this->addSql('ALTER TABLE ps_product_ebay_message ADD CONSTRAINT FK_D1AC6E55A76ED395 FOREIGN KEY (test) REFERENCES user (id)');
        //$this->addSql('CREATE INDEX IDX_D1AC6E55A76ED395 ON ps_product_ebay_message (test)');
    }
}
