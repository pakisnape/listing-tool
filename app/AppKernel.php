<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new Oneup\FlysystemBundle\OneupFlysystemBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            new Realestate\MssqlBundle\RealestateMssqlBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
			new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}

return array (
    'parameters' =>
        array (
            'database_host' => '127.0.0.1',
            'database_port' => NULL,
            'database_name' => 'pawnamerica',
            'database_user' => 'paluser',
            'database_password' => 'PalNet!1ubuntu',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'RLemccBj5IKuMIItPrD9ZmiZNOE0uP1ndSEQieOQIe6p93H5JKUZLtjy',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => 1484092800,
    'locale' => 'en-US',
    'cookie_key' => 'WbHInEPNnQMFCwK1m0CPRdoj3eoPyKwzgALUHrPRZQ5S8WgH4Xwr2q5a',
    'cookie_iv' => 'Ze79fBmv',
    'new_cookie_key' => 'def000001bb55b3f5597e3234ab0492d179735f6645420175f39dc8285e98943e95f0e8f388dfbd446f20e8bc2328c553e589984515bd15a5b0f69693f3b9e8801e7bc14',
  ),
);

