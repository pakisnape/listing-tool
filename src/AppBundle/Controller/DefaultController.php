<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('ROLE_SALES')")
 */
class DefaultController extends Controller
{
	/**
	 * @Route("/", name="homepage")
	 * @Security("is_granted('ROLE_SALES')")
	 */
	public function indexAction(Request $request)
	{
		$this->denyAccessUnlessGranted('ROLE_USER');
		$needInfo = [];

		/** @var User $user */
		$user = $this->getUser();

		$end = date('Y-m-d H:m:s');
		$start = date("Y-m-d H:i:s", strtotime("-2 days", strtotime($end)));


		if ($user->getStore()) {
			$store = $user->getStore();
		} else {
			$store = (empty($request->get('store', '')) ? '' : $request->get('store'));
		}

		if ($store) {
			$pawnOrders = $this->container->get('pawn.order')->getLatestOrdersByStore($store,$start);
		} else {
			$pawnOrders = $this->container->get('pawn.order')->getLatestOrders($start);
		}
		$year = date("Y");
		$month = date('m');
		$day = date('d');

		$end = $year."-".$month."-".$day." 22:00:00";


		$start = date("Y-m-d H:i:s", strtotime("-1 days", strtotime($end)));
		$newOrders = $this->container->get('pawn.order')->getNewOrdersCount($start, date('Y-m-d 22:00:00'), $store);
		$activeProducts = $this->container->get('pawn.products')->getActiveItemsBySupplier($store);


		$activationStart     = date("Y-m-d 00:00:00");
		$activationEnd       = date("Y-m-d 23:59:59");
		$dailyActivatedProducts = $this->container->get('pawn.product_activation')->getActivatedProducts($activationStart,
			$activationEnd, $store);

		$dailyActivatedEbayItems = $this->container->get('pawn.ebay_activation')->getActivatedProducts($activationStart,$activationEnd,$store);


		$allProducts = $this->container->get('pawn.products')->getReadyToSync(2);
		if ($user->getStore()) {
			foreach ($allProducts as $product) {
				if ($product['id_supplier'] == $user->getStore()) {
					$needInfo[] = $product;
				}
			}
		}

		$lastEditedProducts = $this->container->get('pawn.last_edited_service')->getLastEditedByUser($user,10);

		// replace this example code with whatever you need
		return $this->render('default/index.html.twig', [
			'orders' => $pawnOrders,
			'newOrders' => $newOrders,
			'ebay' => $dailyActivatedEbayItems,
			'infoNeeded' => count((empty($needInfo) ? $allProducts : $needInfo)),
			'lastEdited' => $lastEditedProducts,
			'activation' => $dailyActivatedProducts
		]);
	}
}
