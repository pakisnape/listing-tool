<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BugReport;
use AppBundle\Entity\BugReportMessage;
use AppBundle\Entity\User;
use AppBundle\Form\BugForm;
use AppBundle\Form\BugReportForm;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class BugController extends Controller
{
	/**
	 * @Route("/bugs", name="bugs_list")
	 * @Security("is_granted('ROLE_SALES')")
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction(Request $request)
	{
		$bugForm = new \AppBundle\Entity\BugForm();

		$form = $this->createForm(BugForm::class, $bugForm);

		$form->handleRequest($request);
		$search = "";
		if ($form->isSubmitted() && $form->isValid()) {
			$search = $form->get('search')->getData();
		}

		if($search) {
			$repo = $this->getDoctrine()->getRepository(BugReport::class);
			$query = $repo->createQueryBuilder('a')
				->where('a.bug LIKE :bug')
				->andWhere('a.active = :active')
				->setParameter('bug', '%'.$search.'%')
				->setParameter('active', 1)
				->getQuery();
			$bugs = $query->getResult();
			return $this->render('bug/index.html.twig',[
				'bugs' => $bugs,
				'form' => $form->createView(),
			]);
		}
		$bugs = $this->getDoctrine()->getRepository(BugReport::class)->findBy(['active' => 1]);

		return $this->render('bug/index.html.twig',[
			'bugs' => $bugs,
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/create-bug", name="create_bug")
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function newAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUser();

		$form = $this->createForm(BugReportForm::class);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$bugReport = new BugReport();
			$bugReport->setUser($user);
			$date = new DateTime();
			$bugReport->setCreated($date);
			$bugReport->setActive(1);
			$bugReport->setBug($form->get('message')->getData());




			$em = $this->getDoctrine()->getManager();
			$em->persist($bugReport);
			//$em->persist($bugReportMessage);
			$em->flush();
			$this->container->get('pawn.mail_service')->sendBugMail('nam797@gmail.com',$bugReport);
			return $this->redirectToRoute('homepage');
		}

		return $this->render('bug/new.html.twig',[
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/view-bug", name="view_bug")
	 * @Security("is_granted('ROLE_SALES')")
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function viewAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUser();

		$budId = $request->get('id');

		$bug = $this->getDoctrine()->getRepository(BugReport::class)->find($budId);

		if(!$bug) {
			throw $this->createNotFoundException('The bug does not exist');
		}

		$form = $this->createForm(BugReportForm::class);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$date = new DateTime();
			$bugReportMessage = new BugReportMessage();
			$bugReportMessage->setUser($user);
			$bugReportMessage->setCreated($date);
			$bugReportMessage->setBugReport($bug);
			$bugReportMessage->setMessage($form->get('message')->getData());

			$em = $this->getDoctrine()->getManager();
			$em->persist($bugReportMessage);
			$this->container->get('pawn.mail_service')->sendBugMessageMail('nam797@gmail.com',$bugReportMessage);
			$em->flush();
		}

		return $this->render('bug/view.html.twig',[
			'form' => $form->createView(),
			'bug' => $bug
		]);
	}
}
