<?php

namespace AppBundle\Controller;



use AppBundle\Entity\ActivationForm;
use AppBundle\Entity\Product;
use AppBundle\Entity\SupplierForm;
use AppBundle\Entity\User;
use AppBundle\Form\ProductForm;
use AppBundle\Form\StoreForm;
use AppBundle\Services\PrestaShopWebserviceException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/product")
 */
class ProductController extends Controller
{
	/**
	 * @Route("/list", name="product_list")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function indexAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUser();

		$this->denyAccessUnlessGranted('ROLE_USER');

		$subStore = $request->get('store');
		$active = $request->get('active', 1);
		$sort = $request->get('sort', 'id_product');
		$direction = $request->get('direction', 'desc');
		$search = $request->get('search', null);

		$suppliers = $this->container->get('pawn.supplier_service')->getSuppliers();

		$getStroe = ($subStore ? $subStore : ($user->getStore() ? $user->getStore() : 1) );

		$supplierForm = new SupplierForm();
		$supplierForm->setStore($getStroe);
		$supplierForm->setActive($active);
		$supplierForm->setSearch($search);

		$form = $this->createForm(StoreForm::class, $supplierForm, [
			'suppliers' => $this->container->get('pawn.supplier_service')->transformData($suppliers),
		]);

		$form->handleRequest($request);

		$store = ($user->getStore() ? $user->getStore() : 1 );
		if ($form->isSubmitted() && $form->isValid()) {
			$store = $form->get('store')->getData();
			$active = $form->get('active')->getData();
			$search = $form->get('search')->getData();
		}

		if($subStore) {
			$store = $subStore;
		}

		$products = $this->container->get('pawn.products')->getAllProductsTest($store,$active,$sort,$direction,$search);
		$paginator  = $this->get('knp_paginator');

		$pagination = $paginator->paginate(
			$products, /* query NOT result */
			$request->query->getInt('page', 1)/*page number*/,
			10/*limit per page*/
		);
		$pagination->setParam('store', $store);
		$pagination->setParam('active', $active);
		$pagination->setParam('search',$search);
		return $this->render('search/advanced.html.twig', [
			'pagination' => $pagination,
			'form' => $form->createView(),
			'store' => $store,
			'active' => $active
//			'search' => $search,
//			'products' => $products
		]);
	}

	/**
	 * @Route("/activation", name="product_upload")
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function GetActivationAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUser();
		$store = $request->get('store');
		$product = $request->get('product');

		$product = $this->get('pawn.product_activation')->getProdcutactivationDateByStore($store,$product);

		return $this->render('product/activation.html.twig',[
			'date' => $product['created']
		]);
	}

	/**
	 * @Route("/upload", name="product_upload")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function uploadAction(Request $request)
	{
		if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'];          //3
			$targetPath = $this->getParameter('image_directory');  //4
			$targetFile = $targetPath.$_FILES['file']['name'];  //5
			move_uploaded_file($tempFile, $targetFile); //6
			$this->container->get('pawn.apiservice')->uploadPicture($targetFile, $request->get('id'));
			unlink($targetFile);
		}

		return new JsonResponse(['yes']);
	}


	/**
	 * @Route("/edit", name="product_edit")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function editAction(Request $request)
	{
		$product = $request->get('id_product');
		$success = "";
		$error = "";

		/** @var User $user */
		$user = $this->getUser();

		$this->denyAccessUnlessGranted('ROLE_USER');

		$pawnProduct = $this->container->get('pawn.products')->getPawnProduct($product);


		$fastBaySettings = $this->container->get('pawn.products')->getFastBaySettings($product);

		$carriers = $this->container->get('pawn.carrier')->getProductCarriers($product);

		$productData = $this->container->get('pawn.products')->transformData($pawnProduct, $fastBaySettings, $carriers);

		$brands = $this->container->get('pawn.brand_service')->getBrands();

		$currentImages = $this->container->get('pawn.products')->getImages($product);


		$messages = $this->container->get('pawn.message')->getMessages($product);

		$images = [];

		$currentCategories = $this->container->get('pawn.category_service')->getProductCategories($product);
		$categories = $this->container->get('pawn.category_service')->getMainCategories();
//		echo "<pre>";
//		print_r($currentCategories);die;

		//TODO move to service
		if (!empty($currentImages)) {
			foreach ($currentImages as $image) {
				$imageParts = str_split($image['id_image']);
				$path = implode("/", $imageParts);
				$images[$image['id_image']] = [
					'path' => "https://www.pawnamerica.com/img/p/".$path."/".$image['id_image']."-small_default.jpg",
					'cover' => $image['cover'],
					'position' => $image['position']
				];

			}
		}
//		echo "<pre>";
//		print_r($images);die;
		$form = $this->createForm(ProductForm::class, $productData, [
			'brands' => $this->container->get('pawn.brand_service')->transformData($brands),
		]);


		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$this->container->get('pawn.products')->processForm($form,$user);
				$success = "Product was updated!";
				return $this->redirectToRoute('product_list');
			} catch (PrestaShopWebserviceException $ex) {
				$error = "There was an error ".$ex->getMessage();
			}
		}

		return $this->render('product/edit.html.twig', [
			'product' => $pawnProduct,
			'form' => $form->createView(),
			'error' => (isset($error) ? $error : ''),
			'images' => $images,
			'success' => $success,
			'imagesRaw' => $currentImages,
			'messages' => $messages,
			'user' => $user,
			'fastbay' => $fastBaySettings,
			'linkRewrite' => $pawnProduct['link_rewrite'],
			'categories' => $categories,
			'currentCats' => $currentCategories,
			'defaultCat' => $productData->getCatDefault()
		]);
	}

	/**
	 * @Route("/edit-new", name="product_edit_new")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function editNewAction(Request $request)
	{
		$product = $request->get('id_product');
		$success = "";
		$error = "";

		/** @var User $user */
		$user = $this->getUser();

		$this->denyAccessUnlessGranted('ROLE_USER');



		$pawnProduct = $this->container->get('pawn.products')->getPawnProduct($product);
		$pimData = '';

        $pimData = $this->get('pawn.pim')->getProductMasterRecord($pawnProduct);

        if(!$pimData) {
            $this->get('pawn.pim')->addToPim($pawnProduct);
        }


		$fastBaySettings = $this->container->get('pawn.products')->getFastBaySettings($product);

		$carriers = $this->container->get('pawn.carrier')->getProductCarriers($product);

		$productData = $this->container->get('pawn.products')->transformData($pawnProduct, $fastBaySettings, $carriers);

		$brands = $this->container->get('pawn.brand_service')->getBrands();

		$currentImages = $this->container->get('pawn.products')->getImages($product);


		$messages = $this->container->get('pawn.message')->getMessages($product);

		$images = [];

		$currentCategories = $this->container->get('pawn.category_service')->getProductCategories($product);
		$categories = $this->container->get('pawn.category_service')->getMainCategories();




		//TODO move to service
		if (!empty($currentImages)) {
			foreach ($currentImages as $image) {
				$imageParts = str_split($image['id_image']);
				$path = implode("/", $imageParts);
				$images[$image['id_image']] = [
					'path' => "https://www.pawnamerica.com/img/p/".$path."/".$image['id_image']."-small_default.jpg",
					'cover' => $image['cover'],
					'position' => $image['position']
				];

			}
		}
//		echo "<pre>";
//		print_r($images);die;
		$form = $this->createForm(ProductForm::class, $productData, [
			'brands' => $this->container->get('pawn.brand_service')->transformData($brands),
		]);


		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$this->container->get('pawn.products')->processForm($form,$user);
				$success = "Product was updated!";
				return $this->redirectToRoute('product_list');
			} catch (PrestaShopWebserviceException $ex) {
				$error = "There was an error ".$ex->getMessage();
			}
		}

		return $this->render('product/editnew.html.twig', [
			'product' => $pawnProduct,
			'form' => $form->createView(),
			'error' => (isset($error) ? $error : ''),
			'images' => $images,
			'success' => $success,
			'imagesRaw' => $currentImages,
			'messages' => $messages,
			'user' => $user,
			'fastbay' => $fastBaySettings,
			'linkRewrite' => $pawnProduct['link_rewrite'],
			'categories' => $categories,
			'currentCats' => $currentCategories,
			'defaultCat' => $productData->getCatDefault(),
			'pim' => $pimData
		]);
	}

	/**
	 * @Route("/image/order", name="image_order")
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function imageOrderAction(Request $request)
	{
		$images = $request->get('image', []);
		$product = $request->get('product', '');

		if (!empty($images) && !empty($product)) {
			$this->container->get('pawn.apiservice')->changeImageOrder(array_filter($images), $product);
		}
		return new JsonResponse(['confirmation']);
	}

	/**
	 * @Route("/cover/image", name="image_cover")
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function markCoverImage(Request $request)
	{
		$images = $request->get('image', []);
		$product = $request->get('product', '');

		if (!empty($images) && !empty($product)) {
			$this->container->get('pawn.apiservice')->markCoverImage(array_filter($images), $product);
		}

		return new JsonResponse(['confirmation']);
	}

	/**
	 * @Route("/cats", name="product_cats")
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function changeCats(Request $request)
	{
		$cats = $request->get('cats', []);
		$product = $request->get('product', '');

		if(!empty($cats) && !empty($product)) {
			$this->container->get('pawn.products')->UpdateCategories($cats,$product);
		}
		return new JsonResponse(['confirmation']);
	}

	/**
	 * @Route("/delete/images", name="image_delete")
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function deleteImageAction(Request $request)
	{
		$images = $request->get('image', '');
		$product = $request->get('product', '');

		if (!empty($images) && !empty($product)) {
			$this->container->get('pawn.apiservice')->deleteImages($images, $product);
		}

		return new JsonResponse(['confirmation']);
	}

	/**
	 * @Route("/add/message", name="add_ebay_message")
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function addMessageAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUser();
		$message = $request->get('message', '');
		$product = $request->get('product', '');

		if (!$user->getStore()) {
			$this->container->get('pawn.products')->updateSyncSetting($product);
		}

		$this->container->get('pawn.message')->setMessage($product, $message, $user);

		return new JsonResponse(['confirmation']);
	}

	/**
	 * @Route("/edited-list", name="product_edited_list")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function editedListAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUser();

		$this->denyAccessUnlessGranted('ROLE_USER');


		$page = $request->get('page', 1);

		$lastEditedProducts = $this->container->get('pawn.last_edited_service')->getLastEditedByUser($user,null);

		return $this->render('product/edited.html.twig', [
			'products' => $lastEditedProducts,
		]);
	}

	/**
	 * @Route("/activation", name="product_activation")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function activationAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUser();

		$subStore = $request->get('store');
		$start = $request->get('start', date("Y-m-d"));// 00:00:00
		$end = $request->get('end', date("Y-m-d"));// 23:59:59


		$suppliers = $this->container->get('pawn.supplier_service')->getSuppliers();

		$getStroe = ($subStore ? $subStore : ($user->getStore() ? $user->getStore() : 1) );

		$activationForm = new ActivationForm();
		$activationForm->setStore($getStroe);
		$activationForm->setStart($start);
		$activationForm->setEnd($end);


		$form = $this->createForm(\AppBundle\Form\ActivationForm::class, $activationForm, [
			'suppliers' => $this->container->get('pawn.supplier_service')->transformData($suppliers),
		]);

		$form->handleRequest($request);

		$store = ($user->getStore() ? $user->getStore() : 1 );
		if ($form->isSubmitted() && $form->isValid()) {
			$store = $form->get('store')->getData();
			$start = $form->get('start')->getData();
			$end = $form->get('end')->getData();
		}

		if($subStore) {
			$store = $subStore;
		}

		$ids = $this->container->get('pawn.product_activation')->getActivatedProducts($start,
			$end, $store);

		$products = $this->container->get('pawn.product_activation')->getActivatedProductsByIds($ids);

			//$this->container->get('pawn.products')->getAllProductsTest($store,$active,$sort,$direction,$search);
		$paginator  = $this->get('knp_paginator');

		$pagination = $paginator->paginate(
			$products, /* query NOT result */
			$request->query->getInt('page', 1)/*page number*/,
			20/*limit per page*/
		);
		$pagination->setParam('store', $store);
		$pagination->setParam('start', $start);
		$pagination->setParam('end', $end);

		return $this->render('search/activation.html.twig', [
			'pagination' => $pagination,
			'form' => $form->createView(),
			'store' => $store,
//			'search' => $search,
//			'products' => $products
		]);
	}

	/**
	 * @Route("/ebay", name="product_on_ebay")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function ebayAction(Request $request	)
	{
		/** @var User $user */
		$user = $this->getUser();

		$subStore = $request->get('store');
		$start = $request->get('start', date("Y-m-d"));// 00:00:00
		$end = $request->get('end', date("Y-m-d"));// 23:59:59

		$suppliers = $this->container->get('pawn.supplier_service')->getSuppliers();

		$getStroe = ($subStore ? $subStore : ($user->getStore() ? $user->getStore() : 1) );

		$activationForm = new ActivationForm();
		$activationForm->setStore($getStroe);
		$activationForm->setStart($start);
		$activationForm->setEnd($end);

		$form = $this->createForm(\AppBundle\Form\ActivationForm::class, $activationForm, [
			'suppliers' => $this->container->get('pawn.supplier_service')->transformData($suppliers),
		]);

		$form->handleRequest($request);

		$store = ($user->getStore() ? $user->getStore() : 1 );
		if ($form->isSubmitted() && $form->isValid()) {
			$store = $form->get('store')->getData();
			$start = $form->get('start')->getData();
			$end = $form->get('end')->getData();
		}

		if($subStore) {
			$store = $subStore;
		}

		$products = $this->container->get('pawn.ebay_activation')->getActivatedProducts($start,$end,$store);

		//$this->container->get('pawn.products')->getAllProductsTest($store,$active,$sort,$direction,$search);
		$paginator  = $this->get('knp_paginator');

		$pagination = $paginator->paginate(
			$products, /* query NOT result */
			$request->query->getInt('page', 1)/*page number*/,
			20/*limit per page*/
		);
		$pagination->setParam('store', $store);

		return $this->render('search/ebay.html.twig', [
			'pagination' => $pagination,
			'form' => $form->createView(),
			'store' => $store,
//			'search' => $search,
//			'products' => $products
		]);
	}
}