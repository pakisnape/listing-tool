<?php

namespace AppBundle\Controller;


use AppBundle\Entity\CodeRelease;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Form\ReleaseForm;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class ReleaseController extends Controller
{
	/**
	 * @Route("/release-info", name="release_info")
	 * @Security("is_granted('ROLE_SALES')")
	 */
	public function recentReleasesAction(Request $request)
	{

		/** @var User $user */
		$user = $this->getUser();

		$messages = $this->getDoctrine()->getRepository(Message::class)->findBy([
			'user'=> $user,
			'seen' => 0],
			null,
			3
		);

		return $this->render('release/releasenav.html.twig',[
			'messages' => $messages
		]);
	}

	/**
	 * @Route("/releases", name="releases")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function indexAction()
	{
		$release = $this->getDoctrine()->getRepository(CodeRelease::class)->findAll();

		return $this->render('release/index.html.twig', [
			'releases' => $release
		]);
	}

	/**
	 * @Route("/create-release", name="create_release")
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function newAction(Request $request)
	{
		$release = new CodeRelease();

		$form = $this->createForm(ReleaseForm::class, $release);

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$release->setBug($form->get('bug')->getData());

			$date = new DateTime();
			$release->setCreated($date);
			$release->setDevMessage($form->get('devMessage')->getData());
			$release->setUserMessage($form->get('userMessage')->getData());



			$em = $this->getDoctrine()->getManager();
			$em->persist($release);
			$em->flush();

			$notify = $this->container->get('pawn.release_service')->newRelease($release->getId());
			return $this->redirectToRoute('releases');
		}

		return $this->render('release/new.html.twig', [
			'form' => $form->createView(),
		]);
	}

}