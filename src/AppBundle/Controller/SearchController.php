<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Security("is_granted('ROLE_USER')")
 */
class SearchController extends Controller
{
    /**
     * @Route("/search", name="search")
     * @Security("is_granted('ROLE_USER')")
     */
    public function indexAction(Request $request)
    {
        $search = $request->query->get('q', '');

        $products = $this->container->get('pawn.products')->getPawnProducts($search);

        return $this->render('search/index.html.twig', [
            'search' => $search,
            'products' => $products
        ]);
    }

	/**
	 * @Route("/advanced-search", name="advanced_search")
	 * @Security("is_granted('ROLE_USER')")
	 */
    public function advancedAction(Request $request)
	{
		$products = $this->container->get('pawn.products')->getAllProductsTest();
		$paginator  = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$products, /* query NOT result */
			$request->query->getInt('page', 1)/*page number*/,
			10/*limit per page*/
		);

		return $this->render('search/advanced.html.twig', [
			'pagination' => $pagination
//			'search' => $search,
//			'products' => $products
		]);
	}
}