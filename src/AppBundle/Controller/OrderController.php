<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Security("is_granted('ROLE_USER')")
 */
class OrderController extends Controller
{

	/**
	 * @Route("/order", name="order")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function indexAction(Request $request)
	{
		$reference = $request->get('id', 0);
		$pawnOrder = $this->container->get('pawn.order')->getOrder($reference);

		if (empty($pawnOrder)) {
			throw    $this->createNotFoundException('Order not found');
		}

		$shipping = $this->container->get('pawn.order')->getAddress($pawnOrder[0]['id_address_delivery']);
		$billing = $this->container->get('pawn.order')->getAddress($pawnOrder[0]['id_address_invoice']);
		$products = $this->container->get('pawn.order')->getNotifierOrderedProducts($pawnOrder[0]);


		return $this->render('order/order.html.twig', [
			'order' => $pawnOrder[0],
			'shipping' => $shipping,
			'billing' => $billing,
			'products' => $products,
		]);
	}


	/**
	 * @Route("/orders", name="orders")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function ordersAction(Request $request)
	{
		$this->denyAccessUnlessGranted('ROLE_USER');
		$paginator  = $this->get('knp_paginator');

		/** @var User $user */
		$user = $this->getUser();

		$search = $request->get('search', '');
		$end = date('Y-m-d H:m:s');
		$start = date("Y-m-d H:i:s", strtotime("-300 days", strtotime($end)));

		if ($search) {
			$pawnOrders = $this->container->get('pawn.order')->getOrder($search);
			$pagination = $paginator->paginate(
				$pawnOrders, /* query NOT result */
				$request->query->getInt('page', 1)/*page number*/,
				10/*limit per page*/
			);
			return $this->render('order/orders.html.twig', [
				'pagination' => $pagination,
				'start' => $start,
				'end' => $end,
				'search' => $search,
			]);
		}

		if ($user->getStore()) {
			$store = $user->getStore();
		} else {
			$store = (empty($request->get('store', '')) ? '' : $request->get('store'));
		}

		if ($store) {
			$pawnOrders = $this->container->get('pawn.order')->getAllOrdersByStore($store);
		} else {

			$pawnOrders = $this->container->get('pawn.order')->getOrders($start, $end);
		}



		$pagination = $paginator->paginate(
			$pawnOrders, /* query NOT result */
			$request->query->getInt('page', 1)/*page number*/,
			10/*limit per page*/
		);

		return $this->render('order/orders.html.twig', [
			'orders' => $pawnOrders,
			'pagination' => $pagination,
			'start' => $start,
			'end' => $end,
			'search' => $search,
		]);
	}

	/**
	 * @Route("/send-sale", name="run_send_sale")
	 * @Security("is_granted('ROLE_SALES')")
	 */
	public function sendSaleAction()
	{

	}
}
