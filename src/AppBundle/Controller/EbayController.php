<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\SyncForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class EbayController extends Controller
{

	/**
	 * @Route("/ready-to-sync", name="ready-to-sync")
	 * @Security("is_granted('ROLE_EBAY')")
	 */
	public function indexAction(Request $request)
	{
		$status = $request->get('id');


		$this->denyAccessUnlessGranted('ROLE_EBAY');

		$form = $this->createForm(SyncForm::class, $status);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$ebayProducts = $this->container->get('pawn.products')->getReadyToSync($form->get('addToEbay')->getData());
		} else {

			$ebayProducts = $this->container->get('pawn.products')->getReadyToSync();
		}

		return $this->render('ebay/index.html.twig', [
			'products' => $ebayProducts,
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/products-info-needed", name="info_needed")
	 *
	 */
	public function getProductsNeedingInfoAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUser();
		$products = [];

		$allProducts = $this->container->get('pawn.products')->getReadyToSync(2);
		foreach ($allProducts as $product) {
			if ($product['id_supplier'] == $user->getStore()) {
				$products[] = $product;
			}
			if(is_null($user->getStore())) {
				$products[] = $product;
			}
		}


		return $this->render('ebay/info.html.twig', [
			'products' => $products,
		]);
	}
}
