<?php

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Form\UserForm;
use AppBundle\Form\UserRegistrationForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
	/**
	 * @Route("/register", name="user_register")
	 */
	public function registerAction(Request $request)
	{
		$suppliers = $this->container->get('pawn.supplier_service')->getSuppliers();

		$form = $this->createForm(UserRegistrationForm::class, null, [
			'suppliers' => $this->container->get('pawn.supplier_service')->transformData($suppliers),
		]);

		$form->handleRequest($request);


		if ($form->isValid()) {
			/** @var User $user */
			$user = $form->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($user);
			$em->flush();

			$this->addFlash('success', 'Welcome '.$user->getEmail());

			return $this->redirectToRoute('homepage');
		}

		return $this->render('user/register.html.twig', [
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/users", name="users")
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function usersAction()
	{
		$users = $this->getDoctrine()->getRepository(User::class)->findAll();

		return $this->render('user/users.html.twig', [
			'users' => $users,
		]);
	}


	/**
	 * @Route("/user/{id}", name="edit_user")
	 * @param Request $request
	 * @param User $user
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function editAction(Request $request, User $user)
	{

		$suppliers = $this->container->get('pawn.supplier_service')->getSuppliers();

		$form = $this->createForm(UserForm::class, $user, [
			'suppliers' => $this->container->get('pawn.supplier_service')->transformData($suppliers),
		]);

		$form->handleRequest($request);

		if ($form->isValid()) {
			/** @var User $user */
			$user = $form->getData();

			$em = $this->getDoctrine()->getManager();

			$em->persist($user);
			$em->flush();

			return $this->redirectToRoute('users');
		}


		return $this->render('user/user.html.twig', [
			'form' => $form->createView(),
			'user' => $user,
		]);
	}
}