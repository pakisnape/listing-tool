<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class MessagesController extends Controller
{
	/**
	 * @Route("/messages", name="messages")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function indexAction()
	{
		/** @var User $user */
		$user = $this->getUser();

		$messages = $this->getDoctrine()->getRepository(Message::class)->findBy([
			'user'=> $user,
			'seen' => 0]
		);

		return $this->render('messages/index.html.twig',[
			'messages' => $messages
		]);
	}

	/**
	 * @Route("/read-message/{id}", name="read-message")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function readAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUser();

		$id = $request->get('id');

		/** @var Message $messages */
		$messages = $this->getDoctrine()->getRepository(Message::class)->findOneBy([
				'user'=> $user,
				'id' => $id]
		);

		if($messages) {
			$messages->setSeen(1);
			$em = $this->getDoctrine()->getManager();
			$em->persist($messages);
			$em->flush();
			return $this->redirectToRoute('messages');
		}
	}
}
