<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Security("is_granted('ROLE_USER')")
 */
class ReportController extends Controller
{

	/**
	 * @Route("/reports", name="reports")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function indexAction(Request $request)
	{
		return $this->render('reports/reports.html.twig', array('name' => ''));
	}
}
