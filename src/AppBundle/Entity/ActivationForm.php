<?php


namespace AppBundle\Entity;


class ActivationForm
{
	private $store;

	private $start;

	private $end;

	/**
	 * @return mixed
	 */
	public function getStore()
	{
		return $this->store;
	}

	/**
	 * @param mixed $store
	 */
	public function setStore($store)
	{
		$this->store = $store;
	}

	/**
	 * @return mixed
	 */
	public function getStart()
	{
		return $this->start;
	}

	/**
	 * @param mixed $start
	 */
	public function setStart($start)
	{
		$this->start = $start;
	}

	/**
	 * @return mixed
	 */
	public function getEnd()
	{
		return $this->end;
	}

	/**
	 * @param mixed $end
	 */
	public function setEnd($end)
	{
		$this->end = $end;
	}
}