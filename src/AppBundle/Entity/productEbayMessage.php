<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;


/**
 * @ORM\Entity
 * @ORM\Table(name="ps_product_ebay_message")
 */
class productEbayMessage
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="opportunities")
	 * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
	 **/
	private $user;

	/**
	 * @ORM\Column(name="title", type="string", length=400)
	 * @var string
	 */
	private $message;

	/**
	 * Series page
	 *
	 * @ORM\Column(name="seriesPage", type="boolean", nullable=true)
	 * @var bool
	 */
	private $seen = false;

	/**
	 * @var \DateTime $created
	 *
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="id_product", type="integer", nullable=true)
	 */
	private $idProduct;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $store;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $test;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param mixed $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param string $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * @return bool
	 */
	public function isSeen()
	{
		return $this->seen;
	}

	/**
	 * @param bool $seen
	 */
	public function setSeen($seen)
	{
		$this->seen = $seen;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}

	/**
	 * @return string
	 */
	public function getIdProduct()
	{
		return $this->idProduct;
	}

	/**
	 * @param string $idProduct
	 */
	public function setIdProduct($idProduct)
	{
		$this->idProduct = $idProduct;
	}

	/**
	 * @return mixed
	 */
	public function getStore()
	{
		return $this->store;
	}

	/**
	 * @param mixed $store
	 */
	public function setStore($store)
	{
		$this->store = $store;
	}


}