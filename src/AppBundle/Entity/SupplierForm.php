<?php
/**
 * Created by PhpStorm.
 * User: nam797
 * Date: 11/11/17
 * Time: 8:30 AM
 */

namespace AppBundle\Entity;


class SupplierForm
{
	private $store;

	private $active;

	private $search;

	/**
	 * @return mixed
	 */
	public function getStore()
	{
		return $this->store;
	}

	/**
	 * @param mixed $store
	 */
	public function setStore($store)
	{
		$this->store = $store;
	}

	/**
	 * @return mixed
	 */
	public function getActive()
	{
		return $this->active;
	}

	/**
	 * @param mixed $active
	 */
	public function setActive($active)
	{
		$this->active = $active;
	}

	/**
	 * @return mixed
	 */
	public function getSearch()
	{
		return $this->search;
	}

	/**
	 * @param mixed $search
	 */
	public function setSearch($search)
	{
		$this->search = $search;
	}


}