<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;


/**
 * @ORM\Entity
 * @ORM\Table(name="product_activation")
 *
 */
class ProductActivation
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="opportunities")
	 * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
	 **/
	private $user;

	/**
	 * @var \DateTime $created
	 *
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="id_product", type="integer", nullable=true)
	 */
	private $idProduct;

	/**
	 * @ORM\Column(type="json_array")
	 */
	private $productData;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $store;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param mixed $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}

	/**
	 * @return string
	 */
	public function getIdProduct()
	{
		return $this->idProduct;
	}

	/**
	 * @param string $idProduct
	 */
	public function setIdProduct($idProduct)
	{
		$this->idProduct = $idProduct;
	}

	/**
	 * @return mixed
	 */
	public function getProductData()
	{
		return $this->productData;
	}

	/**
	 * @param mixed $productData
	 */
	public function setProductData($productData)
	{
		$this->productData = $productData;
	}

	/**
	 * @return mixed
	 */
	public function getStore()
	{
		return $this->store;
	}

	/**
	 * @param mixed $store
	 */
	public function setStore($store)
	{
		$this->store = $store;
	}


}