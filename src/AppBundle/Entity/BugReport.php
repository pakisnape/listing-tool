<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity
 * @ORM\Table(name="bug_report")
 *
 */
class BugReport
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="opportunities")
	 * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
	 **/
	private $user;

	/**
	 * @var \DateTime $created
	 *
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @ORM\Column(name="bug", type="text")
	 * @var string
	 */
	private $bug;

	/**
	 * @ORM\Column(type="boolean")
	 * @var int
	 */
	private $active = 1;

	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\BugReportMessage", mappedBy="bugReport")
	 */
	private $messages;

	public function __construct()
	{
		$this->messages = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param mixed $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}

	/**
	 * @return mixed
	 */
	public function getMessages()
	{
		return $this->messages;
	}

	/**
	 * @param mixed $messages
	 */
	public function setMessages($messages)
	{
		$this->messages = $messages;
	}

	/**
	 * @return int
	 */
	public function getActive()
	{
		return $this->active;
	}

	/**
	 * @param int $active
	 */
	public function setActive($active)
	{
		$this->active = $active;
	}

	/**
	 * @return string
	 */
	public function getBug()
	{
		return $this->bug;
	}

	/**
	 * @param string $bug
	 */
	public function setBug($bug)
	{
		$this->bug = $bug;
	}


}