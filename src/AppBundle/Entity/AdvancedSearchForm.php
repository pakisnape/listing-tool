<?php
/**
 * Created by PhpStorm.
 * User: nam797
 * Date: 11/11/17
 * Time: 8:30 AM
 */

namespace AppBundle\Entity;


class AdvancedSearchForm
{
	private $store;

	private $active;

	/**
	 * @return mixed
	 */
	public function getStore()
	{
		return $this->store;
	}

	/**
	 * @param mixed $store
	 */
	public function setStore($store)
	{
		$this->store = $store;
	}

	/**
	 * @return mixed
	 */
	public function getActive()
	{
		return $this->active;
	}

	/**
	 * @param mixed $active
	 */
	public function setActive($active)
	{
		$this->active = $active;
	}


}