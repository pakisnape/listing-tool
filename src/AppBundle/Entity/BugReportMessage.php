<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity
 * @ORM\Table(name="bug_report_message")
 *
 */
class BugReportMessage
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="opportunities")
	 * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
	 **/
	private $user;

	/**
	 * @var \DateTime $created
	 *
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @ORM\Column(name="message", type="text")
	 * @var string
	 */
	private $message;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BugReport", inversedBy="messages")
	 * @ORM\JoinColumn(name="bugReport", referencedColumnName="id")
	 */
	private $bugReport;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param mixed $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param string $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * @return mixed
	 */
	public function getBugReport()
	{
		return $this->bugReport;
	}

	/**
	 * @param mixed $bugReport
	 */
	public function setBugReport($bugReport)
	{
		$this->bugReport = $bugReport;
	}


}