<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;


class Product
{
	/**
	 * @Assert\Length(
	 *      min = 12,
	 *      max = 128,
	 *      minMessage = "title must be greater the  {{ limit }} characters long",
	 *      maxMessage = "title can not be longer than {{ limit }} characters"
	 * )
	 *
	 */
	protected $title;

	protected $shortDescription;

	protected $longDescription;

	protected $width;

	protected $height;

	protected $depth;

	protected $weight;

	protected $idProduct;

	private $files;

	private $brand;

	protected $status;

	private $fastBayTitle;

	private $fastBayDescription;

	private $fastBayPrice;

	private $fastBaySync;

	private $fastBayDateUpd;

	private $fastBayEbaycarrier1;

	private $fastBayCostospedizione1;

	private $fastBayadditionalcost1;

	private $epid;

	private $addToEbay;

	private $catDefault;

	private $carrier = false;

	private $carrier2 = false;

	private $taxSettings;

	private $mpn;

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return mixed
	 */
	public function getShortDescription()
	{
		return $this->shortDescription;
	}

	/**
	 * @param mixed $shortDescription
	 */
	public function setShortDescription($shortDescription)
	{
		$this->shortDescription = $shortDescription;
	}

	/**
	 * @return mixed
	 */
	public function getLongDescription()
	{
		return $this->longDescription;
	}

	/**
	 * @param mixed $longDescription
	 */
	public function setLongDescription($longDescription)
	{
		$this->longDescription = $longDescription;
	}

	/**
	 * @return mixed
	 */
	public function getWidth()
	{
		return $this->width;
	}

	/**
	 * @param mixed $width
	 */
	public function setWidth($width)
	{
		$this->width = $width;
	}

	/**
	 * @return mixed
	 */
	public function getHeight()
	{
		return $this->height;
	}

	/**
	 * @param mixed $height
	 */
	public function setHeight($height)
	{
		$this->height = $height;
	}

	/**
	 * @return mixed
	 */
	public function getDepth()
	{
		return $this->depth;
	}

	/**
	 * @param mixed $depth
	 */
	public function setDepth($depth)
	{
		$this->depth = $depth;
	}

	/**
	 * @return mixed
	 */
	public function getWeight()
	{
		return $this->weight;
	}

	/**
	 * @param mixed $weight
	 */
	public function setWeight($weight)
	{
		$this->weight = $weight;
	}

	/**
	 * @return mixed
	 */
	public function getIdProduct()
	{
		return $this->idProduct;
	}

	/**
	 * @param mixed $idProduct
	 */
	public function setIdProduct($idProduct)
	{
		$this->idProduct = $idProduct;
	}

	/**
	 * @return mixed
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * @param mixed $files
	 */
	public function setFiles($files)
	{
		$this->files = $files;
	}

	/**
	 * @return mixed
	 */
	public function getBrand()
	{
		return $this->brand;
	}

	/**
	 * @param mixed $brand
	 */
	public function setBrand($brand)
	{
		$this->brand = $brand;
	}

	/**
	 * @return mixed
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param mixed $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getFastBayTitle()
	{
		return $this->fastBayTitle;
	}

	/**
	 * @param mixed $fastBayTitle
	 */
	public function setFastBayTitle($fastBayTitle)
	{
		$this->fastBayTitle = $fastBayTitle;
	}

	/**
	 * @return mixed
	 */
	public function getFastBayDescription()
	{
		return $this->fastBayDescription;
	}

	/**
	 * @param mixed $fastBayDescription
	 */
	public function setFastBayDescription($fastBayDescription)
	{
		$this->fastBayDescription = $fastBayDescription;
	}

	/**
	 * @return mixed
	 */
	public function getFastBayPrice()
	{
		return $this->fastBayPrice;
	}

	/**
	 * @param mixed $fastBayPrice
	 */
	public function setFastBayPrice($fastBayPrice)
	{
		$this->fastBayPrice = $fastBayPrice;
	}

	/**
	 * @return mixed
	 */
	public function getFastBaySync()
	{
		return $this->fastBaySync;
	}

	/**
	 * @param mixed $fastBaySync
	 */
	public function setFastBaySync($fastBaySync)
	{
		$this->fastBaySync = $fastBaySync;
	}

	/**
	 * @return mixed
	 */
	public function getFastBayDateUpd()
	{
		return $this->fastBayDateUpd;
	}

	/**
	 * @param mixed $fastBayDateUpd
	 */
	public function setFastBayDateUpd($fastBayDateUpd)
	{
		$this->fastBayDateUpd = $fastBayDateUpd;
	}

	/**
	 * @return mixed
	 */
	public function getFastBayEbaycarrier1()
	{
		return $this->fastBayEbaycarrier1;
	}

	/**
	 * @param mixed $fastBayEbaycarrier1
	 */
	public function setFastBayEbaycarrier1($fastBayEbaycarrier1)
	{
		$this->fastBayEbaycarrier1 = $fastBayEbaycarrier1;
	}

	/**
	 * @return mixed
	 */
	public function getFastBayCostospedizione1()
	{
		return $this->fastBayCostospedizione1;
	}

	/**
	 * @param mixed $fastBayCostospedizione1
	 */
	public function setFastBayCostospedizione1($fastBayCostospedizione1)
	{
		$this->fastBayCostospedizione1 = $fastBayCostospedizione1;
	}

	/**
	 * @return mixed
	 */
	public function getAddToEbay()
	{
		return $this->addToEbay;
	}

	/**
	 * @param mixed $addToEbay
	 */
	public function setAddToEbay($addToEbay)
	{
		$this->addToEbay = $addToEbay;
	}

	/**
	 * @return mixed
	 */
	public function getCarrier()
	{
		return (boolean)$this->carrier;
	}

	/**
	 * @param mixed $carrier
	 */
	public function setCarrier($carrier)
	{
		$this->carrier = $carrier;
	}

	/**
	 * @return mixed
	 */
	public function getCarrier2()
	{
		return (boolean)$this->carrier2;
	}

	/**
	 * @param mixed $carrier2
	 */
	public function setCarrier2($carrier2)
	{
		$this->carrier2 = $carrier2;
	}

	/**
	 * @return integer
	 */
	public function getCatDefault()
	{
		return $this->catDefault;
	}

	/**
	 * @param integer $catDefault
	 */
	public function setCatDefault($catDefault)
	{
		$this->catDefault = $catDefault;
	}

	/**
	 * @return mixed
	 */
	public function getFastBayadditionalcost1()
	{
		return $this->fastBayadditionalcost1;
	}

	/**
	 * @param mixed $fastBayadditionalcost1
	 */
	public function setFastBayadditionalcost1($fastBayadditionalcost1)
	{
		$this->fastBayadditionalcost1 = $fastBayadditionalcost1;
	}

	/**
	 * @return mixed
	 */
	public function getTaxSettings()
	{
		return $this->taxSettings;
	}

	/**
	 * @param mixed $taxSettings
	 */
	public function setTaxSettings($taxSettings)
	{
		$this->taxSettings = $taxSettings;
	}

	/**
	 * @return mixed
	 */
	public function getEpid()
	{
		return $this->epid;
	}

	/**
	 * @param mixed $epid
	 */
	public function setEpid($epid)
	{
		$this->epid = $epid;
	}

	/**
	 * @return mixed
	 */
	public function getMpn()
	{
		return $this->mpn;
	}

	/**
	 * @param mixed $mpn
	 */
	public function setMpn($mpn)
	{
		$this->mpn = $mpn;
	}



}