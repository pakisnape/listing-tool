<?php


namespace AppBundle\Entity;


class BugForm
{
	private $search;

	/**
	 * @return mixed
	 */
	public function getSearch()
	{
		return $this->search;
	}

	/**
	 * @param mixed $search
	 */
	public function setSearch($search)
	{
		$this->search = $search;
	}


}