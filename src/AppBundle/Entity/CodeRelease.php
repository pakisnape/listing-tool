<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="code_release")
 *
 */
class CodeRelease
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(name="user_message", type="text")
	 * @Assert\NotBlank()
	 * @var string
	 */
	private $userMessage;

	/**
	 * @ORM\Column(name="dev_message", type="text")
	 * @Assert\NotBlank()
	 * @var string
	 */
	private $devMessage;

	/**
	 * @var \DateTime $created
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @var integer
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $bug;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getUserMessage()
	{
		return $this->userMessage;
	}

	/**
	 * @param string $userMessage
	 */
	public function setUserMessage($userMessage)
	{
		$this->userMessage = $userMessage;
	}

	/**
	 * @return string
	 */
	public function getDevMessage()
	{
		return $this->devMessage;
	}

	/**
	 * @param string $devMessage
	 */
	public function setDevMessage($devMessage)
	{
		$this->devMessage = $devMessage;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}

	/**
	 * @return int
	 */
	public function getBug()
	{
		return $this->bug;
	}

	/**
	 * @param int $bug
	 */
	public function setBug($bug)
	{
		$this->bug = $bug;
	}


}