<?php
/**
 * Created by PhpStorm.
 * User: nam797
 * Date: 12/2/17
 * Time: 9:08 AM
 */

namespace AppBundle\Services;


use Doctrine\DBAL\Connection;

class CategoryService
{

	/**
	 * All Categories
	 *
	 * @var array
	 */
	private $categories = [];

	/**
	 * @var Connection
	 */
	private $conn;

	/**
	 * @var array
	 */
	private $mainCategories = [];

	/**
	 * Categories constructor.
	 * @param Connection $pawnDatabaseConnection
	 */
	public function __construct(Connection $pawnDatabaseConnection)
	{
		$this->conn = $pawnDatabaseConnection;
	}

	/**
	 * @param $id_product
	 * @return array
	 */
	public function getProductCategories($id_product)
	{
		$sql = "SELECT id_parent,ps_category_product.id_category,`name` FROM ps_category_product 
				INNER JOIN ps_category ON ps_category.id_category = ps_category_product.id_category 
				INNER JOIN ps_category_lang ON ps_category_lang.id_category = ps_category_product.id_category  
				WHERE id_product = :product";

		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue("product", (int)trim($id_product));
		$stmt->execute();

		$this->categories = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$cats = [];
		foreach($this->categories as $cat) {
			$cats[] = $cat['id_category'];
		}

		return $cats;
	}

	public function buildTree(array &$elements, $parentId = 0) {

		$branch = array();
		foreach ($elements as $element) {
			if ($element['id_parent'] == $parentId) {
				$children = $this->buildTree($elements, $element['id_category']);
				if ($children) {
					$element['children'] = $children;
				}
				$branch[$element['id_category']] = $element;
			}
		}
		return $branch;
	}

	/**
	 * @return array
	 */
	public function getMainCategories()
	{
		$sql = "SELECT ps_category.id_category,ps_category.id_parent,name FROM  ps_category 
				INNER JOIN ps_category_lang ON ps_category_lang.id_category = ps_category.id_category
				 WHERE ps_category.active = :active";//WHERE ps_category.id_parent = :parent AND ps_category.active = :active

		$stmt = $this->conn->prepare($sql);
		//$stmt->bindValue("parent", (int)2);
		$stmt->bindValue("active", (int)1);
		$stmt->execute();

		$mainCats = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		$data = $this->buildTree($mainCats);
		//echo "<pre>";
		//print_r($data[1]['children'][2]['children']);die;
//		$tmp = $data[1]['children'][2]['children'];
//		foreach($tmp as $key => $value) {
//			foreach($value['children'] as $key1 => $value) {
//				foreach ($value as $k => $v) {
//					print_r($v);
//				}
//			}
//		}
//		die;
		return $data[1]['children'][2]['children'];

	}

	/**
	 * @return array
	 */
	public function getAllChildCategories()
	{
		foreach($this->mainCategories as $main) {
			 $this->mainCategories[$key]['children'] = $this->getChildCats($key);
		}

		return $this->mainCategories;
	}

	/**
	 * @param integer $product
	 */
	public function getUpdateProductCategories($product)
	{

		$sql = "SELECT * FROM ps_category_product
				WHERE ps_category_product.id_product = :product";

		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue("product", $product);

		$stmt->execute();
		$activeCategories =  $stmt->fetchAll(\PDO::FETCH_ASSOC);

		foreach($activeCategories as $catRow) {

			$catParent = $this->getCatParent($catRow['id_category']);
			if($catParent == 1) {
				continue;
			}

			if(!$this->hasCategory($product,$catParent))
			{
				$this->addCategoryToProduct($product,$catParent);
			}
		}
	}

	/**
	 * @param $id_category
	 * @return mixed
	 */
	protected function getCatParent($id_category)
	{

		$sql = "SELECT * FROM ps_category
				WHERE ps_category.id_category = :cat";

		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue("cat", $id_category);

		$stmt->execute();
		$cat =  $stmt->fetch(\PDO::FETCH_ASSOC);

		return $cat['id_parent'];
	}

	/**
	 * @param $product
	 * @param $id_category
	 * @return mixed
	 */
	protected function hasCategory($product, $id_category)
	{

		$sql = "SELECT * FROM ps_category_product
				WHERE ps_category_product.id_product = :product AND ps_category_product.id_category = :cat";

		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue("product", $product);
		$stmt->bindValue("cat", $id_category);

		$stmt->execute();
		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param $product
	 * @param $id_category
	 */
	protected function addCategoryToProduct($product, $id_category)
	{

		$sql = "INSERT INTO ps_category_product (id_category,id_product,position)
					values (:id_category,:id_product,:position)";

		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue("id_category", $id_category);
		$stmt->bindValue("id_product", $product);
		$stmt->bindValue("position", 0);

		$stmt->execute();
	}
}