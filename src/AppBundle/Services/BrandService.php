<?php


namespace AppBundle\Services;


use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BrandService
{
	public $pawnDatabaseConnection;

	/**
	 * Products constructor.
	 * @param Connection $pawnDatabaseConnection
	 */
	public function __construct(Connection $pawnDatabaseConnection)
	{
		$this->pawnDatabaseConnection = $pawnDatabaseConnection;
	}

	/**
	 * @return array
	 */
	public function getBrands()
	{
		$sql = "SELECT id_manufacturer, name FROM ps_manufacturer";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param $data
	 * @return array
	 */
	public function transformData($data)
	{
		$brands = [];
		foreach ($data as $brand) {
			$brands[$brand['name']] = $brand['id_manufacturer'];
		}

		return $brands;
	}
}