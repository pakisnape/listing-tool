<?php


namespace AppBundle\Services;


use AppBundle\Entity\User;
use AppBundle\Repository\LastEditedRepository;
use Doctrine\DBAL\Connection;

class LastEditedProductsService
{
	/**
	 * @var Connection
	 */
	private $connection;

	private $productsService;

	/**
	 * Categories constructor.
	 * @param Connection $pawnDatabaseConnection
	 */
	public function __construct(Connection $pawnDatabaseConnection,Products $products)
	{
		$this->connection = $pawnDatabaseConnection;
		$this->productsService = $products;
	}

	/**
	 * @param User $user
	 * @param int $limit
	 * @return array$limit
	 */
	public function getLastEditedByUser(User $user, $limit)
	{
		$qLimit = ($limit ? "limit $limit" : '');

		$sql = "SELECT id_product FROM last_edited 
					INNER JOIN user ON user.id = last_edited.user_id
					where last_edited.user_id = :user
					ORDER BY last_edited.created DESC $qLimit";


		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("user", $user->getId());

		$stmt->execute();

		$data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		return $this->getUniqueProducts($data);
	}

	/**
	 * @param $data
	 * @return array
	 */
	private function getUniqueProducts($data)
	{
		$products = [];
		$ids = [];
		foreach($data as $prod) {
			if(!in_array($prod['id_product'],$ids)){
				array_push($ids,$prod['id_product']);
				$products[] = $prod;
			}
		}

		return $this->getProductData($products);
	}

	private function getProductData($data)
	{
		$products = [];
		foreach($data as $prod) {
			$product = $this->productsService->getPawnProduct($prod['id_product']);
			$images = $this->productsService->getImages($prod['id_product']);
			$webPath = "";
			$cnt = 1;
			if (!empty($images)) {
				foreach ($images as $image) {
					if($cnt > 1) {continue;}
					$imageParts = str_split($image['id_image']);
					$path = implode("/", $imageParts);
					$webPath = "https://www.pawnamerica.com/img/p/".$path."/".$image['id_image']."-small_default.jpg";
					$cnt++;
				}
			}
			$products[] = [
				'id_product' => $prod['id_product'],
				'name' => $product['name'],
				'price' => number_format($product['price'],2),
				'image' => $webPath,
				'desc' => $product['description_short']
			];
		}
		return $products;
	}
}