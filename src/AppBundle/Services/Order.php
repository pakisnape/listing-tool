<?php
/**
 * Created by PhpStorm.
 * User: nam797
 * Date: 1/24/17
 * Time: 9:52 AM
 */

namespace AppBundle\Services;


use Doctrine\DBAL\Connection;
use PDO;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Order
{
	/**
	 * Connection $connection
	 */
	protected $connection;

	protected $container;

	protected $start;

	protected $end;

	protected $monthly;

	/**
	 * Order constructor.
	 * @param Connection $connection
	 * @param ContainerInterface $container
	 */
	public function __construct(Connection $connection,ContainerInterface $container)
	{
		$this->connection = $connection;
		$this->container = $container;
	}

	/**
	 * @param $id
	 * @return array
	 */
	public function getOrder($id)
	{
		$sql = "SELECT ps_orders.id_order,ps_orders.reference,ps_orders.date_add,ps_orders.total_shipping_tax_incl,ps_orders.total_shipping_tax_excl, 
                        ps_orders.id_order,payment,`module`,ps_orders.id_carrier,ps_orders.id_customer,ps_orders.id_cart,ps_orders.total_paid_tax_incl,
                        ps_orders.total_paid,ps_orders.total_paid_tax_excl,total_shipping_tax_incl,ps_orders.invoice_date,ps_orders.total_paid_tax_excl,
                        ps_orders.date_add,id_address_delivery,ps_carrier.name as carrierName,id_address_invoice, total_products,ps_orders.current_state,
                        ps_orders.reference,ps_customer.firstname,ps_customer.lastname,ps_customer.email, ps_order_state_lang.name,ps_order_history.date_add as orderDate,
                        ps_orders.current_state
                        FROM ps_orders
                         INNER JOIN ps_order_detail ON ps_order_detail.id_order = ps_orders.id_order
                         INNER JOIN ps_product ON ps_product.id_product = ps_order_detail.product_id
                         INNER JOIN ps_order_history ON ps_order_history.id_order = ps_orders.id_order
                         INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer 
                         INNER JOIN ps_order_state_lang ON ps_order_state_lang.id_order_state = ps_orders.current_state  
                         INNER JOIN ps_carrier ON ps_carrier.id_carrier = ps_orders.id_carrier
                         WHERE ps_order_history.id_order_state = :state";


		$sql .= " AND (ps_orders.reference LIKE :reference) OR (ps_orders.id_order LIKE :id_order) OR (ps_product.reference = :id_product)";

		$stmt = $this->connection->prepare($sql);

		$stmt->bindValue("reference",'%'.trim($id).'%',PDO::PARAM_STR);
		$stmt->bindValue("id_order",'%'.trim($id).'%',PDO::PARAM_STR);
		$stmt->bindValue("id_product",'%'.trim($id).'%',PDO::PARAM_INT);


		$stmt->bindValue('state', 2);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param $store
	 * @param $start
	 * @return array
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function getLatestOrdersByStore($store,$start)
	{
		$orders = $this->getLatestOrders($start);
		$storeOrders = [];
		foreach ($orders as $order) {
			$products = $this->getNotifierOrderedProducts($order);
			$isValid = false;
			foreach ($products as $key => $product) {
				if ($store == $key) {
					$isValid = true;
				}
			}
			if ($isValid) {
				$storeOrders[] = $order;
			}
		}

		return $storeOrders;
	}

	public function getAllOrdersByStore($store)
	{
		return $this->getAllOrders();
		$storeOrders = [];
		foreach ($orders as $order) {
			$products = $this->getNotifierOrderedProducts($order);
			$isValid = false;
			foreach ($products as $key => $product) {
				if ($store == $key) {
					$isValid = true;
				}
			}
			if ($isValid) {
				$storeOrders[] = $order;
			}
		}

		return $storeOrders;
	}

	/**
	 * @param int $limit
	 * @return array
	 */
	public function getLatestOrders($start,$limit = 200)
	{
		$sql = "SELECT ps_orders.id_order,ps_orders.reference,ps_orders.date_add,ps_orders.total_shipping_tax_incl,ps_orders.total_shipping_tax_excl, 
                        ps_orders.id_order,payment,`module`,ps_orders.id_carrier,ps_orders.id_customer,ps_orders.id_cart,ps_orders.total_paid_tax_incl,
                        ps_orders.total_paid,ps_orders.total_paid_tax_excl,total_shipping_tax_incl,ps_orders.invoice_date,ps_orders.total_paid_tax_excl,
                        ps_orders.date_add,id_address_delivery,ps_carrier.name as carrierName,id_address_invoice, total_products,ps_orders.current_state,
                        ps_orders.reference,ps_customer.firstname,ps_customer.lastname,ps_customer.email, ps_order_state_lang.name
                        FROM ps_orders
                         INNER JOIN ps_order_history ON ps_order_history.id_order = ps_orders.id_order
                         INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer 
                         INNER JOIN ps_order_state_lang ON ps_order_state_lang.id_order_state = ps_orders.current_state  
                         INNER JOIN ps_carrier ON ps_carrier.id_carrier = ps_orders.id_carrier
                         WHERE ps_order_history.id_order_state = :state 
                         			AND ps_order_history.date_add >= :now
                                    ORDER BY ps_order_history.date_add DESC LIMIT $limit ";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('now', $start);
		$stmt->bindValue('state', 2);
		$stmt->execute();

		return $stmt->fetchAll();
	}

	/**
	 * @return array
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function getAllOrders()
	{
		$sql = "SELECT ps_orders.id_order,ps_orders.reference,ps_orders.date_add,ps_orders.total_shipping_tax_incl,ps_orders.total_shipping_tax_excl, 
                        ps_orders.id_order,payment,`module`,ps_orders.id_carrier,ps_orders.id_customer,ps_orders.id_cart,ps_orders.total_paid_tax_incl,
                        ps_orders.total_paid,ps_orders.total_paid_tax_excl,total_shipping_tax_incl,ps_orders.invoice_date,ps_orders.total_paid_tax_excl,
                        ps_orders.date_add,id_address_delivery,ps_carrier.name as carrierName,id_address_invoice, total_products,ps_orders.current_state,
                        ps_orders.reference,ps_customer.firstname,ps_customer.lastname,ps_customer.email, ps_order_state_lang.name
                        FROM ps_orders
                         INNER JOIN ps_order_history ON ps_order_history.id_order = ps_orders.id_order
                         INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer 
                         INNER JOIN ps_order_state_lang ON ps_order_state_lang.id_order_state = ps_orders.current_state  
                         INNER JOIN ps_carrier ON ps_carrier.id_carrier = ps_orders.id_carrier
                         WHERE ps_order_history.id_order_state = :state 
                                    ORDER BY ps_order_history.date_add DESC ";
		$stmt = $this->connection->prepare($sql);

		$stmt->bindValue('state', 2);
		$stmt->execute();

		return $stmt->fetchAll();
	}

	public function getNewOrdersCount($start, $end, $store)
	{
		$orders = $this->getOrders($start, $end);
		if (empty($store)) {
			return count($orders);
		}
		$cnt = 0;
		foreach ($orders as $order) {
			$products = $this->getNotifierOrderedProducts($order);
			$isValid = false;

			foreach ($products as $key => $product) {
				if ($store == $key) {
					$isValid = true;
				}
			}
			if ($isValid) {
				$cnt++;
			}
		}

		return $cnt;
	}

	/**
	 * get new orders between to dates
	 *
	 * @param $start
	 * @param $end
	 * @return array
	 */
	public function getOrders($start, $end)
	{
		$this->setStart($start);
		$this->setEnd($end);
		$orders = [];

		$sql = "SELECT ps_orders.id_order,ps_orders.reference,ps_orders.date_add,ps_orders.total_shipping_tax_incl,ps_orders.total_shipping_tax_excl, 
                        ps_orders.id_order,payment,`module`,ps_orders.id_carrier,ps_orders.id_customer,ps_orders.id_cart,ps_orders.total_paid_tax_incl,
                        ps_orders.total_paid,ps_orders.total_paid_tax_excl,total_shipping_tax_incl,ps_orders.invoice_date,ps_orders.total_paid_tax_excl,
                        ps_orders.date_add,id_address_delivery,ps_carrier.name as carrierName,id_address_invoice, total_products,
                        ps_orders.reference,ps_customer.firstname,ps_customer.lastname,ps_customer.email, ps_order_state_lang.name,
                        ps_orders.current_state,ps_order_history.date_add as orderedDate
                        FROM ps_orders
                         INNER JOIN ps_order_history ON ps_order_history.id_order = ps_orders.id_order
                         INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer 
                         INNER JOIN ps_order_state_lang ON ps_order_state_lang.id_order_state = ps_orders.current_state  
                         INNER JOIN ps_carrier ON ps_carrier.id_carrier = ps_orders.id_carrier
                         WHERE ps_order_history.id_order_state = :state 
                         	AND ps_order_history.date_add >= :now 
                                AND ps_order_history.date_add < :end
                         ORDER BY ps_order_history.date_add DESC
                         		
                                    ";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('now', $this->getStart());
		$stmt->bindValue('end', $this->getEnd());
		$stmt->bindValue('state', 2);
		$stmt->execute();
		$data = $stmt->fetchAll();
		foreach ($data as $orderData) {
			$orders[$orderData['id_order']] = $orderData;
		}

		return $data;
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function getAddress($id)
	{
		$sql = "SELECT lastname, firstname, address1, address2, city, postcode,iso_code,phone,phone_mobile FROM ps_address
												INNER JOIN ps_state ON ps_address.id_state = ps_state.id_state WHERE id_address = :id_cart";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('id_cart', $id);
		$stmt->execute();

		return $stmt->fetch();
	}


	/**
	 * @return array
	 */
	public function getReturns()
	{
		/** @var Connection $conn */
		$conn = $this->container->get('database_connection');


		if ($this->monthly) {
			// soon to be implemented
		}
		$orders = [];

		$sql = "SELECT   ps_orders.date_add,ps_orders.total_shipping_tax_incl, ps_orders.id_order,payment,
                        ps_orders.reference,ps_customer.firstname,ps_customer.lastname, ps_order_state_lang.name
                         FROM ps_orders
                         INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer 
                         INNER JOIN ps_order_state_lang ON ps_order_state_lang.id_order_state = ps_orders.current_state
                         WHERE ps_orders.date_upd >= :now AND ps_orders.date_upd < :end AND ps_orders.current_state = :state";

		$stmt = $conn->prepare($sql);
		$stmt->bindValue('now', $this->start);
		$stmt->bindValue('end', $this->end);
		$stmt->bindValue('state', 7);
		$stmt->execute();
		$data = $stmt->fetchAll();

		foreach ($data as $orderData) {
			$orders[$orderData['id_order']] = $orderData;
		}

		return $orders;
	}

	/**
	 * gets specific order information for the SendSale xml file
	 *
	 * @param array $order
	 * @return array
	 */
	public function getNotifierOrderedProducts($order)
	{
		$data = $this->getProducts($order);

		$stores = $this->getProductSuppliers($data);

		$shippingInfo = $this->getShipping($order);

		$shping = $shippingInfo['shipping_cost_tax_excl'];
		$shipping = [$shping];

		$shippingTax = [$shippingInfo['shipping_cost_tax_incl'] - $shippingInfo['shipping_cost_tax_excl']];

		if (count($stores) > 1) {
			$shipping = $this->splitShipping(count($data), $order);
			$shippingTax = $this->split(count($data),
				($shippingInfo['shipping_cost_tax_incl'] - $shippingInfo['shipping_cost_tax_excl']));
		}
		$i = 0;
		$products = [];
		foreach ($data as $product) {
			$tax = $this->getItemTax($product);
			$products[$product['id_supplier']][] = [
				"product_name" => $product['product_name'],
				"product_reference" => $product['product_reference'],
				"total_price_tax_incl" => $product['total_price_tax_incl'],
				"total_price_tax_excl" => $product['total_price_tax_excl'],
				"id_supplier" => $product['id_supplier'],
				"supplierName" => $product['supplierName'],
				"product_price" => $product['product_price'],
				"unit_price_tax_excl" => $product['unit_price_tax_excl'],
				"itemTax" => ($tax ? round($tax['unit_amount'], 2) : 0),
				"shipping" => (count($shipping) > 1 ? $shipping[$i] : $shipping[0]),
				"shippingTax" => (count($shippingTax) > 1 ? $shippingTax[$i] : $shippingTax[0]),
			];
			$i++;
		}

		return $products;
	}

	/**
	 * splits a given number
	 *
	 * @param $count
	 * @param $number
	 * @return array
	 */
	protected function split($count, $number)
	{
		$split = $number / $count;
		$tmpTotal = $split * $count;
		$offset = $number - $tmpTotal;
		$returnArray = [];

		for ($i = 1; $i <= $count; $i++) {
			if ($i == $count) {
				array_push($returnArray, $split + $offset);
				continue;
			}
			array_push($returnArray, $split);
		}

		return $returnArray;
	}

	/**
	 * @param $currentOrders
	 * @return array
	 */
	public function getOrderedProducts($currentOrders)
	{
		$orders = [];

		foreach ($currentOrders as $order) {
			$data = $this->getProducts($order);
			//echo $order['id_order'] . "-". count($data) ."\n";
			$stores = $this->getProductSuppliers($data);

			$shipping = $this->splitShipping(count($stores), $order);

			$i = 0;
			//print_r($shipping);
			foreach ($data as $product) {
				$orders[$product['id_supplier']][] = [
					'sold' => $order['date_add'],
					'status' => $order['name'],
					'total' => $product['total_price_tax_incl'],
					'shippingCost' => (count($shipping) > 1 ? $shipping[$i] : $shipping[0]),
					'module' => $order['module'],
				];
				$i++;
			}
		}

		return $orders;
	}

	/**
	 * @param $product
	 * @return array
	 */
	protected function getItemTax($product)
	{
		$sql = "SELECT unit_amount FROM ps_order_detail_tax WHERE id_order_detail = :id_order_detail";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('id_order_detail', $product['id_order_detail']);
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * gets ordered products from an order
	 *
	 * @param $order
	 * @return array
	 */
	protected function getProducts($order)
	{
		$sql = "SELECT product_name, product_reference, total_price_tax_incl, total_price_tax_excl,
                        ps_product.id_supplier, ps_supplier.name as supplierName , product_price,id_order,
                        unit_price_tax_excl,id_order_detail
                    FROM ps_order_detail
                    INNER JOIN ps_product ON ps_product.id_product = ps_order_detail.product_id 
                    INNER JOIN ps_supplier ON ps_supplier.id_supplier = ps_product.id_supplier
                    WHERE id_order=:id_order";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('id_order', $order['id_order']);
		$stmt->execute();

		return $stmt->fetchAll();
	}

	/**
	 * @return array
	 */
	public function getSuppliers()
	{

		$sql = "SELECT * FROM ps_supplier where active = :active";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('active', 1);
		$stmt->execute();

		$reportData = [];
		$suppliers = $stmt->fetchAll();
		foreach ($suppliers as $supplier) {
			$reportData[$supplier['id_supplier']] = $supplier['name'];
		}

		return $reportData;
	}

	/**
	 *
	 *
	 *
	 * @param $ordersByStore
	 * @return array
	 */
	public function parseData($ordersByStore)
	{
		$suppliers = $this->getSuppliers();
		$reportData = [];
		$storesWithSales = [];
		foreach ($ordersByStore as $store => $data) {
			//echo $store. "\n";
			array_push($storesWithSales, $store);
			$websiteTotal = 0;
			$ebayTotal = 0;
			$active = $this->getActiveItemsBySupplier($store);
            $dailySoldTotal = 0;
			foreach ($data as $order) {
				if ($order['module'] == 'fastbay1') {
					$ebayTotal = $ebayTotal + $order['total'] + $order['shippingCost'];
                    $dailySoldTotal++;
					continue;
				}
				$websiteTotal = $websiteTotal + $order['total'] + $order['shippingCost'];
                $dailySoldTotal++;
			}

			$activationStart     = date("Y-m-d",strtotime("-1 days"));
			$activationEnd       = date("Y-m-d");
			$dailyActivatedProducts = $this->container->get('pawn.product_activation')->getActivatedProducts($activationStart,
				$activationEnd, $store);

			$dailyActivatedEbayItems = $this->container->get('pawn.ebay_activation')->getActivatedProducts($activationStart,$activationEnd,$store);

			$dailyActivatedCost = $this->container->get('pawn.product_activation')->getActivatedProductsCost($dailyActivatedProducts);

			$dailySentToEbay = $this->container->get('pawn.ebay_activation')->getStoreSentToEbay($activationStart,$activationEnd,$store);

			$reportData[$store] = [
			    'storeId' => $store,
				'name' => (array_key_exists($store, $suppliers) ? $suppliers[$store] : ''),
				'ebay' => $ebayTotal,
				'website' => $websiteTotal,
				'activeItems' => $active['website'],
				'activeEbayItems' => $active['ebay'],
				'updatedProducts' => '',
				'dailyActivated' => count($dailyActivatedProducts),
				'dailyActivatedCost' => $dailyActivatedCost,
				'dailyActivatedEbay' => count($dailyActivatedEbayItems),
				'newPulls' => $this->container->get('pawn.products')->getProductsByDateStore($activationStart,$store),
				'sentToEbay' => count($dailySentToEbay),
                'dailySoldTotal' => $dailySoldTotal
			];
		}


		$storesWithOutSales = $this->findMissingStores($reportData, $suppliers);
		if ($storesWithOutSales) {
			foreach ($storesWithOutSales as $store) {
				$websiteTotal = 0;
				$ebayTotal = 0;
				$active = $this->getActiveItemsBySupplier($store);
				$activationStart     = date("Y-m-d",strtotime("-1 days"));
				$activationEnd       = date("Y-m-d");
				$dailyActivatedProducts = $this->container->get('pawn.product_activation')->getActivatedProducts($activationStart,
					$activationEnd, $store);

				$dailyActivatedEbayItems = $this->container->get('pawn.ebay_activation')->getActivatedProducts($activationStart,$activationEnd,$store);

				$dailyActivatedCost = $this->container->get('pawn.product_activation')->getActivatedProductsCost($dailyActivatedProducts);

				$dailySentToEbay = $this->container->get('pawn.ebay_activation')->getStoreSentToEbay($activationStart,$activationEnd,$store);

				$reportData[$store] = [
                    'storeId' => $store,
					'name' => (array_key_exists($store, $suppliers) ? $suppliers[$store] : ''),
					'ebay' => $ebayTotal,
					'website' => $websiteTotal,
					'activeItems' => $active['website'],
					'activeEbayItems' => $active['ebay'],
					'updatedProducts' => '',
					'dailyActivated' => count($dailyActivatedProducts),
					'dailyActivatedCost' => $dailyActivatedCost,
					'dailyActivatedEbay' => count($dailyActivatedEbayItems),
					'newPulls' => $this->container->get('pawn.products')->getProductsByDateStore($activationStart,$store),
					'sentToEbay' => count($dailySentToEbay),
                    'dailySoldTotal' => 0
				];
			}
		}

		//print_r($reportData);die;
		return $reportData;
	}

	/**
	 * @param $storesTmp
	 * @param $suppliers
	 * @return array
	 */
	protected function findMissingStores($storesTmp, $suppliers)
	{
		$missingStores = [];
		foreach ($suppliers as $key => $supplier) {
			if (!array_key_exists($key, $storesTmp)) {
				array_push($missingStores, $key);
			}
		}

		return $missingStores;
	}

	/**
	 * @param $supplier
	 * @return array
	 */
	protected function getActiveItemsBySupplier($supplier)
	{
		$sql = "SELECT *  FROM ps_product 
                         INNER JOIN ps_product_shop ON ps_product_shop.id_product = ps_product.id_product
                         WHERE ps_product.id_supplier = :supplier AND ps_product_shop.active = :active";

		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('supplier', $supplier);
		$stmt->bindValue('active', 1);
		$stmt->execute();
		$results = $stmt->fetchAll();

		$ebay = 0;
		$site = 0;

		foreach ($results as $result) {
			if ($this->isEbay($result)) {
				$ebay = $ebay + 1;
				continue;
			}
			$site = $site + 1;
		}

		return ['website' => $site, 'ebay' => $ebay];
	}

	/**
	 * @param $product
	 * @return int
	 */
	protected function isEbay($product)
	{
		$sql = "SELECT * FROM ps_fastbay1_product where id_product = :id_product";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('id_product', $product['id_product']);
		//$stmt->bindValue('sync',1);
		$stmt->execute();

		return count($stmt->fetchAll());
	}

	/**
	 * @param $data
	 * @return array
	 */
	public function getProductSuppliers($data)
	{
		$suppliers = [];
		foreach ($data as $product) {
			$suppliers[] = $product['id_supplier'];
		}

		$data = array_unique($suppliers);

		return $suppliers;
	}

	public function getOrderDetails()
	{

	}

	/**
	 * @param $stores
	 * @param $order
	 * @return array
	 */
	protected function splitShipping($stores, $order)
	{
		$shippingCost = $order['total_shipping_tax_incl'];
		$shippingSplit = round($shippingCost / $stores, 2);
		if($stores == 1) {
			return [$shippingSplit];
		}
		$tmpShippingTotal = $shippingSplit * $stores;
		$shippingOffset = $shippingCost - $tmpShippingTotal;
		$shippingArray = [];
		echo $stores ."\n";
		for ($i = 1; $i <= $stores; $i++) {
		    echo "loop-".$i."\n";
			if ($i == $stores) {
				array_push($shippingArray, $shippingSplit + $shippingOffset);
				continue;
			}
			array_push($shippingArray, $shippingSplit);
		}

		return $shippingArray;
	}

	/**
	 * @return array
	 */
	protected function getStores()
	{

		$sql = "SELECT * FROM ps_supplier WHERE id_supplier != :supplier";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('supplier', 25);
		$stmt->execute();

		$reportData = [];
		$suppliers = $stmt->fetchAll();
		foreach ($suppliers as $supplier) {
			$reportData[$supplier['id_supplier']] = [
				'name' => $supplier['name'],
			];
		}

		return $reportData;
	}

	protected function getShipping($order)
	{

		$sql = "SELECT * from ps_order_carrier 
                            INNER JOIN ps_carrier on ps_carrier.id_carrier = ps_order_carrier.id_carrier 
                            WHERE id_order = :id_order";

		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('id_order', $order['id_order']);
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param $supplier
	 * @return mixed
	 */
	protected function getStoreEmail($supplier)
	{
		$storeEmails = [
			1 => 'pawnamerica-e242@pawnamerica.com',
			2 => 'pawnamerica-e251@pawnamerica.com',
			3 => 'pawnamerica-e259@pawnamerica.com',
			4 => 'pawnamerica-e241@pawnamerica.com',
			5 => 'pawnamerica-e232@pawnamerica.com',
			6 => 'pawnamerica-e243@pawnamerica.com',
			7 => 'pawnamerica-e237@pawnamerica.com',
			8 => 'pawnamerica-e253@pawnamerica.com',
			9 => 'pawnamerica-e248@pawnamerica.com',
			10 => 'pawnamerica-e235@pawnamerica.com',
			11 => 'pawnamerica-e257@pawnamerica.com',
			12 => 'pawnamerica-e254@pawnamerica.com',
			13 => 'pawnamerica-e252@pawnamerica.com',
			14 => 'pawnamerica-e245@pawnamerica.com',
			15 => 'pawnamerica-e240@pawnamerica.com',
			16 => 'pawnamerica-e247@pawnamerica.com',
			17 => 'pawnamerica-e231@pawnamerica.com',
			18 => 'pawnamerica-e238@pawnamerica.com',
			19 => 'pawnamerica-e234@pawnamerica.com',
			20 => 'pawnamerica-e246@pawnamerica.com',
			21 => 'pawnamerica-e236@pawnamerica.com',
			22 => 'pawnamerica-e258@pawnamerica.com',
			23 => 'pawnamerica-e250@pawnamerica.com',
			24 => 'pawnamerica-e255@pawnamerica.com',
			25 => 'marketplace@pawnamerica.com',
			26 => 'paul.hess@pawnamerica.com',
			28 => 'guest.service@everonesjewelryshop.com',
		];

		return $storeEmails[$supplier];
	}

	/**
	 * @param $supplier
	 * @return mixed
	 */
	protected function getMgrEmail($supplier)
	{
		$storeEmails = [
			1 => '4112@pawnamerica.com',
			2 => '4121@pawnamerica.com',
			3 => '4129@pawnamerica.com',
			4 => '4111@pawnamerica.com',
			5 => '4102@pawnamerica.com',
			6 => '4113@pawnamerica.com',
			7 => '4107@pawnamerica.com',
			8 => '4123@pawnamerica.com',
			9 => '4118@pawnamerica.com',
			10 => '4105@pawnamerica.com',
			11 => '4127@pawnamerica.com',
			12 => '4124@pawnamerica.com',
			13 => '4122@pawnamerica.com',
			14 => '4115@pawnamerica.com',
			15 => '4110@pawnamerica.com',
			16 => '4117@pawnamerica.com',
			17 => '4101@pawnamerica.com',
			18 => '4108@pawnamerica.com',
			19 => '4104@pawnamerica.com',
			20 => '4116@pawnamerica.com',
			21 => '4106@pawnamerica.com',
			22 => '4128@pawnamerica.com',
			23 => '4120@pawnamerica.com',
			24 => '4125@pawnamerica.com',
			25 => 'marketplace@pawnamerica.com',
			26 => 'paul.hess@pawnamerica.com',
			28 => 'guest.service@everonesjewelryshop.com',
		];

		return $storeEmails[$supplier];
	}

	/**
	 * @return false|string
	 */
	public function getStart()
	{
		return $this->start;
	}

	/**
	 * @param false|string $start
	 */
	public function setStart($start)
	{
		$this->start = $start;
	}

	/**
	 * @return false|string
	 */
	public function getEnd()
	{
		return $this->end;
	}

	/**
	 * @param false|string $end
	 */
	public function setEnd($end)
	{
		$this->end = $end;
	}

	/**
	 * @return mixed
	 */
	public function getMonthly()
	{
		return $this->monthly;
	}

	/**
	 * @param mixed $monthly
	 */
	public function setMonthly($monthly)
	{
		$this->monthly = $monthly;
	}


}