<?php
/**
 * Created by PhpStorm.
 * User: nam797
 * Date: 7/21/17
 * Time: 6:14 AM
 */

namespace AppBundle\Services;


define('PS_SHOP_PATH', 'http://localhost/prestashop');
define('PS_WS_AUTH_KEY', 'your-key-here');

// external image URL from where you will download the image to local folder
$remoteImageURL = "http://---yoururlforimage---.jpg";


// save the image to local folder
$dir_path_to_save = 'images/';


class ImageSevice {

		function LoadImageCURL($save_to)
		{
			$ch = curl_init($this->source);
			$fp = fopen($save_to, "wb");

	// set URL and other appropriate options
			$options = array(CURLOPT_FILE => $fp,
				CURLOPT_HEADER => 0,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_TIMEOUT => 60); // 1 minute timeout (should be enough)

			curl_setopt_array($ch, $options);

			$save = curl_exec($ch);
			curl_close($ch);
			fclose($fp);

			return $save;
		}
}






// initialize the class
$image = new GetImage;



$image->source = $remoteImageURL;

$image->save_to = $dir_path_to_save; // with trailing slash at the end

$get = $image->download('curl'); // using GD

if($get)
{
	echo "The image has been saved.";
}

$image_name = basename($remoteImageURL);


// change the local path where image has been downloaded "presta-api" is my local folder from where i run API script
$img_path = '\wamp\www\presta-api\images\\'. $image_name;


//image will be associated with product id 4
$url = PS_SHOP_PATH. '/api/images/products/4';

$ch = curl_init();


curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
//curl_setopt($ch, CURLOPT_PUT, true); To edit a picture

curl_setopt($ch, CURLOPT_USERPWD, PS_WS_AUTH_KEY.':');
curl_setopt($ch, CURLOPT_POSTFIELDS, array('image'=>"@".$img_path.";type=image/jpeg"));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


if(curl_exec($ch) === false)
{
	echo "<br><br>Error : ".curl_error($ch)."<br>"; }
else { echo '<br><br> Image added'; }
curl_close($ch);


?>