<?php


namespace AppBundle\Services;

use AppBundle\Entity\User;
use Doctrine\DBAL\Connection;
use PDO;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Products
{
	protected $count;

	/**
	 * @var ContainerInterface
	 */
	protected $container;

	/**
	 * @var PrestaShopApiService
	 */
	private $prestaShopApiService;

	private $numPerPage = 10;

	/**
	 * Connection $connection
	 */
	protected $connection;

	/**
	 * Products constructor.
	 * @param ContainerInterface $container
	 * @param PrestaShopApiService $prestaShopApiService
	 * @param Connection $connection
	 */
	public function __construct(
	  ContainerInterface $container,
	  PrestaShopApiService $prestaShopApiService,
	  Connection $connection
	) {
		$this->container = $container;
		$this->prestaShopApiService = $prestaShopApiService;
		$this->connection = $connection;

	}

	public function getReadyToSync($sync = 1)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');
		$sql = "SELECT ps_product.id_product, ps_product_lang.name,ps_supplier.name as store, store_date,ebay_date,id_product_ref,
                      ps_stock_available.quantity,ps_product_shop.active,description_short,description, ps_supplier.id_supplier
                  FROM ps_product_ebay
                  INNER JOIN ps_product 
                  ON ps_product.id_product = ps_product_ebay.id_product 
                  INNER JOIN ps_product_lang 
                  ON ps_product_lang.id_product = ps_product.id_product
                  INNER JOIN ps_supplier
                  ON ps_supplier.id_supplier = ps_product.id_supplier
                  INNER JOIN ps_stock_available
                  ON ps_stock_available.id_product = ps_product.id_product
                  INNER JOIN ps_product_shop
                  ON ps_product_shop.id_product = ps_product.id_product
                  LEFT JOIN ps_fastbay1_product on ps_fastbay1_product.id_product = ps_product.id_product
                  WHERE ps_product_ebay.sync = :sync AND ps_stock_available.quantity = 1
                  ORDER BY ps_product_ebay.update_date
                  ";

		$stmt = $conn->prepare($sql);
		$stmt->bindValue("sync", $sync);
		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * @param int $page
	 * @param null $supplier
	 * @param int $active
	 * @return array
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function getAllPawnProducts($page = 1, $supplier = null, $active = 1)
	{
		$startFrom = ($page - 1) * $this->getNumPerPage();

		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');

		$where = '';
		if($supplier) {
			$where = " AND ps_supplier.id_supplier =:supplier ";
		}

		$sql = "SELECT ps_product.id_product, ps_product_lang.name,ps_supplier.name as store, 
                      ps_product_shop.active,store_date,ebay_date
                  FROM ps_product
                  INNER JOIN ps_product_lang 
                  ON ps_product_lang.id_product = ps_product.id_product
                  INNER JOIN ps_supplier
                  ON ps_supplier.id_supplier = ps_product.id_supplier
                  INNER JOIN ps_product_shop
                  ON ps_product_shop.id_product = ps_product.id_product 
                  LEFT JOIN ps_product_ebay
                   ON ps_product_ebay.id_product = ps_product.id_product
                  WHERE ps_product_shop.active = :active
                  $where
                  ORDER BY ps_product.id_product DESC LIMIT :start, :perpage";

		$stmt = $conn->prepare($sql);
		$stmt->bindValue("start", (int)$startFrom, PDO::PARAM_INT);
		$stmt->bindValue("perpage", (int)$this->getNumPerPage(), PDO::PARAM_INT);
		if($supplier) {
			$stmt->bindValue("supplier", (int)$supplier, PDO::PARAM_INT);
		}
		$stmt->bindValue("active", (int)$active, PDO::PARAM_INT);
		$stmt->execute();


		$sqlTotal = "SELECT count(ps_product.id_product) as cnt
					FROM ps_product
					INNER JOIN ps_product_lang 
					ON ps_product_lang.id_product = ps_product.id_product
					INNER JOIN ps_supplier
					ON ps_supplier.id_supplier = ps_product.id_supplier
					INNER JOIN ps_product_shop
					ON ps_product_shop.id_product = ps_product.id_product
					LEFT JOIN ps_product_ebay
                    ON ps_product_ebay.id_product = ps_product.id_product
					WHERE ps_product_shop.active = :active
					 $where";
		$stmt1 = $conn->prepare($sqlTotal);
		if($supplier) {
			$stmt1->bindValue("supplier", (int)$supplier, PDO::PARAM_INT);
		}
		$stmt1->bindValue("active", (int)$active, PDO::PARAM_INT);
		$stmt1->execute();
		$cnt = $stmt1->fetch(PDO::FETCH_ASSOC);

		$totalPages = ceil($cnt['cnt'] / $this->getNumPerPage());

		return [$stmt->fetchAll(PDO::FETCH_ASSOC), $totalPages];

	}

	public function getAllProductsTest($supplier = null, $active = 1,$sort, $direction,$search)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');

		$where = '';
		if($supplier) {
			$where = " AND ps_supplier.id_supplier =:supplier ";
		}

		if($search) {
			$where .= " AND ps_product_lang.name LIKE :like";
		}

		$sql = "SELECT ps_product.id_product, ps_product_lang.name,ps_supplier.name as store, ps_product.date_add,
                      ps_product_shop.active,store_date,ebay_date,ps_product_shop.price,ps_product.reference
                  FROM ps_product
                  INNER JOIN ps_product_lang 
                  ON ps_product_lang.id_product = ps_product.id_product
                  INNER JOIN ps_supplier
                  ON ps_supplier.id_supplier = ps_product.id_supplier
                  INNER JOIN ps_product_shop
                  ON ps_product_shop.id_product = ps_product.id_product 
                  LEFT JOIN ps_product_ebay
                   ON ps_product_ebay.id_product = ps_product.id_product
                  WHERE ps_product_shop.active = :active
                  $where
                  ORDER BY $sort $direction";


		$stmt = $conn->prepare($sql);
		if($supplier) {
			$stmt->bindValue("supplier", (int)$supplier, PDO::PARAM_INT);
		}
		if($search) {
			$stmt->bindValue("like", '%'.trim($search).'%', PDO::PARAM_STR);
		}
		$stmt->bindValue("active", (int)$active, PDO::PARAM_INT);
		//$stmt->bindValue("sort", $sort, PDO::PARAM_STR);
		//$stmt->bindValue("direction", $direction, PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_ASSOC);

	}



	/**
	 * @param null $search
	 * @return mixed
	 */
	public function getPawnProducts($search = null)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');

		$sql = "SELECT ps_product.id_product, ps_product_lang.name,ps_supplier.name as store, 
                      ps_stock_available.quantity,ps_product_shop.active,description_short,description
                  FROM ps_product
                  INNER JOIN ps_product_lang 
                  ON ps_product_lang.id_product = ps_product.id_product
                  LEFT JOIN ps_supplier
                  ON ps_supplier.id_supplier = ps_product.id_supplier
                  INNER JOIN ps_stock_available
                  ON ps_stock_available.id_product = ps_product.id_product
                  INNER JOIN ps_product_shop
                  ON ps_product_shop.id_product = ps_product.id_product
                  ";


		$sql .= " WHERE ps_product.reference = :reference ";

		$stmt = $conn->prepare($sql);


		$stmt->bindValue("reference", trim($search), PDO::PARAM_INT);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

//        $result         = new stdClass();
//        $result->page   = $this->getPage();
//        $result->limit  = $this->getLimit();
//        $result->total  = count($data);
//        $result->data   = $data;

		return $data;
	}

	public function getPawnProduct($reference)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');

		$sql = "SELECT ps_product.id_product, ps_product_lang.name,ps_supplier.name as store, ps_tax_rules_group.name as taxName,
                      ps_stock_available.quantity,ps_product_shop.active,description_short,description,
                      height,width,depth,weight,reference,ps_stock_available.quantity,ps_product_shop.price,ps_product_shop.active,
                      ps_product_ebay.sync as ebaySync,ps_product.id_manufacturer,link_rewrite,ps_product.id_category_default,
                       model, class_id,ps_manufacturer.name as make, micro_dept
                  FROM ps_product
                  INNER JOIN ps_product_lang 
                  ON ps_product_lang.id_product = ps_product.id_product
                  INNER JOIN ps_supplier
                  ON ps_supplier.id_supplier = ps_product.id_supplier
                  INNER JOIN ps_stock_available
                  ON ps_stock_available.id_product = ps_product.id_product
                  INNER JOIN ps_product_shop
                  ON ps_product_shop.id_product = ps_product.id_product
                  LEFT JOIN ps_product_ebay 
                  ON ps_product_ebay.id_product = ps_product.id_product
                  LEFT JOIN ps_tax_rules_group
                  ON ps_tax_rules_group.id_tax_rules_group =  ps_product.id_tax_rules_group
                  LEFT JOIN ps_manufacturer
                  ON ps_manufacturer.id_manufacturer = ps_product.id_manufacturer
                  WHERE ps_product.id_product = :id_product LIMIT 1
                  ";

		$stmt = $conn->prepare($sql);
		$stmt->bindValue("id_product", trim($reference));

		$stmt->execute();

		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function getMpn($id_product)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');


		$sql = "SELECT * FROM ps_feature_product WHERE id_product = :id_product AND id_feature = :id_feature";

		$stmt = $conn->prepare($sql);
		$stmt->bindValue("id_product", trim($id_product));
		$stmt->bindValue("id_feature", 31);

		$stmt->execute();

		$data = $stmt->fetch(PDO::FETCH_ASSOC);
		if($data) {
			$sql = "SELECT * FROM ps_feature_value_lang WHERE id_feature_value = :id_feature_value ";
			$stmt = $conn->prepare($sql);
			$stmt->bindValue("id_product", $data['id_feature_value']);

			$stmt->execute();

			$mpn = $stmt->fetch(PDO::FETCH_ASSOC);

			return $mpn;
		}

		return false;
	}
	/**
	 * @param $productId
	 * @return int
	 */
	public function getAddToEbaySetting($productId)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');
		$sql = "SELECT * FROM ps_product_ebay WHERE id_product = :id_product";

		$stmt = $conn->prepare($sql);
		$stmt->bindValue("id_product", trim($productId));

		$stmt->execute();

		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		return (isset($data) ? $data['sync'] : 0);
	}

	/**
	 * @param $productId
	 */
	public function updateSyncSetting($productId)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');

		$sql = "UPDATE ps_product_ebay SET sync= :sync WHERE id_product = :id_product";
		$stmt = $conn->prepare($sql);
		$stmt->bindValue("sync", 2);
		$stmt->bindValue("id_product", $productId);

		$stmt->execute();
	}

	public function getProductsNeedingInfo($store = null)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');
		$sql = "SELECT * FROM ps_product_ebay WHERE sync = :sync";
		if ($store) {
			$sql .= " AND store = :store";
		}
		$stmt = $conn->prepare($sql);

		$stmt->bindValue("sync", 2);
		if ($store) {
			$stmt->bindValue("store", $store);
		}

		$stmt->execute();

		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		return (isset($data) ? $data['sync'] : 0);
	}

	/**
	 * @param $productId
	 * @return mixed
	 */
	public function getFastBaySettings($productId)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');
		$sql = "SELECT * FROM ps_fastbay1_product_settings
 				  LEFT JOIN ps_fastbay1_product ON ps_fastbay1_product.id_product = ps_fastbay1_product_settings.id_product
 				  WHERE ps_fastbay1_product_settings.id_product = :id_product";

		$stmt = $conn->prepare($sql);
		$stmt->bindValue("id_product", trim($productId));

		$stmt->execute();

		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		return $data;
	}

	/**
	 * @param $data
	 * @param $fastBaySettings
	 * @return \AppBundle\Entity\Product
	 *
	 */
	public function transformData($data, $fastBaySettings,$carriers)
	{
		$product = new \AppBundle\Entity\Product();
		$product->setTitle($data['name']);
		$product->setShortDescription($data['description_short']);
		$product->setLongDescription($data['description']);
		$product->setHeight($data['height']);
		$product->setWidth($data['width']);
		$product->setDepth($data['depth']);
		$product->setWeight($data['weight']);
		$product->setIdProduct($data['id_product']);
		$product->setStatus($data['active']);
		$product->setBrand($data['id_manufacturer']);
		$product->setCatDefault($data['id_category_default']);
		// fastbay settings below

		$product->setFastBayTitle($fastBaySettings['title']);
		$product->setFastBayDescription($fastBaySettings['description']);
		$product->setFastBayPrice($fastBaySettings['price']);
		$product->setFastBaySync($fastBaySettings['sync']);
		$product->setFastBayEbaycarrier1($fastBaySettings['ebaycarrier1']);
		$product->setFastBayCostospedizione1($fastBaySettings['costospedizione1']);
		$product->setAddToEbay((isset($data['ebaySync']) ? $data['ebaySync'] : 0));
		$mpn = $this->getMpn($data['id_product']);
		$product->setMpn($mpn['value']);
		$product->setEpid($fastBaySettings['epid']);
		//$product->setFastBayadditionalcost1($fastBaySettings['additionalcost1']);

		if(!empty($carriers))
		{
			foreach($carriers as $carrier) {
				if ($carrier == 1){
					$product->setCarrier($carrier);
				} else {
					$product->setCarrier2($carrier);
				}
			}
		}

		return $product;
	}

	/**
	 * @param $data
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function insertSoldItemIdData($data)
	{
		if ($data[0] == "item_id") {
			return;
		}
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.mssql_connection');

		$insertSql = "INSERT INTO micros_sold_products (item_id)
                                              VALUES (:item_id)";
		$stmt = $conn->prepare($insertSql);
		$stmt->bindValue("item_id", $data[0]);
		$stmt->execute();
	}

	/**
	 * @param $product
	 * @return array
	 */
	public function getImages($product)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');
		$imageSql = "SELECT * FROM ps_image_shop 
					 LEFT JOIN ps_image on ps_image.id_image = ps_image_shop.id_image
					 WHERE ps_image_shop.id_product = :id_product 
					 ORDER BY ps_image.position";
		$stmt = $conn->prepare($imageSql);
		$stmt->bindValue("id_product", $product);
		$stmt->execute();
		$result = $stmt->fetchAll();

		return $result;
	}




	/**
	 * @param $id_product
	 * @return array
	 */
	protected function getAllImageNames($id_product)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.pawn_connection');

		$sql = "SELECT *
												FROM ps_image_shop
												WHERE id_product=:id_product";
		$stmt = $conn->prepare($sql);
		$stmt->bindValue('id_product', $id_product);
		$stmt->execute();

		return $stmt->fetchAll();
	}

	/**
	 * @param $data
	 */
	public function insertActiveData($data)
	{
		if ($data[0] == "item_id") {
			return;
		}
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.mssql_connection');

		$insertSql = "INSERT INTO micros_active_products (item_id)
                                              VALUES (:item_id)";
		$stmt = $conn->prepare($insertSql);
		$stmt->bindValue("item_id", $data[0]);
		$stmt->execute();
	}

	public function UpdateCategories($cats,$product)
	{
		$this->prestaShopApiService->changeProductCategories($cats,$product);
	}

	/**
	 * loops through products sold in pawnamerica micros system
	 */
	public function verifyProductAreNotActive()
	{

		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.mssql_connection');

		$soldMicrosProductsSql = "SELECT * from micros_sold_products";
		$stmt = $conn->prepare($soldMicrosProductsSql);
		$stmt->execute();
		$products = $stmt->fetchAll();
		foreach ($products as $reference) {
			$this->makeProductInactive($reference);
		}
	}

	/**
	 * @param $reference
	 * @throws \Doctrine\DBAL\DBALException
	 */
	protected function makeProductInactive($reference)
	{
		$product = $this->getProductStatusQty($reference);
		if ($product['quantity'] != 0 || $product['active'] != 0) {
			$this->markDisableWithWebService($product['id_product']);
		}
	}

	/**
	 * @param $reference
	 * @return mixed
	 * @throws \Doctrine\DBAL\DBALException
	 */
	protected function getProductStatusQty($reference)
	{
		/** @var Connection $conn */
		$conn = $this->container->get('doctrine.dbal.mssql_connection');

		$activePrestaShopProductsSql = "SELECT id_product,ps_stock_available.quantity, ps_product_shop.active from ps_product 
                                        INNER JOIN ps_product_shop ON ps_product_shop.id_product = ps_product.id_product
                                        INNER JOIN ps_stock_available ON ps_stock_available.id_product = ps_product.id_product
                                        WHERE ps_product.reference = :reference";
		$stmt = $conn->prepare($activePrestaShopProductsSql);
		$stmt->bindValue("reference", $reference);
		$stmt->execute();

		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function verifyProductAreActive()
	{
		$activePrestaShopProductsSql = "SELECT * from ps_product 
                                        INNER JOIN ps_product_shop ON ps_product_shop.id_product = ps_product.id_product
                                        WHERE ps_product_shop.active = :active";
		$stmt = $this->connection->prepare($activePrestaShopProductsSql);
		$stmt->bindValue("active", 1);
		$stmt->execute();
		$products = $stmt->fetchAll();

		foreach ($products as $product) {
			if (!$this->statusInMicros($product['reference'])) {
				$this->markDisableWithWebService($product['id_product']);
			}
		}
	}

	/**
	 * @param $supplier
	 * @return array
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function getActiveItemsBySupplier($supplier)
	{


		$sql = "SELECT *  FROM ps_product 
					 INNER JOIN ps_product_shop ON ps_product_shop.id_product = ps_product.id_product
					 WHERE ps_product.id_supplier = :supplier AND ps_product_shop.active = :active";

		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('supplier', $supplier);
		$stmt->bindValue('active', 1);
		$stmt->execute();
		$results = $stmt->fetchAll();

		$ebay = 0;
		$site = 0;

		foreach ($results as $result) {
			if ($this->isEbay($result)) {
				$ebay = $ebay + 1;
				continue;
			}
			$site = $site + 1;
		}

		return ['website' => $site, 'ebay' => $ebay];
	}

	/**
	 * @param $product
	 * @return int
	 * @throws \Doctrine\DBAL\DBALException
	 */
	protected function isEbay($product)
	{


		$sql = "SELECT * FROM ps_fastbay1_product where id_product = :id_product";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue('id_product', $product['id_product']);
		//$stmt->bindValue('sync',1);
		$stmt->execute();

		return count($stmt->fetchAll());
	}

	/**
	 * @param Form $form
	 * @param User $user
	 */
	public function processForm($form,$user)
	{
		$this->prestaShopApiService->updateProduct($form,$user);
		$this->createOrUpdateMpn($form);
	}

	/**
	 * @param Form $form
	 */
	public function createOrUpdateMpn($form)
	{

		$currentMpn = $this->getMpn($form->get('idProduct')->getData());
		if($currentMpn['value'] == $form->get('mpn')->getData()) { return;}
		if($currentMpn) {
			$sql = "UPDATE ps_feature_value_lang SET value = :value WHERE id_feature_value = :id_feature_value";
			$stmt = $this->connection->prepare($sql);
			$stmt->bindValue("value", $form->get('mpn')->getData());
			$stmt->bindValue("id_feature_value", $currentMpn['id_feature_value']);
			$stmt->execute();
			return;
		}

		$this->addMpn($form);

	}

	/**
	 * @param Form $form
	 */
	protected function addMpn($form)
	{

		//add to ps_feature_value
		$sql = "INSERT INTO ps_feature_value ('id_feature','custom') VALUES 
				(:id_feature,:custom)";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("custom", 1);
		$stmt->bindValue("id_feature", 31);
		$stmt->execute();

		$id_feature_value = $this->connection->lastInsertId();


		$sql = "INSERT INTO ps_feature_value_lang ('id_feature_value','id_lang','value') VALUES 
				(:id_feature_value,:id_lang,:value)";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("value", $form->get('mpn')->getData());
		$stmt->bindValue("id_feature_value", $id_feature_value);
		$stmt->bindValue("id_lang", 1);
		$stmt->execute();

		$sql = "INSERT INTO ps_feature_product ('id_feature_value','id_feature','id_product') VALUES 
				(:id_feature_value,:id_lang,:value)";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("id_feature", 31);
		$stmt->bindValue("id_feature_value", $id_feature_value);
		$stmt->bindValue("id_product", $form->get('idProduct')->getData());
		$stmt->execute();
	}

	/**
	 * @param $item_id
	 * @return bool
	 * @throws \Doctrine\DBAL\DBALException
	 */
	protected function statusInMicros($item_id)
	{

		$activeSql = "SELECT count(*) as cnt FROM micros_active_products WHERE item_id = :item_id";
		$stmt = $this->connection->prepare($activeSql);
		$stmt->bindValue("item_id", $item_id);
		$stmt->execute();
		$data = $stmt->fetch(\PDO::FETCH_ASSOC);

		return ($data['cnt'] == 1 ? true : false);
	}

	protected function markDisableWithWebService($id_product)//
	{
		if (!$this->isActive($id_product)) {
			return;
		}
		$this->setCount($this->getCount() + 1);
		$this->prestaShopApiService->getIdStockAvailableAndSet($id_product, 0);
		$this->prestaShopApiService->setProductActive($id_product, false);
		$this->prestaShopApiService->setProductActive($id_product, true);
		$this->prestaShopApiService->setProductActive($id_product, false);

		return;
	}

	protected function isActive($id_product)
	{


		$productShopSql = "SELECT active FROM ps_product_shop WHERE id_product = :product ";
		$stmt = $this->connection->prepare($productShopSql);
		$stmt->bindValue("product", $id_product);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		if (!$result) {
			return 0;
		}
		//echo $id_product."=".$result['active']."\n";

		return $result['active'];
	}

	/**
	 * @param $date
	 * @param $store
	 * @return int
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function getProductsByDateStore($date,$store = null)
	{
		if($store) {
			$where = " AND id_supplier = :store";
		}

		$sql = "SELECT DISTINCT id_product FROM ps_product 
					where date_add BETWEEN :start AND  :end $where
					ORDER BY id_product";


		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("start", $date . " 00:00:00");
		$stmt->bindValue("end", $date. " 23:59:59");
		if($store) {
			$stmt->bindValue("store", $store);
		}
		$stmt->execute();

		return count($stmt->fetchAll(\PDO::FETCH_ASSOC));
	}


}