<?php

namespace AppBundle\Services;


use CURLFile;
use Exception;

class PrestaShop
{
	/**
	 * @var string Shop URL
	 */
	private $url;

	/**
	 * @var string Authentification key
	 */
	private $key;

	/**
	 * @var boolean is debug activated
	 * */
	private $debug;

	/**
	 * PrestaShop constructor.
	 * @param string $url
	 * @param string $key
	 * @param bool $debug
	 */
	public function __construct($url, $key, $debug)
	{
		$this->url = $url;
		$this->key = $key;
		$this->debug = $debug;
	}


	/**
	 * @param $status_code
	 * @throws \Exception
	 */
	protected function checkStatusCode($status_code)
	{
		$error_label = 'This call to PrestaShop Web Services failed and returned an HTTP status of %d. That means: %s.';
		switch ($status_code) {
			case 200:
			case 201:
				break;
			case 204:
				throw new Exception(sprintf($error_label, $status_code, 'No content'));
				break;
			case 400:
				throw new Exception(sprintf($error_label, $status_code, 'Bad Request'));
				break;
			case 401:
				throw new Exception(sprintf($error_label, $status_code, 'Unauthorized'));
				break;
			case 404:
				throw new Exception(sprintf($error_label, $status_code, 'Not Found'));
				break;
			case 405:
				throw new Exception(sprintf($error_label, $status_code, 'Method Not Allowed'));
				break;
			case 500:
				throw new Exception(sprintf($error_label, $status_code, 'Internal Server Error'));
				break;
			default:
				throw new Exception('This call to PrestaShop Web Services returned an unexpected HTTP status of:'.$status_code);
		}
	}

	/**
	 * @param $response
	 * @return \SimpleXMLElement|array
	 * @throws Exception
	 */
	protected function parseXML($response)
	{
		if ($response != '') {
			libxml_clear_errors();
			libxml_use_internal_errors(true);
			$xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
			if (libxml_get_errors()) {
				$msg = var_export(libxml_get_errors(), true);
				libxml_clear_errors();
				throw new Exception('HTTP XML response is not parsable: '.$msg);
			}

			return $xml;
		} else {
			return [];
		}//throw new Exception('HTTP response is empty');
	}

	/**
	 * @param $url
	 * @param array $curl_params
	 * @return array
	 * @throws Exception
	 */
	protected function executeRequest($url, $curl_params = array())
	{
		$defaultParams = array(
		  CURLOPT_HEADER => true,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLINFO_HEADER_OUT => true,
		  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		  CURLOPT_USERPWD => $this->key.':',
		  CURLOPT_HTTPHEADER => array('Expect:'),
		);
		$session = curl_init($url);
		$curl_options = array();
		foreach ($defaultParams as $defkey => $defval) {
			if (isset($curl_params[$defkey])) {
				$curl_options[$defkey] = $curl_params[$defkey];
			} else {
				$curl_options[$defkey] = $defaultParams[$defkey];
			}
		}
		foreach ($curl_params as $defkey => $defval) {
			if (!isset($curl_options[$defkey])) {
				$curl_options[$defkey] = $curl_params[$defkey];
			}
		}
		//$curl_options['CURLOPT_VERBOSE'] = 1;
		$verbose = fopen('php://temp', 'w+');
		$curl_options[CURLOPT_STDERR] = $verbose;

		curl_setopt_array($session, $curl_options);
		$response = curl_exec($session);
		$index = strpos($response, "\r\n\r\n");
		if ($index === false && $curl_params[CURLOPT_CUSTOMREQUEST] != 'HEAD') {
			throw new Exception('Bad HTTP response');
		}
		$header = substr($response, 0, $index);
		$body = substr($response, $index + 4);
		$headerArrayTmp = explode("\n", $header);
		$headerArray = array();
		foreach ($headerArrayTmp as &$headerItem) {
			$tmp = explode(':', $headerItem);
			$tmp = array_map('trim', $tmp);
			if (count($tmp) == 2) {
				$headerArray[$tmp[0]] = $tmp[1];
			}
		}

		if ($this->debug) {
			$this->printDebug('HTTP REQUEST HEADER', curl_getinfo($session, CURLINFO_HEADER_OUT));
			$this->printDebug('HTTP RESPONSE HEADER', $header);
		}
		$status_code = curl_getinfo($session, CURLINFO_HTTP_CODE);
		if ($status_code === 0) {
			throw new Exception('CURL Error: '.curl_error($session));
		}
		curl_close($session);
		if ($this->debug) {
			if ($curl_params[CURLOPT_CUSTOMREQUEST] == 'PUT' || $curl_params[CURLOPT_CUSTOMREQUEST] == 'POST') {
				$this->printDebug('XML SENT', urldecode($curl_params[CURLOPT_POSTFIELDS]));
			}
			if ($curl_params[CURLOPT_CUSTOMREQUEST] != 'DELETE' && $curl_params[CURLOPT_CUSTOMREQUEST] != 'HEAD') {
				$this->printDebug('RETURN HTTP BODY', $body);
			}
		}

		return array('status_code' => $status_code, 'response' => $body, 'header' => $header);
	}

	public function printDebug($title, $content)
	{
		echo '<div style="display:table;background:#CCC;font-size:8pt;padding:7px"><h6 style="font-size:9pt;margin:0">'.$title.'</h6><pre>'.htmlentities($content).'</pre></div>';
	}


	/**
	 * * Add (POST) a resource
	 * <p>Unique parameter must take : <br><br>
	 * 'resource' => Resource name<br>
	 * 'postXml' => Full XML string to add resource<br><br>
	 * Examples are given in the tutorial</p>
	 * @param $options
	 * @return \SimpleXMLElement
	 * @throws PrestaShopWebserviceException
	 */
	public function add($options)
	{
		$xml = '';
		if (isset($options['resource'], $options['postXml']) || isset($options['url'], $options['postXml'])) {
			$url = (isset($options['resource']) ? $this->getUrl().'/api/'.$options['resource'] : $options['url']);
			$xml = $options['postXml'];
			if (isset($options['id_shop'])) {
				$url .= '&id_shop='.$options['id_shop'];
			}

			if (isset($options['id_group_shop'])) {
				$url .= '&id_group_shop='.$options['id_group_shop'];
			}
		} else {
			throw new PrestaShopWebserviceException('Bad parameters given');
		}

		$request = self::executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'POST', CURLOPT_POSTFIELDS => $xml));
		self::checkStatusCode($request['status_code']);

		return self::parseXML($request['response']);
	}

	/**
	 * * Retrieve (GET) a resource
	 * <p>Unique parameter must take : <br><br>
	 * 'url' => Full URL for a GET request of Webservice (ex: http://mystore.com/api/customers/1/)<br>
	 * OR<br>
	 * 'resource' => Resource name,<br>
	 * 'id' => ID of a resource you want to get<br><br>
	 * </p>
	 * <code>
	 * <?php
	 * require_once('./PrestaShopWebservice.php');
	 * try
	 * {
	 * $ws = new PrestaShopWebservice('http://mystore.com/', 'ZQ88PRJX5VWQHCWE4EE7SQ7HPNX00RAJ', false);
	 * $xml = $ws->get(array('resource' => 'orders', 'id' => 1));
	 *    // Here in $xml, a SimpleXMLElement object you can parse
	 * foreach ($xml->children()->children() as $attName => $attValue)
	 *    echo $attName.' = '.$attValue.'<br />';
	 * }
	 * catch (PrestaShopWebserviceException $ex)
	 * {
	 *    echo 'Error : '.$ex->getMessage();
	 * }
	 * ?>
	 * </code>
	 * @param $options
	 * @return \SimpleXMLElement
	 * @throws Exception
	 */
	public function get($options)
	{
		if (isset($options['url'])) {
			$url = $options['url'];
		} elseif (isset($options['resource'])) {
			$url = $this->getUrl().'/api/'.$options['resource'];
			$url_params = array();
			if (isset($options['id'])) {
				$url .= '/'.$options['id'];
			}
			$params = array('filter', 'display', 'sort', 'limit', 'id_shop', 'id_group_shop');
			foreach ($params as $p) {
				foreach ($options as $k => $o) {
					if (strpos($k, $p) !== false) {
						$url_params[$k] = $options[$k];
					}
				}
			}
			if (count($url_params) > 0) {
				$url .= '?'.http_build_query($url_params);
			}
		} else {
			throw new Exception('Bad parameters given');
		}

		$request = self::executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'GET'));
		if ($this->debug) {
			self::checkStatusCode($request['status_code']);// check the response validity
		}

		return self::parseXML($request['response']);
	}

	/**
	 * @param $options
	 * @return mixed
	 * @throws PrestaShopWebserviceException
	 */
	public function head($options)
	{
		if (isset($options['url'])) {
			$url = $options['url'];
		} elseif (isset($options['resource'])) {
			$url = $this->getUrl().'/api/'.$options['resource'];
			$url_params = array();
			if (isset($options['id'])) {
				$url .= '/'.$options['id'];
			}
			$params = array('filter', 'display', 'sort', 'limit');
			foreach ($params as $p) {
				foreach ($options as $k => $o) {
					if (strpos($k, $p) !== false) {
						$url_params[$k] = $options[$k];
					}
				}
			}
			if (count($url_params) > 0) {
				$url .= '?'.http_build_query($url_params);
			}
		} else {
			throw new PrestaShopWebserviceException('Bad parameters given');
		}
		$request = self::executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'HEAD', CURLOPT_NOBODY => true));
		self::checkStatusCode($request['status_code']);// check the response validity

		return $request['header'];
	}

	/**
	 * Edit (PUT) a resource
	 * <p>Unique parameter must take : <br><br>
	 * 'resource' => Resource name ,<br>
	 * 'id' => ID of a resource you want to edit,<br>
	 * 'putXml' => Modified XML string of a resource<br><br>
	 * Examples are given in the tutorial</p>
	 * @param $options
	 * @return \SimpleXMLElement
	 * @throws PrestaShopWebserviceException
	 */
	public function edit($options)
	{
		$xml = '';
		if (isset($options['url'])) {
			$url = $options['url'];
		} elseif ((isset($options['resource'], $options['id']) || isset($options['url'])) && $options['putXml']) {
			$url = (isset($options['url']) ? $options['url'] : $this->getUrl().'/api/'.$options['resource'].'/'.$options['id']);
			$xml = $options['putXml'];
			if (isset($options['id_shop'])) {
				$url .= '&id_shop='.$options['id_shop'];
			}
			if (isset($options['id_group_shop'])) {
				$url .= '&id_group_shop='.$options['id_group_shop'];
			}
		} else {
			throw new PrestaShopWebserviceException('Bad parameters given');
		}
		$request = self::executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'PUT', CURLOPT_POSTFIELDS => $xml));
		self::checkStatusCode($request['status_code']);// check the response validity

		return self::parseXML($request['response']);
	}

	/**
	 * @param $id_product
	 * @param $file
	 */
	public function upload($id_product, $file)
	{
		//image will be associated with product id 4
		$url = $this->getUrl().'api/images/products/'.$id_product;

		//echo $url;die;
		$ch = curl_init();


		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		//curl_setopt($ch, CURLOPT_PUT, true); To edit a picture

		curl_setopt($ch, CURLOPT_USERPWD, $this->getKey().':');
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('image' => new CurlFile($file)));
		//curl_setopt($ch, CURLOPT_POSTFIELDS, array('image'=>"@"..";type=image/jpeg"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


		if (curl_exec($ch) === false) {
			echo "<br><br>Error : ".curl_error($ch)."<br>";
		} else {
			echo '<br><br> Image added';
		}
		curl_close($ch);
	}

	public function deleteIamges($images, $id_product)
	{
		//$url = "http://192.168.1.124/prestashop/api/images/products/" . $idProduct . "/".$idImage."?ps_method=DELETE";
		//image will be associated with product id 4

		foreach ($images as $image) {
			$url = $this->getUrl().'api/images/products/'.$id_product."/".$image."?ps_method=DELETE";;

			//echo $url;die;
			$ch = curl_init();


			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			//curl_setopt($ch, CURLOPT_PUT, true); To edit a picture

			curl_setopt($ch, CURLOPT_USERPWD, $this->getKey().':');
			//curl_setopt($ch, CURLOPT_POSTFIELDS, array('image'=>"@"..";type=image/jpeg"));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


			if (curl_exec($ch) === false) {
				$tmp =  "<br><br>Error : ".curl_error($ch)."<br>";
			} else {
				$tmp =  '<br><br> Image added';
			}
			curl_close($ch);
		}

	}


	/**
	 * Delete (DELETE) a resource.
	 * Unique parameter must take : <br><br>
	 * 'resource' => Resource name<br>
	 * 'id' => ID or array which contains IDs of a resource(s) you want to delete<br><br>
	 * <code>
	 * <?php
	 * require_once('./PrestaShopWebservice.php');
	 * try
	 * {
	 * $ws = new PrestaShopWebservice('http://mystore.com/', 'ZQ88PRJX5VWQHCWE4EE7SQ7HPNX00RAJ', false);
	 * $xml = $ws->delete(array('resource' => 'orders', 'id' => 1));
	 *    // Following code will not be executed if an exception is thrown.
	 *    echo 'Successfully deleted.';
	 * }
	 * catch (PrestaShopWebserviceException $ex)
	 * {
	 *    echo 'Error : '.$ex->getMessage();
	 * }
	 * ?>
	 * </code>
	 * @param $options
	 * @return bool
	 */
	public function delete($options)
	{
		$url = "";
		if (isset($options['url'])) {
			$url = $options['url'];
		} elseif (isset($options['resource']) && isset($options['id'])) {
			if (is_array($options['id'])) {
				$url = $this->getUrl().'/api/'.$options['resource'].'/?id=['.implode(',', $options['id']).']';
			} else {
				$url = $this->getUrl().'/api/'.$options['resource'].'/'.$options['id'];
			}
		}
		if (isset($options['id_shop'])) {
			$url .= '&id_shop='.$options['id_shop'];
		}
		if (isset($options['id_group_shop'])) {
			$url .= '&id_group_shop='.$options['id_group_shop'];
		}
		$request = self::executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'DELETE'));
		self::checkStatusCode($request['status_code']);// check the response validity

		return true;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * @param string $key
	 */
	public function setKey($key)
	{
		$this->key = $key;
	}

	/**
	 * @return boolean
	 */
	public function isDebug()
	{
		return $this->debug;
	}

	/**
	 * @param boolean $debug
	 */
	public function setDebug($debug)
	{
		$this->debug = $debug;
	}


}