<?php
/**
 * Created by PhpStorm.
 * User: nam797
 * Date: 8/23/17
 * Time: 9:10 AM
 */

namespace AppBundle\Services;


use Doctrine\DBAL\Connection;

class SupplierService
{
	public $pawnDatabaseConnection;

	/**
	 * Supplier constructor.
	 * @param Connection $pawnDatabaseConnection
	 */
	public function __construct(Connection $pawnDatabaseConnection)
	{
		$this->pawnDatabaseConnection = $pawnDatabaseConnection;
	}

	/**
	 * @return array
	 */
	public function getSuppliers()
	{
		$sql = "SELECT id_supplier, name FROM ps_supplier WHERE active = :active";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("active", 1);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param $data
	 * @return array
	 */
	public function transformData($data)
	{
		$brands = [];
		foreach ($data as $brand) {
			$brands[$brand['name']] = $brand['id_supplier'];
		}

		return $brands;
	}
}