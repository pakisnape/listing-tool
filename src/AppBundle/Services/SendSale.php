<?php

namespace AppBundle\Services;


use SimpleXMLElement;
use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class SendSale
{
    /**
     * @var bool
     */
    protected $isShipped = true;

    /**
     * @var float
     */
    protected $subTotal = 0.00;

    /**
     * @var float
     */
    protected $taxTotal = 0.00;

    /**
     * @var float
     */
    protected $total    = 0.00;

    /**
     * POS Transaction sequence number
     *
     * @var int
     */
    protected $seqNum;

    /**
     * store counter
     *
     * @var int
     */
    protected $storeCnt;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var SimpleXMLElement
     */
    public $xml;

    /**
     * order details, customer, shipping address, totals
     *
     * @var array
     */
    public $orderDetails;

    /**
     * products in the order
     *
     * @var array
     */
    public $products;

    /**
     * Shipment array
     *
     * @var array
     */
    protected $shipment;

    /**
     * tax group assigned to each product
     *
     * @var
     */
    protected $taxGroup;

    /**
     * header ID for send sale
     *
     * @var string
     */
    protected $headerId;

    /**
     * prestashop store id
     *
     * @var int
     */
    public $store;

    protected $storeCount;

    /**
     * Register Id from POS
     *
     */
    const REG_ID = 97;

    /**
     *
     * Code description for the type of transaction:
     *
     */
    const TY_TRN = "RETAIL_SALE";

    /**
     *
     * Code representing status of transaction. Valid values are:
     *
     */
    const TRN_STS = "COMPLETE";

    /**
     * Indicates if transaction was voided
     */
    const POST_VOID_FLAG = 0;

    /**
     * Cash drawer ID on which transaction was tendered
     */
    const CASH_DRAWER_ID = "";

    /**
     * A code denoting the type of RetailTransactionLineItem.
     */
    const LINE_ITEM_TYPE = "SALE";

    /**
     * Indicates if line item was voided
     */
    const FL_VD_LN_ITM = 0;

    /**
     * Currency
     */
    const CURRENCY_ID = "USD";

    /**
     * Tax authority charging tax.
     */
    const TAX_AUTH_ID = "TAX";

    /**
     *Tax authority name.
     */
    const TAX_AUTH_NAME = "Taxable";

    /**
     * Tax authority type.
     */
    const TAX_AUTH_TYPE_CODE = "TAX";

    /**
     *
     */
    const SHIPPING_ITEM_ID = 608;

    /**
     * Either "Tender", "Refund" or "Change" depending  on if the tender was received or issued.
     * Valid values : REFUND,TENDER,CHANGE
     */
    const TENDER_STATUS_CODE = "Tender";

    /**
     * Indicates the type of customer account
     * Valid values is - ACCOUNTS_RECEIVABLE, CREDIT_PAYMENT, CUSTOMER_BACKORDER, LAYAWAY, LOYALTY, REMOTE_SEND, SHIP_SALE, or SPECIAL_ORDER, WORK_ORDER
     */
    const CUST_ACCT_CODE = "SHIP_SALE";

    /**
     * Indicates the state code of transaction customer account
     * Valid values is - CLOSED,ABANDONED, CLOSED, DELINQUENT, IN_PROGRESS, INACTIVE, NEW, OPEN, OVERDUE, PENDING, READY_TO_PICKUP, or REFUNDABLE
     */
    const CUST_ACCT_STATECODE = "CLOSED";

    /**
     * indicate the Shipper Method used for shipping..
     * Valid values : DHL,FEDEX,PARCEL,STORE,UPS,USPS
     */
    const SHIPPING_METHOD = "UPS";



    /**
     * SendSale constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->xml = new SimpleXMLElement('<order/>');
        $this->container = $container;
        $this->getTrnSeqNum();
    }

    public function generateXml()
    {
        echo $this->orderDetails['id_order'] . "\n";
        $this->setHeaderId($this->orderDetails['reference']);
        $this->getStoreCount();
        $this->getShipment();
        $this->getTotals();
        $this->addHeader();
        $this->addLines();
        $this->saveXml();
        //echo $this->orderDetails['id_order']. "\n";
    }

    /**
     * @return int
     */
    protected function getPartyId()
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine.dbal.mssql_connection');
        $partyId = false;

        if($partyIdCheck = $this->isPartyIdEntered()) {
            return $partyIdCheck['party_id'];
        }

        $sql = "SELECT TOP 1 p.party_id, p.cust_id, p.first_name, pe.email_address
                  FROM crm_party p
                  INNER JOIN crm_party_email pe
                  ON p.party_id = pe.party_id
                  WHERE pe.email_address = :email";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('email',$this->orderDetails['email']);
        $stmt->execute();

        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        if($data) {
            $partyId = $data['party_id'];
        }

        if(!$partyId) {
            $partyId = $this->prestshopToMicros($this->store)."0".self::REG_ID.str_pad($this->getStoreCnt(), 6, '0',
                    STR_PAD_LEFT);
        }

        $this->savePartyId($partyId);

        return $partyId;
    }



    /**
     * @param $partyId
     */
    protected function savePartyId($partyId)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');
        $customerPartyInsertSql = "INSERT INTO customer_party
                        (id_customer,party_id)
                        VALUES (:id_customer, :party_id )";
        $stmt = $conn->prepare($customerPartyInsertSql);
        $stmt->bindValue("id_customer",$this->orderDetails['id_customer']);
        $stmt->bindValue("party_id",$partyId);
        $stmt->execute();
    }

    /**
     * @return mixed
     */
    protected function isPartyIdEntered()
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');
        $sql = "SELECT * from customer_party WHERE id_customer = :customer";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('customer',$this->orderDetails['id_customer']);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * gets the description from micros POS
     *
     * @param $product
     * @return mixed
     */
    protected function getMicrosDescription($product)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine.dbal.mssql_connection');

        $sql = "SELECT DESCRIPTION FROM itm_item
                   WHERE item_id = :reference";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('reference',$product['product_reference']);
        $stmt->execute();

        $group = $stmt->fetch(\PDO::FETCH_ASSOC);

        return preg_replace('/[^A-Za-z0-9\-]/', '', $group['DESCRIPTION']);
    }

    /**
     * gets the UNIT_COST from micros POS
     *
     * @param $product
     * @return mixed
     */
    protected function getMicrosUnitCost($product)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine.dbal.mssql_connection');

        $sql = "SELECT UNIT_COST FROM itm_item
                   WHERE item_id = :reference";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('reference',$product['product_reference']);
        $stmt->execute();

        $group = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $group['UNIT_COST'];
    }

    /**
     * @param $product
     */
    protected function getTaxGroupId($product)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine.dbal.mssql_connection');

        $sql = "SELECT TAX_GROUP_ID FROM itm_item
                   WHERE item_id = :reference";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('reference',$product['product_reference']);
        $stmt->execute();

        $group = $stmt->fetch(\PDO::FETCH_ASSOC);

        $this->setTaxGroup($group['TAX_GROUP_ID']);
    }

    protected function saveXml()
    {

        $fs = new Filesystem();
        $fileName = "ORDER_T".$this->prestshopToMicros($this->store).date("Ymdhis");
        //echo $fileName. "\n";
        $myfile = fopen("/home/admin/stores/orders/".$fileName.".xml", "w");
        fwrite($myfile, $this->xml->asXML());
        fclose($myfile);
        //mutiple
        //if ($fs->exists("/home/admin/stores/messages/".$fileName.".xml")) {
        $this->markSendSaleComplete($fileName);
        //}

    }

    protected function markSendSaleComplete($fileName)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');
        if(!$this->hasSendSale($this->orderDetails['id_order'])) {
            $sql = "INSERT INTO pa_order_notification (id_order,sendSale,file,headerId) VALUES (:id_order,:sendsale,:file,:headerId)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('id_order', $this->orderDetails['id_order']);
            $stmt->bindValue('sendsale', 1);
            $stmt->bindValue('file',$fileName);
            $stmt->bindValue('headerId',$this->getHeaderId());
            $stmt->execute();
        } else {
            $sendSale = $this->getSendSaleCount($this->orderDetails['id_order']);
            $sql = "UPDATE pa_order_notification set sendSale = :sendSale,file = :file WHERE id_order = :id_order";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('sendSale',$sendSale +1 );
            $stmt->bindValue('id_order',$this->orderDetails['id_order']);
            $stmt->bindValue('file',$fileName);
            $stmt->execute();
        }

    }

    protected function getSendSaleCount($id)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');

        $sql = "SELECT sendSale FROM pa_order_notification WHERE id_order = :id_order";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$id);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return ( count($data) > 0 ? $data['sendSale'] : 0 );
    }

    /**
     * @param $id
     * @return array
     */
    protected function hasSendSale($id)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');

        $sql = "SELECT sendSale FROM pa_order_notification WHERE id_order = :id_order";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$id);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return ( count($data) > 0 ? $data['sendSale'] : 0 );
    }

    /**
     *  gets the shipping cost tax included
     */
    protected function getShipment()
    {
        $shipping = $this->getShipping();
        if($shipping['shipping_cost_tax_incl'] > 0) {
            $this->shipment = $shipping;
        }

    }

    /**
     *  creates the Header node
     */
    protected function addHeader()
    {
        $tilSessionId = $this->prestshopToMicros($this->store)."0".self::REG_ID.str_pad($this->getSeqNum(), 12, '0',
                STR_PAD_LEFT);
        $header = $this->xml->addChild('header');
        $header->addChild('STG_HEADER_ID',$this->getHeaderId());
        $header->addChild('TRANSACTION_TYPE',0);
        $header->addChild('MESSAGE_TYPE');
        $header->addChild('STORE_ID',$this->prestshopToMicros($this->store));
        $header->addChild('DY_BSN',$this->convertDate());
        $header->addChild('REG_ID', self::REG_ID);
        $header->addChild('TRN_SEQ_NUM',$this->getSeqNum());
        $header->addChild('TR_BEGIN_TIME',$this->convertDate());
        $header->addChild('TR_END_TIME',$this->convertDate());
        $header->addChild('TY_TRN', self::TY_TRN);
        $header->addChild('TRN_STS', self::TRN_STS);
        $header->addChild('POST_VOID_FLAG',self::POST_VOID_FLAG);
        $header->addChild('TILL_SESSION_ID',$tilSessionId);// handled by Micros
        $header->addChild('CASH_DRAWER_ID', self::CASH_DRAWER_ID);
        $header->addChild('OPERATOR_ID');
        $header->addChild('SUB_TOTAL',round($this->getSubTotal(),2));
        $header->addChild('TAX_AMT',$this->getTaxTotal());
        $header->addChild('TOTAL',$this->getTotal());
        $header->addChild('CUSTOMER_PARTY_ID',$this->getPartyId());
        $header->addChild('TAX_EXEMPT_ID');
        $header->addChild('CANCEL_REASON_CODE');
    }


    protected function addLines()
    {
        $lines = $this->xml->addChild('LINES');
        $lineSeqNum = 1;

        foreach($this->products as $product) {
            $this->getTaxGroupId($product);
            $line = $lines->addChild('LINE');
            $this->addTransLineItem($line,$product,$lineSeqNum);
            if($lineSeqNum == 1) {
                if($this->shipment['shipping_cost_tax_incl'] > 0) {
                    $lineSeqNum++;
                    $shippingLine = $lines->addChild('LINE');
                    $this->addShippingLine($shippingLine,$lineSeqNum);
                }
            }

            $lineSeqNum++;
        }
        $line = $lines->addChild('LINE');
        $this->addTransTax($line,$lineSeqNum);
        $line = $lines->addChild('LINE');
        $this->addTender($line,$lineSeqNum + 1);
        $this->addCustomer();
        $this->addShipping();
    }

    /**
     * @param SimpleXMLElement $lines
     * @param $lineSeqNum
     */
    protected function addTender($lines,$lineSeqNum)
    {
        $tenderId = "PAYPAL";
        if($this->orderDetails['module'] == 'authorizeaim') {
            $tenderId = "AMAZON";
        }


        $tender = $lines->addChild('TENDER_LINEITEM');
        $tender->addChild('STG_HEADER_ID',$this->getHeaderId());
        $tender->addChild('TRANSACTION_TYPE',0);
        $tender->addChild('MESSAGE_TYPE');
        $tender->addChild('STORE_ID',$this->prestshopToMicros($this->store));
        $tender->addChild('DY_BSN',$this->convertDate());
        $tender->addChild('REG_ID',self::REG_ID);
        $tender->addChild('TRN_SEQ_NUM',$this->getSeqNum());
        $tender->addChild('LINE_SEQ_NUM',$lineSeqNum);
        $tender->addChild('LINE_START_TIME',$this->convertDate());
        $tender->addChild('LINE_END_TIME',$this->convertDate());
        $tender->addChild('FL_VD_LN_ITM', self::FL_VD_LN_ITM);
        $tender->addChild('VD_REASON_CODE');
        $tender->addChild('CURRENCY_ID', self::CURRENCY_ID);
        $tender->addChild('TENDER_ID',$tenderId);//E-ECOMMERCE/PAYPAL
        $tender->addChild('TENDER_AMT', $this->getTotal());
        $tender->addChild('SERIAL_NUMBER');
        $tender->addChild('TENDER_STATUS_CODE',self::TENDER_STATUS_CODE);
        $tender->addChild('CHANGE_FLAG');
        $tender->addChild('HOST_VALIDATION_FLAG');
        $tender->addChild('APPROVAL_CODE');
        $tender->addChild('ADJUDICATION_CODE');
        $tender->addChild('FOREIGN_AMOUNT');
        $tender->addChild('ACCT_USER_NAME');
    }

    /**
     * @param SimpleXMLElement $lines
     * @param $lineSeqNum
     */
    protected function addTransTax($lines,$lineSeqNum)
    {
        $transTax = $lines->addChild('TRANS_TAX');
        $transTax->addChild('STG_HEADER_ID',$this->getHeaderId());
        $transTax->addChild('TRANSACTION_TYPE',0);
        $transTax->addChild('MESSAGE_TYPE');
        $transTax->addChild('STORE_ID',$this->prestshopToMicros($this->store));
        $transTax->addChild('DY_BSN',$this->convertDate());
        $transTax->addChild('REG_ID',self::REG_ID);
        $transTax->addChild('TRN_SEQ_NUM',$this->getSeqNum());
        $transTax->addChild('LINE_SEQ_NUM',$lineSeqNum);// talk to tj
        $transTax->addChild('LINE_START_TIME',$this->convertDate());
        $transTax->addChild('LINE_END_TIME',$this->convertDate());
        $transTax->addChild('VOID_FLAG',0);
        $transTax->addChild('VD_REASON_CODE');
        $transTax->addChild('CURRENCY_ID',self::CURRENCY_ID);
        $transTax->addChild('TAXABLE_AMT',$this->getSubTotal());
        $transTax->addChild('TAX_PCT'); // need to get this
        $transTax->addChild('TAX_AMT',$this->getTaxTotal());
        $transTax->addChild('TAX_GROUP_ID', $this->getTaxGroup());
        $transTax->addChild('TAX_LOC_ID',$this->getTaxLocId());
        $transTax->addChild('TAX_RULE_SEQ',1);
        $transTax->addChild('TAX_AUTH_ID',self::TAX_AUTH_ID);
        $transTax->addChild('TAX_AUTH_NAME',self::TAX_AUTH_NAME);
        $transTax->addChild('TAX_AUTH_TYPE_CODE',self::TAX_AUTH_TYPE_CODE);
        $transTax->addChild('FL_TAX_OVERRIDE');
        $transTax->addChild('TAX_OVERRIDE_AMT');
        $transTax->addChild('TAX_OVERRIDE_PCT');
        $transTax->addChild('TAX_OVERRIDE_REASON_CODE');
        $transTax->addChild('POSTAL_CODE');
        $transTax->addChild('CITY');
    }

    /**
     * @param SimpleXMLElement $shippingLine
     * @param $lineSeqNum
     */
    protected function addShippingLine($shippingLine,$lineSeqNum)
    {
        $transLineItem = $shippingLine->addChild('TRANS_LINE_ITEM');
        $transLineItem->addChild('STG_HEADER_ID',$this->getHeaderId());
        $transLineItem->addChild('TRANSACTION_TYPE',0);
        $transLineItem->addChild('MESSAGE_TYPE');
        $transLineItem->addChild('STORE_ID',$this->prestshopToMicros($this->store));
        $transLineItem->addChild('DY_BSN',$this->convertDate());
        $transLineItem->addChild('REG_ID',self::REG_ID);
        $transLineItem->addChild('TRN_SEQ_NUM',$this->getSeqNum());
        $transLineItem->addChild('LINE_SEQ_NUM',$lineSeqNum);// talk to tj
        $transLineItem->addChild('LINE_START_TIME',$this->convertDate());
        $transLineItem->addChild('LINE_END_TIME',$this->convertDate());
        $transLineItem->addChild('LINE_ITEM_TYPE', self::LINE_ITEM_TYPE);
        $transLineItem->addChild('FL_VD_LN_ITM',self::FL_VD_LN_ITM);
        $transLineItem->addChild('VD_REASON_CODE');
        $transLineItem->addChild('CURRENCY_ID', self::CURRENCY_ID);
        $transLineItem->addChild('ITEM_ID',self::SHIPPING_ITEM_ID );
        $transLineItem->addChild('ITEM_SERIAL_NO');
        $transLineItem->addChild('ITEM_QTY',1);
        $transLineItem->addChild('UNIT_COST',0);
        $transLineItem->addChild('ITEM_DESCRIPTION','Shipping Fee $');
        $transLineItem->addChild('UNIT_RTL_PRC',$this->shipment['shipping_cost_tax_excl']);
        $transLineItem->addChild('EXTND_NET_AMT',$this->shipment['shipping_cost_tax_excl']);

        // line item total
        $total = 0;
        $shippingTax = 0;
        foreach($this->products as $product) {
            $total = $total + $product['shipping'];
            $shippingTax = $shippingTax + $product['shippingTax'];
        }

        $transLineItem->addChild('TTL_LN_ITM_AMT',$total);
        $transLineItem->addChild('RETURN_FLAG',0);
        $transLineItem->addChild('TAX_GROUP_ID',$this->getTaxGroup());// talk to tj

        //$shippingTax = $this->shipment['shipping_cost_tax_incl'] - $this->shipment['shipping_cost_tax_excl'];

        $transLineItemTax = $shippingLine->addChild('TRANS_LINE_ITEM_TAX');
        $transLineItemTax->addChild('STG_HEADER_ID',$this->getHeaderId());
        $transLineItemTax->addChild('TRANSACTION_TYPE',0);
        $transLineItemTax->addChild('MESSAGE_TYPE');
        $transLineItemTax->addChild('STORE_ID',$this->prestshopToMicros($this->store));
        $transLineItemTax->addChild('DY_BSN',$this->convertDate());
        $transLineItemTax->addChild('REG_ID',self::REG_ID);
        $transLineItemTax->addChild('TRN_SEQ_NUM',$this->getSeqNum());//. talk to tj
        $transLineItemTax->addChild('LINE_SEQ_NUM',$lineSeqNum);//. talk to tj
        $transLineItemTax->addChild('TAX_LINE_ITEM',1);
        $transLineItemTax->addChild('VOID_FLAG',0);
        $transLineItemTax->addChild('TAXABLE_AMT',$this->shipment['shipping_cost_tax_excl']);
        $transLineItemTax->addChild('TAX_PCT');// prestashop???
        $transLineItemTax->addChild('TAX_AMT', $shippingTax); // need to figure this out
        $transLineItemTax->addChild('TAX_EXEMPT_ID');
        $transLineItemTax->addChild('TAX_EXEMPT_AMT');
        $transLineItemTax->addChild('TAX_GROUP_ID',$this->getTaxGroup());// from micros
        $transLineItemTax->addChild('TAX_LOC_ID',$this->getTaxLocId());
        $transLineItemTax->addChild('TAX_RULE_SEQ',1);
        $transLineItemTax->addChild('TAX_AUTH_ID',self::TAX_AUTH_ID);
        $transLineItemTax->addChild('TAX_AUTH_NAME',self::TAX_AUTH_NAME);
        $transLineItemTax->addChild('TAX_AUTH_TYPE_CODE',self::TAX_AUTH_TYPE_CODE);
        $transLineItemTax->addChild('FL_TAX_OVERRIDE');
        $transLineItemTax->addChild('TAX_OVERRIDE_AMT');
        $transLineItemTax->addChild('TAX_OVERRIDE_PCT');
        $transLineItemTax->addChild('TAX_OVERRIDE_REASON_CODE');
        $transLineItemTax->addChild('POSTAL_CODE');
        $transLineItemTax->addChild('CITY');
    }

    /**
     * @param SimpleXMLElement $line
     * @param $product
     */
    protected function addTransLineItem($line,$product,$lineSeqNum)
    {
        $transLineItem = $line->addChild('TRANS_LINE_ITEM');
        $transLineItem->addChild('STG_HEADER_ID',$this->getHeaderId());
        $transLineItem->addChild('TRANSACTION_TYPE',0);
        $transLineItem->addChild('MESSAGE_TYPE');
        $transLineItem->addChild('STORE_ID',$this->prestshopToMicros($this->store));
        $transLineItem->addChild('DY_BSN',$this->convertDate());
        $transLineItem->addChild('REG_ID',self::REG_ID);
        $transLineItem->addChild('TRN_SEQ_NUM',$this->getSeqNum());
        $transLineItem->addChild('LINE_SEQ_NUM',$lineSeqNum);// talk to tj
        $transLineItem->addChild('LINE_START_TIME',$this->convertDate());
        $transLineItem->addChild('LINE_END_TIME',$this->convertDate());
        $transLineItem->addChild('LINE_ITEM_TYPE', self::LINE_ITEM_TYPE);
        $transLineItem->addChild('FL_VD_LN_ITM',self::FL_VD_LN_ITM);
        $transLineItem->addChild('VD_REASON_CODE');


        $transLineItem->addChild('CURRENCY_ID', self::CURRENCY_ID);
        $transLineItem->addChild('ITEM_ID',$product['product_reference']);
        $transLineItem->addChild('ITEM_SERIAL_NO');
        $transLineItem->addChild('ITEM_QTY',1);
        $transLineItem->addChild('UNIT_COST',$this->getMicrosUnitCost($product));
        $transLineItem->addChild('ITEM_DESCRIPTION',$this->getMicrosDescription($product));
        $transLineItem->addChild('UNIT_RTL_PRC',$product['total_price_tax_excl']);
        $transLineItem->addChild('EXTND_NET_AMT',$product['total_price_tax_excl']);

        // line item total
        $total = $product['total_price_tax_excl'] + $product['itemTax'];
        $transLineItem->addChild('TTL_LN_ITM_AMT',$total);
        $transLineItem->addChild('RETURN_FLAG',0);
        $transLineItem->addChild('TAX_GROUP_ID',$this->getTaxGroup());// talk to tj

        $transLineItemTax = $line->addChild('TRANS_LINE_ITEM_TAX');
        $transLineItemTax->addChild('STG_HEADER_ID',$this->getHeaderId());
        $transLineItemTax->addChild('TRANSACTION_TYPE',0);
        $transLineItemTax->addChild('MESSAGE_TYPE');
        $transLineItemTax->addChild('STORE_ID',$this->prestshopToMicros($this->store));
        $transLineItemTax->addChild('DY_BSN',$this->convertDate());
        $transLineItemTax->addChild('REG_ID',self::REG_ID);
        $transLineItemTax->addChild('TRN_SEQ_NUM',$this->getSeqNum());
        $transLineItemTax->addChild('LINE_SEQ_NUM', $lineSeqNum);
        $transLineItemTax->addChild('TAX_LINE_ITEM',1);
        $transLineItemTax->addChild('VOID_FLAG',0);
        $transLineItemTax->addChild('TAXABLE_AMT',$product['total_price_tax_excl']);
        $transLineItemTax->addChild('TAX_PCT');// prestashop???-----------------------------
        $transLineItemTax->addChild('TAX_AMT', $product['itemTax']);
        $transLineItemTax->addChild('TAX_EXEMPT_ID');
        $transLineItemTax->addChild('TAX_EXEMPT_AMT');
        $transLineItemTax->addChild('TAX_GROUP_ID',$this->getTaxGroup());// from micros
        $transLineItemTax->addChild('TAX_LOC_ID',$this->getTaxLocId());
        $transLineItemTax->addChild('TAX_RULE_SEQ',1);
        $transLineItemTax->addChild('TAX_AUTH_ID',self::TAX_AUTH_ID);
        $transLineItemTax->addChild('TAX_AUTH_NAME',self::TAX_AUTH_NAME);
        $transLineItemTax->addChild('TAX_AUTH_TYPE_CODE',self::TAX_AUTH_TYPE_CODE);
        $transLineItemTax->addChild('FL_TAX_OVERRIDE');
        $transLineItemTax->addChild('TAX_OVERRIDE_AMT');
        $transLineItemTax->addChild('TAX_OVERRIDE_PCT');
        $transLineItemTax->addChild('TAX_OVERRIDE_REASON_CODE');
        $transLineItemTax->addChild('POSTAL_CODE');
        $transLineItemTax->addChild('CITY');


    }


    protected function addCustomer()
    {
        $invoiceAddress  = $this->getAddress($this->orderDetails['id_address_invoice']);

        $customer = $this->xml->addChild('CUSTOMER');
        $customer->addChild('STG_HEADER_ID',$this->getHeaderId());
        $customer->addChild('TRANSACTION_TYPE',0);
        $customer->addChild('MESSAGE_TYPE');
        $customer->addChild('STORE_ID',$this->prestshopToMicros($this->store));
        $customer->addChild('CUST_ID',$this->getPartyId()); // TODO: need to figure this out
        $first = str_replace('&','',$invoiceAddress['firstname']);
        $customer->addChild('FIRST_NAME',$first);
        $last = str_replace('&','',$invoiceAddress['lastname']);
        $customer->addChild('LAST_NAME',$last);

        $address1 = preg_replace('/[^A-Za-z0-9\-]/', '', $invoiceAddress['address1']);//str_replace(['&','.','/','(',')'],'',$invoiceAddress['address1']);
        //echo $address1. "\n";die;
        $customer->addChild('ADDRESS1',$address1);
        $address2 = preg_replace('/[^A-Za-z0-9\-]/', '', $invoiceAddress['address2']);
        $customer->addChild('ADDRESS2',$address2);
        $customer->addChild('ADDRESS3');
        $customer->addChild('ADDRESS4');
        $customer->addChild('CITY',$invoiceAddress['city']);
        $customer->addChild('STATE',$invoiceAddress['iso_code']);
        $customer->addChild('POSTAL_CODE',$invoiceAddress['postcode']);
        $customer->addChild('COUNTRY',"US");
        $customer->addChild('EMAIL',$this->orderDetails['email']);
        $phone = ($invoiceAddress['phone_mobile'] ? $invoiceAddress['phone_mobile'] : $invoiceAddress['phone']);
        $customer->addChild('TELEPHONE1',$phone);
        $customer->addChild('TELEPHONE2');
        $customer->addChild('TELEPHONE3');
        $customer->addChild('TELEPHONE4');
        $customer->addChild('CUST_ACCT_CODE', self::CUST_ACCT_CODE);
        $customer->addChild('CUST_ACCT_STATECODE', self::CUST_ACCT_STATECODE);
        $customer->addChild('CUST_ACCT_SETUP_DATE',$this->convertDate());
    }


    protected function addShipping()
    {
        $shippingAddress = $this->getAddress($this->orderDetails['id_address_delivery']);
        $invoiceAddress  = $this->getAddress($this->orderDetails['id_address_invoice']);

        $shipping = $this->xml->addChild('SHIPPING_ADDRESS');
        $shipping->addChild('STG_HEADER_ID',$this->getHeaderId());
        $shipping->addChild('TRANSACTION_TYPE',0);
        $shipping->addChild('MESSAGE_TYPE');
        $shipping->addChild('STORE_ID',$this->prestshopToMicros($this->store));
        $shipping->addChild('CUST_ACCT_ID'); // TODO: need to figure this out
        $first = str_replace('&','',$shippingAddress['firstname']);
        $shipping->addChild('FIRST_NAME',$first);
        $last = str_replace('&','',$shippingAddress['lastname']);
        $shipping->addChild('LAST_NAME',$last);

        $address1 = preg_replace('/[^A-Za-z0-9\-]/', '', $shippingAddress['address1']);// str_replace(['&','.','/','(',')'],'',$shippingAddress['address1']);
        $shipping->addChild('ADDRESS1',$address1);
        $address2 = preg_replace('/[^A-Za-z0-9\-]/', '', $shippingAddress['address2']);
        $shipping->addChild('ADDRESS2',$address2);
        $shipping->addChild('ADDRESS3');
        $shipping->addChild('ADDRESS4');
        $shipping->addChild('CITY',$shippingAddress['city']);
        $shipping->addChild('STATE',$shippingAddress['iso_code']);
        $shipping->addChild('POSTAL_CODE',$shippingAddress['postcode']);
        $shipping->addChild('COUNTRY',"US");
        $shipping->addChild('EMAIL',$this->orderDetails['email']);
        $shipping->addChild('INSTRUCTION');
        $phone = ($invoiceAddress['phone_mobile'] ? $invoiceAddress['phone_mobile'] : $invoiceAddress['phone']);
        $shipping->addChild('TELEPHONE1',$phone);
        $shipping->addChild('TELEPHONE2');
        $shipping->addChild('TELEPHONE3');
        $shipping->addChild('TELEPHONE4');
        $shipping->addChild('SHIPPING_METHOD',self::SHIPPING_METHOD);
    }

    /**
     * @param $supplier
     * @return mixed
     */
    protected function prestshopToMicros($supplier)
    {
        $supplierListPresta = [
            1 => 242,
            2=> 251,
            3=> 259,
            4=> 241,
            5=> 232,
            6=> 243,
            7=> 237,
            8=> 253,
            9=> 248,
            10=> 235,
            11=> 257,
            12=> 254,
            13=> 252,
            14=> 245,
            15=> 240,
            16=> 247,
            17=> 231,
            18=> 238,
            19=> 234,
            20=> 246,
            21=>236,
            22=> 258,
            23=> 250,
            24=> 255,
            25 => 900
        ];

        return $supplierListPresta[$supplier];
    }

    /**
     * Sets class variables for totals.
     */
    protected function getTotals()
    {
        $subTotal = 0;
        $taxTotal = 0;
        $total    = 0;
        foreach($this->products as $product) {
            $subTotal = $subTotal + $product['unit_price_tax_excl'] + $product['shipping'];
            $taxTotal = $taxTotal + $product['itemTax'] +  $product['shippingTax'];

        }
        /*
        if($this->shipment['shipping_cost_tax_incl'] > 0) {
            $subTotal = $subTotal + $this->shipment['shipping_cost_tax_excl'];
            $shippingTax = $this->shipment['shipping_cost_tax_incl'] - $this->shipment['shipping_cost_tax_excl'];
            $taxTotal = $taxTotal + $shippingTax; // add shipping tax to total
        }*/

        $this->setTotal(round($subTotal + $taxTotal,2));
        $this->setSubTotal($subTotal);
        $this->setTaxTotal(round($taxTotal,2));
    }

    /**
     * @param $id
     * @return mixed
     */
    protected function getAddress($id)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');

        $sql = "SELECT lastname, firstname, address1, address2, city, postcode,iso_code,phone,phone_mobile FROM ps_address
                    INNER JOIN ps_state ON ps_address.id_state = ps_state.id_state WHERE id_address = :id_cart";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_cart',$id);
        $stmt->execute();
        return $stmt->fetch();
    }

    /**
     * @return mixed
     */
    protected function getShipping()
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');
        $sql = "SELECT * from ps_order_carrier 
                            INNER JOIN ps_carrier on ps_carrier.id_carrier = ps_order_carrier.id_carrier 
                            WHERE id_order = :id_order";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$this->orderDetails['id_order']);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);

    }

    /**
     * @return mixed|string
     */
    protected function getTaxLocId()
    {
        $shippingAddress = $this->getAddress($this->orderDetails['id_address_delivery']);


        $taxLoc = [
            'MN' => '6.875',
            'WI' => '5.000',
            "ND" => '5.000',
            "SD" => '4.000'
        ];
        
        return (array_key_exists($shippingAddress['iso_code'],$taxLoc) ? $taxLoc[$shippingAddress['iso_code']] : $this->storeLookUp());
    }

    protected function getStoreCount()
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');
        $sql = "SELECT `". $this->store ."` from micros_tansaction_num";
        $stmt = $conn->prepare($sql);

        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        $num = $data[$this->store];
        if($this->isSeqNumToHigh($num)) {
            $num = 0;
        }

        $this->setStoreCnt($num + 1);

        $insertSql = "UPDATE micros_tansaction_num SET `". $this->store ."` = :num";
        $stmt = $conn->prepare($insertSql);

        $stmt->bindValue('num',$this->getStoreCnt());
        $stmt->execute();
    }

    protected function getTrnSeqNum()
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');
        $sql = "SELECT trn_num from micros_tansaction_num";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        $trnNum = $data['trn_num'];
        if($this->isSeqNumToHigh($trnNum)) {
            $trnNum = 0;
        }

        $this->setSeqNum($trnNum + 1);


        $insertSql = "UPDATE micros_tansaction_num SET trn_num = :trn_num";
        $stmt = $conn->prepare($insertSql);
        $stmt->bindValue('trn_num',$this->getSeqNum());
        $stmt->execute();
    }


    protected function isSeqNumToHigh($seqNum)
    {
        return ($seqNum < 1000000 ? false : true);
    }

    protected function setItemTaxes()
    {
        $productCnt = count($this->products);
        $shipping = $this->getShipping();
        if(!$shipping['name'] == 'In-Store Pick Up') {
            $productCnt = $productCnt + 1;
        }
    }

    /**
     * Looks up store tax info for send sale requirements.
     *
     * @return mixed
     */
    protected function storeLookUp()
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');
        $sql = "SELECT * FROM store_supplier_map WHERE id_supplier = :store";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('store',$this->getStore());
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $data['tax_code'];
    }

    protected function convertDate()
    {
        $time=strtotime("+2 hour");
        return date('Y-m-d\TH:i:s',$time);
    }

    /**
     * @return mixed
     */
    public function getTaxGroup()
    {
        return $this->taxGroup;
    }

    /**
     * @param mixed $taxGroup
     */
    public function setTaxGroup($taxGroup)
    {
        $this->taxGroup = $taxGroup;
    }

    /**
     * @return string
     */
    public function getHeaderId()
    {
        return $this->headerId;
    }

    /**
     * @param string $headerId
     */
    public function setHeaderId($headerId)
    {
        $tmp = explode('-',$headerId);
        $this->headerId = $this->prestshopToMicros($this->store)."-".$tmp[1];
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getTaxTotal()
    {
        return $this->taxTotal;
    }

    /**
     * @param float $taxTotal
     */
    public function setTaxTotal($taxTotal)
    {
        $this->taxTotal = $taxTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getOrderDetails()
    {
        return $this->orderDetails;
    }

    /**
     * @param array $orderDetails
     */
    public function setOrderDetails($orderDetails)
    {
        $this->orderDetails = $orderDetails;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return int
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param int $store
     */
    public function setStore($store)
    {
        $this->store = $store;
    }

    /**
     * @return int
     */
    public function getSeqNum()
    {
        return $this->seqNum;
    }

    /**
     * @param int $seqNum
     */
    public function setSeqNum($seqNum)
    {
        $this->seqNum = $seqNum;
    }

    /**
     * @return int
     */
    public function getStoreCnt()
    {
        return $this->storeCnt;
    }

    /**
     * @param int $storeCnt
     */
    public function setStoreCnt($storeCnt)
    {
        $this->storeCnt = $storeCnt;
    }



}