<?php

namespace AppBundle\Services;


use AppBundle\Entity\CodeRelease;
use AppBundle\Entity\Message;
use AppBundle\Entity\ReleaseConfig;
use AppBundle\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReleaseService
{
	private $entityManager;

	private $container;

	/**
	 * ReleaseService constructor.
	 * @param EntityManager $entityManager
	 */
	public function __construct(EntityManager $entityManager, ContainerInterface $container)
	{
		$this->entityManager = $entityManager;
		$this->container = $container;
	}

	/**
	 * @param $id
	 * @throws \Doctrine\ORM\OptimisticLockException
	 * @throws \Twig_Error
	 */
	public function newRelease($id)
	{
		$release = $this->entityManager->getRepository(CodeRelease::class)->find($id);

		$this->createMessages($release);

		$this->sendEmails($release);


	}

	/**
	 * @param CodeRelease $release
	 * @throws \Twig_Error
	 */
	private function sendEmails($release)
	{
		$emails = $this->entityManager->getRepository(ReleaseConfig::class)->findAll();
		foreach($emails as $email) {
			/** @var ReleaseConfig $email */
			$this->sendEmail($email->getEmail(),$email->getName(),$release);
		}
	}

	/**
	 * @param CodeRelease $release
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	private function createMessages($release)
	{
		$users = $this->entityManager->getRepository(User::class)->findBy([
			'status' => 1
		]);
		foreach($users as $user) {
			$message = new Message();
			$message->setMessage($release->getUserMessage());
			$message->setSeen(0);
			$message->setTitle('Code Update');
			$message->setUser($user);
			$date = new DateTime();
			$message->setCreated($date);
			$this->entityManager->persist($message);
			$this->entityManager->flush();
		}
	}

	/**
	 * @param $to
	 * @param $name
	 * @param CodeRelease $release
	 * @throws \Twig_Error
	 */
	private function sendEmail($to,$name, $release)
	{
		$message = \Swift_Message::newInstance()
			->setSubject('New Pail code Release')
			->setFrom('marketplace@pawnamerica.com')
			->addTo($to)
			->setBody(
				$this->container->get('templating')->render('email/release.html.twig',
					[
						'release' => $release
					]
				),
				'text/html');

		$failures = [];
		$this->container->get('mailer')->send($message,$failures);
	}

}