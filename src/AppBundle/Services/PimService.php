<?php


namespace AppBundle\Services;


use Doctrine\DBAL\Connection;

class PimService
{
    const ACTIVE_STATUS = 3;

    const PENDING_STATUS = 1;

	private $connection;

	private $pawnConnection;

	private $mysqlConnection;


	/**
	 * PimService constructor.
	 * @param Connection $connection
	 * @param Connection $pawnConnection
	 * @param Connection $mysqlConnection
	 */
	public function __construct(Connection $connection, Connection $pawnConnection, Connection $mysqlConnection)
	{
		$this->connection = $connection;
		$this->pawnConnection = $pawnConnection;
		$this->mysqlConnection = $mysqlConnection;
	}

	/**
	 * @param $product
	 * @return bool
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function getProductMasterRecord($product)
	{
		//echo "make=".$product['make']."Department_id=".$product['class_id']."Itemcategorycode=".$product['micro_dept']."modul_number=".$product['model'];
		$sql = "SELECT * FROM Product WHERE CategoryCode = :cat AND ItemCategoryCode = :subCat AND Make = :make AND ModelNumber = :model";


		$stmt = $this->mysqlConnection->prepare($sql);
		$stmt->bindValue("make", $product['make']);
		$stmt->bindValue("model", $product['model']);
		$stmt->bindValue("cat", $product['class_id']);
		$stmt->bindValue("subCat", $product['micro_dept']);

		$stmt->execute();
		$data = $stmt->fetch(\PDO::FETCH_ASSOC);
//		echo "<pre>";
//		print_r($data);die;

		if(!$data) {
			return false;
		}

		if($mapped = $this->getMappedProduct($data)) {
			return $data;
		}
	}

	/**
	 * @param $productMaster
	 * @return bool|mixed
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function getMappedProduct($productMaster)
	{
		$sql = "SELECT * FROM ProductItemMapping WHERE itemID = :ItemID AND Status = :status";

		$stmt = $this->mysqlConnection->prepare($sql);
		$stmt->bindValue("ItemID", $productMaster['ItemId']);
        $stmt->bindValue("status", self::ACTIVE_STATUS);

		$stmt->execute();

		$productMapping = $stmt->fetch(\PDO::FETCH_ASSOC);
		//print_r($productMapping);die;
		if($productMapping) {
			return $this->getProductInfo($productMapping);
		}

		return false;
	}

	/**
	 * @param $productMapping
	 * @return mixed
	 * @throws \Doctrine\DBAL\DBALException
	 */
	private function getProductInfo($productMapping)
	{
		$sql = "SELECT * FROM Product WHERE productid = :productid";

		$stmt = $this->mysqlConnection->prepare($sql);
		$stmt->bindValue("productid", $productMapping['ProductID']);

		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param $product
	 */
	public function addToPim($product)
	{
        $sql = "INSERT INTO ProductMaster (categorycode,Itemcategory,make,model_number)
                            VALUES (:categorycode,:Itemcategory,:make,:model_number)";
        $stmt = $this->mysqlConnection->prepare($sql);
        $stmt->bindValue("make", $product['make']);
        $stmt->bindValue("model_number", $product['model']);
        $stmt->bindValue("categorycode", $product['class_id']);
        $stmt->bindValue("Itemcategory", $product['micro_dept']);

        $stmt->execute();

        $lastInsertedId = $this->mysqlConnection->lastInsertId();

        $this->addToProductTable($product,$lastInsertedId);


	}

	/**
	 * @param $product
	 * @param $item_id
 	 * @throws \Doctrine\DBAL\DBALException
	 */
	private function addToProductTable($product,$item_id)
    {
	$sql = "INSERT INTO Product (ItemId,CategoryCode,Make,ModelNumber,Sumary,Title,Description,CreationDate,CreatedBy)
                            VALUES (:item_id,:categorycode,:make,:modelnumber,:sumary,:Title,:description,:CreattionDate,:CreatedBy)";//:description Description
        $stmt1 = $this->mysqlConnection->prepare($sql);
	$stmt1->bindValue("item_id", $item_id);
        $stmt1->bindValue("make", $product['make']);
        $stmt1->bindValue("modelnumber", $product['model']);
        $stmt1->bindValue("categorycode", $product['class_id']);
	$stmt1->bindValue("sumary", $product['description_short']);
	$stmt1->bindValue("Title", $product['name']);
        $stmt1->bindValue("description", htmlentities($product['description']));
	$stmt1->bindValue("CreattionDate", date("Y-m-d"));
        $stmt1->bindValue("CreatedBy", 999);
		$stmt1->execute();

		$lastInsertedId = $this->mysqlConnection->lastInsertId();

		$this->productItemMapping($lastInsertedId,$product,$item_id);
    }

    private function productItemMapping($productId,$product,$itemId)
	{
		$sql = "INSERT INTO ProductItemMapping (ProductID,ItemID,Status,CreatedBy,CreatedDate)
                            VALUES (:ProductID,:ItemID,:Status,:CreatedBy,:CreatedDate)";
		$stmt = $this->mysqlConnection->prepare($sql);
		$stmt->bindValue("ProductID", $productId);
		$stmt->bindValue("ItemID", $itemId);
		$stmt->bindValue("Status", SELF::PENDING_STATUS);
        $stmt->bindValue("CreatedBy", 999);
        $stmt->bindValue("CreatedDate", date("Y-m-d") );

		$stmt->execute();
	}
}
