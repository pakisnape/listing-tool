<?php

namespace AppBundle\Services;

use Doctrine\DBAL\Connection;
use PDO;


class ProductActivationService
{
	private $connection;

	private $pawnConnection;

	/**
	 * ProductActivationService constructor.
	 * @param Connection $connection
	 */
	public function __construct(Connection $connection, Connection $pawnConnection)
	{
		$this->connection = $connection;
		$this->pawnConnection = $pawnConnection;
	}

	/**
	 *
	 */
	public function getProdcutactivationDateByStore($store,$product)
	{
		$sql = "SELECT created FROM product_activation 
					where store = :store and id_product = :id_product
					ORDER BY created Desc LIMIT 1";


		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("store", $store);
		$stmt->bindValue("id_product", $product);

		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param $start
	 * @param $end
	 * @param null $store
	 * @return mixed
	 */
	public function getActivatedProducts($start,$end, $store = null)
	{
		$where = "";
		if($store) {
			$where = " AND store = :store";
		}

		$sql = "SELECT DISTINCT id_product FROM product_activation 
					where created BETWEEN :start AND  :end $where
					ORDER BY id_product";


		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("start", $start . " 00:00:00");
		$stmt->bindValue("end", $end. " 23:59:59");
		if($store) {
			$stmt->bindValue("store", $store);
		}
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);

	}

	public function getActivatedProductsCost($dailyActivatedProducts)
	{
		$ids = [];

		foreach ($dailyActivatedProducts as $key => $value) {
			$ids[] = $value['id_product'];
		}
		if(empty($ids))
		{
			return 0;
		}

		$in  = str_repeat('?,', count($ids) - 1) . '?';

		$sql = "SELECT price FROM ps_product_shop 
					WHERE ps_product_shop.id_product IN($in)";

		$stmt = $this->pawnConnection->prepare($sql);
		$stmt->execute($ids);

		$data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$total = 0;
		foreach ($data as $key => $cost) {
			$total = $total + number_format($cost['price'],0);
		}

		return $total;
	}

	/**
	 * @param $products
	 * @return array
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function getActivatedProductsByIds($products)
	{
		$ids = [];

		foreach ($products as $key => $id) {
			$ids[] = $id['id_product'];
		}
		if(empty($ids))
		{
			return [];
		}

		$in  = str_repeat('?,', count($ids) - 1) . '?';
		$sql = "SELECT ps_product.id_product, ps_product_lang.name,ps_supplier.name as store, ps_product.date_add,
                      ps_product_shop.active,store_date,ebay_date,ps_product_shop.price,ps_product.reference
                  FROM ps_product
                  INNER JOIN ps_product_lang 
                  ON ps_product_lang.id_product = ps_product.id_product
                  INNER JOIN ps_supplier
                  ON ps_supplier.id_supplier = ps_product.id_supplier
                  INNER JOIN ps_product_shop
                  ON ps_product_shop.id_product = ps_product.id_product 
                  LEFT JOIN ps_product_ebay
                   ON ps_product_ebay.id_product = ps_product.id_product
                  WHERE ps_product.id_product IN($in)
                  ";


		$stmt = $this->pawnConnection->prepare($sql);

		$stmt->execute($ids);


		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public static function bindParamArray($prefix, $values, &$bindArray)
	{
		$str = "";
		foreach($values as $index => $value){
			$str .= ":".$prefix.$index.",";
			$bindArray[$prefix.$index] = $value;
		}
		return rtrim($str,",");
	}
}