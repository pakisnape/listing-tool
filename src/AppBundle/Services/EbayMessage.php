<?php
/**
 * Created by PhpStorm.
 * User: nam797
 * Date: 9/8/17
 * Time: 7:16 AM
 */

namespace AppBundle\Services;


use AppBundle\Entity\User;
use Doctrine\DBAL\Connection;

class EbayMessage
{

	private $connection;

	/**
	 * EbayMessage constructor.
	 * @param Connection $connection
	 */
	public function __construct(Connection $connection)
	{
		$this->connection = $connection;
	}

	public function getMessages($product, $store = null)
	{
		$sql = "SELECT * FROM ps_product_ebay_message 
					INNER JOIN user ON user.id = ps_product_ebay_message.user
					 WHERE id_product = :product";

		if ($store) {
			$sql .= " AND store = :store";
		}

		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("product", $product);
		if ($store) {
			$stmt->bindValue("store", $store);
		}

		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param $product
	 * @param $message
	 * @param User $user
	 */
	public function setMessage($product, $message, $user)
	{
		$sql = "INSERT INTO ps_product_ebay_message (user,message, seen,created,id_product,store)
																									VALUES (:user,:message, :seen, :created, :id_product,:store)";

		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("user", $user->getId());
		$stmt->bindValue("id_product", $product);
		$stmt->bindValue("message", $message);
		$stmt->bindValue("seen", 0);
		$stmt->bindValue("store", $user->getStore());
		$stmt->bindValue("created", date("Y-m-d H:m:s"));

		$stmt->execute();
	}

	public function setMessageRead($messageId)
	{
		$sql = "UPDATE ps_product_ebay_message SET seen= :seen WHERE  id = :id";
		$stmt = $this->connection->prepare($sql);
		$stmt->bindValue("seen", 1);
		$stmt->bindValue("id", $messageId);

		$stmt->execute();
	}
}