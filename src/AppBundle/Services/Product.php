<?php

namespace AppBundle\Services;

use Doctrine\DBAL\Connection;
use stdClass;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Product
{
    /**
     * @var  integer
     */
    private $_limit = 25;

    /**
     * @var
     */
    private $_page = 1;

    /**
     * @var
     */
    private $_query;

    /**
     * @var
     */
    private $_total;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getProducts($search)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine.dbal.pawn_connection');

        $sql = "SELECT ps_product.id_product, ps_product_lang.name,ps_supplier.name as store, 
                      ps_stock_available.quantity,ps_product_shop.active
                  FROM ps_product
                  INNER JOIN ps_product_lang 
                  ON ps_product_lang.id_product = ps_product.id_product
                  INNER JOIN ps_supplier
                  ON ps_supplier.id_supplier = ps_product.id_supplier
                  INNER JOIN ps_stock_available
                  ON ps_stock_available.id_product = ps_product.id_supplier
                  INNER JOIN ps_product_shop
                  ON ps_product_shop.id_product = ps_product.id_product
                  ";
        if ($search) {
            $sql .= " WHERE ps_product.id_product = :reference";
        }
        $sql .= " LIMIT " . ( ( $this->getPage() - 1 ) * $this->getLimit() ) . ", $this->getLimit()";

        $stmt = $conn->prepare($sql);
        if ($search) {
            $stmt->bindValue("reference",trim($search));
        }
        $stmt->execute();

        $data = $stmt->fetchAll();

//        $result         = new stdClass();
//        $result->page   = $this->getPage();
//        $result->limit  = $this->getLimit();
//        $result->total  = count($data);
//        $result->data   = $data;

        return $data;
    }

    /**
     * @param $links
     * @param $list_class
     * @return string
     */
    public function createLinks( $links, $list_class ) {
        if ( $this->_limit == 'all' ) {
            return '';
        }

        $last       = ceil( $this->_total / $this->_limit );

        $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
        $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;

        $html       = '<ul class="' . $list_class . '">';

        $class      = ( $this->_page == 1 ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="?limit=' . $this->_limit . '&page=' . ( $this->_page - 1 ) . '">&laquo;</a></li>';

        if ( $start > 1 ) {
            $html   .= '<li><a href="?limit=' . $this->_limit . '&page=1">1</a></li>';
            $html   .= '<li class="disabled"><span>...</span></li>';
        }

        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $this->_page == $i ) ? "active" : "";
            $html   .= '<li class="' . $class . '"><a href="?limit=' . $this->_limit . '&page=' . $i . '">' . $i . '</a></li>';
        }

        if ( $end < $last ) {
            $html   .= '<li class="disabled"><span>...</span></li>';
            $html   .= '<li><a href="?limit=' . $this->_limit . '&page=' . $last . '">' . $last . '</a></li>';
        }

        $class      = ( $this->_page == $last ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="?limit=' . $this->_limit . '&page=' . ( $this->_page + 1 ) . '">&raquo;</a></li>';

        $html       .= '</ul>';

        return $html;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->_limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->_limit = $limit;
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->_page;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->_page = $page;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->_query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->_query = $query;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->_total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->_total = $total;
    }


}