<?php

namespace AppBundle\Services;

use AppBundle\Entity\LastEdited;
use AppBundle\Entity\ProductActivation;
use AppBundle\Entity\User;
use AppBundle\Services\PrestaShop;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\From;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\DBAL\Connection;


class PrestaShopApiService
{


	/**
	 * @var \AppBundle\Services\PrestaShop $prestaShop
	 */
	private $prestaShop;

	public $pawnDatabaseConnection;

	/**
	 * @var EntityManager $entityManager
	 */
	public $entityManager;

	/**
	 * @var CategoryService $categoryService
	 */
	public $categoryService;

	/**
	 * PrestaShopApiService constructor.
	 * @param \AppBundle\Services\PrestaShop $prestaShop
	 * @param Connection $pawnDatabaseConnection
	 * @param EntityManager $entityManager
	 * @param CategoryService $categoryService
	 */
	public function __construct(
		PrestaShop $prestaShop,
		Connection $pawnDatabaseConnection,
		EntityManager $entityManager,
		CategoryService $categoryService
	) {
		$this->prestaShop = $prestaShop;
		$this->pawnDatabaseConnection = $pawnDatabaseConnection;
		$this->entityManager = $entityManager;
		$this->categoryService = $categoryService;
	}


	/**
	 * @param int $ProductId
	 * @param $qty
	 */
	public function getIdStockAvailableAndSet($ProductId = 235935, $qty)
	{
		$opt['resource'] = 'products';
		//$opt['filter']['reference'] = '237063472';//328102
		$opt['id'] = $ProductId;
		//print_r($opt);
		$xml = $this->prestaShop->get($opt);
		unset($xml->position_in_category);
		foreach ($xml->product->associations->stock_availables->stock_available as $item) {
			//echo "ID: ".$item->id."\n\r";
			//echo "Id Attribute: ".$item->id_product_attribute."\n\r";
			$this->set_product_quantity($ProductId, $item->id, $item->id_product_attribute, $qty);
		}
	}

	/**
	 * @param Form $form
	 */
	public function markAddToEbay($form)
	{
		$sql = "SELECT * FROM ps_product_ebay WHERE id_product = :id_product";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_product", $form->get('idProduct')->getData());
		$stmt->execute();
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);

		$now = date('Y-m-d H:m:s');

		if (!$result) {

			$sql = "INSERT INTO ps_product_ebay (id_product,sync,create_date,update_date,store_date,ebay_date) 
																VALUES(:id_product,:sync,:create_date,:update_date,:store_date,:ebay_date)";
			$stmt = $this->pawnDatabaseConnection->prepare($sql);
			$stmt->bindValue("sync", $form->get('addToEbay')->getData());
			$stmt->bindValue("id_product", $form->get('idProduct')->getData());
			$stmt->bindValue("create_date", $now);
			$stmt->bindValue("update_date", $now);

			$storeDate = ($form->get('addToEbay')->getData() == 1 ? $now : null);


			$ebayDate = ($form->get('addToEbay')->getData() == 3 ? $now : null);

			if($form->get('addToEbay')->getData() == 3) {
				$storeDate = $ebayDate;
			}

			$stmt->bindValue("ebay_date", $ebayDate);
			$stmt->bindValue("store_date", $storeDate);

			$stmt->execute();

			return;
		}

		$currentStoreDate = $result['store_date'];
		if($form->get('addToEbay')->getData() == 1 && empty($result['store_date'])) {
			$currentStoreDate = $now;
		}

		$currentEbayDate = $result['ebay_date'];
		if($form->get('addToEbay')->getData() == 3 && empty($result['ebay_date'])) {
			$currentEbayDate = $now;
		}

		if($form->get('addToEbay')->getData() == 3 && empty($result['store_date'])) {
			$currentEbayDate = $currentStoreDate;
		}


		$sql = "UPDATE ps_product_ebay SET sync = :sync, store_date = :store_date,ebay_date = :ebay_date  WHERE  id_product = :id_product";
		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("sync", $form->get('addToEbay')->getData());
		$stmt->bindValue("id_product", $form->get('idProduct')->getData());
		$stmt->bindValue("store_date", $currentStoreDate);
		$stmt->bindValue("ebay_date", $currentEbayDate);
		$stmt->execute();
	}

	/**
	 * @param Form $form
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function disableFastBaySync($form)
	{
		$sql = "SELECT * FROM ps_fastbay1_product_settings WHERE id_product = :id_product";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);

		$stmt->bindValue("id_product", $form->get('idProduct')->getData());
		$stmt->execute();
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);
		$now = date('Y-m-d H:m:s');
		if ($result) {
			$sql1 = "UPDATE ps_fastbay1_product_settings SET sync= :sync,
										date_upd =:date_upd
										WHERE  id_product = :id_product";
			$stmt1 = $this->pawnDatabaseConnection->prepare($sql1);
			$stmt1->bindValue("sync", 0);
			$stmt1->bindValue("id_product", $form->get('idProduct')->getData());
			$stmt1->bindValue("date_upd", $now);
			$stmt1->execute();
		}
	}


	/**
	 * @param Form $form
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function saveFastBaySettings($form)
	{
		$sql = "SELECT * FROM ps_fastbay1_product_settings WHERE id_product = :id_product";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);

		$stmt->bindValue("id_product", $form->get('idProduct')->getData());
		$stmt->execute();
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);
		$now = date('Y-m-d H:m:s');
		// TODO test this
		if (!$result) {

			if ($form->has('fastBayPrice') && $form->get('fastBayPrice')->getData()) {
				$sql = "INSERT INTO ps_fastbay1_product_settings 
							(id_product,title,price,sync,ebaycarrier1,costospedizione1,date_upd,epid,additionalcost1) 
							VALUES(:id_product,:title,:price,:sync,:ebaycarrier1,:costospedizione1,:date_upd,:epid,:additionalcost1)";
				$stmt = $this->pawnDatabaseConnection->prepare($sql);
				$stmt->bindValue("sync", $form->get('fastBaySync')->getData());
				$stmt->bindValue("id_product", $form->get('idProduct')->getData());
				$stmt->bindValue("title", $form->get('fastBayTitle')->getData());
				$stmt->bindValue("price", $form->get('fastBayPrice')->getData());
				$stmt->bindValue("ebaycarrier1", $form->get('fastBayEbaycarrier1')->getData());
				$stmt->bindValue("costospedizione1", $form->get('fastBayCostospedizione1')->getData());
				$stmt->bindValue("additionalcost1", 0.000);
				$stmt->bindValue("date_upd", $now);
				$stmt->bindValue("epid", $form->get('epid')->getData());
				//$stmt->bindValue('additionalcost1', $form->get('additionalcost1')->getData());
				$stmt->execute();

				return;
			}

			$sql = "INSERT INTO ps_fastbay1_product_settings 
						(id_product,date_upd,epid) 
						VALUES(:id_product,:date_upd,:epid)";
			$stmt = $this->pawnDatabaseConnection->prepare($sql);
			$stmt->bindValue("id_product", $form->get('idProduct')->getData());
			$stmt->bindValue("date_upd", $now);
			$stmt->bindValue("epid", ($form->get('epid')->getData() ? $form->get('epid')->getData() : '' ));
			$stmt->execute();

			return;
		}


        if ($form->has('fastBayPrice') && $form->get('fastBayPrice')->getData()) {
            $sql = "UPDATE ps_fastbay1_product_settings SET title = :title, price= :price,sync= :sync,ebaycarrier1 =:ebaycarrier1,epid = :epid,
																costospedizione1 = :costospedizione1,date_upd =:date_upd,additionalcost1 = :additionalcost1
																WHERE  id_product = :id_product";
            $stmt = $this->pawnDatabaseConnection->prepare($sql);
            $stmt->bindValue("price", $form->get('fastBayPrice')->getData());
            $stmt->bindValue("sync", $form->get('fastBaySync')->getData());
            $stmt->bindValue("id_product", $form->get('idProduct')->getData());
            $stmt->bindValue("title",$form->get('fastBayTitle')->getData());
            $stmt->bindValue("ebaycarrier1", $form->get('fastBayEbaycarrier1')->getData());
            $stmt->bindValue("costospedizione1", $form->get('fastBayCostospedizione1')->getData());
            $stmt->bindValue("additionalcost1", 0.000);
            $stmt->bindValue("date_upd", $now);
            $stmt->bindValue("epid", $form->get('epid')->getData());
        } else if ($form->has('epid') && $form->get('epid')->getData()) {
            $sql = "UPDATE ps_fastbay1_product_settings SET epid = :epid WHERE  id_product = :id_product";
            $stmt = $this->pawnDatabaseConnection->prepare($sql);
            $stmt->bindValue("epid", $form->get('epid')->getData());
            $stmt->bindValue("id_product", $form->get('idProduct')->getData());
        }




		$stmt->execute();
	}

	/**
	 * @param Form $form
	 * @return array
	 */
	public function getProductState($form)
	{
		$product = $form->get('idProduct')->getData();
		$sqlDelete = "SELECT ps_product_shop.active FROM ps_product 
						LEFT JOIN ps_product_shop 
						ON ps_product_shop.id_product = ps_product.id_product 
						WHERE ps_product_shop.id_product = :product";

		$stmt = $this->pawnDatabaseConnection->prepare($sqlDelete);
		$stmt->bindValue("product", trim($product));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param Form $form
	 * @param User $user
	 */
	public function updateProduct($form, $user)
	{
		$category = null;
		$this->markAddToEbay($form);
		$this->addToEditList($form,$user);
		$currentProductState = $this->getProductState($form);
		if($currentProductState['active'] == 0) {
			$this->markNewProductActivation($form,$user);
		}

		//if ($form->has('fastBayPrice') && $form->get('fastBayPrice')->getData()) {
			$this->saveFastBaySettings($form);
		//}

		if($currentProductState['active'] == 1 && $form->get('status')->getData() == 0){
			$this->disableFastBaySync($form);
		}


		if ($form->get('carrier')->getData() || $form->get('carrier2')->getData())
		{
			$this->saveDeleteCarrier($form);
			if($form->get('carrier')->getData()) {
				$this->saveCarrier(1,$form->get('idProduct')->getData());
			}

			if($form->get('carrier2')->getData()) {
				$this->saveCarrier(2,$form->get('idProduct')->getData());
			}
		}

		if($form->get('brand')->getData()) {

			$this->saveBrand($form);
		}
		//echo $form->get('idProduct')->getData();die;
		$xml = $this->prestaShop->get(['resource' => 'products', 'id' => $form->get('idProduct')->getData()]);
		$resources = $xml->children()->children();


		$resources->description = $form->get('longDescription')->getData();
		$resources->width = $form->get('width')->getData();
		$resources->height = $form->get('height')->getData();
		$resources->depth = $form->get('depth')->getData();
		$resources->weight = $form->get('weight')->getData();
		$resources->active = $form->get('status')->getData();
		$resources->name = $form->get('title')->getData();
		$resources->description_short = $form->get('shortDescription')->getData();
		//$resources->id_manufacturer = $form->get('brand')->getData();
		$resources->id_category_default = $form->get('catDefault')->getData();
//		if ($form->get('addToEbay')->getData() == 3) {
//
//		}



		// Unset fields that may not be updated
		unset($resources->manufacturer_name);
		unset($resources->quantity);
		unset($resources->position_in_category);

		$resources->id = $form->get('idProduct')->getData();

		$opt = array('resource' => 'products');
		$opt['putXml'] = $xml->asXML();
		$opt['id'] = $form->get('idProduct')->getData();
		$xml = $this->prestaShop->edit($opt);

		$this->categoryService->getUpdateProductCategories($form->get('idProduct')->getData());

	}



	/**
	 * @param $id_product
	 * @return bool
	 */
	private function isProductActivated($id_product)
	{
		$activatedProduct = $this->entityManager->getRepository('AppBundle:ProductActivation')->find($id_product);
		if(!$activatedProduct)
		{
			return false;
		}

		return true;
	}

	/**
	 * @param $form
	 * @param $user
	 */
	private function markNewProductActivation($form,$user)
	{
		$now = date('Y-m-d H:m:s');
		$today = new DateTime($now);
		if(!$this->isProductActivated($form->get('idProduct')->getData()))
		{
			/** @var ProductActivation $lastEdited */
			$newProductActivation = new ProductActivation();
			$newProductActivation->setIdProduct($form->get('idProduct')->getData());
			$newProductActivation->setCreated($today);
			$newProductActivation->setUser($user);
			$newProductActivation->setStore($user->getStore());
			$newProductActivation->setProductData($form->all());

			$this->entityManager->persist($newProductActivation);
			$this->entityManager->flush();
		}
	}

	/**
	 * @param Form $form
	 * @param User $user
	 */
	protected function addToEditList($form,$user)
	{
		$now = date('Y-m-d H:m:s');
		$today = new DateTime($now);

		/** @var LastEdited $lastEdited */
		$lastEdited = new LastEdited();
		$lastEdited->setIdProduct($form->get('idProduct')->getData());
		$lastEdited->setCreated($today);
		$lastEdited->setUser($user);

		$this->entityManager->persist($lastEdited);
		$this->entityManager->flush();

	}

	/**
	 * @param Form $form
	 */
	protected function saveBrand($form)
	{
		$sql = "UPDATE ps_product set id_manufacturer = :id_manufacturer WHERE id_product = :id_product";
		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_product", $form->get('idProduct')->getData());
		$stmt->bindValue("id_manufacturer", $form->get('brand')->getData());

		$stmt->execute();
	}

	protected function saveDefaultCategory($category,$id_product)
	{
		$sql = "UPDATE ps_product set id_category_default = :id_category_default WHERE id_product = :id_product";
		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_product", $id_product);
		$stmt->bindValue("id_category_default", $category);

		$stmt->execute();
	}

	/**
	 * @param $carrier
	 * @param $product
	 */
	protected function saveCarrier($carrier,$product)
	{
		$sql = "INSERT INTO ps_product_carrier (id_product,id_carrier_reference,id_shop) 
																VALUES(:id_product,:id_carrier_reference,:id_shop)";
		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_product", $product);
		$stmt->bindValue("id_carrier_reference", $carrier);
		$stmt->bindValue("id_shop", 1);

		$stmt->execute();
	}

	/**
	 * @param Form $form
	 */
	protected function saveDeleteCarrier($form)
	{
		$product = $form->get('idProduct')->getData();
		$sqlDelete = "DELETE FROM ps_product_carrier WHERE id_product = :prdouct";

		$stmt = $this->pawnDatabaseConnection->prepare($sqlDelete);
		$stmt->bindValue("prdouct", trim($product));
		$stmt->execute();
	}

	/**
	 * @param $id_product
	 * @return mixed
	 */
	protected function setDefaultCategories($id_product)
	{
		$sql = "SELECT * FROM ps_category_product WHERE id_product = :id_product ORDER BY position DESC LIMIT 1";
		$category = $this->setDefaultCategories($form->get('idProduct')->getData());
		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_product", $id_product);
		$stmt->execute();
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);

		$category = $result['id_category'];
		return $category;
		//$this->saveDefaultCategory($category,$id_product);

	}

	/**
	 * @param $file
	 * @param $id_product
	 */
	public function uploadPicture($file, $id_product)
	{
		$this->prestaShop->upload($id_product, $file);
	}

	/**
	 * @param $images
	 * @param $id_product
	 */
	public function deleteImages($images, $id_product)
	{
		$this->prestaShop->deleteIamges($images, $id_product);
	}

	public function changeProductCategories($cats, $id_product)
	{
		$this->deleteProductCategories($id_product);
		$sql = "INSERT INTO ps_category_product 
                                          (id_category,id_product,position)
                                          VALUES (:id_category,:id_product,:position)";
		foreach($cats as $cat) {
			$stmt = $this->pawnDatabaseConnection->prepare($sql);
			$stmt->bindValue("id_category",$cat);
			$stmt->bindValue("id_product",$id_product);
			$stmt->bindValue("position",0);
			$stmt->execute();
		}

	}

	/**
	 * @param $id_product
	 */
	protected function deleteProductCategories($id_product)
	{

		$sqlDelete = "DELETE FROM ps_category_product WHERE id_product = :product";

		$stmt = $this->pawnDatabaseConnection->prepare($sqlDelete);
		$stmt->bindValue("product", trim($id_product));
		$stmt->execute();
	}

	/**
	 * @param $images
	 * @param $product
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function changeImageOrder($images, $product)
	{
		$imgs = [];
		foreach ($images as $image) {
			$parts = explode('-', $image);
			if (count($parts) != 3) {
				return;
			}
			$imgs[] = [
				'position' => $parts[0],
				'id_image' => $parts[1],
				'cover' => $parts[2],
				'id_product' => $product
			];
		}

		$this->removeAllimages($product);
		$this->addNewOrderImages($imgs);

	}

	/**
	 * @param $imgs
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function addNewOrderImages($imgs)
	{
		foreach($imgs as $img) {
			$sql = "INSERT INTO ps_image 
				  (id_product,id_image,`position`,`cover`)
				  VALUES (:product,:id_image,:position,:cover)";
			$stmt = $this->pawnDatabaseConnection->prepare($sql);
			$stmt->bindValue("product", $img['id_product']);
			$stmt->bindValue("id_image", $img['id_image']);
			$stmt->bindValue("position", $img['position']);
			$stmt->bindValue("cover", ($img['cover'] ? $img['cover'] : null ) );
			$stmt->execute();
		}
	}

	/**
	 * removes all images for a product so they can be reordered
	 * @param $id_product
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function removeAllimages($id_product)
	{
		$sql = "DELETE FROM ps_image WHERE id_product = :id_product";
		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_product", $id_product);
		$stmt->execute();
	}

	/**
	 * @param $images
	 * @param $id_product
	 */
	public function markCoverImage($images, $id_product)
	{
		$sql = "SELECT * FROM ps_image_shop WHERE id_product = :id_product";
		$img = $images[0];
		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_product", $id_product);
		$stmt->execute();
		$results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$this->removeCoverImage($id_product);
		foreach ($results as $result)
		{
			if($result['id_image'] == $img) {
				$this->updateImageCover($img,1);
			}
		}
		$this->reOrderImages($img,$id_product);
	}

	/**
	 * @param $img
	 * @param $id_product
	 */
	protected function reOrderImages($img,$id_product)
	{
		$sql = "SELECT * FROM ps_image WHERE id_product = :id_product";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_product", $id_product);
		$stmt->execute();
		$results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		$newOrder = 2;
		foreach($results as $result) {
			if($result['id_image'] == $img) {
				$this->updatePsImage(1,$result['id_image']);
				continue;
			}
			$this->updatePsImage($newOrder,$result['id_image']);
			$newOrder++;
		}
	}

	/**
	 * @param $order
	 * @param $img
	 */
	protected function updatePsImage($order, $img)
	{
		$sql = "UPDATE ps_image set position = :position WHERE id_image = :id_image";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_image", $img);
		$stmt->bindValue("position", $order);
		$stmt->execute();
	}

	/**
	 * @param $id_product
	 */
	protected function removeCoverImage($id_product)
	{
		$sql = "SELECT * FROM ps_image_shop WHERE id_product = :id_product";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_product", $id_product);
		$stmt->execute();
		$results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		foreach ($results as $result)
		{
			$this->updateImageCover($result['id_image']);
		}

	}

	/**
	 * @param $img
	 * @param null $cover
	 */
	protected function updateImageCover($img,$cover = null)
	{
		$sql = "UPDATE ps_image_shop set cover = :cover WHERE id_image = :id_image";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("id_image", $img);
		$stmt->bindValue("cover", $cover);
		$stmt->execute();
	}


	/**
	 * @param $ProductId
	 * @param $status
	 * @param bool $price
	 */
	public function setProductActive($ProductId, $status, $price = false)
	{
		$xml = $this->prestaShop->get(['resource' => 'products', 'id' => $ProductId]);
		$resources = $xml->children()->children();

		// Unset fields that may not be updated
		unset($resources->manufacturer_name);
		unset($resources->quantity);
		unset($resources->position_in_category);
		$resources->id = $ProductId;
		$resources->active = $status;
		if ($price) {
			$resources->price = $price;
		}

		//echo $xml->asXML();die;
		try {
			$opt = array('resource' => 'products');
			$opt['putXml'] = $xml->asXML();
			$opt['id'] = $ProductId;
			$xml = $this->prestaShop->edit($opt);
		} catch (PrestaShopWebserviceException $ex) {
			echo "<bhow>Error   ->Error : </bhow>".$ex->getMessage().'<br>';
		}
	}

	/**
	 * @param $ProductId
	 * @param $StokId
	 * @param $AttributeId
	 */
	public function set_product_quantity($ProductId, $StokId, $AttributeId, $qty = 0)
	{
		$xml = $this->prestaShop->get(array('url' => $this->prestaShop->getUrl().'/api/stock_availables?schema=blank'));
		$resources = $xml->children()->children();
		$resources->id = $StokId;
		$resources->id_product = $ProductId;
		$resources->quantity = $qty;
		$resources->id_shop = 1;
		$resources->out_of_stock = 0;
		$resources->depends_on_stock = 0;
		$resources->id_product_attribute = $AttributeId;
		try {
			$opt = array('resource' => 'stock_availables');
			$opt['putXml'] = $xml->asXML();
			$opt['id'] = $StokId;
			$xml = $this->prestaShop->edit($opt);
		} catch (PrestaShopWebserviceException $ex) {
			echo "\n Error:".$ex->getMessage().'\n';
		}
	}

	/**
	 * @param $productId
	 * @return null
	 */
	public function getSupplier($productId)
	{
		$supplier = null;

		$opt['resource'] = 'product_suppliers';
		$opt['filter']['id_product'] = $productId;//328102

		$xml = $this->prestaShop->get($opt);
		$resources = $xml->children()->children();

		foreach ($resources->product_supplier->attributes() as $key => $sup) {
			$supplier = $sup;
		}

		return $supplier;
	}

	/**
	 * @param $prodSupplier
	 * @param $productId
	 * @param $supplier
	 */
	public function setSupplier($prodSupplier, $productId, $supplier)
	{
		$xml = $this->prestaShop->get(array('resource' => 'product_suppliers', 'id' => $prodSupplier));
		$resources = $xml->children()->children();
		$resources->id = $prodSupplier;
		$resources->id_product = $productId;
		$resources->id_supplier = $supplier;
		try {
			$opt = array('resource' => 'product_suppliers');
			$opt['putXml'] = $xml->asXML();
			$opt['id'] = $prodSupplier;
			$xml = $this->prestaShop->edit($opt);
		} catch (PrestaShopWebserviceException $ex) {
			echo $ex->getMessage();
		}
	}
}
