<?php

namespace AppBundle\Services;


use AppBundle\Entity\BugReport;
use AppBundle\Entity\BugReportMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailService
{
	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * MailService constructor.
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function sendBugMail($to, BugReport $bugReport)
	{
		$message = \Swift_Message::newInstance()
			->setSubject('New Pail code Release')
			->setFrom('marketplace@pawnamerica.com')
			->addTo($to)
			->addTo('Dave.Willmott@pawnamerica.com')
			->setBody(
				$this->container->get('templating')->render('email/bug.html.twig',
					[
						'bug' => $bugReport
					]
				),
				'text/html');

		$failures = [];
		$this->container->get('mailer')->send($message,$failures);
	}

	public function sendBugMessageMail($to, BugReportMessage $bugReport)
	{
		$message = \Swift_Message::newInstance()
			->setSubject('New Pail code Release')
			->setFrom('marketplace@pawnamerica.com')
			->addTo($to)
			->addTo('Dave.Willmott@pawnamerica.com')
			->setBody(
				$this->container->get('templating')->render('email/bugMessage.html.twig',
					[
						'bug' => $bugReport
					]
				),
				'text/html');

		$failures = [];
		$this->container->get('mailer')->send($message,$failures);
	}
}