<?php


namespace AppBundle\Services;


use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CarrierService
{
	public $pawnDatabaseConnection;

	/**
	 * Products constructor.
	 * @param Connection $pawnDatabaseConnection
	 */
	public function __construct(Connection $pawnDatabaseConnection)
	{
		$this->pawnDatabaseConnection = $pawnDatabaseConnection;
	}

	/**
	 * @return array
	 */
	public function getProductCarriers($product)
	{
		$sql = "SELECT * FROM ps_product_carrier WHERE id_product = :prdouct";

		$stmt = $this->pawnDatabaseConnection->prepare($sql);
		$stmt->bindValue("prdouct", trim($product));
		$stmt->execute();

		$data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		return $this->transformData($data);
		}

	/**
	 * @param $data
	 * @return array
	 */
	private function transformData($data)
	{
		$carrier = [];
		foreach ($data as $car) {
			$carrier[] = $car['id_carrier_reference'];
		}

		return $carrier;
	}
}