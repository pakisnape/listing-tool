<?php

namespace AppBundle\Services;

use Doctrine\DBAL\Connection;
use PDO;

class EbayActivationService
{
	private $connection;

	private $pawnConnection;

	/**
	 * ProductActivationService constructor.
	 * @param Connection $connection
	 */
	public function __construct(Connection $connection, Connection $pawnConnection)
	{
		$this->connection = $connection;
		$this->pawnConnection = $pawnConnection;
	}

	/**
	 * @param $start
	 * @param $end
	 * @param null $store
	 * @return mixed
	 */
	public function getActivatedProducts($start,$end, $store = null)
	{
		$where = "";
		if($store) {
			$where = " AND ps_supplier.id_supplier = :store";
		}

		$sql = "SELECT ps_product.id_product, ps_product_lang.name,ps_supplier.name as store, ps_product.date_add,
                      ps_product_shop.active,store_date,ebay_date,ps_product_shop.price,ps_product.reference
                  FROM ps_product
                  INNER JOIN ps_product_lang 
                  ON ps_product_lang.id_product = ps_product.id_product
                  INNER JOIN ps_supplier
                  ON ps_supplier.id_supplier = ps_product.id_supplier
                  INNER JOIN ps_product_shop
                  ON ps_product_shop.id_product = ps_product.id_product 
                  INNER JOIN ps_product_ebay 
                  ON ps_product_ebay.id_product = ps_product.id_product
					where ps_product_ebay.ebay_date BETWEEN :start AND  :end $where
					ORDER BY id_product";


		$stmt = $this->pawnConnection->prepare($sql);
		$stmt->bindValue("start", $start." 00:00:00");
		$stmt->bindValue("end", $end. " 23:59:59");
		if($store) {
			$stmt->bindValue("store", $store);
		}
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);

	}

	public function getStoreSentToEbay($start,$end, $store = null)
	{
		$where = "";
		if($store) {
			$where = " AND ps_supplier.id_supplier = :store";
		}

		$sql = "SELECT ps_product.id_product, ps_product_lang.name,ps_supplier.name as store, ps_product.date_add,
                      ps_product_shop.active,store_date,ebay_date,ps_product_shop.price,ps_product.reference
                  FROM ps_product
                  INNER JOIN ps_product_lang 
                  ON ps_product_lang.id_product = ps_product.id_product
                  INNER JOIN ps_supplier
                  ON ps_supplier.id_supplier = ps_product.id_supplier
                  INNER JOIN ps_product_shop
                  ON ps_product_shop.id_product = ps_product.id_product 
                  INNER JOIN ps_product_ebay 
                  ON ps_product_ebay.id_product = ps_product.id_product
					where ps_product_ebay.store_date BETWEEN :start AND  :end $where
					ORDER BY id_product";


		$stmt = $this->pawnConnection->prepare($sql);
		$stmt->bindValue("start", $start." 00:00:00");
		$stmt->bindValue("end", $end. " 23:59:59");
		if($store) {
			$stmt->bindValue("store", $store);
		}
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}