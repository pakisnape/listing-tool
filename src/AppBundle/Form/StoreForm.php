<?php

namespace AppBundle\Form;

use AppBundle\Entity\SupplierForm;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StoreForm extends AbstractType
{
	public $suppliers;

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->suppliers = $options['suppliers'];

		$builder
			->add('store', ChoiceType::class, [
				'attr' => [
					'class' => 'form-control',
				],
				'placeholder' => 'No Supplier',
				'choices' => $this->suppliers,
				'empty_data' => 'Anoka',
			])
			->add('active', ChoiceType::class, [
				'attr' => [
					'class' => 'form-control',
				],
				'placeholder' => 'No Supplier',
				'choices' => [
					'Active' => '1',
					'InActive' => '0',
				],
				'empty_data' => 'Active',
			])
			->add('search', TextType::class,[
				'attr' => [
					'class' => 'form-control',
				],
			]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'attr' => array('novalidate' => 'novalidate'),
			'data_class' => SupplierForm::class,
			'suppliers' => null,
		));
	}

	public function getBlockPrefix()
	{
		return 'app_bundle_sync_form';
	}
}
