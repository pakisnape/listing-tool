<?php

namespace AppBundle\Form;


use Doctrine\DBAL\Types\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActivationForm extends AbstractType
{
	public $suppliers;

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->suppliers = $options['suppliers'];

		$builder
			->add('store', ChoiceType::class, [
				'attr' => [
					'class' => 'form-control',
				],
				'placeholder' => 'No Supplier',
				'choices' => $this->suppliers,
				'empty_data' => 'Anoka',
			])
			->add('start', TextType::class,[
				'attr' => [
					'class' => 'js-datepicker',
				],
			])
		->add('end', TextType::class,[
			'attr' => [
				'class' => 'js-datepicker',
			],
		]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'attr' => array('novalidate' => 'novalidate'),
			'data_class' => \AppBundle\Entity\ActivationForm::class,
			'suppliers' => null,
		));
	}

	public function getBlockPrefix()
	{
		return 'app_bundle_activation_form';
	}
}
