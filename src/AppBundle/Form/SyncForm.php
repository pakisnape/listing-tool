<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SyncForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('addToEbay', ChoiceType::class, [
				//'label' => 'Add to Ebay',
				'attr' => [
					'class' => 'form-control col-xs-3',
				],
				'choices' => [
					'Ready to Sync' => '1',
					'No' => '0',
					'Synced' => '3',
					'Needs Info' => 2,
				],
				'data' => 'Ready to Sync',
			]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'attr' => array('novalidate' => 'novalidate'),
		));
	}

	public function getBlockPrefix()
	{
		return 'app_bundle_sync_form';
	}
}
