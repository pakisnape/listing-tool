<?php

namespace AppBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReleaseForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('userMessage', CKEditorType::class, array(
				'attr' => array(
					'class' => 'form-control',
				),
			))
			->add('devMessage', CKEditorType::class, array(
				'attr' => array(
					'class' => 'form-control',
				),
			))
			->add('bug', TextType::class, [
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('save', SubmitType::class, [
				'attr' => array(
					'class' => 'btn btn-primary',
				),
				'label' => 'Create Release'
			]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
//		$resolver->setDefaults(array(
//			'attr' => array('novalidate' => 'novalidate'),
//		));
	}

	public function getBlockPrefix()
	{
		return 'app_bundle_release';
	}
}
