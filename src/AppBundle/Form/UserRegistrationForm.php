<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegistrationForm extends AbstractType
{

	public $suppliers;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->suppliers = $options['suppliers'];

		$builder
			->add('email', EmailType::class, [
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('store', ChoiceType::class, [
				'attr' => [
					'class' => 'form-control',
				],
				'placeholder' => 'No Supplier',
				'choices' => $this->suppliers,
			])
			->add('roles', ChoiceType::class, [
				'multiple' => true,
				'expanded' => false, // render check-boxes
				'choices' => [
					'Admin' => 'ROLE_ADMIN',
					'Sales' => 'ROLE_SALES',
					'EBAY' => 'ROLE_EBAY',
					// ...
				],
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('plainPassword', RepeatedType::class, [
				'attr' => [
					'class' => 'form-control',
				],
				'type' => PasswordType::class,
				'options' => array('attr' => array('class' => 'form-control')),

			]);

	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => User::class,
			'suppliers' => null,
		]);
	}
}