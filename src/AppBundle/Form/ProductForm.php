<?php

namespace AppBundle\Form;


use AppBundle\Entity\Product;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataMapper\CheckboxListMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ProductForm extends AbstractType
{

	public $brands;

	private $authorization;


	public function __construct(AuthorizationChecker $authorizationChecker)
	{
		$this->authorization = $authorizationChecker;
	}


	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->brands = $options['brands'];


		$builder
			->add('title', TextType::class, [
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('shortDescription', CKEditorType::class, array(
				'attr' => array(
					'class' => 'form-control',
				),
			))
			->add('longDescription', CKEditorType::class, array(
				'attr' => array(
					'class' => 'form-control',
				),
			))
			->add('width', TextType::class, [
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('height', TextType::class, [
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('depth', TextType::class, [
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('weight', TextType::class, [
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('carrier', CheckboxType::class, [
				'label' => 'In-Store Pick Up (Pick up in-store where item is located)',
			])
			->add('carrier2', CheckboxType::class, [
				'label' => 'Standard Ground Shipping Preferred Carrier Disclosure',
			])
			->add('brand', ChoiceType::class, [
				'attr' => [
					'class' => 'form-control',
				],
				'choices' => $this->brands,
			])
			->add('status', ChoiceType::class, [
				'attr' => [
					'class' => 'form-control',
				],
				'choices' => [
					'Disabled' => '0',
					'Active' => '1',
				],
			])
			->add('addToEbay', ChoiceType::class, [
				'label' => 'Add to Ebay',
				'attr' => [
					'class' => 'form-control',
				],
				'choices' => [
					'Yes' => '1',
					'No' => '0',
					'Synced' => '3',
					'Needs Info' => 2,
				],
			])
			->add('epid', TextType::class, [
				'label' => 'Ebay Product Id',
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('mpn', TextType::class, [
				'label' => 'MPN',
				'attr' => [
					'class' => 'form-control',
				],
			])
			->add('idProduct', HiddenType::class)
			->add('catDefault', HiddenType::class)
			->add('mpn', TextType::class,[
				'attr' => [
					'class' => 'form-control',
				],
			]);

		if ($this->authorization->isGranted('ROLE_EBAY')) {
			// do as you want if admin
			$builder
				->add('fastBayTitle', TextType::class, [
					'label' => 'Ebay Title',
					'attr' => [
						'class' => 'form-control',
					],
				])
				->add('fastBayDescription', TextareaType::class, [
					'label' => 'Ebay Desription',
					'attr' => [
						'class' => 'form-control',
						'rows' => '6',
					],
				])
				->add('fastBayPrice', TextType::class, [
					'label' => 'Ebay Price',
					'attr' => [
						'class' => 'form-control',
					],
				])
				->add('fastBaySync', ChoiceType::class, [
					'label' => 'Disable sync:',
					'attr' => [
						'class' => 'form-control',
					],
					'choices' => [
						'Yes' => '0',
						'No' => '1',
					],
				])
				->add('fastBayEbaycarrier1', ChoiceType::class, [
					'label' => 'Ebay Carrier',
					'attr' => [
						'class' => 'form-control',
					],
					'choices' => [
						'ShippingMethodStandard' => 'ShippingMethodStandard',
						'Pickup' => 'Pickup',
					],
				])
				->add('fastBayCostospedizione1', TextType::class, [
					'label' => 'Ebay Shipping Cost',
					'attr' => [
						'class' => 'form-control',
					],
				]);
		}
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'attr' => array('novalidate' => 'novalidate'),
			'data_class' => Product::class,
			'brands' => null,
		));
	}

	public function getBlockPrefix()
	{
		return 'app_bundle_product_form';
	}
}
