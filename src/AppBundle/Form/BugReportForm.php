<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BugReportForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('message', TextareaType::class, array(
				'attr' => array(
					'class' => 'form-control',
				),
			));
	}

	public function configureOptions(OptionsResolver $resolver)
	{

	}

	public function getBlockPrefix()
	{
		return 'app_bundle_bug_report_form';
	}
}
