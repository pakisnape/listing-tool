<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AdvancedSearchForm extends AbstractType
{
	public $suppliers;

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->suppliers = $options['suppliers'];

		$builder
			->add('store', ChoiceType::class, [
				'attr' => [
					'class' => 'form-control',
				],
				'placeholder' => 'No Supplier',
				'choices' => $this->suppliers,
				'empty_data' => 'Anoka',
			])
			->add('active', ChoiceType::class, [
				'attr' => [
					'class' => 'form-control',
				],
				'placeholder' => 'No Supplier',
				'choices' => [
					'Active' => '1',
					'InActive' => '0',
				],
				'empty_data' => 'Active',
			]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => AdvancedSearchForm::class,
			'suppliers' => null,
		));
	}

	public function getBlockPrefix()
	{
		return 'app_bundle_advanced_search_form';
	}
}
