<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class FixMissingSupplierCommand  extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:add-shippers')

            // the short description shown while running "php bin/console list"
            ->setDescription('emails all active ebay products')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("emails all active ebay products")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $products = $conn->fetchAll('SELECT * FROM ps_product');

        foreach ($products as $product) {
            if(count($this->hasInStorePickup($product)) < 1) {
                $this->addInStorePickup($product);
            }
            if ($product['width'] > 0 && $product['height'] > 0 && $product['depth'] > 0 && $product['weight'] > 0) {
                if(count($this->hasFreeShipping($product)) < 1) {
                    $this->AddFreeShipping($product);
                }
            }
            /*
            $currentProducts = $this->getExistingProduct($product['reference']);
            if(count($currentProducts) > 1) {
                $this->fixDuplicate($currentProducts);
            }*/
        }
    }

    protected function AddFreeShipping($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $ps_product_carrierSql = "INSERT INTO ps_product_carrier (id_product, id_carrier_reference, id_shop)
                                          VALUES (:id_product,:id_carrier_reference,:id_shop)";
        $stmt = $conn->prepare($ps_product_carrierSql);
        $stmt->bindValue("id_carrier_reference",2);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->bindValue("id_shop",1);
        $stmt->execute();
    }

    protected function addInStorePickup($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $ps_product_carrierSql = "INSERT INTO ps_product_carrier (id_product, id_carrier_reference, id_shop)
                                          VALUES (:id_product,:id_carrier_reference,:id_shop)";
        $stmt = $conn->prepare($ps_product_carrierSql);
        $stmt->bindValue("id_carrier_reference",1);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->bindValue("id_shop",1);
        $stmt->execute();
    }

    protected function hasInStorePickup($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT * FROM ps_product_carrier where id_product = :id_product AND id_carrier_reference = :reference";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_product',$product['id_product']);
        $stmt->bindValue('reference',1);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    protected function hasFreeShipping($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT * FROM ps_product_carrier where id_product = :id_product AND id_carrier_reference = :reference";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_product',$product['id_product']);
        $stmt->bindValue('reference',2);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    protected function deleteCurrentRow($product)
    {

        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $deleteSql = "DELETE FROM pawn_import WHERE id = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$product['id']);
        $stmt->execute();
    }

    protected function fixDuplicate($currentProducts)
    {
        $active = [];
        $inactive = [];
        foreach ($currentProducts as $product) {
            $qty = $this->getStockAvailable($product);
            if(!$product['active'] && $qty['quantity']) {
                $inactive[] = $product['id_product'];
            }
            if($product['active'] && $qty['quantity']) {
                $active[] = $product['id_product'];
            }
        }
        if ($active) {
            foreach($inactive as $row) {
                $this->deleteProduct($row);
            }
            return true;
        }
        $save = true;
        foreach($inactive as $row) {
            if($save) {
                continue;
            }
            $this->deleteProduct($row);
            $save = false;
        }

        return true;
    }

    protected function deleteProduct($row)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $deleteSql = "DELETE FROM ps_product WHERE id_product = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$row);
        $stmt->execute();
        $conn->commit();

        $deleteSql = "DELETE FROM ps_product WHERE id_product = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$row);
        $stmt->execute();
        $conn->commit();

        $deleteSql = "DELETE FROM ps_product_supplier WHERE id_product = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$row);
        $stmt->execute();
        $conn->commit();

        $deleteSql = "DELETE FROM ps_product_lang WHERE id_product = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$row);
        $stmt->execute();
        $conn->commit();

        $deleteSql = "DELETE FROM ps_product_shop WHERE id_product = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$row);
        $stmt->execute();
        $conn->commit();

        $deleteSql = "DELETE FROM ps_stock_available WHERE id_product = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$row);
        $stmt->execute();
        $conn->commit();

        $deleteSql = "DELETE FROM ps_category_product WHERE id_product = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$row);
        $stmt->execute();
        $conn->commit();
    }

    protected function getStockAvailable($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT `quantity` FROM ps_stock_available where id_product = :id_product";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_product',$product['id_product']);
        $stmt->execute();
        return $stmt->fetch();
    }

    protected function getExistingProduct($reference)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT * FROM ps_product where reference = :reference";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('reference',trim($reference));
        $stmt->execute();

        return $stmt->fetchAll();
    }

    protected function insertSupplier($product,$location)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $deleteSql = "DELETE FROM ps_product_supplier WHERE id_product = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$product['id_product']);
        $stmt->execute();
        $conn->commit();


        $ProductSupplierInsertSql = "INSERT INTO ps_product_supplier
                        (id_product, id_product_attribute, id_supplier, product_supplier_reference, product_supplier_price_te, id_currency)
                        VALUES (:product_id, :prod_attribute, :supplier, :supplier_reference, :supplier_price, :currency)";
        $stmt = $conn->prepare($ProductSupplierInsertSql);
        $stmt->bindValue("product_id",$product['id_product']);
        $stmt->bindValue("supplier",$location);

        $stmt->bindValue("prod_attribute",0);
        $stmt->bindValue("supplier_reference",'');
        $stmt->bindValue("supplier_price",0.00000);
        $stmt->bindValue("currency",0);
        $stmt->execute();
    }

    protected function updateProduct($product,$location)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $productSql = "UPDATE ps_product set id_supplier = :supplier WHERE id_product = :id_product";
        $stmt = $conn->prepare($productSql);
        $stmt->bindValue("supplier",$location);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->execute();
    }

    protected function setSupplier($product,$productSupplier)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $productSql = "UPDATE ps_product set id_supplier = :supplier WHERE id_product = :id_product";
        $stmt = $conn->prepare($productSql);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->bindValue("supplier",$productSupplier);
        $stmt->execute();
    }

    /**
     * @param $product
     * @return int
     */
    protected function getSupplier($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT * FROM ps_product_supplier where id_product = :id_product";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_product',$product["id_product"]);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach($result as $product_supplier) {
            return $product_supplier['id_supplier'];
        }

        return 0;
    }

    protected function getLocation($location)
    {
        // MS SQL ID => PrestaShop ID
        $supplierList = [
            242 => 1,
            251 => 2,
            259 => 3,
            241 => 4,
            232 => 5,
            243 => 6,
            237 => 7,
            253 => 8,
            248 => 9,
            235 => 10,
            257 => 11,
            254 => 12,
            252 => 13,
            245 => 14,
            240 => 15,
            247 => 16,
            231 => 17,
            238 => 18,
            234 => 19,
            246 => 20,
            236 => 21,
            258 => 22,
            250 => 23,
            255 => 24,
        ];

        return ( array_key_exists($location,$supplierList) ? $supplierList[$location] : '');
    }
}