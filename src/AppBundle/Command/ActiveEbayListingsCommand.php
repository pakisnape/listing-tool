<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use PHPExcel_Writer_Excel2007;
use Swift_Attachment;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use PHPExcel_IOFactory;
use PHPExcel;


class ActiveEbayListingsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:active-ebay-products')

            // the short description shown while running "php bin/console list"
            ->setDescription('emails all active ebay products')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("emails all active ebay products")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $products = $conn->fetchAll("SELECT ps_product.id_product, ps_product.reference,ps_product.id_supplier, ps_product_lang.description_short,
                                      ps_product_shop.price
                                    FROM ps_product 
                                    INNER JOIN ps_product_lang ON ps_product_lang.id_product = ps_product.id_product
                                    INNER JOIN ps_product_shop ON ps_product_shop.id_product = ps_product.id_product
                                    WHERE ps_product.active = 1  AND ps_product.quantity = 1 ORDER BY id_supplier ");

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'reference');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'name');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'price');


        $rowCount =2;
        foreach ($products as $product) {
            if (!$this->isEbay($product)) {
                continue;
            }
            $desc_short = preg_replace('/<[^>]*>/', '', $product['description_short']);
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $product['reference']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $product['id_supplier']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $desc_short);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $product['price']);

            $rowCount++;
        }

        $now = date('Y-m');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $fileName = "ebayListings-".$now .".xlsx";

        $objWriter->save($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $fileName);

        $this->sendEbayListings($fileName);
    }

    protected function sendEbayListings($file)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Ebay Listings')
            ->setFrom('support@pawnamerica.com')
            ->addTo('ndahl@nerdery.com')
            ->addTo('Dave.Willmott@pawnamerica.com')
            ->attach(Swift_Attachment::fromPath($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $file));

        $this->getContainer()->get('mailer')->send($message);
    }

    /**
     * @param $product
     * @return int
     */
    protected function isEbay($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT * FROM ps_fastbay1_product_settings where id_product = :id_product and sync = :sync";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_product',$product['id_product']);
        $stmt->bindValue('sync',1);
        $stmt->execute();
        return count($stmt->fetchAll());
    }
}