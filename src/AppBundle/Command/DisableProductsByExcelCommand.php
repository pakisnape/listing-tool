<?php

namespace AppBundle\Command;

use PDO;
use PHPExcel_IOFactory;
use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class DisableProductsByExcelCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:disable-products-excel')

            // the short description shown while running "php bin/console list"
            ->setDescription('disable products.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks to disable products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT * FROM pawn_import_active";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $activeProducts = $stmt->fetchAll();

        foreach($activeProducts as $product) {
            $psProduct = $this->getProduct($product['reference']);
            if (!$psProduct) {
                continue;
            }
            if($this->getImages($psProduct)) {
                $this->activateProduct($psProduct,$product['id']);
            } else {
                $this->deactivateProduct($psProduct,$product['id']);
            }
        }
    }

    protected function activateProduct($psProduct,$id)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $conn->beginTransaction();

        try {
            $productSql = "UPDATE ps_product set active = :active, quantity = :qty WHERE id_product = :id_product";
            $stmt = $conn->prepare($productSql);
            $stmt->bindValue("active",1);
            $stmt->bindValue("qty",1);
            $stmt->bindValue("id_product",$psProduct['id_product']);
            $stmt->execute();


            $ProductStockAvailableSql = "UPDATE ps_product_shop set active = :active WHERE id_product = :id_product";
            $stmt = $conn->prepare($ProductStockAvailableSql);
            $stmt->bindValue("id_product",$psProduct['id_product']);
            $stmt->bindValue("active",1);
            $stmt->execute();

            $sql = "UPDATE ps_stock_available SET quantity = :qty WHERE id_product = :id_product";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue("id_product",$psProduct['id_product']);
            $stmt->bindValue("qty",1);
            $stmt->execute();

            $deleteSql = "DELETE FROM pawn_import_active WHERE id = :id";
            $stmt = $conn->prepare($deleteSql);
            $stmt->bindValue("id",$id);
            $stmt->execute();

            $conn->commit();

        } catch (Exception $e) {
            $conn->rollBack();
        }
    }

    protected function deactivateProduct($psProduct,$id)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $conn->beginTransaction();

        try {
            $productSql = "UPDATE ps_product set active = :active, quantity = :qty WHERE id_product = :id_product";
            $stmt = $conn->prepare($productSql);
            $stmt->bindValue("active",0);
            $stmt->bindValue("qty",0);
            $stmt->bindValue("id_product",$psProduct['id_product']);
            $stmt->execute();


            $ProductStockAvailableSql = "UPDATE ps_product_shop set active = :active WHERE id_product = :id_product";
            $stmt = $conn->prepare($ProductStockAvailableSql);
            $stmt->bindValue("id_product",$psProduct['id_product']);
            $stmt->bindValue("active",0);
            $stmt->execute();

            $sql = "UPDATE ps_stock_available SET quantity = :qty WHERE id_product = :id_product";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue("id_product",$psProduct['id_product']);
            $stmt->bindValue("qty",0);
            $stmt->execute();

            $deleteSql = "DELETE FROM pawn_import_active WHERE id = :id";
            $stmt = $conn->prepare($deleteSql);
            $stmt->bindValue("id",$id);
            $stmt->execute();

            $conn->commit();

        } catch (Exception $e) {
            $conn->rollBack();
        }
        //ldCur3.execute('UPDATE ps_stock_available SET quantity = %s WHERE id_product=%s', (0, paProdId))
    }

    protected function getImages($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $imageSql = "SELECT * FROM ps_image WHERE id_product = :id_product";
        $stmt = $conn->prepare($imageSql);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return ( count($result) > 0 ? true : false);

    }

    protected function getProduct($reference)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT * FROM ps_product WHERE reference = :reference limit 1";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('reference',$reference);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}