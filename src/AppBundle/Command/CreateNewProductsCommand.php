<?php
/**
 * Created by PhpStorm.
 * User: nam797
 * Date: 9/1/16
 * Time: 7:50 AM
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CreateNewProductsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:new-products')

            // the short description shown while running "php bin/console list"
            ->setDescription('Adds new products.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for created and updated date to see if it's a new product. It then checks to see if it exists. if it doesn't then it creates")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $conn = $this->getContainer()->get('database_connection');
        $product = $conn->fetchAll('SELECT * FROM ps_product');

        $output->writeln(count($product));
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        // outputs a message followed by a "\n"
        $output->writeln('Whoa!');

        // outputs a message without adding a "\n" at the end of the line
        $output->write('You are about to ');
        $output->write('create a user.');
    }
}