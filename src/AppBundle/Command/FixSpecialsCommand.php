<?php


namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class FixSpecialsCommand  extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:fix-specials')

            // the short description shown while running "php bin/console list"
            ->setDescription('insert new products.')

            ->setDefinition(
                new InputDefinition([
                    new InputOption('id','i',InputOption::VALUE_OPTIONAL)
                ])
            )

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for new products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $productInserts = $conn->fetchAll('SELECT * FROM ps_specific_price');

        $i = 1;
        foreach($productInserts as $insertProduct) {
            $conn->beginTransaction();

            try {
                $sql = "SELECT * FROM ps_product where active = :active AND id_product = :id_product LIMIT 1";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('active',1);
                $stmt->bindValue('id_product',trim($insertProduct["id_product"]));
                $stmt->execute();
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                var_dump($result);
                if(!isset($result['id_product'])) {
                    $deleteSql = "DELETE FROM ps_specific_price WHERE id_specific_price = :id";
                    $stmt = $conn->prepare($deleteSql);
                    $stmt->bindValue("id",$insertProduct['id_specific_price']);
                    $stmt->execute();
                    //$conn->commit();
                }
            } catch (Exception $e) {
                $conn->rollBack();

            }
            $i++;
        }
    }
}