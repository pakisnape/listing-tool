<?php

namespace AppBundle\Command;


use Doctrine\DBAL\Connection;
use PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class FixJewelryDescriptionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:fix-jewelry')

            // the short description shown while running "php bin/console list"
            ->setDescription('disable products.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks to disable products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $products = $conn->fetchAll('SELECT * FROM pawn_import_fix');
        $cnt = 0;
        foreach($products as $product) {
            $productSql = "SELECT ps_product_shop.active,ps_stock_available.quantity,ps_product.id_product
                              FROM ps_product 
                              INNER JOIN ps_product_shop on ps_product_shop.id_product = ps_product.id_product
                              INNER JOIN ps_stock_available ON ps_stock_available.id_product = ps_product.id_product
                              where ps_product.reference = :reference AND ps_stock_available.quantity = :qty AND ps_product_shop.active = :active LIMIT 1";
            $stmt = $conn->prepare($productSql);
            $stmt->bindValue("reference",$product['item_id']);
            $stmt->bindValue("qty",1);
            $stmt->bindValue("active",0);
            $stmt->execute();
            $productFix = $stmt->fetch(PDO::FETCH_ASSOC);;
            if ($productFix && $this->getImages($productFix)) {
                //$this->activateProduct($productFix);
                $cnt++;
            }
            echo $cnt."\n";
        }

    }



    /**
     * @param $product
     */
    protected function activateProduct($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $productSql = "UPDATE ps_product set active = :active WHERE id_product = :id_product";
        $stmt = $conn->prepare($productSql);
        $stmt->bindValue("active",1);
        $stmt->bindValue("id_product",$product['id_product']);
        //$stmt->bindValue("quantity",1);
        $stmt->execute();

        $productShopSql = "UPDATE ps_product_shop SET active = :active WHERE id_product = :product ";
        $stmt = $conn->prepare($productShopSql);
        $stmt->bindValue("active",1);
        $stmt->bindValue("product",$product['id_product']);
        $stmt->execute();
    }

    /**
     * @param $product
     * @return bool
     */
    protected function getImages($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $imageSql = "SELECT * FROM ps_image WHERE id_product = :id_product";
        $stmt = $conn->prepare($imageSql);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return ( count($result) > 0 ? true : false);

    }

    protected function fixProduct($fixProducts,$product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        foreach($fixProducts as $product) {
            $productSql = "UPDATE ps_product_lang set description_short = :description_short, `name` = :nName WHERE id_product = :id_product";
            $stmt = $conn->prepare($productSql);
            $stmt->bindValue("description_short",$product['custom_description']);
            $stmt->bindValue("nName",$product['custom_description']);
            $stmt->bindValue("id_product",$product['id_product']);
            $stmt->execute();
        }
    }
}