<?php


namespace AppBundle\Command;

use PHPExcel_IOFactory;
use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class DisableInventoryCommand extends ContainerAwareCommand
{


    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:disable-products')

            // the short description shown while running "php bin/console list"
            ->setDescription('1st time not found')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks to disable products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fs = new Filesystem();
        $dir = $this->getContainer()->getParameter('disable_inventory');
        $file = "inventoryVariance.xlsx";

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //$objReader->setReadDataOnly(false);
        $objPHPExcel = $objReader->load($dir.$file);
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(3);

        $excelRow =1;
        foreach ($objWorksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $excelCell = 1;
            $data = [];
            $broken = false;
            foreach ($cellIterator as $cell) {
                if($excelRow == 1 and $excelCell == 1) {
                    if($cell->getValue() != "Item")
                    {
                        $output->writeln("improperly formatted excel doc");
                        die();
                    }
                }
                $data[] = $cell->getValue();
                $excelCell++;
            }
            $this->updateProduct($data);

            $excelRow++;
        }

        $fs->remove($dir.$file);
    }

    protected function updateProduct($data)
    {
        $products = $this->getProduct($data);

        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $productSql = "UPDATE ps_product set active = :active WHERE reference = :reference";
        $stmt = $conn->prepare($productSql);
        $stmt->bindValue("reference",$data[0]);
        $stmt->bindValue("active",0);
        $stmt->execute();



        foreach($products as $product) {
            $productShopSql = "UPDATE ps_product_shop SET active = :active WHERE id_product = :product ";
            $stmt = $conn->prepare($productShopSql);
            $stmt->bindValue("active",0);
            $stmt->bindValue("product",$product['id_product']);
            $stmt->execute();

        }

    }

    /**
     * @param $data
     * @return array
     */
    protected function getProduct($data)
    {
        $reference = (string)$data[0];
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT * FROM ps_product where active = :active AND reference = :reference";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('active',1);
        $stmt->bindValue('reference',$reference);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }
}