<?php


namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use PHPExcel_IOFactory;
use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class NewCustmerSignupCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:monthly-new-customers')

            // the short description shown while running "php bin/console list"
            ->setDescription('updates products from an excel file.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for updated products from an excel file.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lastMonth = strtotime("-2 week");
        $now = date('Y-m-d',$lastMonth);
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT * FROM ps_customer WHERE dat_add > :now";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('now',$now);
        $stmt->execute();
        $customers = $stmt->fetchAll();



        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $rowCount =1;
        foreach ($customers as $customer) {
            $address = $this->getAddress($customer['id_customer']);
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $customer['id_customer']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $customer['firstname']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $customer['lastname']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $address['address1']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $address['address2']);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $address['city']);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $address['iso_code']);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowCount, $address['postcode']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowCount, $address['phone']);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowCount, $address['phone_mobile']);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$rowCount, $customer['email']);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$rowCount, $customer['date_add']);
            $rowCount++;
        }

        $now = date('Y-m');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $fileName = "PawnAmerica-".time() .".xlsx";

        $objWriter->save($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $fileName);

        $this->sendToFtp($fileName);
        $output->writeln(count($customers));
    }

    protected function sendToFtp($fileName)
    {

        $filesystem = $this->getContainer()->get('cuneo_ftp');
        $file = file_get_contents($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $fileName);

        $contents = $filesystem->write($fileName,$file);
    }


    protected function getAddress($customerId)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        return $conn->fetchAssoc("SELECT * FROM ps_address inner join ps_state on ps_state.id_state = ps_address.id_state WHERE id_customer =".$customerId." LIMIT 1");
    }
}