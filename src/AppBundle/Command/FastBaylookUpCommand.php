<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use PHPExcel_IOFactory;
use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Swift_Attachment;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class FastBaylookUpCommand  extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:fastbay-lookup')

            // the short description shown while running "php bin/console list"
            ->setDescription('updates products from an excel file.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for updated products from an excel file.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "select * from ps_fastbay1_product 
		INNER JOIN ps_product ON ps_product.id_product = ps_fastbay1_product.id_product 
		where ps_product.active = :tmp";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('tmp',true);
        $stmt->execute();

        $productsToFix = [];
        $products = $stmt->fetchAll();

        foreach($products as $product)
        {
            //if(!$this->hasFastBayOrder($product)) {
                $productsToFix[] = $product;
            //}
        }
        $fileName = $this->buildExcel($productsToFix);
        //$this->markProductActive($productsToFix);
        $this->sendSalesReport($fileName);
    }

    protected function markProductActive($productsToFix)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        foreach($productsToFix as $product) {
            $sql = "UPDATE ps_product set active = :active WHERE id_product = :id_product";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('active',1);
            $stmt->bindValue('id_product',$product['id_product']);
            $stmt->execute();

            $sqlShop = "update ps_product_shop SET active = :active WHERE id_product = :id_product";
            $stmt = $conn->prepare($sqlShop);
            $stmt->bindValue('active',1);
            $stmt->bindValue('id_product',$product['id_product']);
            $stmt->execute();
        }
    }

    protected function hasFastBayOrder($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "select * from ps_fastbay1_order 
		         where id_order_ref LIKE :tmp";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('tmp','%'.$product['id_product_ref'].'%');
        $stmt->execute();
        $products = $stmt->fetchAll();

        return count($products);
    }

    protected function buildExcel($productsToFix)
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'product id');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'reference');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'status');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'online order');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'qty');


        $rowCount =2;
        foreach ($productsToFix as $product) {

            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $product['id_product']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $product['reference']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $product['active']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $product['available_for_order']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $this->getStockAvailable($product));

            $rowCount++;
        }

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $fileName = "inActiveProducts.xlsx";

        $objWriter->save($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $fileName);

        return $fileName;
    }

    protected function sendSalesReport($file)
    {

        $message = \Swift_Message::newInstance()
            ->setSubject(' In-active Fastbay products on production')
            ->setFrom('support@pawnamerica.com')
            ->addTo('ndahl@nerdery.com')
            //->addTo('dave.willmott@pawnamerica.com')
            ->attach(Swift_Attachment::fromPath($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $file));
        //->setBcc([,'todd.hyland@cashpass.com','web.store11@pawnamerica.com'])


        $this->getContainer()->get('mailer')->send($message);

    }

    protected function getStockAvailable($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "select * from ps_stock_available 
		         where id_product LIKE :product";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('product',$product['id_product']);
        $stmt->execute();
        $products = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $products['quantity'];
    }

}