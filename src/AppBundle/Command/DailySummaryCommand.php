<?php


namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DailySummaryCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:daily-summary')

            // the short description shown while running "php bin/console list"
            ->setDescription('Store daily summary.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for new products.")
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reportData = [];
        $start     = date("Y-m-d H:i:s",strtotime("-1 days"));//"2018-03-01 22:00:00";//
        $end       = date("Y-m-d H:i:s");//"2018-03-02 22:00:00";//

        $orders = $this->getContainer()->get('pawn.order')->getOrders($start,$end);

        $ordersByStore = $this->getContainer()->get('pawn.order')->getOrderedProducts($orders);

        $storeTotals = $this->getContainer()->get('pawn.order')->parseData($ordersByStore);
        ksort($storeTotals);
        $this->sendDailySummary($storeTotals);
    }

    /**
     * @param $ordersByStore
     * @return array
	 * @deprecated
     */
    protected function parseData($ordersByStore)
    {
        $suppliers = $this->getSuppliers();
        $reportData = [];
        $storesWithSales = [];
        foreach ($ordersByStore as $store => $data) {
            array_push($storesWithSales,$store);
            $websiteTotal = 0;
            $ebayTotal = 0;
            $active = $this->getActiveItemsBySupplier($store);
            foreach($data as $order) {
                if($order['module'] == 'fastbay1') {
                    $ebayTotal = $ebayTotal + $order['total'] + $order['shippingCost'];
                    continue;
                }
                $websiteTotal = $websiteTotal + $order['total'] + $order['shippingCost'];
            }
			$activationStart     = date("Y-m-d");
			$activationEnd       = date("Y-m-d");
			$dailyActivatedProducts = $this->getContainer()->get('pawn.product_activation')->getActivatedProducts($activationStart,
				$activationEnd, $store);

			$dailyActivatedEbayItems = $this->getContainer()->get('pawn.ebay_activation')->getActivatedProducts($activationStart,$activationEnd,$store);

			$dailyActivatedCost = $this->getContainer()->get('pawn.product_activation')->getActivatedProductsCost($dailyActivatedProducts);

			$dailySentToEbay = $this->getContainer()->get('pawn.ebay_activation')->getStoreSentToEbay($activationStart,$activationEnd,$store);

			$reportData[$store] = [
                'name' => (array_key_exists($store,$suppliers) ? $suppliers[$store] : $store.' - not found'),
                'ebay' => $ebayTotal,
                'website' => $websiteTotal,
                'activeItems' => $active['website'],
                'activeEbayItems' => $active['ebay'],
                'updatedProducts' => $this->getUpdatedProductsCount($store),
				'dailyActivated' => count($dailyActivatedProducts),
				'dailyActivatedCost' => $dailyActivatedCost,
				'dailyActivatedEbay' => count($dailyActivatedEbayItems),
				'newPulls' => $this->getContainer()->get('pawn.products')->getProductsByDateStore($activationStart,$store),
				'sentToEbay' => $dailySentToEbay
            ];
        }
        $storesTmp = array_unique($storesWithSales);
        $storesWithOutSales = $this->findMissingStores($storesTmp,$suppliers);
        if($storesWithOutSales) {
            foreach ($storesWithOutSales as $store) {
                $websiteTotal = 0;
                $ebayTotal = 0;
                $active = $this->getActiveItemsBySupplier($store);

				$activationStart     = date("Y-m-d");
				$activationEnd       = date("Y-m-d");
				$dailyActivatedProducts = $this->getContainer()->get('pawn.product_activation')->getActivatedProducts($activationStart,
					$activationEnd, $store);

				$dailyActivatedEbayItems = $this->getContainer()->get('pawn.ebay_activation')->getActivatedProducts($activationStart,$activationEnd,$store);

				$dailyActivatedCost = $this->getContainer()->get('pawn.product_activation')->getActivatedProductsCost($dailyActivatedProducts);

				$dailySentToEbay = $this->getContainer()->get('pawn.ebay_activation')->getStoreSentToEbay($activationStart,$activationEnd,$store);

                $reportData[$store] = [
                    'name' => (array_key_exists($store,$suppliers) ? $suppliers[$store] : ''),
                    'ebay' => $ebayTotal,
                    'website' => $websiteTotal,
                    'activeItems' => $active['website'],
                    'activeEbayItems' => $active['ebay'],
                    'updatedProducts' => $this->getUpdatedProductsCount($store),
					'dailyActivated' => count($dailyActivatedProducts),
					'dailyActivatedCost' => $dailyActivatedCost,
					'dailyActivatedEbay' => count($dailyActivatedEbayItems),
					'newPulls' => $this->getContainer()->get('pawn.products')->getProductsByDateStore($activationStart,$store),
					'sentToEbay' => $dailySentToEbay
                ];
            }
        }
        return $reportData;
    }

    /**
     * @param $storesTmp
     * @param $suppliers
     * @return array
	 * @deprecated
     */
    protected function findMissingStores($storesTmp,$suppliers)
    {
        $missingStores = [];
        foreach($suppliers as $key => $supplier) {
            if(!array_key_exists($key,$storesTmp)) {
                array_push($missingStores,$key);
            }
        }

        return $missingStores;
    }

    /**
     * @param $reportData
     */
    protected function sendDailySummary($reportData)
    {
    	//print_r($reportData);die;
        $message = \Swift_Message::newInstance()
            ->setSubject('Store Summary - ' .date("Y-m-d"))//date("Y-m-d")
            ->setFrom('support@pawnamerica.com')
            ->addTo('nam797@gmail.com')
            ->addTo('steve.caulfield@pawnamerica.com')
            ->addTo('dave.willmott@pawnamerica.com')
            ->addTo('todd.hyland@cashpass.com')
            ->addTo('keith.kaestner@pawnamerica.com')
            ->addTo('matt.johnson@pawnamerica.com')
            ->addTo('james.slarks@pawnamerica.com')
            ->addTo('david.neilitz@pawnamerica.com')
            ->addTo('dan.kealey@pawnamerica.com')
            //->addTo('laura.regnier@pawnamerica.com')
			->addTo('Brad.Rixmann@pawnamerica.com')
			->addTo('myron.bailey@pawnamerica.com')
			->addTo('dave.caulfield@pawnamerica.com')
			->addTo('adam.dukek@pawnamerica.com')
			->addTo('andy.frauenholtz@pawnamerica.com')
			->addTo('kristen.roberts@pawnamerica.com')
			->addTo('anthony.atwood@pawnamerica.com')
            //->setBcc([,'todd.hyland@cashpass.com','web.store11@pawnamerica.com'])
            ->setBody(
                $this->getContainer()->get('templating')->render('summary/summary.html.twig',
                    [
                        'data' => $reportData
                    ]
                ),
                'text/html'
            );

        $this->getContainer()->get('mailer')->send($message);
    }

    /**
     * @return array
     */
    public function getSuppliers()
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT * FROM ps_supplier where active = 1";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $reportData = [];
        $suppliers = $stmt->fetchAll();
        foreach ($suppliers as $supplier) {
            $reportData[$supplier['id_supplier']] = $supplier['name'];
        }

        return $reportData;
    }

    /**
     * @param $supplier
     * @return mixed
     */
    protected function getUpdatedProductsCount($supplier)
    {
        $now = date("Y-m-d H:i:s",strtotime("-1 days"));

        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = 'SELECT count(*) as cnt 
                                FROM ps_product 
                                WHERE ps_product.id_supplier = :suppler AND ps_product.date_upd >= :yesterday';
        $stmt = $conn->prepare($sql);

        $stmt->bindValue('suppler',$supplier);
        $stmt->bindValue('yesterday',$now);

        $stmt->execute();

        $results = $stmt->fetch();

        return $results['cnt'];
    }

    /**
     * @param $supplier
     * @return array
     */
    protected function getActiveItemsBySupplier($supplier)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT *  FROM ps_product 
                         INNER JOIN ps_product_shop ON ps_product_shop.id_product = ps_product.id_product
                         WHERE ps_product.id_supplier = :supplier AND ps_product_shop.active = :active";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('supplier',$supplier);
        $stmt->bindValue('active',1);
        $stmt->execute();
        $results = $stmt->fetchAll();

        $ebay =0;
        $site =0;

        foreach ($results as $result) {
            if ($this->isEbay($result)) {
                $ebay = $ebay + 1;
                continue;
            }
            $site = $site + 1;
        }

        return ['website' =>$site,'ebay' =>$ebay];
    }

    /**
     * @param $product
     * @return int
     */
    protected function isEbay($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT * FROM ps_fastbay1_product where id_product = :id_product";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_product',$product['id_product']);
        //$stmt->bindValue('sync',1);
        $stmt->execute();
        return count($stmt->fetchAll());
    }


    /**
     * @param $currentOrders
     * @return array
     */
    protected function getOrderedProducts($currentOrders)
    {
        $orders = [];

        foreach($currentOrders as $order) {
            $data = $this->getProducts($order);


            $stores = $this->getProductSuppliers($data);

            $shping = $order['total_shipping_tax_incl'];
            $shipping = [$shping];
            if(count($stores) > 1) {
                $shipping = $this->splitShipping(count($data),$order);
            }

            $i =0;
            foreach($data as $product) {
                $orders[$product['id_supplier']][] = [
                        'sold' => $order['date_add'],
                        'status' => $order['name'],
                        'total' => $product['total_price_tax_incl'],
                        'shippingCost' => (count($shipping) > 1 ? $shipping[$i] : $shipping[0]),
                        'module' => $order['module']
                    ];
                $i++;
            }
        }

        return $orders;
    }

    /**
     * @param $order
     * @return array
     */
    protected function getProducts($order)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT product_name, product_reference, total_price_tax_incl, total_price_tax_excl,ps_product.id_supplier, 
                        ps_supplier.name as supplierName , product_price,id_order
                    FROM ps_order_detail
                    INNER JOIN ps_product ON ps_product.id_product = ps_order_detail.product_id 
                    INNER JOIN ps_supplier ON ps_supplier.id_supplier = ps_product.id_supplier
                    WHERE id_order=:id_order";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$order['id_order']);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * @param $data
     * @return array
     */
    protected function getProductSuppliers($data)
    {
        $suppliers = [];
        foreach($data as $product) {
            $suppliers[] = $product['id_supplier'];
        }

        return array_unique($suppliers);
    }

    /**
     * @return array
     */
    protected function getOrders()
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $start = date("Y-m-d H:i:s",strtotime("-1 days"));
        $end = date("Y-m-d H:i:s");

        $sql = "SELECT id_order, reference, ps_orders.id_carrier, ps_orders.id_customer, id_cart, total_paid, total_shipping_tax_excl, ps_orders.date_add,
                        total_paid_tax_incl,total_paid_tax_excl,`module`,total_shipping_tax_incl,
                       ps_carrier.name as carrierName,ps_order_state_lang.name
                  FROM ps_orders 
                  INNER JOIN ps_carrier ON ps_carrier.id_carrier = ps_orders.id_carrier
                  INNER JOIN ps_order_state_lang ON ps_order_state_lang.id_order_state = ps_orders.current_state
                  WHERE ps_orders.date_add >= :now AND ps_orders.date_add < :end";//
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('now',$start);
        $stmt->bindValue('end',$end);
        $stmt->execute();
        return $stmt->fetchAll();

    }

    /**
     * @param $stores
     * @param $order
     * @return array
     */
    protected function splitShipping($stores,$order)
    {
        $shippingCost = $order['total_shipping_tax_incl'];
        $shippingSplit = round($shippingCost / $stores,2);
        $tmpShippingTotal = $shippingSplit * $stores;
        $shippingOffset = $shippingCost - $tmpShippingTotal;
        $shippingArray = [];
        for($i =1; $i <= $stores; $i++) {
            if($i == $stores) {
                array_push($shippingArray,$shippingSplit + $shippingOffset);
            }
            array_push($shippingArray,$shippingSplit);
        }

        return $shippingArray;
    }

}