<?php


namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class InsertNewProductsCommand extends ContainerAwareCommand
{
    const HOME_CATEGORY = 2;

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:insert-new-products')

            // the short description shown while running "php bin/console list"
            ->setDescription('insert new products.')

            ->setDefinition(
                new InputDefinition([
                    new InputOption('id','i',InputOption::VALUE_OPTIONAL)
                ])
            )

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for new products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getOption('id',0);

        $date_add = date("Y-m-d H:i:s");


        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        if($id) {
            $productSql = "SELECT * FROM pawn_import where id = :reference";
            $stmt = $conn->prepare($productSql);
            $stmt->bindValue("reference",$id);
            $stmt->execute();
            $productInserts = $stmt->fetchAll();
        } else {
            $productInserts = $conn->fetchAll('SELECT * FROM pawn_import');
        }



        foreach($productInserts as $insertProduct) {

            $sql = "SELECT * FROM ps_product where reference = :reference";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('reference',trim($insertProduct["item_id"]));
            $stmt->execute();
            $result = $stmt->fetchAll();


            $subCategory = $this->getCategoryDefault($insertProduct['subdepartment_id']);
            $class_id = $this->getCategoryDefault($insertProduct['class_id']);

            if(count($result) > 0 || !$subCategory) {
                $deleteSql = "DELETE FROM pawn_import WHERE id = :id";
                $stmt = $conn->prepare($deleteSql);
                $stmt->bindValue("id",$insertProduct['id']);
                $stmt->execute();
                continue;
            }

            $nextHomePosition =  $this->getCategoryPosition(self::HOME_CATEGORY);
            $nextSubCategoryPosition = $this->getCategoryPosition($subCategory);
            $nextSubSubCategoryPosition = $this->getCategoryPosition($class_id);


            $manufacturer = $this->getManufacturer($insertProduct['make']);
            $supplier = $this->getLocation($insertProduct['rtl_loc_id']);

            if (!$supplier) {
                $deleteSql = "DELETE FROM pawn_import WHERE id = :id";
                $stmt = $conn->prepare($deleteSql);
                $stmt->bindValue("id",$insertProduct['id']);
                $stmt->execute();
                continue;
            }

            if($insertProduct['retail_price'] == '' || $insertProduct['retail_price'] == 0.00) {
                $deleteSql = "DELETE FROM pawn_import WHERE id = :id";
                $stmt = $conn->prepare($deleteSql);
                $stmt->bindValue("id",$insertProduct['id']);
                $stmt->execute();
                continue;
            }

            $conn->beginTransaction();

            try {
                $price = $insertProduct['retail_price'];

                $ProductInsertSql = "INSERT INTO ps_product 
                                      (id_manufacturer,price,id_supplier,id_tax_rules_group,quantity,`condition`,reference,id_category_default,date_add,date_upd,width,height,depth,weight,model) 
                                      VALUES (:manufacturer,:price,:supplier,:tax_group,:qty,:used,:reference,:id_category_default,:date_add,:date_upd,:width,:height,:depth,:weight,:model)";
                $stmt = $conn->prepare($ProductInsertSql);
                $stmt->bindValue("manufacturer",$manufacturer);
                $stmt->bindValue("price",$price);
                $stmt->bindValue("supplier",$supplier);
                $stmt->bindValue("tax_group",2);
                $stmt->bindValue("qty",1);
                $stmt->bindValue("used",'used');
                $stmt->bindValue('model',$insertProduct['model']);
                //
                $stmt->bindValue("width",($subCategory == 15 ? 6.000000 : 0.000000));
                $stmt->bindValue("height",($subCategory == 15 ? 6.000000 : 0.000000));
                $stmt->bindValue("depth",($subCategory == 15 ? 6.000000 : 0.000000));
                $stmt->bindValue("weight",($subCategory == 15 ? 1.0000000 : 0.000000));


                $stmt->bindValue("reference",$insertProduct['item_id']);
                $stmt->bindValue("id_category_default",self::HOME_CATEGORY);// this needs to be sub-department later
                $stmt->bindValue("date_add",$date_add);
                $stmt->bindValue("date_upd",$date_add);

                $stmt->execute();

                $lastInsertedId = $conn->lastInsertId();

                $ProductSupplierInsertSql = "INSERT INTO ps_product_supplier
                        (id_product, id_product_attribute, id_supplier, product_supplier_reference, product_supplier_price_te, id_currency)
                        VALUES (:product_id, :prod_attribute, :supplier, :supplier_reference, :supplier_price, :currency)";
                $stmt = $conn->prepare($ProductSupplierInsertSql);
                $stmt->bindValue("product_id",$lastInsertedId);
                $stmt->bindValue("supplier",$supplier);

                $stmt->bindValue("prod_attribute",0);
                $stmt->bindValue("supplier_reference",'');
                $stmt->bindValue("supplier_price",0.00000);
                $stmt->bindValue("currency",0);
                $stmt->execute();


                $customDescription = $insertProduct['custom_description'];
                $keyWords = $insertProduct['description'];
                $formattedManufacturer = strtolower(str_replace(array('.', '/', ' ',), '', $insertProduct['subdepartment_id']));

                $ProductLangInsertSql = "INSERT INTO ps_product_lang 
                                          (id_product,id_lang,link_rewrite,`name`,description_short,meta_description,meta_keywords,meta_title,description)
                                          VALUES (:product,:id_lang,:linkRewrite,:langName,:short_description,:metaDesc,:metaKeywords,:metaTitele,:description)";
                $stmt = $conn->prepare($ProductLangInsertSql);
                $stmt->bindValue("product",$lastInsertedId);
                $stmt->bindValue("id_lang",1);
                $stmt->bindValue("linkRewrite",($formattedManufacturer == 1 ? 'none' : $formattedManufacturer ));
                $stmt->bindValue("langName",$insertProduct['custom_description']);

                $item  = $insertProduct['class_id'];
                $gender = ($insertProduct['gender'] == "None" ? '' : ($insertProduct['gender'] == "M" ? "Mens" : "Ladies"));
                $style = $this->getRingStyle($insertProduct['subclass_id']);
                $karat_purity = $insertProduct['karat_purity'];
                $metal = $insertProduct['model'];
                $metalWeight = round($insertProduct['metal_weight'],2);
                $size = ($insertProduct['size'] == "None" ? '' : $insertProduct['size']);
                $length = ($insertProduct['length'] == "None" ? '' : $insertProduct['length'] > 0 ? round($insertProduct['length'],2) : '');
                $primaryGem = $insertProduct['primary_gem'];
                $primaryGemCut = $insertProduct['primary_gem_cut'];
                $primaryGemQty = $insertProduct['aps_gem_qty'];
                $caratWeight = ($insertProduct['aps_gem_carat_weight'] ? round($insertProduct['aps_gem_carat_weight'],2) : '');





                $jewelry = "<p>Your satisfaction is our number one goal. With all of our fine jewelry, each item has been professionally cleaned, polished and inspected to insure long lasting beauty and durability. The pictures are of the actual item being sold. Buy with confidence; we stand behind what we sell.</p>
                <ul>
                    <li><strong>Inv#</strong> - ". $insertProduct['item_id'] . "</li>
                    <li><strong>Item</strong> - ". $item . "</li>
                    <li><strong>Gender</strong> - ". $gender . "</li>
                    <li><strong>Style</strong> - " . $style ."</li>
                    <li><strong>Metal Karats</strong> - " . $karat_purity . "</li>
                    <li><strong>Metal</strong> - " . $metal ."</li>
                    <li><strong>Metal Weight</strong> - " . $metalWeight ."</li>
                    <li><strong>Ring Size</strong> - " . $size . "</li>
                    <li><strong>Chain Length</strong> - " .$length ."</li>
                    <li><strong>Primary Gem</strong> - " . $primaryGem ."</li>
                    <li><strong>Primary Gem Cut</strong> - " . $primaryGemCut . "</li>
                    <li><strong>Primary Gem Qty</strong> - " . $primaryGemQty . "</li>
                    <li><strong>Carat Weight</strong> - " . $caratWeight ."</li>
                    <li><strong>Color</strong> -</li>
                    <li><strong>Clarity</strong> -</li>
                    <li><strong>Additional Gem</strong> -</li>
                    <li><strong>Additional Gem Qty</strong> -</li>
                    <li><strong>Carat Weight</strong> -</li>
                    <li><strong>Color</strong> -</li>
                    <li><strong>Clarity</strong> -</li>
                </ul>
                ";
                $description = "
                <p>Your satisfaction is our number one goal. Keep in mind that we offer hassle-free returns if needed. If you have any questions or problems, please contact us.</p>
                <p>Please Note: All included items are shown in the pictures</p>
                <p>". $insertProduct['item_id'] . "</p>
                <p><strong>Features:</strong></p>
                <ul>
                    <li>Feature 1</li>
                </ul>
                <p><b>Whats' included:</b></p>
                <ul>
                    <li>Accessory 1</li>
                </ul>
                <p><b>What's not included:</b></p>
                <ul>
                    <li>Accessory 1</li>
                </ul>
                <p><strong>Condition:</strong></p>
                <ul>
                    <li>Condition 1</li>
                </ul>
                ";
                $desc = ($subCategory == 15 ? $jewelry : $description);
                $stmt->bindValue("description",$desc);
                $stmt->bindValue("short_description",$customDescription);
                $stmt->bindValue("metaDesc",$customDescription);
                $stmt->bindValue("metaKeywords",$keyWords);
                $stmt->bindValue("metaTitle",$customDescription);
                $stmt->execute();

                $ProductShopInsertSql ="INSERT INTO ps_product_shop 
                        (id_product,id_shop, id_category_default, id_tax_rules_group,
                          on_sale, online_only, ecotax, minimal_quantity, price, wholesale_price,
                           unity, unit_price_ratio, additional_shipping_cost, customizable, uploadable_files,
                            text_fields, active, redirect_type, id_product_redirected, available_for_order,
                              `condition`, show_price, indexed, visibility, cache_default_attribute,
                              advanced_stock_management, pack_stock_type,date_add,date_upd) 
                        VALUES (:product,:shop, :category, :id_tax_rules_group, :on_sale, :online_only, 
                                :ecotax, :minimal_quantity, :price, :wholesale_price, :unity, :unit_price_ratio,
                                 :additional_shipping_cost, :customizable, :uploadable_files, :text_fields, :active,
                                  :redirect_type, :id_product_redirected, :available_for_order,
                                   :condition, :show_price, :indexed, :visibility, :cache_default_attribute, :advanced_stock_management, :pack_stock_type,:date_add,:date_upd)";

                $stmt = $conn->prepare($ProductShopInsertSql);
                $stmt->bindValue("product",$lastInsertedId);
                $stmt->bindValue("shop",1);
                $stmt->bindValue("category",self::HOME_CATEGORY);// this needs to be sub-cat later on
                $stmt->bindValue("id_tax_rules_group",2);
                $stmt->bindValue("on_sale",0);
                $stmt->bindValue("online_only",0);
                $stmt->bindValue("ecotax",0.00000);
                $stmt->bindValue("minimal_quantity",1);
                $stmt->bindValue("price",$price);
                $stmt->bindValue("wholesale_price",0.000000);
                $stmt->bindValue("unity",1);
                $stmt->bindValue("unit_price_ratio",0.000000);
                $stmt->bindValue("additional_shipping_cost",0.00);
                $stmt->bindValue("customizable",0);
                $stmt->bindValue("uploadable_files",0);
                $stmt->bindValue("text_fields",0);
                $stmt->bindValue("active",0);
                $stmt->bindValue("redirect_type",'');
                $stmt->bindValue("id_product_redirected",0);
                $stmt->bindValue("available_for_order",1);

                $stmt->bindValue("condition",'used');
                $stmt->bindValue("show_price",1);
                $stmt->bindValue("indexed",1);
                $stmt->bindValue("visibility",'both');
                $stmt->bindValue("cache_default_attribute",0);
                $stmt->bindValue("advanced_stock_management",1);
                $stmt->bindValue("pack_stock_type",3);
                $stmt->bindValue("date_add",$date_add);
                $stmt->bindValue("date_upd",$date_add);
                $stmt->execute();


                $ProductStockAvailableSql = "INSERT INTO ps_stock_available 
                                          (id_product,id_product_attribute,id_shop,id_shop_group,quantity,depends_on_stock,out_of_stock)
                                          VALUES (:product,:prod_attr,:shop,:shop_group,:qty,:depends,:out_of_stock)";
                $stmt = $conn->prepare($ProductStockAvailableSql);
                $stmt->bindValue("product",$lastInsertedId);
                $stmt->bindValue("prod_attr",0);
                $stmt->bindValue("shop",1);
                $stmt->bindValue("shop_group",0);
                $stmt->bindValue("qty",1);
                $stmt->bindValue("depends",0);
                $stmt->bindValue("out_of_stock",0);
                $stmt->execute();

                $ps_product_carrierSql = "INSERT INTO ps_product_carrier (id_product, id_carrier_reference, id_shop)
                                          VALUES (:id_product,:id_carrier_reference,:id_shop)";
                $stmt = $conn->prepare($ps_product_carrierSql);
                $stmt->bindValue("id_carrier_reference",1);
                $stmt->bindValue("id_product",$lastInsertedId);
                $stmt->bindValue("id_shop",1);
                $stmt->execute();

                if ($subCategory == 15) {
                    $ps_product_carrierSql = "INSERT INTO ps_product_carrier (id_product, id_carrier_reference, id_shop)
                                              VALUES (:id_product,:id_carrier_reference,:id_shop)";
                    $stmt = $conn->prepare($ps_product_carrierSql);
                    $stmt->bindValue("id_carrier_reference", 2);
                    $stmt->bindValue("id_product", $lastInsertedId);
                    $stmt->bindValue("id_shop", 1);
                    $stmt->execute();
                }


                // put it in the home category
                $ProductCategorySql = "INSERT INTO ps_category_product 
                                          (id_category,id_product,position)
                                          VALUES (:id_category,:id_product,:position)";
                $stmt = $conn->prepare($ProductCategorySql);
                $stmt->bindValue("id_category",self::HOME_CATEGORY);
                $stmt->bindValue("id_product",$lastInsertedId);
                $stmt->bindValue("position",($nextHomePosition + 1));
                $stmt->execute();

                if($nextSubCategoryPosition && $subCategory) {
                    $SubCategorySql = "INSERT INTO ps_category_product 
                                          (id_category,id_product,position)
                                          VALUES (:id_category,:id_product,:position)";
                    $stmt = $conn->prepare($SubCategorySql);
                    $stmt->bindValue("id_category",$subCategory);
                    $stmt->bindValue("id_product",$lastInsertedId);
                    $stmt->bindValue("position",($nextSubCategoryPosition + 1));
                    $stmt->execute();
                }

                if($nextSubSubCategoryPosition && $class_id && $nextSubSubCategoryPosition != $nextSubCategoryPosition) {
                    $SubSubCategorySql = "INSERT INTO ps_category_product 
                                          (id_category,id_product,position)
                                          VALUES (:id_category,:id_product,:position)";
                    $stmt = $conn->prepare($SubSubCategorySql);
                    $stmt->bindValue("id_category",$class_id);
                    $stmt->bindValue("id_product",$lastInsertedId);
                    $stmt->bindValue("position",($nextSubSubCategoryPosition + 1));
                    $stmt->execute();
                }
                if($subCategory == 15) {
                    // create product features
                    $data = [
                        //'JewelryItemCode' => $insertProduct['item'],
                        'gender' => $gender,
                        'jewelryStyle'  => $this->getRingStyle($insertProduct['subclass_id'],''),
                        'metalPurity' => $karat_purity,
                        'metalType' => $metal,
                        'PrimaryGem' => $insertProduct['primary_gem'],
                        'primaryGemCut' => ''
                    ];

                    // Gender
                    //$genderFeature = $this->getFeature(9);
                    $genderSelection = $this->getFeatureSelection(8,$gender);
                    if($gender != '') {
                        $featureSql = "INSERT INTO ps_feature_product (id_feature, id_product, id_feature_value)
                                              VALUES (:id_feature,:id_product,:id_feature_value)";
                        $stmt = $conn->prepare($featureSql);
                        $stmt->bindValue("id_feature",8);
                        $stmt->bindValue("id_product",$lastInsertedId);
                        $stmt->bindValue("id_feature_value",$genderSelection['id_feature_value']);
                        $stmt->execute();
                    }

                    //Jewelry Style
                    //$jewelryFeature = $this->getFeature(10);

                    $jewelrySelection = $this->getFeatureSelection(9,$insertProduct['subclass_id']);

                    if($jewelrySelection) {
                        $featureSql = "INSERT INTO ps_feature_product (id_feature, id_product, id_feature_value)
                                              VALUES (:id_feature,:id_product,:id_feature_value)";
                        $stmt = $conn->prepare($featureSql);
                        $stmt->bindValue("id_feature",9);
                        $stmt->bindValue("id_product",$lastInsertedId);
                        $stmt->bindValue("id_feature_value",$jewelrySelection['id_feature_value']);
                        $stmt->execute();
                    }

                    //Metal Purity
                    $metalPuritySelection = $this->getFeatureSelection(10,$karat_purity);
                    if($metalPuritySelection) {
                        $featureSql = "INSERT INTO ps_feature_product (id_feature, id_product, id_feature_value)
                                              VALUES (:id_feature,:id_product,:id_feature_value)";
                        $stmt = $conn->prepare($featureSql);
                        $stmt->bindValue("id_feature",10);
                        $stmt->bindValue("id_product",$lastInsertedId);
                        $stmt->bindValue("id_feature_value",$metalPuritySelection['id_feature_value']);
                        $stmt->execute();
                    }

                    //Metal Type
                    $metalTypeSelection = $this->getFeatureSelection(11,$metal);
                    if($metalTypeSelection) {
                        $featureSql = "INSERT INTO ps_feature_product (id_feature, id_product, id_feature_value)
                                              VALUES (:id_feature,:id_product,:id_feature_value)";
                        $stmt = $conn->prepare($featureSql);
                        $stmt->bindValue("id_feature",11);
                        $stmt->bindValue("id_product",$lastInsertedId);
                        $stmt->bindValue("id_feature_value",$metalTypeSelection['id_feature_value']);
                        $stmt->execute();
                    }

                    //Primary Gem
                    $primaryGemSelection = $this->getFeatureSelection(14,$insertProduct['primary_gem']);
                    if($insertProduct['primary_gem'] != '') {
                        $featureSql = "INSERT INTO ps_feature_product (id_feature, id_product, id_feature_value)
                                              VALUES (:id_feature,:id_product,:id_feature_value)";
                        $stmt = $conn->prepare($featureSql);
                        $stmt->bindValue("id_feature",14);
                        $stmt->bindValue("id_product",$lastInsertedId);
                        $stmt->bindValue("id_feature_value",$primaryGemSelection['id_feature_value']);
                        $stmt->execute();
                    }

                    //Primary Gem Cut $primaryGemCut
                    $primaryGemCutSelection = $this->getFeatureSelection(15,$primaryGemCut);
                    if($primaryGemCut != "" && $primaryGemCutSelection['id_feature_value'] != '') {
                        $featureSql = "INSERT INTO ps_feature_product (id_feature, id_product, id_feature_value)
                                              VALUES (:id_feature,:id_product,:id_feature_value)";
                        $stmt = $conn->prepare($featureSql);
                        $stmt->bindValue("id_feature",15);
                        $stmt->bindValue("id_product",$lastInsertedId);
                        $stmt->bindValue("id_feature_value",$primaryGemCutSelection['id_feature_value']);
                        $stmt->execute();
                    }
                }



                $deleteSql = "DELETE FROM pawn_import WHERE id = :id";
                $stmt = $conn->prepare($deleteSql);
                $stmt->bindValue("id",$insertProduct['id']);
                $stmt->execute();
                $conn->commit();

            } catch (Exception $e) {
                $conn->rollBack();
            }
        }
    }

    protected function getFeatureSelection($feature,$value)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT b.*, a.* 
                      FROM `ps_feature_value` a 
                      LEFT JOIN `ps_feature_value_lang` b ON (b.`id_feature_value` = a.`id_feature_value` AND b.`id_lang` = 1) 
                      WHERE `id_feature` = :id_feature AND b.value LIKE :value  AND (a.custom = 0 OR a.custom IS NULL)";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("id_feature",$feature);
        $stmt->bindValue('value', '%'.$value.'%');
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    protected function getFeature($featureId)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT b.*, a.* 
                      FROM `ps_feature` a 
                      LEFT JOIN `ps_feature_lang` b ON (b.`id_feature` = a.`id_feature` AND b.`id_lang` = 1) 
                      WHERE b.id_feature = :id_feature";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("id_feature",$featureId);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    protected function getCategoryPosition($cat)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $manufactureSql = "select * from ps_category_product WHERE `id_category` = :cat ORDER BY position DESC LIMIT 1";
        $stmt = $conn->prepare($manufactureSql);
        $stmt->bindValue("cat",$cat);
        $stmt->execute();
        $result = $stmt->fetch();

        if($result) {
            return $result['position'];
        }

        return 1;
    }

    protected function getManufacturer($make)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $manufactureSql = "select id_manufacturer from ps_manufacturer WHERE `name` = :make LIMIT 1";
        $stmt = $conn->prepare($manufactureSql);
        $stmt->bindValue("make",strtoupper($make));
        $stmt->execute();
        $result = $stmt->fetch();

        if($result) {
            return $result['id_manufacturer'];
        }

        return 1;
    }

    protected function getRingStyle($subClassId, $ret = 'val')
    {
        if(!$subClassId) {
            return '';
        }
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT SQL_CALC_FOUND_ROWS b.*, a.* 
		FROM `ps_feature_value` a 
		LEFT JOIN `ps_feature_value_lang` b ON (b.`id_feature_value` = a.`id_feature_value` AND b.`id_lang` = 1) 
		WHERE 1 AND `id_feature` = 9 AND value LIKE :subclass  ORDER BY `id_feature`";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("subclass","%".$subClassId."%'");
        $stmt->execute();
        $style = $stmt->fetch(PDO::FETCH_ASSOC);

        if(!$style) {
            return '';
        }

        if($ret != 'val') {
            return $style;
        }

        return $style['value'];
    }

    protected function getProductFeatures()
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT SQL_CALC_FOUND_ROWS b.*, a.* 
                FROM `ps_feature` a 
                LEFT JOIN `ps_feature_lang` b ON (b.`id_feature` = a.`id_feature` AND b.`id_lang` = 1) 
                WHERE a.id_feature > :cnt ORDER BY a.`position` ASC";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("cnt",7);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    protected function getLocation($location)
    {
        // MS SQL ID => PrestaShop ID
        $supplierList = [
            242 => 1,
            251 => 2,
            259 => 3,
            241 => 4,
            232 => 5,
            243 => 6,
            237 => 7,
            253 => 8,
            248 => 9,
            235 => 10,
            257 => 11,
            254 => 12,
            252 => 13,
            245 => 14,
            240 => 15,
            247 => 16,
            231 => 17,
            238 => 18,
            234 => 19,
            246 => 20,
            236 => 21,
            258 => 22,
            250 => 23,
            255 => 24,
            901 => 27,
            249 => 26,
            230 => 28
        ];

        return ( array_key_exists($location,$supplierList) ? $supplierList[$location] : 25);
    }

    protected function getCategoryDefault($category)
    {
        $categoryList = [
            'ACCORD' =>  23, 'ACCOUN' =>  24, 'ACTUAT' =>  25, 'ADAPTO' =>  26,
            'ADDING' =>  27, 'ADVERT' =>  28, 'AIRBRS' =>  29, 'AIRCHI' =>  30,
            'AIRCON' =>  31, 'AIRFIL' =>  32, 'AIRHOC' =>  33, 'AIRPUR' =>  34,
            'AIRWRN' =>  35, 'ALARM' =>  36, 'ALBUM' =>  37, 'AMMETE' =>  38,
            'AMPLIF' =>  39, 'ANCHOR' =>  40, 'ANGLE' =>  41, 'ANIMAT' =>  42,
            'ANSWER' =>  43, 'ANTENN' =>  44, 'APNMON' =>  45, 'APPL' =>  3,
            'APUMP' =>  46, 'ARCADE' =>  47, 'ARTGLA' =>  48, 'ARTPOT' =>  49,
            'ASSORT' =>  50, 'ATAPE' =>  51, 'ATV' =>  52, 'AUDIO' =>  4,
            'AUDIOM' =>  53, 'AUGER' =>  54, 'AUTO' =>  5, 'AUTOHA' =>  55,
            'BALL' =>  56, 'BANJO' =>  57, 'BARCOD' =>  58, 'BARITO' =>  59,
            'BASS' =>  60, 'BATCHG' =>  61, 'BBALL' =>  62, 'BBGUN' =>  63,
            'BEANIE' =>  64, 'BED' =>  65, 'BELLS' =>  66, 'BENCH' =>  67,
            'BGAME' =>  68, 'BGLOVE' =>  69, 'BICYCL' =>  70, 'BIKE' =>  6,
            'BINOCU' =>  71, 'BIT' =>  72, 'BLADE' =>  73, 'BLENDE' =>  74,
            'BOCCE' =>  75, 'BODY' =>  76, 'BOLTCU' =>  77, 'BOOK' =>  78,
            'BOOKCA' =>  79, 'BOW' =>  80, 'BPACK' =>  81, 'BPART' =>  82,
            'BRACEL' =>  83, 'BRAKE' =>  84, 'BRDMAC' =>  85, 'BREWER' =>  86,
            'BSCALE' =>  87, 'BTRAIL' =>  88, 'BUFFER' =>  89, 'BUGLIG' =>  90,
            'BULLIO' =>  91, 'CABINE' =>  92, 'CABLE' =>  93, 'CABLEC' =>  94,
            'CABLES' =>  95, 'CALCUL' =>  96, 'CALIPE' =>  97, 'CAM' =>  7,
            'CAMCOR' =>  98, 'CAMERA' =>  99, 'CAMKIT' =>  100, 'CAMPIN' =>  101,
            'CANDLE' =>  102, 'CART' =>  103, 'CARTRI' =>  104, 'CASE' =>  105,
            'CASHRE' =>  106, 'CBRADI' =>  107, 'CD RW' =>  108, 'CDISC' =>  109,
            'CDPLAY' =>  110, 'CDREC' =>  111, 'CDRECO' =>  112, 'CDROMD' =>  113,
            'CELLO' =>  114, 'CELTEL' =>  115, 'CHAINS' =>  116, 'CHAIR' =>  117,
            'CHANGE' =>  118, 'CHARGE' =>  119, 'CHARM' =>  120, 'CHESS' =>  121,
            'CHINA' =>  122, 'CHISEL' =>  123, 'CHLDST' =>  124, 'CIRCUI' =>  125,
            'CLAMPS' =>  126, 'CLARIN' =>  127, 'CLIMB' =>  128, 'CLIPPE' =>  129,
            'CLOCK' =>  130, 'COFFEE' =>  131, 'COIN' =>  8, 'COMBO' =>  133,
            'COMIC' =>  134, 'COMM' =>  9, 'COMP' =>  10, 'COMPAS' =>  135,
            'COMPRE' =>  136, 'COMPUT' =>  137, 'CONDEN' =>  138, 'CONSOL' =>  139,
            'CONTRO' =>  140, 'CONVEC' =>  141, 'COOKWA' =>  142, 'COOLER' =>  143,
            'COPIER' =>  144, 'CORNET' =>  145, 'CPU' =>  146, 'CREEPE' =>  147,
            'CROQUE' =>  148, 'CROSSO' =>  149, 'CRT' =>  150, 'CRYSTA' =>  151,
            'CSTERE' =>  152, 'CSTOVE' =>  153, 'CUFFLN' =>  154, 'CUTGLA' =>  155,
            'CYMBAL' =>  156, 'DART' =>  157, 'DECOY' =>  158, 'DEHUMI' =>  159,
            'DELAY' =>  160, 'DEPTHF' =>  161, 'DESK' =>  162, 'DETECT' =>  163,
            'DIAGSC' =>  164, 'DICTAT' =>  165, 'DIESET' =>  166, 'DIGCAM' =>  167,
            'DIGIT' =>  168, 'DIGREC' =>  169, 'DIRECT' =>  170, 'DISHWA' =>  171,
            'DISK' =>  172, 'DISNEY' =>  173, 'DOCKIN' =>  174, 'DOCSCN' =>  175,
            'DOLL' =>  176, 'DPLCTR' =>  177, 'DRILL' =>  178,
            'DRILLP' =>  179, 'DRIVES' =>  180, 'DRUM' =>  181, 'DRUMMA' =>  182,
            'DVDTV' =>  183, 'DVDVCR' =>  184, 'EARRIN' =>  185, 'ECYCLE' =>  186,
            'EDGER' =>  187, 'ELCDCT' =>  188, 'ELCENC' =>  189, 'ELCORG' =>  190,
            'ELGAME' =>  191, 'ENGRAV' =>  192, 'ENHANC' =>  193, 'ENLARG' =>  194,
            'EQUALI' =>  195, 'EXDATA' =>  196, 'EXONUM' =>  197, 'EXTEND' =>  198,
            'FAN' =>  199, 'FAX' =>  200, 'FAXMOD' =>  201, 'FCOIN' =>  202,
            'FDMXR' =>  203, 'FIGURI' =>  204, 'FILE' =>  205, 'FILECA' =>  206,
            'FILTER' =>  207, 'FINISH' =>  208, 'FISHIN' =>  209, 'FKNIFE' =>  210,
            'FLASH' =>  211, 'FLASHL' =>  212, 'FLATWA' =>  213, 'FLUTE' =>  214,
            'FMCONV' =>  215, 'FNOTE' =>  216, 'FOODPR' =>  217, 'FOOSBA' =>  218,
            'FRAME' =>  219, 'FREEZE' =>  220, 'FRYER' =>  221, 'FURNAC' =>  222,
            'GAME' =>  11, 'GBAG' =>  223, 'GBALL' =>  224, 'GEM' =>  225,
            'GENERA' =>  226, 'GLASSW' =>  227, 'GLOVE' =>  228, 'GOGGLE' =>  229,
            'GOLFCA' =>  230, 'GOLFCL' =>  231, 'GPOSYS' =>  232, 'GPS' =>  233,
            'GRILL' =>  234, 'GRINDB' =>  235, 'GRINDE' =>  236, 'GUITAR' =>  237,
            'HAMMER' =>  238, 'HANDCO' =>  239, 'HANDPC' =>  240, 'HANDTR' =>  241,
            'HARMON' =>  242, 'HARP' =>  243, 'HEADSE' =>  244, 'HEATPU' =>  245,
            'HEDGEC' =>  246, 'HELMET' =>  247, 'HISTOR' =>  248, 'HKNIFE' =>  249,
            'HNDMXR' =>  250, 'HOE' =>  251, 'HOMGYM' =>  252, 'HOOD' =>  253,
            'HOUSE' =>  12, 'HSHOES' =>  254, 'HTOOL' =>  13, 'HUBCAP' =>  255,
            'HUMIDI' =>  256, 'ICEHOU' =>  257, 'ICEMAK' =>  258, 'ICREMA' =>  259,
            'INRAFT' =>  260, 'INSTR' =>  14, 'INTERC' =>  261, 'INTERF' =>  262,
            'IRON' =>  263, 'ISKATE' =>  264, 'JACK' =>  265, 'JEWEL' =>  15,
            'JOINTE' =>  266, 'JUICER' =>  267, 'JUKEBO' =>  268, 'KARAOK' =>  269,
            'KAYAK' =>  270, 'KEEP' =>  16, 'KEYBOA' =>  271, 'KEYPAD' =>  272, 'KNIFE' =>  273,
            'LABELE' =>  274, 'LADDER' =>  275, 'LAMINA' =>  276, 'LAMP' =>  277, 'LANTER' =>  278,
            'LAPTOP' =>  279, 'LASGUN' =>  280, 'LATHE' =>  281, 'LAWN' =>  17,
            'LEAFBL' =>  282, 'LENS' =>  283, 'LEVEL' =>  284, 'LIFEPR' =>  285,
            'LIFT' =>  286, 'LIGHTM' =>  287, 'LIGHTP' =>  288, 'LINEFI' =>  289,
            'LOCK' =>  290, 'LOCKSM' =>  291, 'LOGSPL' =>  292, 'LORAN' =>  293,
            'LUGRAC' =>  294, 'MAILIN' =>  295, 'MANDOL' =>  296, 'MARIN' =>  18,
            'MCYCLE' =>  297, 'MEATSL' =>  298, 'MEMORY' =>  299, 'MICROM' =>  300,
            'MICROP' =>  301, 'MICROS' =>  302, 'MILITA' =>  303, 'MIRROR' =>  304,
            'MITERB' =>  305, 'MLTPRS' =>  306, 'MODEL' =>  307, 'MODEM' =>  308,
            'MONITO' =>  309, 'MOUSE' =>  310, 'MOWER' =>  311, 'MP3' =>  312,
            'MSCOOT' =>  313, 'NAILGU' =>  314, 'NAVIGA' =>  315, 'NECKLA' =>  316,
            'NEONSN' =>  317, 'NOISER' =>  318, 'OARS' =>  319, 'OBOE' =>  320,
            'OFF' =>  19, 'OHMMET' =>  321, 'OPENER' =>  322, 'ORGAN' =>  323,
            'OSCILL' =>  324, 'OTHER' =>  325, 'OUTBRD' =>  326, 'OVEN' =>  327,
            'PAGER' =>  328, 'PAINTI' =>  329, 'PAPERW' =>  330, 'PATCHB' =>  331,
            'PBALL' =>  332, 'PCUE' =>  333, 'PEDAL' =>  334, 'PEDOME' =>  335,
            'PENDAN' =>  336, 'PERISC' =>  337, 'PHONOG' =>  338, 'PIANO' =>  339,
            'PICCOL' =>  340, 'PIN' =>  341, 'PIPETH' =>  342, 'PLANE' =>  343,
            'PLANER' =>  344, 'PLATE' =>  345, 'PLIERS' =>  346, 'POLISH' =>  347,
            'POOLTA' =>  348, 'POPCOR' =>  349, 'POST' =>  350, 'POSTAG' =>  351,
            'POSTIN' =>  352, 'POWERB' =>  353, 'POWERP' =>  354, 'POWERS' =>  355,
            'PREAMP' =>  356, 'PRINT' =>  357, 'PRINTE' =>  358, 'PROCES' =>  359,
            'PROJEC' =>  360, 'PROLL' =>  361, 'PROPEL' =>  362, 'PRRAKE' =>  363,
            'PSTRPR' =>  364, 'PSWEEP' =>  365, 'PTOOL' =>  20, 'PURIFI' =>  366,
            'RACK' =>  367, 'RADICD' =>  368, 'RADIO' =>  369, 'RADIOR' =>  370,
            'RADITA' =>  371, 'RAKE' =>  372, 'RAMP' =>  373, 'RANGE' =>  374,
            'RANGEF' =>  375, 'RATCHE' =>  376, 'RCBOAT' =>  377, 'RCCAR' =>  378,
            'RCPLNE' =>  379, 'RCTRCK' =>  380, 'READER' =>  381, 'RECEIV' =>  382,
            'RECORD' =>  383, 'REEL' =>  384, 'REFRIG' =>  385, 'REMOTE' =>  386,
            'RING' =>  387, 'RIVETG' =>  388, 'RMOWER' =>  389, 'ROAST' =>  390,
            'ROCKET' =>  391, 'ROLBLD' =>  392, 'ROLLER' =>  393, 'ROTARY' =>  394,
            'ROUTER' =>  395, 'RSKATE' =>  396, 'RSNOWB' =>  397, 'SADDLE' =>  398,
            'SAFE' =>  399, 'SANDER' =>  400, 'SANDSL' =>  401, 'SAW' =>  402,
            'SAWZAL' =>  403, 'SAXOPH' =>  404, 'SBAG' =>  405, 'SBOARD' =>  406,
            'SCANNE' =>  407, 'SCARDS' =>  408, 'SCISSO' =>  409, 'SCOOT' =>  410,
            'SCOPE' =>  411, 'SCRAP' =>  412, 'SCREEN' =>  413, 'SCREWD' =>  414,
            'SCREWG' =>  415, 'SCUBA' =>  416, 'SECURI' =>  417, 'SENSOR' =>  418,
            'SEWING' =>  419, 'SHAMPO' =>  420, 'SHARPE' =>  421, 'SHEARS' =>  422,
            'SHOVEL' =>  423, 'SHREDD' =>  424, 'SILVER' =>  425, 'SKATEB' =>  426,
            'SKBOOT' =>  427, 'SKIMAC' =>  428, 'SKIS' =>  429, 'SLANTB' =>  430,
            'SLICER' =>  431, 'SLOTMA' =>  432, 'SMOKED' =>  433, 'SNAKE' =>  434,
            'SNOWBL' =>  435, 'SNOWMO' =>  436, 'SOCKET' =>  437, 'SODCUT' =>  438,
            'SOLDER' =>  439, 'SONAR' =>  440, 'SOUNDM' =>  441, 'SPACEH' =>  442,
            'SPEAKE' =>  443, 'SPEECH' =>  444, 'SPEKRS' =>  445, 'SPINWR' =>  446,
            'SPORT' =>  21, 'SPRAYE' =>  447, 'SPREAD' =>  448, 'SPRINK' =>  449,
            'SSHOE' =>  450, 'STAING' =>  451, 'STAMP' =>  452, 'STAND' =>  453,
            'STAPLE' =>  454, 'STEAMC' =>  455, 'STEREO' =>  456, 'STUDGU' =>  457,
            'SUB' =>  458, 'SURFBO' =>  459, 'SWITCH' =>  460, 'SWORD' =>  461,
            'SYNTHE' =>  462, 'TABLE' =>  463, 'TACKLE' =>  464, 'TAPDIE' =>  465,
            'TAPEDE' =>  466, 'TAPEDI' =>  467, 'TAPEME' =>  468, 'TAPEPL' =>  469,
            'TAPERE' =>  470, 'TATOO' =>  471, 'TELEPH' =>  472, 'TELETY' =>  473,
            'TELEVI' =>  474, 'TELVCR' =>  475, 'TENNIS' =>  476,'TENT' =>  477,
            'THERMO' =>  478, 'THERMS' =>  479, 'THESYS' =>  480, 'TIETAC' =>  481,
            'TILLER' =>  482, 'TIMECL' =>  483, 'TIMING' =>  484, 'TIRE' =>  485,
            'TK' =>  486, 'TOASTE' =>  487, 'TOBAC' =>  488, 'TONOME' =>  489,
            'TOOLBO' =>  490, 'TOOLSE' =>  491, 'TORCH' =>  492, 'TRAILE' =>  493,
            'TRANSM' =>  494, 'TREADM' =>  495, 'TREEST' =>  496, 'TRENCH' =>  497,
            'TRIMME' =>  498, 'TRIPOD' =>  499, 'TROLL' =>  500, 'TROMBO' =>  501,
            'TROWEL' =>  502, 'TRUMPE' =>  503, 'TSTOVN' =>  504, 'TUNER' =>  505,
            'TURNTA' =>  506, 'TVGAME' =>  507, 'TVREC' =>  508, 'TYPEWR' =>  509,
            'UKULEL' =>  510, 'USCOIN' =>  511, 'USNOTE' =>  512, 'VACATT' =>  513,
            'VACUUM' =>  514, 'VASE' =>  515, 'VCONVE' =>  516, 'VCRWN' =>  517,
            'VDISK' =>  518, 'VESCTO' =>  519, 'VIDEO' =>  22, 'VIDEOC' =>  520,
            'VIDEOD' =>  521, 'VIDEOR' =>  522, 'VIEWER' =>  523, 'VIOLA' =>  524,
            'VIOLIN' =>  525, 'VISE' =>  526, 'VMULTI' =>  527, 'VOICEC' =>  528,
            'VOICEW' =>  529, 'VOLLEY' =>  530, 'VOLTME' =>  531, 'VPROJ' =>  532,
            'VRGLAS' =>  533, 'VTAPE' =>  534, 'WADERS' =>  535, 'WALKIE' =>  536,
            'WASHER' =>  537, 'WATCH' =>  538, 'WATERB' =>  539, 'WATERH' =>  540,
            'WCOMP' =>  541, 'WEBCAM' =>  542, 'WEBTV' =>  543, 'WEEDER' =>  544,
            'WEIGHT' =>  545, 'WELDER' =>  546, 'WINDER' =>  547, 'WINDRK' =>  548,
            'WIREST' =>  549, 'WLIGHT' =>  550, 'WOOFER' =>  551, 'WRACK' =>  552,
            'WRENCH' =>  553, 'XYLOPH' =>  554];

        return ( array_key_exists($category,$categoryList) ? $categoryList[$category] : false);
    }
}