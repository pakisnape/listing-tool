<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class DisableProductWithoutImagesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:disable-products-without-images')

            // the short description shown while running "php bin/console list"
            ->setDescription('disable products without images')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("emails all active ebay products")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $products = $conn->fetchAll('SELECT * FROM ps_product WHERE active = 1');//
        $products = $conn->fetchAll('SELECT ps_product.id_product,ps_supplier.name,reference
                                        FROM ps_product 
                                        INNER join ps_product_shop ON ps_product_shop.id_product = ps_product.id_product 
                                        INNER join ps_supplier ON ps_supplier.id_supplier = ps_product.id_supplier
                                        WHERE ps_product_shop.price = 0 AND ps_product.active = 1');

        foreach ($products as $product) {
            if(!$this->getImages($product)){
                $this->updateProduct($product);
            }
        }
    }

    protected function getImages($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $imageSql = "SELECT * FROM ps_image WHERE id_product = :id_product";
        $stmt = $conn->prepare($imageSql);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return ( count($result) > 0 ? true : false);

    }

    protected function updateProduct($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $productSql = "UPDATE ps_product set active = :active WHERE id_product = :id_product";
        $stmt = $conn->prepare($productSql);
        $stmt->bindValue("active",0);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->execute();


        $ProductStockAvailableSql = "UPDATE ps_product_shop set active = :active WHERE id_product = :id_product";
        $stmt = $conn->prepare($ProductStockAvailableSql);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->bindValue("active",0);
        $stmt->execute();
    }
}