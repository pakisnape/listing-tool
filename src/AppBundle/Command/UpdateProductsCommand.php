<?php

namespace AppBundle\Command;

use AppBundle\Services\PrestaShopApiService;
use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\PrestaShopWebservice;
use AppBundle\Services\PrestaShopWebserviceException;


class UpdateProductsCommand extends ContainerAwareCommand
{
    /**
     * Debug setting
     */
    const DEBUG = false;

    /**
     * presta shop path
     */
    const PS_SHOP_PATH  = "https://www.pawnamerica.com/";

    /**
     * Auth key
     */
    const PS_WS_AUTH_KEY = 'GA2EKZHPBNTA4ULTEAYG8BTBR2ETKYA7';

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:update-products')

            // the short description shown while running "php bin/console list"
            ->setDescription('updates products.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for updated products.")
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //2016-11-18 08:00:02
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $productUpdates = $conn->fetchAll('SELECT * FROM product_update');
        /** @var PrestaShopApiService $apiService */
        $apiService = $this->getContainer()->get('pawn.apiservice');

//        $tmp = 2;//245070476;//$psProduct['id_product']
//
//        /** @var PrestaShopApiService $apiService */
//        $apiService = $this->getContainer()->get('pawn.apiservice');
//
//
//        //$this->getIdStockAvailableAndSet($tmp);
//        $apiService->setProductActive($tmp,false,79.94);//79.94,5
//
//        //$this->setProductActive($tmp,true);
//        //$this->setProductActive($tmp,false);



        foreach($productUpdates as $updatedProduct) {
            $sql = "SELECT * FROM ps_product WHERE reference = :reference limit 1";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('reference',$updatedProduct["reference"]);
            $stmt->execute();
            $result = $stmt->fetchAll();

            $location = $this->getLocation($updatedProduct['location']);

            if(count($result) < 1) {
                $deleteSql = "DELETE FROM product_update WHERE id = :id";
                $stmt = $conn->prepare($deleteSql);
                $stmt->bindValue("id",$updatedProduct['id']);
                $stmt->execute();
                continue;
            }
            $product = $result[0];
            $conn->beginTransaction();
            try {

                $prodSupplier = $apiService->getSupplier($product['id_product']);
                $apiService->setSupplier($prodSupplier,$product['id_product'],$location);

                // marks product active false and updates price
                $apiService->setProductActive($product['id_product'],false,$updatedProduct['price']);

                //sets qty to 1
                $apiService->getIdStockAvailableAndSet($product['id_product'],1);


                $exclude = ['900','901','902'];

                if($this->getImages($product) && !in_array($updatedProduct['location'],$exclude)){
                    $apiService->setProductActive($product['id_product'],true);
                }

                $deleteSql = "DELETE FROM product_update WHERE id = :id";
                $stmt = $conn->prepare($deleteSql);
                $stmt->bindValue("id",$updatedProduct['id']);
                $stmt->execute();

                $conn->commit();
            } catch (PrestaShopWebserviceException $ex) {
                $this->sendError($ex->getMessage(),$updatedProduct["reference"]);
                echo "\n Error:".$ex->getMessage().'\n';
                $conn->rollBack();
            }
        }
    }

    /**
     * @param string $msg
     */
    protected function sendError($msg,$reference)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Update Product Error ' .$reference)
            ->setFrom('support@pawnamerica.com')
            ->addTo('ndahl@nerdery.com')
            ->addTo('dave.willmott@pawnamerica.com')
            ->setBody($msg);
        //->setBcc([,'todd.hyland@cashpass.com','web.store11@pawnamerica.com'])


        $this->getContainer()->get('mailer')->send($message);
    }

    /**
     * @param int $ProductId
     * @param $qty
     */
    protected function getIdStockAvailableAndSet($ProductId = 235935,$qty){
        $webService = new PrestaShopWebservice(self::PS_SHOP_PATH,self::PS_WS_AUTH_KEY,self::DEBUG);
        $opt['resource'] = 'products';
        //$opt['filter']['reference'] = '237063472';//328102
        $opt['id'] = $ProductId;
        //print_r($opt);
        $xml = $webService->get($opt);

        foreach ($xml->product->associations->stock_availables->stock_available as $item) {
            //echo "ID: ".$item->id."\n\r";
            //echo "Id Attribute: ".$item->id_product_attribute."\n\r";
            $this->set_product_quantity($ProductId, $item->id, $item->id_product_attribute,$qty);
        }
    }

    /**
     * @param $ProductId
     * @param $StokId
     * @param $AttributeId
     */
    protected function set_product_quantity($ProductId, $StokId, $AttributeId,$qty =0){
        $webService = new PrestaShopWebservice(self::PS_SHOP_PATH,self::PS_WS_AUTH_KEY,self::DEBUG);
        $xml = $webService->get(array('url' => self::PS_SHOP_PATH . '/api/stock_availables?schema=blank'));
        $resources = $xml->children()->children();
        $resources->id = $StokId;
        $resources->id_product  = $ProductId;
        $resources->quantity = $qty;
        $resources->id_shop = 1;
        $resources->out_of_stock=1;
        $resources->depends_on_stock = 0;
        $resources->id_product_attribute=$AttributeId;
        try {
            $opt = array('resource' => 'stock_availables');
            $opt['putXml'] = $xml->asXML();
            $opt['id'] = $StokId ;
            $xml = $webService->edit($opt);
        }catch (PrestaShopWebserviceException $ex) {
            echo "<b>Error   ->Error : </b>".$ex->getMessage().'<br>';
        }
    }

    /**
     * @param $ProductId
     * @param $status
     */
    protected function setProductActive($ProductId, $status, $price)
    {
        $webService = new PrestaShopWebservice(self::PS_SHOP_PATH,self::PS_WS_AUTH_KEY,self::DEBUG);
        $xml = $webService->get(array('resource' => 'products', 'id' => $ProductId));
        $resources = $xml->children()->children();

        // Unset fields that may not be updated
        unset($resources->manufacturer_name);
        unset($resources->quantity);

        $resources->id = $ProductId;
        $resources->active = $status;
        $resources->price = $price;
        //echo $xml->asXML();die;
        try {
            $opt = array('resource' => 'products');
            $opt['putXml'] = $xml->asXML();
            $opt['id'] = $ProductId ;
            $xml = $webService->edit($opt);
        }catch (PrestaShopWebserviceException $ex) {
            echo "<bhow>Error   ->Error : </bhow>".$ex->getMessage().'<br>';
        }
    }

    protected function getSupplier($productId)
    {
        $supplier = null;
        $webService = new PrestaShopWebservice(self::PS_SHOP_PATH,self::PS_WS_AUTH_KEY,self::DEBUG);

        $opt['resource'] = 'product_suppliers';
        $opt['filter']['id_product'] = $productId;//328102

        $xml = $webService->get($opt);
        $resources = $xml->children()->children();

        foreach($resources->product_supplier->attributes() as $key => $sup) {
            $supplier = $sup;
        }

        return $supplier;
    }

    protected function setSupplier($prodSupplier,$productId,$supplier)
    {
        $webService = new PrestaShopWebservice(self::PS_SHOP_PATH,self::PS_WS_AUTH_KEY,self::DEBUG);
        $xml = $webService->get(array('resource' => 'product_suppliers', 'id' => $prodSupplier));
        $resources = $xml->children()->children();
        $resources->id = $prodSupplier;
        $resources->id_product = $productId;
        $resources->id_supplier = $supplier;
        try {
            $opt = array('resource' => 'product_suppliers');
            $opt['putXml'] = $xml->asXML();
            $opt['id'] = $prodSupplier ;
            $xml = $webService->edit($opt);
        }catch (PrestaShopWebserviceException $ex) {
            echo $ex->getMessage();
        }
    }

    /**
     * @param $product
     */
    protected function activateProduct($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $productSql = "UPDATE ps_product set active = :active WHERE id_product = :id_product";
        $stmt = $conn->prepare($productSql);
        $stmt->bindValue("active",1);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->bindValue("quantity",1);
        $stmt->execute();

        $productShopSql = "UPDATE ps_product_shop SET active = :active WHERE id_product = :product ";
        $stmt = $conn->prepare($productShopSql);
        $stmt->bindValue("active",1);
        $stmt->bindValue("product",$product['id_product']);
        $stmt->execute();
    }


    /**
     * @param $product
     * @return bool
     */
    protected function getImages($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $imageSql = "SELECT * FROM ps_image WHERE id_product = :id_product";
        $stmt = $conn->prepare($imageSql);
        $stmt->bindValue("id_product",$product['id_product']);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return ( count($result) > 0 ? true : false);

    }

    /**
     * @param $location
     * @return int|mixed
     */
    protected function getLocation($location)
    {
        // MS SQL ID => PrestaShop ID
        $supplierList = [
            242 => 1,
            251 => 2,
            259 => 3,
            241 => 4,
            232 => 5,
            243 => 6,
            237 => 7,
            253 => 8,
            248 => 9,
            235 => 10,
            257 => 11,
            254 => 12,
            252 => 13,
            245 => 14,
            240 => 15,
            247 => 16,
            231 => 17,
            238 => 18,
            234 => 19,
            246 => 20,
            236 => 21,
            258 => 22,
            250 => 23,
            255 => 24,
            901 => 27,
            249 => 26,
            230 => 28,
            900 => 25
        ];

        return ( array_key_exists($location,$supplierList) ? $supplierList[$location] : 25);
    }
}