<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use PHPExcel_IOFactory;
use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Swift_Attachment;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;


class SalesReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:sales-report')

            // the short description shown while running "php bin/console list"
            ->setDescription('Sales report.')

            ->setDefinition(
                new InputDefinition([
                    new InputOption('monthly','m',InputOption::VALUE_OPTIONAL)
                ])
            )

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for updated products from an excel file.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $monthly = $input->getOption('monthly', 0);

        $suppliers = $this->getStores();
        $currentOrders = $this->getOrders($monthly);
        $returns = $this->getReturns($monthly);

        $orderedProducts = $this->getOrderedProducts($currentOrders,$returns);

        $sendToStore = true;
        $this->sendSalesReport($suppliers, $orderedProducts, $monthly, $sendToStore);

    }

    /**
     * @param $suppliers
     * @param $orders
     * @param $monthly
     * @param bool $sendToStore
     */
    protected function sendSalesReport($suppliers,$orders,$monthly,$sendToStore)
    {
        $excelAll = $this->buildExcelAllStores($orders);
        $this->sendFullSalesReport($excelAll);
        if ($sendToStore)
        {
            foreach($orders as $key => $product) {
                if ($key == 5) {
                    continue;
                }
                $subject = ($monthly ? 'Monthly': 'Daily');
                $excel = $this->buildExcel($product,$key,$subject);
                $summary = $this->getSummary($key,$product);
                $message = \Swift_Message::newInstance()
                    ->setSubject($subject.' Pawn America Sales Report - New')
                    ->setFrom('support@pawnamerica.com')
                    ->addTo('ndahl@nerdery.com')
                    ->addTo('steve.caulfield@pawnamerica.com')
                    ->addTo('dave.willmott@pawnamerica.com')
                    ->addTo('todd.hyland@cashpass.com')
                    ->addTo('keith.kaestner@pawnamerica.com')
                    ->addTo('matt.johnson@pawnamerica.com')
                    ->addTo('james.slarks@pawnamerica.com')
                    ->addTo('david.neilitz@pawnamerica.com')
                    ->addTo('dan.kealey@pawnamerica.com')
                    ->addTo('laura.regnier@pawnamerica.com')
                    ->addTo($this->getStoreEmail($key))
                    ->addTo($this->getMgrEmail($key))
                    ->attach(Swift_Attachment::fromPath($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $excel))
                    //->setBcc([,'todd.hyland@cashpass.com','web.store11@pawnamerica.com'])
                    ->setBody(
                        $this->getContainer()->get('templating')->render('email/salesReport.html.twig',
                            [
                                'products' => $product,
                                'order' => $orders[$key],
                                'store' => $suppliers[$key],
                                'subject' => $subject,
                                'websiteSales' => $summary['website'],
                                'ebaySales' => $summary['ebay'],
                                'activeWeb' => $summary['activeItems'],
                                'activeEbay' => $summary['activeEbayItems']
                            ]
                        ),
                        'text/html'
                    );

                $this->getContainer()->get('mailer')->send($message);
            }
        }

    }

    /**
     * @param $excelAll
     */
    protected function sendFullSalesReport($excelAll)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Pawn America All Sales Report')
            ->setFrom('support@pawnamerica.com')
            ->addTo('ndahl@nerdery.com')
            ->addTo('support@pawnamerica.com')
            ->attach(Swift_Attachment::fromPath($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $excelAll));
            //->setBcc([,'todd.hyland@cashpass.com','web.store11@pawnamerica.com'])


        $this->getContainer()->get('mailer')->send($message);
    }

    /**
     * @param $key
     * @param $products
     * @return array
     */
    protected function getSummary($key,$products)
    {
        $active = $this->getActiveItemsBySupplier($key);
        $ebayTotal = 0;
        $websiteTotal = 0;
        foreach($products as $product) {
            if($product['paymentType'] == "fastbay1") {
                if($product['status'] == "Refunded") {
                    $ebayTotal = $ebayTotal - $product['total'];
                    continue;
                }
                $ebayTotal = $ebayTotal + $product['total'];
                continue;
            }
            if($product['status'] == "Refunded") {
                $websiteTotal = $websiteTotal - $product['total'];
                continue;
            }
            $websiteTotal = $websiteTotal + $product['total'];
        }

        return [
            'ebay' => $ebayTotal,
            'website' => $websiteTotal,
            'activeItems' => $active['website'],
            'activeEbayItems' => $active['ebay'],
        ];
    }

    /**
     * @param $products
     * @param $supplierId
     * @param $subject
     * @return string
     */
    protected function buildExcel($products,$supplierId,$subject)
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Order Reference');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Payment');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Order Status');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Customer Name');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Shipping Total');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Order Total');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Sold Date');

        $rowCount =2;
        foreach ($products as $product) {
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $product['order_reference']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $product['payment']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $product['status']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $product['customer']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, round($product['shippingCost'],2));
            $total = $product['shippingCost'] + $product['total'];
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, round($total,2));
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $product['soldDate']);
            $rowCount++;
        }

        $now = date('Y-m');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $fileName = "salesReport-".$supplierId."-".$subject. $now .".xlsx";

        $objWriter->save($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $fileName);

        return $fileName;
    }

    /**
     * @param $orders
     * @return string
     */
    protected function buildExcelAllStores($orders)
    {
        $tmpOrders = [];
        foreach($orders as $key => $products) {
            foreach ($products as $product) {
                $tmpOrders[] = $product;
            }
        }

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Order Reference');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Payment');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Order Status');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Customer Name');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Shipping Total');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Order Total');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Sold Date');

        $rowCount =2;
        foreach ($tmpOrders as $product) {
            $tmp = explode('-',$product['order_reference']);
            $reference = $this->prestshopToMicros($product['store'])."-".$tmp[1];
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $reference);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $product['payment']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $product['status']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $product['customer']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, round($product['shippingCost'],2));
            $total = $product['shippingCost'] + $product['total'];
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, round($total,2));
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $product['soldDate']);
            $rowCount++;
        }

        $now = date('Y-m');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $fileName = "salesReportAll-". $now .".xlsx";

        $objWriter->save($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $fileName);

        return $fileName;

    }

    protected function getOrders($monthly)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $start = date("2017-04-27 22:00:00",strtotime("-1 days"));
        $end = date("2017-04-27 22:00:00");
        $start = date("Y-m-d H:i:s",strtotime("-1 days"));
        $end = date("Y-m-d H:i:s");
        if ($monthly) {
            $now = date('Y-m-d', strtotime("first day of -1 month"));
            //$now = date('Y-m-d', strtotime("first day of this month"));
        }
        $orders = [];

        $sql = "SELECT ps_orders.date_add,ps_orders.total_shipping_tax_incl, ps_orders.id_order,payment,module,
                        ps_orders.reference,ps_customer.firstname,ps_customer.lastname, ps_order_state_lang.name,id_address_invoice
                        FROM ps_orders 
                         INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer 
                         INNER JOIN ps_order_state_lang ON ps_order_state_lang.id_order_state = ps_orders.current_state                        
                         WHERE ps_orders.invoice_date >= :now AND ps_orders.invoice_date < :end";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('now',$start);
        $stmt->bindValue('end',$end);
        $stmt->execute();
        $data = $stmt->fetchAll();
        foreach($data as $orderData) {
            $orders[$orderData['id_order']] = $orderData;
        }

        return $orders;
    }

    protected function getReturns($monthly)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $start = date("Y-m-d H:i:s",strtotime("-1 days"));
        $end = date("Y-m-d H:i:s");
        if ($monthly) {
            $now = date('Y-m-d', strtotime("first day of -1 month"));
            //$now = date('Y-m-d', strtotime("first day of this month"));
        }
        $orders = [];

        $sql = "SELECT   ps_orders.date_add,ps_orders.total_shipping_tax_incl, ps_orders.id_order,payment,module,
                        ps_orders.reference,ps_customer.firstname,ps_customer.lastname, ps_order_state_lang.name,id_address_invoice 
                         FROM ps_orders 
                         INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer 
                         INNER JOIN ps_order_state_lang ON ps_order_state_lang.id_order_state = ps_orders.current_state
                         WHERE ps_orders.date_upd >= :now AND ps_orders.date_upd < :end AND ps_orders.current_state = :state";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('now',$start);
        $stmt->bindValue('end',$end);
        $stmt->bindValue('state',7);
        $stmt->execute();
        $data = $stmt->fetchAll();

        foreach($data as $orderData) {
            $orders[$orderData['id_order']] = $orderData;
        }

        return $orders;
    }

    /**
     * @param $supplier
     * @return array
     */
    protected function getActiveItemsBySupplier($supplier)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT *  FROM ps_product 
                         INNER JOIN ps_product_shop ON ps_product_shop.id_product = ps_product.id_product
                         WHERE ps_product.id_supplier = :supplier AND ps_product_shop.active = :active";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('supplier',$supplier);
        $stmt->bindValue('active',1);
        $stmt->execute();
        $results = $stmt->fetchAll();

        $ebay =0;
        $site =0;

        foreach ($results as $result) {
            if ($this->isEbay($result)) {
                $ebay = $ebay + 1;
                continue;
            }
            $site = $site + 1;
        }

        return ['website' =>$site,'ebay' =>$ebay];
    }

    /**
     * @param $currentOrders
     * @return array
     */
    protected function getOrderedProducts($currentOrders,$returns)
    {
        $orders = [];

        foreach($currentOrders as $order) {
            $data = $this->getProducts($order);

            $stores = $this->getProductSuppliers($data);

            $shping = $order['total_shipping_tax_incl'];
            $shipping = [$shping];
            if(count($stores) > 1) {
                $shipping = $this->splitShipping(count($data),$order);
            }
            $customer = $this->getAddress($order['id_address_invoice']);
            $i =0;
            foreach($data as $product) {
                $orders[$product['id_supplier']][] = [
                        'order_id' => $order['id_order'],
                        'order_reference' => $order['reference'],
                        'payment' => $order['payment'],
                        'paymentType' => $order['module'],
                        'customer' => $customer['firstname'] . ' ' . $customer['lastname'],
                        'sold' => $order['date_add'],
                        'status' => $order['name'],
                        'total' => $product['total_price_tax_incl'],
                        'shippingCost' => (count($shipping) > 1 ? $shipping[$i] : $shipping[0]),
                        'soldDate' => $order['date_add'],
                        'store' => $product['id_supplier'],

                    ] + $product;
                $i++;
            }
        }

        foreach($returns as $order) {
            $data = $this->getProducts($order);

            $stores = $this->getProductSuppliers($data);

            $shping = $order['total_shipping_tax_incl'];
            $shipping = [$shping];
            if(count($stores) > 1) {
                $shipping = $this->splitShipping(count($data),$order);
            }
            $customer = $this->getAddress();
            $i =0;
            foreach($data as $product) {
                $orders[$product['id_supplier']][] = [
                        'order_id' => $order['id_order'],
                        'order_reference' => $order['reference'],
                        'customer' => $customer['firstname'] . ' ' . $customer['lastname'],
                        'sold' => $order['date_add'],
                        'paymentType' => $order['module'],
                        'payment' => $order['payment'],
                        'status' => $order['name'],
                        'total' => $product['total_price_tax_incl'],
                        'shippingCost' => (count($shipping) > 1 ? $shipping[$i] : $shipping[0]),
                        'soldDate' => $order['date_add']

                    ] + $product;
                $i++;
            }
        }

        return $orders;
    }

    protected function getProductSuppliers($data)
    {
        $suppliers = [];
        foreach($data as $product) {
            $suppliers[] = $product['id_supplier'];
        }

        return array_unique($suppliers);
    }

    /**
     * @param $order
     * @return array
     */
    protected function getProducts($order)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT product_name, product_reference, total_price_tax_incl, total_price_tax_excl,ps_product.id_supplier, 
                        ps_supplier.name as supplierName , product_price,id_order
                    FROM ps_order_detail
                    INNER JOIN ps_product ON ps_product.id_product = ps_order_detail.product_id 
                    INNER JOIN ps_supplier ON ps_supplier.id_supplier = ps_product.id_supplier
                    WHERE id_order=:id_order";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$order['id_order']);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * @param $stores
     * @param $order
     * @return array
     */
    protected function splitShipping($stores,$order)
    {
        $shippingCost = $order['total_shipping_tax_incl'];
        $shippingSplit = round($shippingCost / $stores,2);
        $tmpShippingTotal = $shippingSplit * $stores;
        $shippingOffset = $shippingCost - $tmpShippingTotal;
        $shippingArray = [];
        for($i =1; $i <= $stores; $i++) {
            if($i == $stores) {
                array_push($shippingArray,$shippingSplit + $shippingOffset);
            }
            array_push($shippingArray,$shippingSplit);
        }

        return $shippingArray;
    }

    /**
     * @return array
     */
    protected function getStores()
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $sql = "SELECT * FROM ps_supplier WHERE id_supplier != :supplier";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('supplier',25);
        $stmt->execute();

        $reportData = [];
        $suppliers = $stmt->fetchAll();
        foreach ($suppliers as $supplier) {
            $reportData[$supplier['id_supplier']] = [
                'name' => $supplier['name'],
            ];
        }

        return $reportData;
    }

    /**
     * @param $supplier
     * @return mixed
     */
    protected function getStoreEmail($supplier)
    {
        $storeEmails = [
            1 => 'pawnamerica-e242@pawnamerica.com',
            2 => 'pawnamerica-e251@pawnamerica.com',
            3 => 'pawnamerica-e259@pawnamerica.com',
            4 => 'pawnamerica-e241@pawnamerica.com',
            5 => 'pawnamerica-e232@pawnamerica.com',
            6 => 'pawnamerica-e243@pawnamerica.com',
            7 => 'pawnamerica-e237@pawnamerica.com',
            8 => 'pawnamerica-e253@pawnamerica.com',
            9 => 'pawnamerica-e248@pawnamerica.com',
            10 => 'pawnamerica-e235@pawnamerica.com',
            11 => 'pawnamerica-e257@pawnamerica.com',
            12 => 'pawnamerica-e254@pawnamerica.com',
            13 => 'pawnamerica-e252@pawnamerica.com',
            14 => 'pawnamerica-e245@pawnamerica.com',
            15 => 'pawnamerica-e240@pawnamerica.com',
            16 => 'pawnamerica-e247@pawnamerica.com',
            17 => 'pawnamerica-e231@pawnamerica.com',
            18 => 'pawnamerica-e238@pawnamerica.com',
            19 => 'pawnamerica-e234@pawnamerica.com',
            20 => 'pawnamerica-e246@pawnamerica.com',
            21 => 'pawnamerica-e236@pawnamerica.com',
            22 => 'pawnamerica-e258@pawnamerica.com',
            23 => 'pawnamerica-e250@pawnamerica.com',
            24 => 'pawnamerica-e255@pawnamerica.com',
            25 => 'marketplace@pawnamerica.com',
            26 => 'paul.hess@pawnamerica.com',
            28 => 'guest.service@everonesjewelryshop.com'
            //TODO check to see if 28 is 230
        ];

        return $storeEmails[$supplier];
    }

    /**
     * @param $supplier
     * @return mixed
     */
    protected function getMgrEmail($supplier)
    {
        $storeEmails = [
            1 => '4112@pawnamerica.com',
            2 => '4121@pawnamerica.com',
            3 => '4129@pawnamerica.com',
            4 => '4111@pawnamerica.com',
            5 => '4102@pawnamerica.com',
            6 => '4113@pawnamerica.com',
            7 => '4107@pawnamerica.com',
            8 => '4123@pawnamerica.com',
            9 => '4118@pawnamerica.com',
            10 => '4105@pawnamerica.com',
            11 => '4127@pawnamerica.com',
            12 => '4124@pawnamerica.com',
            13 => '4122@pawnamerica.com',
            14 => '4115@pawnamerica.com',
            15 => '4110@pawnamerica.com',
            16 => '4117@pawnamerica.com',
            17 => '4101@pawnamerica.com',
            18 => '4108@pawnamerica.com',
            19 => '4104@pawnamerica.com',
            20 => '4116@pawnamerica.com',
            21 => '4106@pawnamerica.com',
            22 => '4128@pawnamerica.com',
            23 => '4120@pawnamerica.com',
            24 => '4125@pawnamerica.com',
            25 => 'marketplace@pawnamerica.com',
            26 => 'paul.hess@pawnamerica.com',
            28 => 'guest.service@everonesjewelryshop.com'
        ];

        return $storeEmails[$supplier];
    }

    /**
     * @param $supplier
     * @return mixed
     */
    protected function prestshopToMicros($supplier)
    {
        $supplierListPresta = [
            1 => 242,
            2=> 251,
            3=> 259,
            4=> 241,
            5=> 232,
            6=> 243,
            7=> 237,
            8=> 253,
            9=> 248,
            10=> 235,
            11=> 257,
            12=> 254,
            13=> 252,
            14=> 245,
            15=> 240,
            16=> 247,
            17=> 231,
            18=> 238,
            19=> 234,
            20=> 246,
            21=>236,
            22=> 258,
            23=> 250,
            24=> 255,
            25 => 900
        ];

        return $supplierListPresta[$supplier];
    }

    /**
     * @param $id
     * @return mixed
     */
    protected function getAddress($id)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT lastname, firstname, address1, address2, city, postcode,iso_code,phone,phone_mobile FROM ps_address
                    INNER JOIN ps_state ON ps_address.id_state = ps_state.id_state WHERE id_address = :id_cart";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_cart',$id);
        $stmt->execute();
        return $stmt->fetch();
    }
}