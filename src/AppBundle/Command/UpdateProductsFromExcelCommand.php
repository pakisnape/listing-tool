<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use PHPExcel_IOFactory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class UpdateProductsFromExcelCommand  extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:update-products-excel')

            // the short description shown while running "php bin/console list"
            ->setDescription('updates products from an excel file.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for updated products from an excel file.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/Shipping_Complete.xlsx";

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //$objReader->setReadDataOnly(false);
        $objPHPExcel = $objReader->load($file);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        foreach ($objWorksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $data = [];
            foreach ($cellIterator as $cell) {
                $data[] = $cell->getValue();

            }
            $retData = $this->updateProduct($data);
            $output->writeln($retData);
        }
    }

    /**
     * @param array $data
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    private function updateProduct($data)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $reference = (int)$data[0];
        $sql = "SELECT * FROM ps_product WHERE reference= :reference";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('reference',$reference);
        $stmt->execute();
        $product = $stmt->fetchAll();

        //print_r($product);
        if (empty($product)) {
            return 'Product not found';
        }
        $productId = $product[0]['id_product'];
        if ($data[1] == '0' && $data[2] == '0' && $data[3] == '0' && $data[4] == '0') {
            return $reference .' Import data had 0 for width, height, weight and depth';
        }

        if ($product[0]['width'] != '0.000000' &&  $product[0]['height'] != '0.000000' && $product[0]['depth'] != '0.000000' && $product[0]['weight'] != '0.000000') {
            return $reference .' Product has values for width, height, weight and depth';
        }

        $conn->executeUpdate("UPDATE ps_product set width = ".$data[1].",height=".$data[2].",depth= ".$data[3].",weight= ".$data[4]." WHERE reference='".$reference."'");

        $suppliers = $conn->fetchAll("SELECT * FROM ps_product_carrier WHERE id_product = $productId");
        if (empty($suppliers)) {
            $conn->executeUpdate("INSERT INTO ps_product_carrier (id_product, id_carrier_reference, id_shop) VALUES (".$product[0]['id_product'].",1,1)");
            $conn->executeUpdate("INSERT INTO ps_product_carrier (id_product, id_carrier_reference, id_shop) VALUES (".$product[0]['id_product'].",2,1)");
            $conn->executeUpdate("INSERT INTO ps_product_carrier (id_product, id_carrier_reference, id_shop) VALUES (".$product[0]['id_product'].",10,1)");
            $conn->executeUpdate("INSERT INTO ps_product_carrier (id_product, id_carrier_reference, id_shop) VALUES (".$product[0]['id_product'].",11,1)");
            return $reference .' Product updated and shipping added';
        }
        return $reference .' Product updated and shipping does not need to be added';

    }
}