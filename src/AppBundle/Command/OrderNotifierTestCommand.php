<?php

namespace AppBundle\Command;

use AppBundle\Services\SendSale;
use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use Swift_Attachment;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class OrderNotifierTestCommand extends ContainerAwareCommand
{
    const folder = 'invoices/';

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:order-notifier-test')

            // the short description shown while running "php bin/console list"
            ->setDescription('sends emails for new orders.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for updated products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $now = date("Y-m-d");
        //$now = "2016-12-08";
        $currentOrders = [];


        $sql = "SELECT id_order, reference, ps_orders.id_carrier, ps_orders.id_customer, id_cart, total_paid, total_shipping_tax_excl, ps_orders.date_add,module,
                        total_paid_tax_incl,total_paid_tax_excl,`module`,total_shipping_tax_incl,payment,ps_orders.invoice_date,ps_orders.date_add,id_address_delivery,
                        firstname,lastname,email, ps_carrier.name as carrierName,id_address_invoice, total_products
                  FROM ps_orders 
                  INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer
                  INNER JOIN ps_carrier ON ps_carrier.id_carrier = ps_orders.id_carrier
                  WHERE ps_orders.date_add >= :date AND ps_orders.date_add < DATE_ADD(:date, Interval 1 day)";//
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('date',$now);
        $stmt->execute();
        $orders = $stmt->fetchAll();

        foreach($orders as $order) {

            if($this->hasBeenSent($order['id_order'])) {
                continue;
            }

            $currentOrder = [
                'order'    => $order,
                'product' => $this->getOrderedProducts($order),
                'message'  => $this->getOrderMessage($order)
            ];

            if (!$this->sendOrderDetails($currentOrder)) {
                $this->updateSentOrders($order['id_order']);
            }

        }
    }

    /**
     * @param $id
     * @return array
     */
    protected function hasBeenSent($id)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT id_order FROM pa_order_notification_copy WHERE id_order = :id_order";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$id);
        $stmt->execute();
        return count($stmt->fetchAll());
    }


    protected function updateSentOrders($id_order)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "INSERT INTO pa_order_notification_copy (id_order) VALUES (:id_order)";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$id_order);
        $stmt->execute();
    }

    protected function getAddress($id)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT lastname, firstname, address1, address2, city, postcode,iso_code,phone,phone_mobile,ps_address.id_state 
                        FROM ps_address
                    INNER JOIN ps_state ON ps_address.id_state = ps_state.id_state WHERE id_address = :id_cart";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_cart',$id);
        $stmt->execute();
        return $stmt->fetch();
    }

    protected function getAllProductsForOrder($products)
    {
        $allProducts = [];
        foreach($products as $key => $product) {
            foreach ($product as $orderedProduct) {
                $allProducts[] = $orderedProduct;
            }
        }
        return $allProducts;
    }

    protected function getOrderedProducts($order)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $products = [];
        $suppliers = [];

        $sql = "SELECT product_name, product_reference, total_price_tax_incl, total_price_tax_excl,ps_product.id_supplier, 
                        ps_supplier.name as supplierName , product_price,unit_price_tax_excl,id_order_detail
                    FROM ps_order_detail
                    INNER JOIN ps_product ON ps_product.id_product = ps_order_detail.product_id 
                    INNER JOIN ps_supplier ON ps_supplier.id_supplier = ps_product.id_supplier
                    WHERE id_order=:id_order";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$order['id_order']);
        $stmt->execute();
        $data = $stmt->fetchAll();

        if(count($data) > 1 ) {
            foreach ($data as $product) {
                $suppliers[] = $product['id_supplier'];
            }
        }
        $stores = array_unique($suppliers);
        $shping = $order['total_shipping_tax_incl'];
        $shipping = [$shping];


        if(count($stores) > 1) {
            $shipping = $this->splitShipping(count($data),$order);

        }
        $i = 0;
        echo  $order['id_order']. "\n";
        foreach($data as $product) {
            $tax = $this->getItemTax($product);
            $products[$product['id_supplier']][] = [
                "product_name" => $product['product_name'],
                "product_reference" => $product['product_reference'],
                "total_price_tax_incl" => $product['total_price_tax_incl'],
                "total_price_tax_excl" => $product['total_price_tax_excl'],
                "id_supplier" => $product['id_supplier'],
                "supplierName" => $product['supplierName'],
                "product_price" => $product['product_price'],
                "unit_price_tax_excl" => number_format($product['unit_price_tax_excl'],2),
                "shipping" => (count($shipping) > 1 ? $shipping[$i] : $shipping[0]),
                "itemTax" => ($tax ? $tax['total_amount'] : 0)
                ];
            $i++;
        }

        return $products;
    }

    protected function getItemTax($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT total_amount FROM ps_order_detail_tax WHERE id_order_detail = :id_order_detail";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order_detail',$product['id_order_detail']);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    protected function getOrderMessage($order)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT message FROM ps_message WHERE id_cart = :id_cart";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_cart',$order['id_cart']);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * @param $order
     * @return bool
     */
    protected function sendOrderDetails($order)
    {
        $orderDetails = $order['order'];
        $orderMessage = $order['message'];
        $products     = $order['product'];
        $shippingAddress = $this->getAddress($orderDetails['id_address_delivery']);
        $invoiceAddress  = $this->getAddress($orderDetails['id_address_invoice']);

        $error = false;
        $orderProducts = $this->getAllProductsForOrder($products);

        foreach($products as $key => $product) {
            $fileName = rand();
            $snappy = $this->getContainer()->get('knp_snappy.pdf');
            $snappy->setOption('page-size', 'Letter');
            $snappy->generateFromHtml(
                $this->getContainer()->get('templating')->render(
                    'email/newOrderPdf.html.twig',
                    [
                        'order' => $orderDetails,
                        'products' => $orderProducts,
                        'messages' => $orderMessage,
                        'shippingAddress' => $shippingAddress,
                        'invoiceAddress' => $invoiceAddress,
                        'store' => $key
                    ]
                ),
                self::folder.$fileName.'.pdf'
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Pawn America Online Sale')
                ->setFrom('marketplace@pawnamerica.com')
                ->addTo('ndahl@nerdery.com')//Dave.Willmott@pawnamerica.com
                //->addTo($this->getStoreEmail($key))
                //->addTo('todd.hyland@cashpass.com')
                //->addTo('marketplace@pawnamerica.com')
                //->setBcc([,'todd.hyland@cashpass.com','web.store11@pawnamerica.com'])
                ->attach(Swift_Attachment::fromPath(self::folder.$fileName.'.pdf'))
                ->setBody(
                    $this->getContainer()->get('templating')->render('email/newOrder.html.twig',
                        [
                            'order' => $orderDetails,
                            'products' => $product,
                            'messages' => $orderMessage,
                            'shippingAddress' => $shippingAddress,
                            'invoiceAddress' => $invoiceAddress,
                        ]
                    ),
                    'text/html'
                );
            $this->createPosNotification($orderDetails,$product,$key);
            /** @var SendSale $sendSale */
            $sendSale = $this->getContainer()->get('pawn.sendsale');
            $sendSale->setProducts($product);
            $sendSale->setOrderDetails($orderDetails);
            $sendSale->setStore($key);
            $sendSale->generateXml();


            $failures = [];

            $send = $this->getContainer()->get('mailer')->send($message,$failures);
            if (!$send) {
                $error = true;
            }
        }

        return $error;
        
    }

    /**
     * @param $orderDetails
     * @param $orderProducts
     * @param $store
     */
    protected function createPosNotification($orderDetails,$orderProducts,$store)
    {

        $pawnStore = $this->prestshopToMicros($store);
        $orderDate = explode(" ",$orderDetails['invoice_date']);
        $dueDate =  date('Y-m-d', strtotime($orderDate[0] . ' +1 day'));
        $prefix = "pa-";
        $title = '';
        if($orderDetails['module'] == 'fastbay1') {
            $prefix = "ebay-";
        }
        $title .= $prefix. ";";
        $lineItem = '';
        $title = '';
        $cnt = 1;
        foreach($orderProducts as $product) {
            $title .= $product['product_reference']. ";";
            $lineItem .= $product['product_reference'] . " " . $product['product_name'].";";
        }

        //et-store-order
        $fileName = "et-".$pawnStore."-". $orderDetails['id_order'];

        $myfile = fopen("/home/admin/stores/messages/".$pawnStore."/".$fileName.".dat", "w");
        $header = '<Header download_id="XLINK.GLOBAL.'.$fileName.'" application_date="'. $orderDate[0]. '" target_org_node="STORE:'.$pawnStore.'" deployment_name="XLINK.GLOBAL.'.$fileName.'" download_time="IMMEDIATE" apply_immediately="true" />'.PHP_EOL;
        //$header = "Action Code|Record Identifier|Retail Location ID|Task ID|Start Date|Due Date|Type Code|Title|Description|Priority|Visibility|Assignment ID|Void Flag\n";
        fwrite($myfile, $header);
        $taskId = $pawnStore.str_pad($orderDetails['id_order'],12,'0',STR_PAD_LEFT);
        $order = "INSERT|EMPLOYEE_TASK|".$pawnStore."|". $taskId ."|".$orderDate[0]."|".$dueDate."|GENERAL|".$title."|".$lineItem."|HIGH|STORE||".PHP_EOL;
        fwrite($myfile, $order);
        fclose($myfile);
    }

    /**
     * @param $supplier
     * @return mixed
     */
    protected function getStoreEmail($supplier)
    {
        $storeEmails = [
            1 => 'pawnamerica-e242@pawnamerica.com',
           2 => 'pawnamerica-e251@pawnamerica.com',
           3 => 'pawnamerica-e259@pawnamerica.com',
           4 => 'pawnamerica-e241@pawnamerica.com',
           5 => 'pawnamerica-e232@pawnamerica.com',
           6 => 'pawnamerica-e243@pawnamerica.com',
           7 => 'pawnamerica-e237@pawnamerica.com',
           8 => 'pawnamerica-e253@pawnamerica.com',
           9 => 'pawnamerica-e248@pawnamerica.com',
           10 => 'pawnamerica-e235@pawnamerica.com',
           11 => 'pawnamerica-e257@pawnamerica.com',
           12 => 'pawnamerica-e254@pawnamerica.com',
           13 => 'pawnamerica-e252@pawnamerica.com',
           14 => 'pawnamerica-e245@pawnamerica.com',
           15 => 'pawnamerica-e240@pawnamerica.com',
           16 => 'pawnamerica-e247@pawnamerica.com',
           17 => 'pawnamerica-e231@pawnamerica.com',
           18 => 'pawnamerica-e238@pawnamerica.com',
           19 => 'pawnamerica-e234@pawnamerica.com',
           20 => 'pawnamerica-e246@pawnamerica.com',
           21 => 'pawnamerica-e236@pawnamerica.com',
           22 => 'pawnamerica-e258@pawnamerica.com',
           23 => 'pawnamerica-e250@pawnamerica.com',
           24 => 'pawnamerica-e255@pawnamerica.com',
           25 => 'marketplace@pawnamerica.com',//guest.service@everonesjewlery.com
           26 => 'paul.hess@pawnamerica.com',
           28 => 'guest.service@everonesjewelryshop.com'
        ];

        return $storeEmails[$supplier];
    }

    /**
     * @param $supplier
     * @return mixed
     */
    protected function prestshopToMicros($supplier)
    {
//        $supplierListPresta = [
//            1 => 242,
//            2=> 251,
//            3=> 259,
//            4=> 241,
//            5=> 232,
//            6=> 243,
//            7=> 237,
//            8=> 253,
//            9=> 248,
//            10=> 235,
//            11=> 257,
//            12=> 254,
//            13=> 252,
//            14=> 245,
//            15=> 240,
//            16=> 247,
//            17=> 231,
//            18=> 238,
//            19=> 234,
//            20=> 246,
//            21=>236,
//            22=> 258,
//            23=> 250,
//            24=> 255
//        ];

        $supplierListPresta = [
            1 => 101,
            2=> 101,
            3=> 101,
            4=> 101,
            5=> 101,
            6=> 101,
            7=> 101,
            8=> 101,
            9=> 101,
            10=> 102,
            11=> 102,
            12=> 102,
            13=> 102,
            14=> 102,
            15=> 102,
            16=> 102,
            17=> 102,
            18=> 103,
            19=> 103,
            20=> 103,
            21=>103,
            22=> 103,
            23=> 103,
            24=> 103
        ];

        return $supplierListPresta[$supplier];
    }

    /**
     * @param $stores
     * @param $order
     * @return array
     */
    protected function splitShipping($stores,$order)
    {
        $shippingCost = $order['total_shipping_tax_incl'];
        //echo $shippingCost;
        $shippingSplit = $shippingCost / $stores;
        $tmpShippingTotal = $shippingSplit * $stores;
        $shippingOffset = $shippingCost - $tmpShippingTotal;
        $shippingArray = [];

        for($i =1; $i <= $stores; $i++) {
            if($i == $stores) {
                array_push($shippingArray,$shippingSplit + $shippingOffset);
                continue;
            }
            array_push($shippingArray,$shippingSplit);
        }
        return $shippingArray;
    }

    protected function splitTax($stores,$order)
    {
        $taxTotal = $order['total_paid'] - $order['total_products'] - $order['total_shipping_tax_excl'];
        $taxSplit = $taxTotal / $stores;
        $tmpTaxTotal = $taxSplit * $stores;
        $taxOffSet = $taxTotal - $tmpTaxTotal;
        $taxArray = [];

        for($i = 1; $i <= $stores; $i++) {
            if($i = $stores) {
                array_push($taxArray, $taxSplit + $taxOffSet);
                continue;
            }
            array_push($taxArray, $taxSplit);
        }

        return $taxArray;
    }
}