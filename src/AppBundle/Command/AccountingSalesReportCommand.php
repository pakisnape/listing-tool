<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use PHPExcel_IOFactory;
use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Swift_Attachment;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class AccountingSalesReportCommand extends ContainerAwareCommand
{
    private $subject;

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:accounting-sales-report')

            // the short description shown while running "php bin/console list"
            ->setDescription('Accounting sales report')
            ->addArgument('monthly', InputArgument::OPTIONAL, 'monthly?')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('monthly','m',InputOption::VALUE_OPTIONAL)
                ])
            )

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for new products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $monthly = $input->getArgument('monthly',0);
        $subject = ($monthly ? 'Monthly': 'Daily');
        $this->subject = $subject;


        $currentOrders = $this->getOrders($monthly);
        $returns = $this->getReturns($monthly);

        $orders = array_merge($currentOrders,$returns);
        $fileName = $this->buildExcel($orders);

        $this->sendSalesReport($fileName);

    }

    protected function combine($currentOrders,$returns)
    {
        $orders = [];
        foreach ($currentOrders as $current) {

        }
    }

    /**
     * @param $orders
     * @return string
     */
    protected function buildExcel($orders)
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);



        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Order Reference');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Order Status');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Customer Name');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Shipping Total');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Shipping Tax');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Order Total');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Tax');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Sold Date');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Payment Type');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Store');
        $objPHPExcel->getActiveSheet()->setCellValue('K1', 'City');
        $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Zip');

        $rowCount =2;
        foreach ($orders as $order) {
            $address = $this->getAddress($order['id_address_delivery']);
            $name = $this->getStateName($order['id_order_state']);

            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $order['reference']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $name);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $order['firstname'] . " " . $order['lastname']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, round($order['total_shipping_tax_excl'],2));
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, round($order['total_shipping_tax_incl'] - $order['total_shipping_tax_excl'],2));

            $total = ($name != "Refunded" ? round($order['total_paid'],2) : "-".round($order['total_paid'],2) );

            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $total);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, round($order['total_paid_tax_incl'] - $order['total_paid_tax_excl'],2));
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowCount, $order['date_add']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowCount, $order['payment']);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowCount, implode(",", $this->getStoresFromOrder($order)));
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$rowCount, $address['city']);
            $zip = explode('-',$address['postcode']);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$rowCount, $zip[0]);
            $rowCount++;
        }

        $now = date('Y-m');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $fileName = "salesReport-".$this->subject. $now .".xlsx";

        $objWriter->save($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $fileName);

        return $fileName;
    }

    /**
     * @param $monthly
     * @return array
     */
    protected function getReturns($monthly)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $year = date("y");
        $month = date('m');
        $day = date('d');

        $end = $year."-".$month."-".$day ." 22:00:00";

        $start = date("Y-m-d H:i:s",strtotime("-1 days",$end));
        if ($monthly) {
            $start = date('Y-m-d', strtotime("first day of -1 month"));
            $end = date('Y-m-d', strtotime('last day of previous month'));
        }
        $orders = [];

        $sql = "SELECT ps_order_history.date_add,ps_orders.total_shipping_tax_incl, ps_orders.id_order, ps_orders.payment
                        ps_orders.reference,ps_customer.firstname,ps_customer.lastname, ps_order_history.id_order_state,
                        ps_orders.total_shipping_tax_excl,ps_orders.total_shipping_tax_incl,ps_orders.total_paid_tax_incl,
                        ps_orders.total_paid_tax_excl,ps_orders.id_address_delivery,ps_orders.total_paid
                         FROM ps_orders 
                         INNER JOIN ps_order_history ON ps_order_history.id_order = ps_orders.id_order
                         INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer 
                         WHERE ps_order_history.id_order_state = :state 
                                  AND ps_order_history.date_add >= :now 
                                  AND ps_order_history.date_add < :end";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('now',$start);
        $stmt->bindValue('end',$end);
        $stmt->bindValue('state',7);
        $stmt->execute();
        $data = $stmt->fetchAll();

        foreach($data as $orderData) {
            $orders[] = $orderData;
        }

        return $orders;
    }

    /**
     * @param $state
     * @return mixed
     */
    protected function getStateName($state)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $suppliers = [];

        $sql = "SELECT  *
                    FROM ps_order_state_lang
                    WHERE id_order_state = :state";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('state',$state);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $data['name'];
    }


    /**
     * @param $monthly
     * @return array
     */
    protected function getOrders($monthly)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $year = date("y");
        $month = date('m');
        $day = date('d');
        $end = $year."-".$month."-".$day ." 17:00:00";
        $stmp = "2016-01-11 17:00:00";
        $etmp = "2016-01-12 17:00:00";

        $start = date("Y-m-d H:i:s",strtotime("-1 days",$end));
        if ($monthly) {
            $start = date('Y-m-d', strtotime("first day of -1 month"));
            $end = date('Y-m-d', strtotime('last day of previous month'));
        }
        $orders = [];



        $sql = "SELECT ps_order_history.date_add,ps_orders.total_shipping_tax_incl, ps_orders.id_order, ps_orders.payment,
                        ps_orders.reference,ps_customer.firstname,ps_customer.lastname,ps_order_history.id_order_state,
                        ps_orders.total_shipping_tax_excl,ps_orders.total_shipping_tax_incl,ps_orders.total_paid_tax_incl,
                        ps_orders.total_paid_tax_excl,ps_orders.id_address_delivery,ps_orders.total_paid
                        FROM ps_orders 
                         INNER JOIN ps_order_history ON ps_order_history.id_order = ps_orders.id_order
                         INNER JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer                         
                         WHERE ps_order_history.id_order_state = :state 
                                    AND ps_order_history.date_add >= :now 
                                    AND ps_order_history.date_add < :end";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('now',$start);
        $stmt->bindValue('end',$end);
        $stmt->bindValue('state',2);
        $stmt->execute();
        $data = $stmt->fetchAll();
        foreach($data as $orderData) {
            $orders[] = $orderData;
        }

        return $orders;
    }

    protected function getStoresFromOrder($order)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $suppliers = [];

        $sql = "SELECT  ps_supplier.name as supplierName
                    FROM ps_order_detail
                    INNER JOIN ps_product ON ps_product.id_product = ps_order_detail.product_id 
                    INNER JOIN ps_supplier ON ps_supplier.id_supplier = ps_product.id_supplier
                    WHERE id_order=:id_order";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$order['id_order']);
        $stmt->execute();
        $data = $stmt->fetchAll();


        foreach($data as $product) {
            $suppliers[] = $product['supplierName'];
        }


        return array_unique($suppliers);


    }

    protected function getAddress($customerId)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        return $conn->fetchAssoc("SELECT * FROM ps_address inner join ps_state on ps_state.id_state = ps_address.id_state WHERE id_address =".$customerId." LIMIT 1");
    }


    protected function sendSalesReport($file)
    {

        $message = \Swift_Message::newInstance()
            ->setSubject($this->subject.' Pawn America Sales Report')
            ->setFrom('support@pawnamerica.com')
            ->addTo('ndahl@nerdery.com')
                ->addTo('steve.caulfield@pawnamerica.com')
                ->addTo('dave.willmott@pawnamerica.com')
                ->addTo('todd.hyland@cashpass.com')
                ->addTo('keith.kaestner@pawnamerica.com')
                ->addTo('matt.johnson@pawnamerica.com')
                ->addTo('james.slarks@pawnamerica.com')
                ->addTo('david.neilitz@pawnamerica.com')
                ->addTo('dan.kealey@pawnamerica.com')//
            ->attach(Swift_Attachment::fromPath($this->getContainer()->get('kernel')->getRootDir()."/../web/Excel/". $file));
            //->setBcc([,'todd.hyland@cashpass.com','web.store11@pawnamerica.com'])


        $this->getContainer()->get('mailer')->send($message);

    }
}