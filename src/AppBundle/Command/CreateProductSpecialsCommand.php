<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use PDO;
use PHPExcel_Shared_Date;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use PHPExcel_IOFactory;


class CreateProductSpecialsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:create-specials')

            // the short description shown while running "php bin/console list"
            ->setDescription('Store daily summary.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for new products.")
        ;
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
//        $fs = new Filesystem();
//
//
//        $dir = $this->getContainer()->getParameter('product_specials');
//        $file = "specials.xlsx";
//
//        if(!$fs->exists($dir.$file)) {
//            return;
//        }
//
//        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
//        //$objReader->setReadDataOnly(false);
//        $objPHPExcel = $objReader->load($dir.$file);
//        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
//
//        $excelRow =1;
//        foreach ($objWorksheet->getRowIterator() as $row) {
//            $cellIterator = $row->getCellIterator();
//            $cellIterator->setIterateOnlyExistingCells(false);
//
//            $excelCell = 0;
//            $columns = ['reference','percent','start','end'];
//            $data = [];
//            $broken = false;
//            foreach ($cellIterator as $cell) {
//                if ($excelCell == 2 || $excelCell == 3) {
//                    $date = PHPExcel_Shared_Date::ExcelToPHP($cell->getValue());
//                    $data[] = date('Y-m-d',$date) ." 00:00:00";
//                    continue;
//                }
//                $data[] =  $cell->getValue();
//                $excelCell++;
//            }
//            //$this->addToMysql($data);
//            //$this->addProductSpecial($data);
//
//            $excelRow++;
//        }
//
//        $fs->remove($dir.$file);
         $productInserts = $conn->fetchAll('SELECT * FROM pawn_special_import');

         foreach($productInserts as $special) {
             $product = $this->addProductSpecial($special);
         }
    }

    protected function addToMysql($data)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $ps_product_carrierSql = "INSERT INTO pawn_special_import (reference, percent, start, end)
                                              VALUES (:reference,:percent,:start,:end)";
        $stmt = $conn->prepare($ps_product_carrierSql);
        $stmt->bindValue("reference", $data[0]);
        $stmt->bindValue("percent", $data[1]);
        $stmt->bindValue("start", $data[2]);
        $stmt->bindValue("end", $data[3]);
        $stmt->execute();
    }

    protected function addProductSpecial($data)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $product = $this->getProduct($data);
        if($product) {
            $conn->beginTransaction();

            try {
                $sql = "INSERT INTO ps_specific_price (id_specific_price_rule,id_cart,id_product,id_shop,
                                                      id_shop_group,id_currency,id_country,id_group,id_customer,
                                                      id_product_attribute,price,from_quantity,reduction,
                                                      reduction_tax,reduction_type,`from`,`to`)
                                                      VALUES (:id_specific_price_rule,:id_cart,:id_product,:id_shop,
                                                      :id_shop_group,:id_currency,:id_country,:id_group,:id_customer,
                                                      :id_product_attribute,:price,:from_quantity,:reduction,
                                                      :reduction_tax,:reduction_type,:from,:to)";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue("id_specific_price_rule",0);
                $stmt->bindValue("id_cart",0);
                $stmt->bindValue("id_product",$product['id_product']);
                $stmt->bindValue("id_shop",1);
                $stmt->bindValue("id_shop_group",0);
                $stmt->bindValue("id_currency",0);
                $stmt->bindValue("id_country",0);
                $stmt->bindValue("id_group",0);
                $stmt->bindValue("id_customer",0);
                $stmt->bindValue("id_product_attribute",0);
                $stmt->bindValue("price",-1.000000);
                $stmt->bindValue("from_quantity",1);
                $stmt->bindValue("reduction",$data['percent']);
                $stmt->bindValue("reduction_tax",1);
                $stmt->bindValue("reduction_type",'percentage');
                $stmt->bindValue("from",$data['start']);
                $stmt->bindValue("to",$data['to']);

                $stmt->execute();
                $conn->commit();

                $deleteSql = "DELETE FROM pawn_special_import WHERE id = :id";
                $stmt = $conn->prepare($deleteSql);
                $stmt->bindValue("id",$data['id']);
                $stmt->execute();

            } catch (Exception $e) {
                $conn->rollBack();
            }

        }

    }


    protected function getProduct($data)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT * FROM ps_product WHERE reference = :reference LIMIT 1";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('reference',$data['reference']);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}