<?php

namespace AppBundle\Command;

use AppBundle\Services\PrestaShopApiService;
use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\PrestaShopWebservice;
use AppBundle\Services\PrestaShopWebserviceException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class SoldProductsCommand extends ContainerAwareCommand
{
	/**
	 * Debug setting
	 */
	const DEBUG = false;

	/**
	 * presta shop path
	 */
	const PS_SHOP_PATH = "https://www.pawnamerica.com/";

	/**
	 * Auth key
	 */
	const PS_WS_AUTH_KEY = 'GA2EKZHPBNTA4ULTEAYG8BTBR2ETKYA7';

	private $path = '/var/www/pawnamerica/img/p';

	protected function configure()
	{
		$this
		  // the name of the command (the part after "bin/console")
		  ->setName('app:sold-products')
		  // the short description shown while running "php bin/console list"
		  ->setDescription('idisable sold products.')
		  // the full command description shown when running the command with
		  // the "--help" option
		  ->setHelp("This command looks for new products.");
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->getSoldItemsFromExcel();
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');
		$products = $conn->fetchAll('SELECT * FROM pawn_sold_products');

		//$this->markDisableWithWebService(350103);


		//die;
		foreach ($products as $product) {
			if (!is_numeric($product['item_id'])) {
//                continue;
			}
			//echo "id = " .$product['item_id']."\n";die;
			$psProductSQL = "SELECT * FROM ps_product WHERE reference = :reference LIMIT 1";
			$stmt = $conn->prepare($psProductSQL);
			$stmt->bindValue("reference", $product['item_id']);
			$stmt->execute();
			$psProduct = $stmt->fetch(PDO::FETCH_ASSOC);
			if (empty($psProduct)) {
				$this->delete($product['item_id']);
				continue;
			}
			$images = $this->getImageName($psProduct['id_product']);
			$this->deleteImages($images, $psProduct['id_product']);
			$this->addToDisabledList($product['item_id']);
			$this->changeFastBayStatus($psProduct['id_product']);
			// if(!$this->isActive($psProduct['id_product'])) {
			//$this->delete($product['item_id']);
			//continue;
			//}

			if ($this->isEbay($psProduct)) {
				$this->changeFastBayStatus($psProduct['id_product']);
				$this->markDisableWithWebService($psProduct['id_product'], $product);

				continue;
			}


			$this->addToDisabledList($product['item_id']);
			$conn->beginTransaction();

			try {

				$productSql = "UPDATE ps_product SET active = :active, redirect_type = :redirect_type WHERE id_product = :id_product";
				$stmt = $conn->prepare($productSql);
				$stmt->bindValue("active", 0);
				$stmt->bindValue("redirect_type", '404');
				$stmt->bindValue("id_product", $psProduct['id_product']);
				$stmt->execute();

				$productShopSql = "UPDATE ps_product_shop SET active = :active,redirect_type = :redirect_type WHERE id_product = :product ";
				$stmt = $conn->prepare($productShopSql);
				$stmt->bindValue("active", 0);
				$stmt->bindValue("redirect_type", '404');
				$stmt->bindValue("product", $psProduct['id_product']);
				$stmt->execute();

				$ps_stock_availableSql = "UPDATE ps_stock_available SET quantity = :quantity WHERE id_product = :id_product";
				$stmt = $conn->prepare($ps_stock_availableSql);
				$stmt->bindValue("id_product", $psProduct['id_product']);
				$stmt->bindValue("quantity", 0);
				$stmt->execute();


				$deleteSql = "DELETE FROM pawn_sold_products WHERE item_id = :id";
				$stmt = $conn->prepare($deleteSql);
				$stmt->bindValue("id", $product['item_id']);
				$stmt->execute();
				$this->addToDisabledList($product['item_id']);
				$conn->commit();
			} catch (PrestaShopWebserviceException $e) {
				$conn->rollBack();
				// Here we are dealing with errors
				$trace = $e->getMessage();
				if ($trace[0]['args'][0] == 404) {
					echo 'Bad ID';
				} else {
					if ($trace[0]['args'][0] == 401) {
						echo 'Bad auth key';
					} else {
						echo 'Other error<br />'.$e->getMessage();
					}
				}
			}
		}
	}

	/**
	 * @param $id_product
	 * @return array
	 */
	protected function getImageName($id_product)
	{
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');

		$sql = "SELECT *
                    FROM ps_image_shop
                    WHERE id_product=:id_product";
		$stmt = $conn->prepare($sql);
		$stmt->bindValue('id_product', $id_product);
		$stmt->execute();

		return $stmt->fetchAll();
	}


	/**
	 * @param $psProduct
	 */
	protected function changeFastBayStatus($psProduct)
	{
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');

		$now = date('Y-m-d H:m:s');

		$sql = "UPDATE ps_fastbay1_product_settings SET sync= :sync,date_upd =:date_upd
																	WHERE  id_product = :id_product";
		$stmt = $conn->prepare($sql);
		$stmt->bindValue("sync", 1);
		$stmt->bindValue("id_product", $psProduct);
		$stmt->bindValue("date_upd", $now);
		$stmt->execute();
	}


	/**
	 * @param $id_product
	 * @return array
	 */
	protected function getImageIds($id_product)
	{
		$imageIds = [];
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');

		$imageSql = "SELECT * FROM ps_image WHERE id_product = :id_product";
		$stmt = $conn->prepare($imageSql);
		$stmt->bindValue("id_product", $id_product);
		$stmt->execute();
		$results = $stmt->fetchAll();

		foreach ($results as $image) {
			$imageIds[] = $image['id_image'];
		}

		return $imageIds;
	}


	/**
	 * @param $images
	 * @param $id_product
	 */
	protected function removeImages($images, $id_product)
	{
		foreach ($images as $image) {
			$imageFullName = $image['id_image'];
			$imageParts = str_split($image['id_image']);
			$path = implode("/", $imageParts);

			/** @var Finder $finder */
			$finder = new Finder();
			try {
				$finder->files()->in($this->path.$path."/");
			} catch (\Exception $e) {
				continue;
			}


			foreach ($finder as $file) {
				$realPath = $file->getRealpath();
				$fs = new Filesystem();
				$fs->remove($this->path.$path."/".$file);

			}
			$this->removeImagesFromDatabase($id_product);
		}
	}

	/**
	 * @param $images
	 * @param $id_product
	 * @deprecated
	 */
	protected function deleteImages($images, $id_product)
	{
		foreach ($images as $image) {
			$imageFullName = $image['id_image'];
			$imageParts = str_split($image['id_image']);
			$path = implode("/", $imageParts);
			// reset some counters
			$cnt_files = 0;
			$cnt_checked = 0;

			$handle = opendir($this->path.$path."/");
			if ($handle) {
				while (false !== ($entry = readdir($handle))) {
					if ($entry != "." && $entry != "..") {
						$cnt_files++;
						if (is_file($this->path.$path."/".$entry)) {
							$pi = explode('-', $entry);
							if ($pi[0] > 0) {
								$cnt_checked++;
								if ($pi[0] == $image || $pi[0] == $imageFullName.'.jpg') {
									//echo $this->path.$path."/".$entry;
									unlink($this->path.$path."/".$entry);
								}

							}
						}
					}
				}
				closedir($handle);
				$this->removeImagesFromDatabase($id_product);
			}

		}
	}

	protected function removeImagesFromDatabase($id_product)
	{
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');

		$imagesIds = $this->getImageIds($id_product);

		$conn->beginTransaction();
		try {

			$deleteSql = "DELETE FROM ps_image WHERE id_product = :id_product";
			$stmt = $conn->prepare($deleteSql);
			$stmt->bindValue("id_product", $id_product);
			$stmt->execute();

			$deleteShopSql = "DELETE FROM ps_image_shop WHERE id_product = :id_product";
			$stmt = $conn->prepare($deleteShopSql);
			$stmt->bindValue("id_product", $id_product);
			$stmt->execute();

			foreach ($imagesIds as $image) {
				$deleteSql = "DELETE FROM ps_image_lang WHERE id_image = :id_image";
				$stmt = $conn->prepare($deleteSql);
				$stmt->bindValue("id_image", $image);
				$stmt->execute();
			}


			$conn->commit();
		} catch (Exception $ex) {
			$conn->rollBack();
		}
	}

	/**
	 * get item_id from excel
	 */
	protected function getSoldItemsFromExcel()
	{
		$files = $this->getFiles();
		$filesystem = new Filesystem();
		foreach ($files as $file) {
			$dir = $this->getContainer()->getParameter('sold_inventory');
			if (!$filesystem->exists($dir.$file)) {
				return false;
			}

			if (($handle = fopen($dir.$file, "r")) !== false) {
				$i = 1;
				while (($row = fgetcsv($handle)) !== false) {
					$this->insertData($row[0], $i);
					//var_dump($row); // process the row.
					$i++;
				}
			}


			$filesystem->remove($dir.$file);
		}

	}

	/**
	 * @return array
	 */
	protected function getFiles()
	{
		$finder = new Finder();
		$dir = $this->getContainer()->getParameter('sold_inventory');
		$finder->files()->in($dir);
		$files = [];
		foreach ($finder as $file) {
			// Dump the absolute path
			///var_dump($file->getRealPath());

			// Dump the relative path to the file, omitting the filename
			//var_dump($file->getRelativePath());

			// Dump the relative path to the file
			//var_dump($file->getRelativePathname());
			$files[] = $file->getFilename();
		}

		return $files;
	}

	/**
	 * @param $data
	 * @param $i
	 */
	protected function insertData($data, $i)
	{
		if ($i == 1) {
			return;
		}
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');
		$ps_product_carrierSql = "INSERT INTO pawn_sold_products (item_id)
                                          VALUES (:item_id)";
		$stmt = $conn->prepare($ps_product_carrierSql);
		$stmt->bindValue("item_id", $data);

		$stmt->execute();
	}

	protected function markDisableWithWebService($id_product, $product)//
	{
		echo $id_product." Fastbay \n";
		/** @var PrestaShopApiService $apiService */
		$apiService = $this->getContainer()->get('pawn.apiservice');
		$apiService->getIdStockAvailableAndSet($id_product, 0);
		$apiService->setProductActive($id_product, false);
		$apiService->setProductActive($id_product, true);
		$apiService->setProductActive($id_product, false);
		$this->delete($product['item_id']);
	}

	/**
	 * @param $product
	 * @return int
	 */
	protected function isEbay($product)
	{
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');

		$sql = "SELECT * FROM ps_fastbay1_product_settings where id_product = :id_product";
		$stmt = $conn->prepare($sql);
		$stmt->bindValue('id_product', $product['id_product']);

		$stmt->execute();

		return count($stmt->fetchAll());
	}

	/**
	 * @param int $ProductId
	 * @param $qty
	 */
	protected function getIdStockAvailableAndSet($ProductId = 235935, $qty)
	{
		$webService = new PrestaShopWebservice(self::PS_SHOP_PATH, self::PS_WS_AUTH_KEY, self::DEBUG);
		$opt['resource'] = 'products';
		//$opt['filter']['reference'] = '237063472';//328102
		$opt['id'] = $ProductId;
		//print_r($opt);
		$xml = $webService->get($opt);

		foreach ($xml->product->associations->stock_availables->stock_available as $item) {
			//echo "ID: ".$item->id."\n\r";
			//echo "Id Attribute: ".$item->id_product_attribute."\n\r";
			$this->set_product_quantity($ProductId, $item->id, $item->id_product_attribute, $qty);
		}
	}

	/**
	 * @param $ProductId
	 * @param $status
	 */
	protected function setProductActive($ProductId, $status)
	{
		$webService = new PrestaShopWebservice(self::PS_SHOP_PATH, self::PS_WS_AUTH_KEY, self::DEBUG);
		$xml = $webService->get(array('resource' => 'products', 'id' => $ProductId));
		$resources = $xml->children()->children();

		// Unset fields that may not be updated
		unset($resources->manufacturer_name);
		unset($resources->quantity);

		$resources->id = $ProductId;
		$resources->active = $status;
		//echo $xml->asXML();die;
		try {
			$opt = array('resource' => 'products');
			$opt['putXml'] = $xml->asXML();
			$opt['id'] = $ProductId;
			$xml = $webService->edit($opt);
		} catch (PrestaShopWebserviceException $ex) {
			echo "<bhow>Error   ->Error : </bhow>".$ex->getMessage().'<br>';
		}
	}

	/**
	 * @param $ProductId
	 * @param $StokId
	 * @param $AttributeId
	 */
	protected function set_product_quantity($ProductId, $StokId, $AttributeId, $qty = 0)
	{
		$webService = new PrestaShopWebservice(self::PS_SHOP_PATH, self::PS_WS_AUTH_KEY, self::DEBUG);
		$xml = $webService->get(array('url' => self::PS_SHOP_PATH.'/api/stock_availables?schema=blank'));
		$resources = $xml->children()->children();
		$resources->id = $StokId;
		$resources->id_product = $ProductId;
		$resources->quantity = $qty;
		$resources->id_shop = 1;
		$resources->out_of_stock = 1;
		$resources->depends_on_stock = 0;
		$resources->id_product_attribute = $AttributeId;
		try {
			$opt = array('resource' => 'stock_availables');
			$opt['putXml'] = $xml->asXML();
			$opt['id'] = $StokId;
			$xml = $webService->edit($opt);
		} catch (PrestaShopWebserviceException $ex) {
			echo "<b>Error   ->Error : </b>".$ex->getMessage().'<br>';
		}
	}


	/**
	 * @param $reference
	 */
	protected function addToDisabledList($reference)
	{
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');
		$ps_product_carrierSql = "INSERT INTO pawn_deactivated (item_id,date_add)
																						VALUES (:item_id,:date_add)";
		$stmt = $conn->prepare($ps_product_carrierSql);
		$stmt->bindValue("item_id", $reference);
		$stmt->bindValue("date_add", date("Y-m-d"));

		$stmt->execute();
	}


	/**
	 * @param $product
	 */
	protected function delete($product)
	{
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');
//echo $product . "\n";
		$deleteSql = "DELETE FROM pawn_sold_products WHERE item_id = :id";
		$stmt = $conn->prepare($deleteSql);
		$stmt->bindValue("id", $product);
		$stmt->execute();
	}

	protected function isActive($id_product)
	{
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');

		$productShopSql = "SELECT active FROM ps_product_shop WHERE id_product = :product ";
		$stmt = $conn->prepare($productShopSql);
		$stmt->bindValue("product", $id_product);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		if (!$result) {
			return 0;
		}

		return $result['active'];
	}

	protected function haveQty($id_product)
	{
		/** @var Connection $conn */
		$conn = $this->getContainer()->get('database_connection');

		$ps_stock_availableSql = "SELECT quantity FROM ps_stock_available  WHERE id_product = :id_product";
		$stmt = $conn->prepare($ps_stock_availableSql);
		$stmt->bindValue("id_product", $id_product);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		if (!$result) {
			return 0;
		}

		return ($result['quantity'] ? 1 : 0);
	}

}
