<?php


namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



class AddDescriptionTemplateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:add-description-template')

            // the short description shown while running "php bin/console list"
            ->setDescription('add desription template.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for new products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $products = $conn->fetchAll('SELECT * FROM ps_product_lang where description = "" ');

        foreach ($products as $product) {
            $ProductLangInsertSql = "UPDATE ps_product_lang SET description = :description WHERE id_product = :id_product";
            $stmt = $conn->prepare($ProductLangInsertSql);
            $stmt->bindValue("id_product",$product['id_product']);

            $description = "
                <p>Your satisfaction is our number one goal. Keep in mind that we offer hassle-free returns if needed. If you have any questions or problems, please contact us.</p>
                <p>Please Note: All included items are shown in the pictures</p>
                <hr />
                <p><strong>Features:</strong></p>
                <ul>
                    <li>Feature 1</li>
                </ul>
                <p><b>Whats' included:</b></p>
                <ul>
                    <li>Accessory 1</li>
                </ul>
                <p><b>What's not included:</b></p>
                <ul>
                    <li>Accessory 1</li>
                </ul>
                <p><strong>Condition:</strong></p>
                <ul>
                    <li>Condition 1</li>
                </ul>
                ";
            $stmt->bindValue("description",$description);
            $stmt->execute();
        }
    }
}