<?php
/**
 * Created by PhpStorm.
 * User: nam797
 * Date: 3/15/17
 * Time: 10:38 AM
 */

namespace AppBundle\Command;


use PHPExcel_Cell;
use PHPExcel_IOFactory;
use PHPExcel_Worksheet_CellIterator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\DBAL\Connection;



class CurrentRetailStockCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:check-current-retail')

            // the short description shown while running "php bin/console list"
            ->setDescription('check prestashp vs micros.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks through excel doc to find if sold or not.")
        ;
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fs = new Filesystem();
        $dir = $this->getContainer()->getParameter('active_products');
        $file = "ActiveInventory.xlsx";

        if(!$fs->exists($dir.$file)) {
            $output->write('not found');
        }

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($dir.$file);
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);

        $excelRow =1;
        foreach ($objWorksheet->getRowIterator() as $row) {
            /** @var PHPExcel_Worksheet_CellIterator|PHPExcel_Cell[] $cellIterator */
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $excelCell = 1;
            $data = [];

            foreach ($cellIterator as $cell) {
                $data[] = $cell->getValue();
                $excelCell++;
            }

            $this->getContainer()->get('pawn.products')->insertActiveData($data);

            $excelRow++;
        }

        $this->getContainer()->get('pawn.products')->verifyProductAreActive();
        $output->write($this->getContainer()->get('pawn.products')->getCount());
        $fs->remove($dir.$file);
    }

}