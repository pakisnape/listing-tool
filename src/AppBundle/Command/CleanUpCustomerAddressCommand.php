<?php


namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CleanUpCustomerAddressCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:address-cleanup')

            // the short description shown while running "php bin/console list"
            ->setDescription('only allow 1 address per cusomter. delete rest')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for updated products from an excel file.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $customers = $conn->fetchAll("SELECT * FROM ps_customer");

        foreach ($customers as $customer) {
            $this->getAddress($customer['id_customer']);
        }
    }

    protected function getAddress($customerId)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $addresses = $conn->fetchAll("SELECT * FROM ps_address inner join ps_state on ps_state.id_state = ps_address.id_state WHERE id_customer =".$customerId);

        if (count($addresses) < 2) {
            return null;
        }

        $cnt = 1;
        foreach ($addresses as $address) {
            if ($cnt > 1 ) {
                $conn->exec("DELETE FROM ps_address WHERE id_address = " .$address['id_address']);
            }
            $cnt++;
        }
    }
}