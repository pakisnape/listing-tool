<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use League\Flysystem\Exception;
use PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\PrestaShopWebservice;
use AppBundle\Services\PrestaShopWebserviceException;



class TrasferecProductsCommand extends ContainerAwareCommand
{
    /**
     * Debug setting
     */
    const DEBUG = false;

    /**
     * presta shop path
     */
    const PS_SHOP_PATH  = "https://www.pawnamerica.com/";

    /**
     * Auth key
     */
    const PS_WS_AUTH_KEY = 'GA2EKZHPBNTA4ULTEAYG8BTBR2ETKYA7';

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:transfer-products')

            // the short description shown while running "php bin/console list"
            ->setDescription('transfer products.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for new products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');



        $products = $conn->fetchAll('SELECT * FROM pawn_transfered_products');

        foreach($products as $product) {
            $psProductSQL = "SELECT * FROM ps_product WHERE reference = :reference LIMIT 1";
            $stmt = $conn->prepare($psProductSQL);
            $stmt->bindValue("reference",$product['item_id']);
            $stmt->execute();
            $psProduct = $stmt->fetch(PDO::FETCH_ASSOC);
            if (count($psProduct) < 1 ) {
                $this->delete($psProduct['id_product']);
                continue;
            }

            $conn->beginTransaction();

            try {

                $this->setProductActive($psProduct['id_product'],false);
                $this->setProductActive($psProduct['id_product'],true);
                $this->getIdStockAvailableAndSet($psProduct['id_product']);
                $this->setProductActive($psProduct['id_product'],false);


                $deleteSql = "DELETE FROM pawn_sold_products WHERE id = :id";
                $stmt = $conn->prepare($deleteSql);
                $stmt->bindValue("id",$product['id']);
                $stmt->execute();
                $conn->commit();
            } catch (Exception $e) {
                //$conn->rollBack();
                // Here we are dealing with errors
                $trace = $e->getTrace();
                if ($trace[0]['args'][0] == 404) echo 'Bad ID';
                else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
                else echo 'Other error<br />'.$e->getMessage();
            }

        }
    }

    protected function getIdStockAvailableAndSet($ProductId = 235935){
        $webService = new PrestaShopWebservice(self::PS_SHOP_PATH,self::PS_WS_AUTH_KEY,self::DEBUG);
        $opt['resource'] = 'products';
        //$opt['filter']['reference'] = '237063472';//328102
        $opt['id'] = $ProductId;
        $xml = $webService->get($opt);

        foreach ($xml->product->associations->stock_availables->stock_available as $item) {
            //echo "ID: ".$item->id."\n\r";
            //echo "Id Attribute: ".$item->id_product_attribute."\n\r";
            $this->set_product_quantity($ProductId, $item->id, $item->id_product_attribute);
        }
    }

    /**
     * @param $ProductId
     * @param $status
     */
    protected function setProductActive($ProductId, $status)
    {
        $webService = new PrestaShopWebservice(self::PS_SHOP_PATH,self::PS_WS_AUTH_KEY,self::DEBUG);
        $xml = $webService->get(array('resource' => 'products', 'id' => $ProductId));
        $resources = $xml->children()->children();

        // Unset fields that may not be updated
        unset($resources->manufacturer_name);
        unset($resources->quantity);

        $resources->id_category_default = 2;
        $resources->id = $ProductId;
        $resources->active = $status;
        //echo $xml->asXML();die;
        try {
            $opt = array('resource' => 'products');
            $opt['putXml'] = $xml->asXML();
            $opt['id'] = $ProductId ;
            $xml = $webService->edit($opt);
        }catch (PrestaShopWebserviceException $ex) {
            echo "<bhow>Error   ->Error : </bhow>".$ex->getMessage().'<br>';
        }
    }

    /**
     * @param $ProductId
     * @param $StokId
     * @param $AttributeId
     */
    protected function set_product_quantity($ProductId, $StokId, $AttributeId){
        $webService = new PrestaShopWebservice(self::PS_SHOP_PATH,self::PS_WS_AUTH_KEY,self::DEBUG);
        $xml = $webService->get(array('url' => self::PS_SHOP_PATH . '/api/stock_availables?schema=blank'));
        $resources = $xml->children()->children();
        $resources->id = $StokId;
        $resources->id_product  = $ProductId;
        $resources->quantity = 0;
        $resources->id_shop = 1;
        $resources->out_of_stock=1;
        $resources->depends_on_stock = 0;
        $resources->id_product_attribute=$AttributeId;
        try {
            $opt = array('resource' => 'stock_availables');
            $opt['putXml'] = $xml->asXML();
            $opt['id'] = $StokId ;
            $xml = $webService->edit($opt);
        }catch (PrestaShopWebserviceException $ex) {
            echo "<b>Error   ->Error : </b>".$ex->getMessage().'<br>';
        }
    }

    /**
     * @param $product
     */
    protected function delete($product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $deleteSql = "DELETE FROM pawn_transfered_products WHERE id = :id";
        $stmt = $conn->prepare($deleteSql);
        $stmt->bindValue("id",$product);
        $stmt->execute();
    }
}