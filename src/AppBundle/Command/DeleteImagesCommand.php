<?php

namespace AppBundle\Command;

use League\Flysystem\Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\Connection;

class DeleteImagesCommand extends ContainerAwareCommand
{
    private $products = [];

    private $path = '../../../../img/p/';

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:delete-images')

            // the short description shown while running "php bin/console list"
            ->setDescription('Accounting sales report')
            ->addArgument('monthly', InputArgument::OPTIONAL, 'monthly?')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('monthly','m',InputOption::VALUE_OPTIONAL)
                ])
            )

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command looks for new products.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');
        $orders = $conn->fetchAll('SELECT * FROM pa_order_notification');


        foreach($orders as $order) {
            $this->getOrderedProducts($order);
        }

        foreach($this->products as $product) {
            echo $product. "\n";
            $images = $this->getImageName($product);//$product
            //$this->deleteImages($images,$product);
        }

    }

		/**
		 * @param $images
		 * @param $id_product
		 */
    protected function deleteImages($images,$id_product)
    {


        foreach($images as $image) {
            $imageFullName = $image['id_image'];
            $imageParts = str_split($image['id_image']);
            $path = implode("/", $imageParts);
            // reset some counters
            $cnt_files=0;
            $cnt_checked=0;

            if ($handle = opendir($this->path.$path."/")) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $cnt_files++;
                        if(is_file($this->path.$path."/".$entry)) {
                            $pi = explode('-', $entry);
                            if ($pi[0] > 0) {
                                $cnt_checked++;
                                if ($pi[0] == $image || $pi[0] == $imageFullName.'.jpg') {
                                    //echo $this->path.$path."/".$entry;
                                    //unlink($this->path.$path."/".$entry);
                                }

                            }
                        }
                    }
                }
                $this->removeImagesFromDatabase($id_product);
            }

        }
    }

    protected function removeImagesFromDatabase($id_product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $imagesIds = $this->getImageIds($id_product);

        $conn->beginTransaction();
        try {

            $deleteSql = "DELETE FROM ps_image WHERE id_product = :id_product";
            $stmt = $conn->prepare($deleteSql);
            $stmt->bindValue("id_product",$id_product);
            $stmt->execute();

            $deleteShopSql = "DELETE FROM ps_image_shop WHERE id_product = :id_product";
            $stmt = $conn->prepare($deleteShopSql);
            $stmt->bindValue("id_product",$id_product);
            $stmt->execute();

            foreach($imagesIds as $image) {
                $deleteSql = "DELETE FROM ps_image_lang WHERE id_image = :id_image";
                $stmt = $conn->prepare($deleteSql);
                $stmt->bindValue("id_image",$image);
                $stmt->execute();
            }


            $conn->commit();
        } catch (Exception $ex) {
            $conn->rollBack();
        }
    }

		/**
		 * @param $id_product
		 * @return array
		 */
    protected function getImageIds($id_product)
    {
        $imageIds = [];
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $imageSql = "SELECT * FROM ps_image WHERE id_product = :id_product";
        $stmt = $conn->prepare($imageSql);
        $stmt->bindValue("id_product",$id_product);
        $stmt->execute();
        $results = $stmt->fetchAll();

        foreach($results as $image) {
            $imageIds[] = $image['id_image'];
        }

        return $imageIds;
    }

    protected function getOrderedProducts($order)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT product_id
                    FROM ps_order_detail
                    WHERE id_order=:id_order";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_order',$order['id_order']);
        $stmt->execute();
        $products = $stmt->fetchAll();

        foreach($products as $product) {
            $this->products[] = $product['product_id'];
        }
    }


		/**
		 * @param $id_product
		 * @return array
		 */
    protected function getImageName($id_product)
    {
        /** @var Connection $conn */
        $conn = $this->getContainer()->get('database_connection');

        $sql = "SELECT *
                    FROM ps_image_shop
                    WHERE id_product=:id_product";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id_product',$id_product);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}