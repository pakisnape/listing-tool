<?php

namespace AppBundle\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Knp\Component\Pager\Event\ItemsEvent;
use Doctrine\DBAL\Query\QueryBuilder;

class PaginationSubscriber implements EventSubscriberInterface
{
    /**
     * @param ItemsEvent $event
     */
    public function items(ItemsEvent $event)
    {
        if ($event->target instanceof QueryBuilder) {
            /** @var QueryBuilder $target */
            $target = $event->target;
            //get the query
            $sql = $target->getSQL();
            //count results
            $qb = clone $target;
            $qb->resetQueryParts()
                ->select('count(*) as cnt')
                ->from('('.$sql.')', 'ololoshke_trololoshke');
            $event->count = $qb->execute()
                ->fetchColumn(0);
            //if there is results
            $event->items = [];
            if ($event->count) {
                $q = clone $target;
                $q->setFirstResult($event->getOffset())
                    ->setMaxResults($event->getLimit());
                $event->items = $q->execute()
                    ->fetchAll();
            }
            $event->stopPropagation();
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            'knp_pager.items' => array('items', 10 /*make sure to transform before any further modifications*/)
        );
    }
}