<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class LastEditedRepository extends EntityRepository
{
	public function getlastEditedByUser($user)
	{
		return  $this->getEntityManager()
			->createQuery('SELECT * FROM AppBundle:LastEdited a')
			->getResult();
	}
}