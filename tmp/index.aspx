﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title id="lblTitle" runat="server"></title>

    <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
    <link rel='dns-prefetch' href='http://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="CashPass &raquo; Feed" href="http://staging111.com/cashpass/feed/" />
    <link rel="alternate" type="application/rss+xml" title="CashPass &raquo; Comments Feed" href="http://staging111.com/cashpass/comments/feed/" />
    <!-- For iPad Retina display -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="#">

    <meta property="og:title" content="Enrollment"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="http://staging111.com/cashpass/enrollment/"/>
    <meta property="og:site_name" content="CashPass"/>
    <meta property="og:description" content="Fast &amp; Easy!

          No Credit Checks!

          No Overdraft Fees!
          Choose Your Plan    Select Card
          Premier Card

          Flat Monthly Fee with Unlimited Purchases

          Flat Retail Load Fee

          $6.95 Monthly Fee

          No Activation Fee

          No Load Fees for Direct Deposits

          Add up to"/>



    <meta property="og:image" content="images/2017/01/logo.png"/>
            <script type="text/javascript">
                window._wpemojiSettings = {"baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/", "ext": ".png", "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/", "svgExt": ".svg", "source": {"concatemoji": "http:\/\/staging111.com\/cashpass\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.2"}};
                !function (a, b, c) {
                    function d(a) {
                        var b, c, d, e, f = String.fromCharCode;
                        if (!k || !k.fillText)
                            return!1;
                        switch (k.clearRect(0, 0, j.width, j.height), k.textBaseline = "top", k.font = "600 32px Arial", a) {
                            case"flag":
                                return k.fillText(f(55356, 56826, 55356, 56819), 0, 0), !(j.toDataURL().length < 3e3) && (k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57331, 65039, 8205, 55356, 57096), 0, 0), b = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57331, 55356, 57096), 0, 0), c = j.toDataURL(), b !== c);
                            case"emoji4":
                                return k.fillText(f(55357, 56425, 55356, 57341, 8205, 55357, 56507), 0, 0), d = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55357, 56425, 55356, 57341, 55357, 56507), 0, 0), e = j.toDataURL(), d !== e
                        }
                        return!1
                    }
                    function e(a) {
                        var c = b.createElement("script");
                        c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
                    }
                    var f, g, h, i, j = b.createElement("canvas"), k = j.getContext && j.getContext("2d");
                    for (i = Array("flag", "emoji4"), c.supports = {everything:!0, everythingExceptFlag:!0}, h = 0; h < i.length; h++)
                        c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]);
                    c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                        c.DOMReady = !0
                    }, c.supports.everything || (g = function () {
                        c.readyCallback()
                    }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)) : (a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function () {
                        "complete" === b.readyState && c.readyCallback()
                    })), f = c.source || {}, f.concatemoji ? e(f.concatemoji) : f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))
                }(window, document, window._wpemojiSettings);
            </script>
            <style type="text/css">
                img.wp-smiley,
                img.emoji {
                    display: inline !important;
                    border: none !important;
                    box-shadow: none !important;
                    height: 1em !important;
                    width: 1em !important;
                    margin: 0 .07em !important;
                    vertical-align: -0.1em !important;
                    background: none !important;
                    padding: 0 !important;
                }
            </style>
            <link rel='stylesheet' id='rs-plugin-settings-css'  href='revslider/css/settings4ee1.css?ver=5.3.1.5' type='text/css' media='all' />
            <style id='rs-plugin-settings-inline-css' type='text/css'>
                #rs-demo-id {}
            </style>
            <link rel='stylesheet' id='avada-stylesheet-css'  href='Avada/assets/css/style.min066b.css?ver=5.0.6' type='text/css' media='all' />
            <link rel='stylesheet' id='child-style-css'  href='css/style.css' type='text/css' media='all' />
            <!--[if lte IE 9]>
            <link rel='stylesheet' id='avada-shortcodes-css'  href='Avada/shortcodes.css' type='text/css' media='all' />
            <![endif]-->
            <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
            <!--[if lte IE 9]>
            <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='font-awesome/css/font-awesome.css' type='text/css' media='all' />
            <![endif]-->
            <!--[if lte IE 8]>
            <link rel='stylesheet' id='avada-IE8-css'  href='Avada/ie8.css?ver=5.0.6' type='text/css' media='all' />
            <![endif]-->
            <!--[if IE]>
            <link rel='stylesheet' id='avada-IE-css'  href='Avada/ie.css?ver=5.0.6' type='text/css' media='all' />
            <![endif]-->
            <link rel='stylesheet' id='avada-iLightbox-css'  href='Avada/ilightbox066b.css?ver=5.0.6' type='text/css' media='all' />
            <link rel='stylesheet' id='avada-animations-css'  href='Avada/animations066b.css?ver=5.0.6' type='text/css' media='all' />
            <link rel='stylesheet' id='fusion-builder-shortcodes-css'  href='css/fusion-shortcodes.min0ba6.css?ver=1.0.6' type='text/css' media='all' />
            <link rel='stylesheet' id='avada-dynamic-css-css'  href='images/avada-styles/avada-128171ea6.css?timestamp=1487868215&amp;ver=5.0.6' type='text/css' media='all' />
            <link rel='stylesheet' id='gforms_reset_css-css'  href='gravityforms/css/formreset.min4c71.css?ver=2.1.3' type='text/css' media='all' />
            <link rel='stylesheet' id='gforms_datepicker_css-css'  href='gravityforms/css/datepicker.min4c71.css?ver=2.1.3' type='text/css' media='all' />
            <link rel='stylesheet' id='gforms_formsmain_css-css'  href='gravityforms/css/formsmain.min4c71.css?ver=2.1.3' type='text/css' media='all' />
            <link rel='stylesheet' id='gforms_ready_class_css-css'  href='gravityforms/css/readyclass.min4c71.css?ver=2.1.3' type='text/css' media='all' />
            <link rel='stylesheet' id='gforms_browsers_css-css'  href='gravityforms/css/browsers.min4c71.css?ver=2.1.3' type='text/css' media='all' />
            <link rel='stylesheet' id='avada_google_fonts-css'  href='https://fonts.googleapis.com/css?family=Montserrat%3A400%7CPT+Sans%3A700&amp;subset=latin' type='text/css' media='all' />
            <script type='text/javascript' src='js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
            <script type='text/javascript' src='js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
            <script type='text/javascript' src='revslider/js/jquery.themepunch.tools.min4ee1.js?ver=5.3.1.5'></script>
            <script type='text/javascript' src='revslider/js/jquery.themepunch.revolution.min4ee1.js?ver=5.3.1.5'></script>
            <!--[if lt IE 9]>
            <script type='text/javascript' src='js/html5shiv.js'></script>
            <![endif]-->
            <!--[if lt IE 9]>
            <script type='text/javascript' src='js/excanvas.js'></script>
            <![endif]-->
            <script type='text/javascript' src='gravityforms/js/jquery.json.min4c71.js?ver=2.1.3'></script>
            <script type='text/javascript' src='gravityforms/js/gravityforms.min4c71.js?ver=2.1.3'></script>
            <script type='text/javascript' src='gravityforms/js/jquery.maskedinput.min4c71.js?ver=2.1.3'></script>
            <link rel='https://api.w.org/' href='http://staging111.com/cashpass/wp-json/' />
            <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
            <meta name="generator" content="Powered by Slider Revolution 5.3.1.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />


            <script type="text/javascript">
                var doc = document.documentElement;
                doc.setAttribute('data-useragent', navigator.userAgent);
            </script>
</head>

<body class="page-template page-template-100-width page-template-100-width-php page page-id-12817 fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-image-hovers fusion-show-pagination-text">
    <div id="wrapper" class="">
        <div id="home" style="position:relative;top:1px;"></div>
        <header class="fusion-header-wrapper">
            <div class="fusion-header-v3 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo- fusion-mobile-menu-design-modern ">

                <div class="fusion-secondary-header">
                    <div class="fusion-row">
                        <div class="fusion-alignright"><div class="fusion-social-links-header"><div class="fusion-social-networks boxed-icons"><div class="fusion-social-networks-wrapper"><a  class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" style="color:#ffffff;background-color:#969594;border-color:#969594;border-radius:4px;" href="https://www.facebook.com/cashpass" target="_blank" rel="noopener noreferrer" data-placement="bottom" data-title="Facebook" data-toggle="tooltip" title="Facebook"><span class="screen-reader-text">Facebook</span></a><a  class="fusion-social-network-icon fusion-tooltip fusion-twitter fusion-icon-twitter" style="color:#ffffff;background-color:#969594;border-color:#969594;border-radius:4px;" href="https://twitter.com/CashPassNetwork" target="_blank" rel="noopener noreferrer" data-placement="bottom" data-title="Twitter" data-toggle="tooltip" title="Twitter"><span class="screen-reader-text">Twitter</span></a><a  class="fusion-social-network-icon fusion-tooltip fusion-linkedin fusion-icon-linkedin" style="color:#ffffff;background-color:#969594;border-color:#969594;border-radius:4px;" href="https://www.linkedin.com/company/cashpass-network" target="_blank" rel="noopener noreferrer" data-placement="bottom" data-title="Linkedin" data-toggle="tooltip" title="Linkedin"><span class="screen-reader-text">Linkedin</span></a><a  class="custom fusion-social-network-icon fusion-tooltip fusion-custom fusion-icon-custom" style="color:#ffffff;position:relative;top:-2px;" href="https://secure-cashpass.pd.gpsrv.com/" target="_blank" rel="noopener noreferrer" data-placement="bottom" data-title="My Account" data-toggle="tooltip" title="My Account"><span class="screen-reader-text">My Account</span><img src="images/2017/01/my_account_btn.png" style="width:auto;max-height:calc(8px + (2 * 2px) + 2px);" alt="My Account" /></a><a  class="custom fusion-social-network-icon fusion-tooltip fusion-custom fusion-icon-custom" style="color:#ffffff;position:relative;top:-2px;" href="https://secure-cashpass.pd.gpsrv.com/register" target="_blank" rel="noopener noreferrer" data-placement="bottom" data-title="Active Card" data-toggle="tooltip" title="Active Card"><span class="screen-reader-text">Active Card</span><img src="images/2017/01/active_card_btn.png" style="width:auto;max-height:calc(8px + (2 * 2px) + 2px);" alt="Active Card" /></a></div></div></div></div>
                    </div>
                </div>
                <div class="fusion-header-sticky-height"></div>
                <div class="fusion-header">
                    <div class="fusion-row">

                        <div class="fusion-logo" data-margin-top="13px" data-margin-bottom="13px" data-margin-left="0px" data-margin-right="0px">
                            <a class="fusion-logo-link" href="#">
                                <img src="images/2017/01/logo.png" width="250" height="60" alt="CashPass" class="fusion-logo-1x fusion-standard-logo" />

                                <img src="images/2017/01/logo.png" width="250" height="60" alt="CashPass" class="fusion-standard-logo fusion-logo-2x" />

                                <!-- mobile logo -->

                                <!-- sticky header logo -->
                            </a>
                        </div>      <nav class="fusion-main-menu">
                            <ul id="menu-cashpass-menu" class="fusion-menu">
                                <li  id="menu-item-12813"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-12813 fusion-dropdown-menu"  ><a  href="#"><span class="menu-text">CashPass Card</span></a>
                                    <ul class="sub-menu">
                                        <li  id="menu-item-12197"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12197 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/how-it-works/"><span class="">How It Works</span></a></li>
                                        <li  id="menu-item-12199"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12199 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/direct-deposit/"><span class="">Direct Deposit</span></a></li>
                                        <li  id="menu-item-12198"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12198 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/tax-returns/"><span class="">Tax Returns</span></a></li>
                                        <li  id="menu-item-12196"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12196 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/get-a-card/"><span class="">Get A Card</span></a></li>
                                        <li  id="menu-item-12195"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12195 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/activate-card/"><span class="">Activate Card</span></a></li>
                                        <li  id="menu-item-12194"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12194 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/load-locations-retailers/"><span class="">Load Locations &#038; Retailers</span></a></li>
                                        <li  id="menu-item-12203"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12203 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/fee-schedule/"><span class="">Fee Schedule</span></a></li>
                                    </ul>
                                </li>
                                <li  id="menu-item-12814"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-12814 fusion-dropdown-menu"  ><a  href="#"><span class="menu-text">CashPass App</span></a><ul class="sub-menu"><li  id="menu-item-12193"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12193 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/overview/"><span class="">Overview</span></a></li></ul></li>
                                <li  id="menu-item-12815"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-12815 fusion-dropdown-menu"  ><a  href="#"><span class="menu-text">CashPass for Business</span></a>
                                    <ul class="sub-menu">
                                        <li  id="menu-item-12201"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12201 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/become-a-distributor/"><span class="">Become a Distributor</span></a></li>
                                        <li  id="menu-item-12192"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12192 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/premium-card-program/"><span class="">Premium Card Program</span></a></li>
                                        <li  id="menu-item-12191"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12191 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/staffing-payroll/"><span class="">Staffing &#038; Payroll</span></a></li>
                                        <li  id="menu-item-12190"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12190 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/business-expense/"><span class="">Business Expense</span></a></li>
                                        <li  id="menu-item-12189"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12189 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/non-profit/"><span class="">Non-Profit</span></a></li>
                                    </ul>
                                </li>
                                <li  id="menu-item-12816"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-12816 fusion-dropdown-menu"  ><a  href="#"><span class="menu-text">Help/Support</span></a><ul class="sub-menu"><li  id="menu-item-12684"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12684 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/faq/"><span class="">FAQ&#8217;S</span></a></li>
                                        <li  id="menu-item-12683"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12683 fusion-dropdown-submenu"  ><a  href="http://staging111.com/cashpass/contact-us/"><span class="">Contact Us</span></a></li>
                                    </ul>
                                </li>
                                <li class="fusion-custom-menu-item fusion-main-menu-search"><a class="fusion-main-menu-icon"></a><div class="fusion-custom-menu-item-contents"><form role="search" class="searchform" method="get" action="http://staging111.com/cashpass/">
                                            <div class="search-table">
                                                <div class="search-field">
                                                    <input type="text" value="" name="s" class="s" placeholder="Search ..." />
                                                </div>
                                                <div class="search-button">
                                                    <input type="submit" class="searchsubmit" value="&#xf002;" />
                                                </div>
                                            </div>
                                        </form>
                                    </div></li></ul></nav>          <div class="fusion-mobile-menu-icons">
                            <a href="#" class="fusion-icon fusion-icon-bars"></a>


                        </div>


                        <nav class="fusion-mobile-nav-holder"></nav>

                    </div>
                </div>
            </div>
            <div class="fusion-clearfix"></div>
        </header>

        <div id="sliders-container">
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all">
            <div id="rev_slider_20_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background-color:#eaeaea;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.3.1.5 auto mode -->
                <div id="rev_slider_20_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.1.5">
                    <ul>    <!-- SLIDE  -->
                        <li data-index="rs-43" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="http://staging111.com/cashpass/wp-content/uploads/2017/02/HeaderEnrollmentPage1400x435-100x50.png"  data-delay="8990"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="images/2017/02/HeaderEnrollmentPage1400x435.png"  alt="" title="HeaderEnrollmentPage1400x435"  width="1400" height="435" data-bgposition="center center" data-kenburns="on" data-duration="9000" data-ease="Linear.easeNone" data-scalestart="150" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-43-layer-13"
                                 data-x=""
                                 data-y="-1"
                                 data-width="['none','none','none','none']"
                                 data-height="['none','none','none','none']"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 5;"><img src="images/revslider/cashpass/slider_shadow.png" alt="" data-ww="1400px" data-hh="435px" width="1400" height="435" data-no-retina> </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                 id="slide-43-layer-6"
                                 data-x="126"
                                 data-y="287"
                                 data-width="['676']"
                                 data-height="['106']"

                                 data-type="shape"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":90,"speed":5000,"frame":"0","from":"x:-200px;skX:85px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":10,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 6;font-family:Open Sans;background-color:rgba(255, 255, 255, 0.90);"> </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-43-layer-19"
                                 data-x="199"
                                 data-y="126"
                                 data-width="['auto']"
                                 data-height="['auto']"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 7; white-space: nowrap; font-size: 40px; line-height: 40px; font-weight: 700; color: rgba(10, 10, 10, 1.00);font-family:Open Sans;">A better way to save, <br>
                                manage and spend your money. </div>
                        </li>
                    </ul>
                    <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                        var htmlDivCss = "";
                        if (htmlDiv) {
                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                            var htmlDiv = document.createElement("div");
                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
                <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                    var htmlDivCss = "";
                    if (htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                    } else {
                        var htmlDiv = document.createElement("div");
                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                    }
                </script>
                <script type="text/javascript">
                    /******************************************
                     -  PREPARE PLACEHOLDER FOR SLIDER  -
                     ******************************************/

                    var setREVStartSize = function () {
                        try {
                            var e = new Object, i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                            e.c = jQuery('#rev_slider_20_1');
                            e.gridwidth = [1400];
                            e.gridheight = [435];

                            e.sliderLayout = "auto";
                            if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                                f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                            }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                                var u = (e.c.width(), jQuery(window).height());
                                if (void 0 != e.fullScreenOffsetContainer) {
                                    var c = e.fullScreenOffsetContainer.split(",");
                                    if (c)
                                        jQuery.each(c, function (e, i) {
                                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                                }
                                f = u
                            } else
                                void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                            e.c.closest(".rev_slider_wrapper").css({height: f})

                        } catch (d) {
                            console.log("Failure at Presize of Slider:" + d)
                        }
                    };

                    setREVStartSize();

                    var tpj = jQuery;

                    var revapi20;
                    tpj(document).ready(function () {
                        if (tpj("#rev_slider_20_1").revolution == undefined) {
                            revslider_showDoubleJqueryError("#rev_slider_20_1");
                        } else {
                            revapi20 = tpj("#rev_slider_20_1").show().revolution({
                                sliderType: "standard",
                                jsFileLocation: "//enrollsecure.cashpass.com/revslider/js/",
                                sliderLayout: "auto",
                                dottedOverlay: "none",
                                delay: 9000,
                                navigation: {
                                    keyboardNavigation: "off",
                                    keyboard_direction: "horizontal",
                                    mouseScrollNavigation: "off",
                                    mouseScrollReverse: "default",
                                    onHoverStop: "off",
                                    touch: {
                                        touchenabled: "on",
                                        swipe_threshold: 75,
                                        swipe_min_touches: 1,
                                        swipe_direction: "horizontal",
                                        drag_block_vertical: false
                                    }
                                    ,
                                    arrows: {
                                        style: "custom",
                                        enable: true,
                                        hide_onmobile: false,
                                        hide_onleave: true,
                                        hide_delay: 200,
                                        hide_delay_mobile: 1200,
                                        tmp: '',
                                        left: {
                                            h_align: "left",
                                            v_align: "center",
                                            h_offset: 20,
                                            v_offset: 0
                                        },
                                        right: {
                                            h_align: "right",
                                            v_align: "center",
                                            h_offset: 20,
                                            v_offset: 0
                                        }
                                    }
                                },
                                visibilityLevels: [1240, 1024, 778, 480],
                                gridwidth: 1400,
                                gridheight: 435,
                                lazyType: "none",
                                shadow: 0,
                                spinner: "spinner3",
                                stopLoop: "off",
                                stopAfterLoops: -1,
                                stopAtSlide: -1,
                                shuffle: "off",
                                autoHeight: "off",
                                disableProgressBar: "on",
                                hideThumbsOnMobile: "off",
                                hideSliderAtLimit: 0,
                                hideCaptionAtLimit: 0,
                                hideAllCaptionAtLilmit: 0,
                                debugMode: false,
                                fallbacks: {
                                    simplifyAll: "off",
                                    nextSlideOnWindowFocus: "off",
                                    disableFocusListener: false,
                                }
                            });
                        }
                    }); /*ready*/
                </script>
                <script>
                    var htmlDivCss = '  #rev_slider_20_1_wrapper .tp-loader.spinner3 div { background-color: #0088cc !important; } ';
                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                    if (htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                    }
                    else {
                        var htmlDiv = document.createElement('div');
                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                    }
                </script>
                <script>
                    var htmlDivCss = unescape(".custom.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3A%23000%3B%0A%09background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%09width%3A40px%3B%0A%09height%3A40px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%7D%0A.custom.tparrows%3Ahover%20%7B%0A%09background%3A%23000%3B%0A%7D%0A.custom.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A15px%3B%0A%09color%3A%23fff%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%2040px%3B%0A%09text-align%3A%20center%3B%0A%7D%0A.custom.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce824%22%3B%0A%7D%0A.custom.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce825%22%3B%0A%7D%0A%0A%0A");
                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                    if (htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                    }
                    else {
                        var htmlDiv = document.createElement('div');
                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                    }
                </script>
            </div><!-- END REVOLUTION SLIDER -->
        </div>
        <div id="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
                <div id="content" class="full-width">
                    <div id="post-12817" class="post-12817 page type-page status-publish hentry">
                        <div class="post-content">
                            <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth" style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-first 1_5"
                                         style='margin-top:0px;margin-bottom:20px;width:20%;width:calc(20% - ( ( 4% + 4% ) * 0.2 ) );margin-right: 4%;'>
                                        <div class="fusion-column-wrapper"
                                             style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-column-content-centered">
                                                <div class="fusion-column-content"></div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth 3_5"
                                         style='margin-top:0px;margin-bottom:20px;width:60%;width:calc(60% - ( ( 4% + 4% ) * 0.6 ) );margin-right: 4%;'>
                                        <div class="fusion-column-wrapper"
                                             style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-column-content-centered">
                                                <div class="fusion-column-content">
                                                    <ul class="fusion-checklist fusion-checklist-1 enroll-checklist"
                                                        style="font-size:17px;line-height:28.9px;">
                                                        <li class="fusion-li-item"><span
                                                                style="height:28.9px;width:28.9px;margin-right:11.9px;"
                                                                class="icon-wrapper circle-no"><i
                                                                class="fusion-li-icon fa fa-check"
                                                                style="color:#8bc34a;"></i></span>
                                                            <div class="fusion-li-item-content" style="margin-left:40.8px;">
                                                                <p>Fast &amp; Easy!</p>
                                                            </div>
                                                        </li>
                                                        <li class="fusion-li-item"><span
                                                                style="height:28.9px;width:28.9px;margin-right:11.9px;"
                                                                class="icon-wrapper circle-no"><i
                                                                class="fusion-li-icon fa fa-check"
                                                                style="color:#8bc34a;"></i></span>
                                                            <div class="fusion-li-item-content" style="margin-left:40.8px;">
                                                                <p>No Credit Checks!</p>
                                                            </div>
                                                        </li>
                                                        <li class="fusion-li-item"><span
                                                                style="height:28.9px;width:28.9px;margin-right:11.9px;"
                                                                class="icon-wrapper circle-no"><i
                                                                class="fusion-li-icon fa fa-check"
                                                                style="color:#8bc34a;"></i></span>
                                                            <div class="fusion-li-item-content" style="margin-left:40.8px;">
                                                                <p>No Overdraft Fees!</p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="fusion-clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-last 1_5"
                                         style='margin-top:0px;margin-bottom:20px;width:20%;width:calc(20% - ( ( 4% + 4% ) * 0.2 ) );'>
                                        <div class="fusion-column-wrapper"
                                             style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-column-content-centered">
                                                <div class="fusion-column-content"></div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth" style='background-color: #ebebeb;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                                         style='margin-top:0px;margin-bottom:20px;'>
                                        <div class="fusion-column-wrapper"
                                             style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <h1 style="text-align: center;">Choose Your Plan</h1>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth" style='background-color: #efeeeb;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"
                                         style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                        <div class="fusion-column-wrapper"
                                             style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="imageframe-align-center"><span
                                                    class="fusion-imageframe imageframe-none imageframe-1 hover-type-none choose_plan_card"><img
                                                    src="images/2014/11/card.jpg" width="228" height="144" alt=""
                                                    title="card" class="img-responsive wp-image-11815"
                                                    /></span></div>
                                            <div class="fusion-button-wrapper fusion-aligncenter">
                                                <style type="text/css"
                                                       scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {
                                                    color: rgba(255, 255, 255, .8);
                                                }

                                                .fusion-button.button-1 {
                                                    border-width: 0px;
                                                    border-color: rgba(255, 255, 255, .8);
                                                }

                                                .fusion-button.button-1 .fusion-button-icon-divider {
                                                    border-color: rgba(255, 255, 255, .8);
                                                }

                                                .fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i, .fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i, .fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active {
                                                    color: rgba(255, 255, 255, .9);
                                                }

                                                .fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active {
                                                    border-width: 0px;
                                                    border-color: rgba(255, 255, 255, .9);
                                                }

                                                .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider {
                                                    border-color: rgba(255, 255, 255, .9);
                                                }

                                                .fusion-button.button-1 {
                                                    background: #ed8100;
                                                }

                                                .fusion-button.button-1:hover, .button-1:focus, .fusion-button.button-1:active {
                                                    background: #c66000;
                                                    background-image: -webkit-gradient(linear, left bottom, left top, from(#e86000), to(#c66000));
                                                    background-image: -webkit-linear-gradient(bottom, #e86000, #c66000);
                                                    background-image: -moz-linear-gradient(bottom, #e86000, #c66000);
                                                    background-image: -o-linear-gradient(bottom, #e86000, #c66000);
                                                    background-image: linear-gradient(to top, #e86000, #c66000);
                                                }

                                                .fusion-button.button-1 {
                                                    width: auto;
                                                }</style>
                                                <a class="fusion-button button-flat button-square button-large button-custom button-1 think_btn"
                                                   target="_self" title="Select Card" href="#" id="cashpasscard_1"><span
                                                        class="fusion-button-text">Select Card</span></a></div>
                                            <p style="text-align: center;"><strong>Premier Card<br/>
                                            </strong></p>
                                            <p style="text-align: center;"><strong><em>Flat Monthly Fee with Unlimited
                                                Purchases</em></strong></p>
                                            <ul class="fusion-checklist fusion-checklist-2 center"
                                                style="font-size:13px;line-height:22.1px;">
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>Flat Retail Load Fee</p>
                                                    </div>
                                                </li>
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>$6.95 Monthly Fee</p>
                                                    </div>
                                                </li>
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>No Activation Fee</p>
                                                    </div>
                                                </li>
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>No Load Fees for Direct Deposits</p>
                                                    </div>
                                                </li>
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>Add up to 5 Companion Cards</p>
                                                    </div>
                                                </li>
                                            </ul>
                                            <p style="text-align: center;"><a
                                                    href="http://staging111.com/cashpass/fee-schedule/">View Fees</a></p>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6  fusion-one-sixth 1_6"
                                         style='margin-top:0px;margin-bottom:20px;width:16.66%;width:calc(16.66% - ( ( 4% + 4% + 4% ) * 0.1666 ) );margin-right: 4%;'>
                                        <div class="fusion-column-wrapper"
                                             style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6  fusion-one-sixth 1_6"
                                         style='margin-top:0px;margin-bottom:20px;width:16.66%;width:calc(16.66% - ( ( 4% + 4% + 4% ) * 0.1666 ) );margin-right: 4%;'>
                                        <div class="fusion-column-wrapper"
                                             style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"
                                         style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% + 4% ) * 0.3333 ) );'>
                                        <div class="fusion-column-wrapper"
                                             style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="imageframe-align-center"><span
                                                    class="fusion-imageframe imageframe-none imageframe-2 hover-type-none choose_plan_card"><img
                                                    src="images/2014/11/card.jpg" width="228" height="144" alt=""
                                                    title="card" class="img-responsive wp-image-11815"
                                                    /></span></div>
                                            <div class="fusion-button-wrapper fusion-aligncenter">
                                                <style type="text/css"
                                                       scoped="scoped">.fusion-button.button-2 .fusion-button-text, .fusion-button.button-2 i {
                                                    color: rgba(255, 255, 255, .8);
                                                }

                                                .fusion-button.button-2 {
                                                    border-width: 0px;
                                                    border-color: rgba(255, 255, 255, .8);
                                                }

                                                .fusion-button.button-2 .fusion-button-icon-divider {
                                                    border-color: rgba(255, 255, 255, .8);
                                                }

                                                .fusion-button.button-2:hover .fusion-button-text, .fusion-button.button-2:hover i, .fusion-button.button-2:focus .fusion-button-text, .fusion-button.button-2:focus i, .fusion-button.button-2:active .fusion-button-text, .fusion-button.button-2:active {
                                                    color: rgba(255, 255, 255, .9);
                                                }

                                                .fusion-button.button-2:hover, .fusion-button.button-2:focus, .fusion-button.button-2:active {
                                                    border-width: 0px;
                                                    border-color: rgba(255, 255, 255, .9);
                                                }

                                                .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:active .fusion-button-icon-divider {
                                                    border-color: rgba(255, 255, 255, .9);
                                                }

                                                .fusion-button.button-2 {
                                                    background: #ed8100;
                                                }

                                                .fusion-button.button-2:hover, .button-2:focus, .fusion-button.button-2:active {
                                                    background: #c66000;
                                                    background-image: -webkit-gradient(linear, left bottom, left top, from(#e86000), to(#c66000));
                                                    background-image: -webkit-linear-gradient(bottom, #e86000, #c66000);
                                                    background-image: -moz-linear-gradient(bottom, #e86000, #c66000);
                                                    background-image: -o-linear-gradient(bottom, #e86000, #c66000);
                                                    background-image: linear-gradient(to top, #e86000, #c66000);
                                                }

                                                .fusion-button.button-2 {
                                                    width: auto;
                                                }</style>
                                                <a class="fusion-button button-flat button-square button-large button-custom button-2 think_btn"
                                                   target="_self" title="SELECT CARD" href="#" id="cashpasscard_2"><span
                                                        class="fusion-button-text">SELECT CARD</span></a></div>
                                            <p style="text-align: center;"><strong>Premier % Card<br/>
                                            </strong></p>
                                            <p style="text-align: center;"><strong><em>Unlimited Signature Transactions</em></strong>
                                            </p>
                                            <ul class="fusion-checklist fusion-checklist-3 center"
                                                style="font-size:13px;line-height:22.1px;">
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>2% Load Fee</p>
                                                    </div>
                                                </li>
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>No Monthly Fee</p>
                                                    </div>
                                                </li>
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>No Activation Fee</p>
                                                    </div>
                                                </li>
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>No Load Fees for Direct Deposits</p>
                                                    </div>
                                                </li>
                                                <li class="fusion-li-item"><span
                                                        style="height:22.1px;width:22.1px;margin-right:9.1px;"
                                                        class="icon-wrapper circle-no"><i class="fusion-li-icon fa fa-check"
                                                                                          style="color:#a0ce4e;"></i></span>
                                                    <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                        <p>Add up to 5 Companion Cards</p>
                                                    </div>
                                                </li>
                                            </ul>
                                            <p style="text-align: center;"><a
                                                    href="http://staging111.com/cashpass/fee-schedule/">View Fees</a></p>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth" style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:40px;padding-left:30px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:20px;'>
                                        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:20px;'>
                                            <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                                <div class='gf_browser_unknown gform_wrapper' id='gform_wrapper_2' >
                                                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                                    <!-- Design Information -->
                                                    <div id="plDesign" runat="server"></div>
                                                    <form id="form1" runat="server">
                                                        <div class='gform_body'>
                                                            <ul id='gform_fields_2' class='gform_fields top_label form_sublabel_below description_below'>
                                                                <li id='field_2_16' class='gfield field_sublabel_below field_description_below gfield_visibility_visible'>
                                                                    <label class='gfield_label'>Card Type</label>
                                                                    <div class='ginput_container ginput_container_radio'>
                                                                        <asp:RadioButtonList ID="rlistProducts" runat="server" DataTextField="Image" DataValueField="Id"  AutoPostBack="True" OnSelectedIndexChanged="rlistProducts_SelectedIndexChanged" ></asp:RadioButtonList>
                                                                    </div>
                                                                </li>
                                                                <li id='field_2_17' class='gfield gsection field_sublabel_below field_description_below gfield_visibility_visible'>
                                                                    <h2 class='gsection_title'>Personal Information</h2>
                                                                </li>
                                                                <li id='field_2_1' class='gfield gfield_contains_required field_sublabel_below field_description_above gfield_visibility_visible'>
                                                                    <label class='gfield_label' for='input_2_1'>First Name<span class='gfield_required'>*</span></label>
                                                                    <div class='ginput_container ginput_container_text'>
                                                                        <asp:TextBox ID="txtLast" cssClass="large" runat="server" TabIndex="4"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                                          ControlToValidate="txtLast" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </li>
                                                                <li id='field_2_2' class='gfield gfield_contains_required field_sublabel_below field_description_above gfield_visibility_visible'>
                                                                    <label class='gfield_label' for='input_2_2'>Last Name<span class='gfield_required'>*</span></label>
                                                                    <div class='ginput_container ginput_container_text'>
                                                                        <asp:TextBox ID="txtLast"  cssClass="large" runat="server" TabIndex="4"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                                            ControlToValidate="txtLast" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </li>
                                                                <li id='field_2_2' class='gfield gfield_contains_required field_sublabel_below field_description_above gfield_visibility_visible'>
                                                                    <label class='gfield_label' for='input_2_2'>Date of Birth<span class='gfield_required'>*</span></label>
                                                                    <asp:DropDownList ID="ddlDOBM" runat="server" TabIndex="6" CssClass="help">
                                                                        <asp:ListItem value="Month">Month</asp:ListItem>
                                                                        <asp:ListItem value="01">January</asp:ListItem>
                                                                        <asp:ListItem value="02">February</asp:ListItem>
                                                                        <asp:ListItem value="03">March</asp:ListItem>
                                                                        <asp:ListItem value="04">April</asp:ListItem>
                                                                        <asp:ListItem value="05">May</asp:ListItem>
                                                                        <asp:ListItem value="06">June</asp:ListItem>
                                                                        <asp:ListItem value="07">July</asp:ListItem>
                                                                        <asp:ListItem value="08">August</asp:ListItem>
                                                                        <asp:ListItem value="09">September</asp:ListItem>
                                                                        <asp:ListItem value="10">October</asp:ListItem>
                                                                        <asp:ListItem value="11">November</asp:ListItem>
                                                                        <asp:ListItem value="12">December</asp:ListItem>
                                                                      </asp:DropDownList>
                                                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server"
                                                                            ControlToValidate="ddlDOBM" Display="Dynamic" ErrorMessage="*"
                                                                            InitialValue="Month" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                    -&nbsp;<asp:DropDownList ID="ddlDOBD" runat="server" TabIndex="7">
                                                                        <asp:ListItem value="Day">Day</asp:ListItem>
                                                                        <asp:ListItem value="01">1</asp:ListItem>
                                                                        <asp:ListItem value="02">2</asp:ListItem>
                                                                        <asp:ListItem value="03">3</asp:ListItem>
                                                                        <asp:ListItem value="04">4</asp:ListItem>
                                                                        <asp:ListItem value="05">5</asp:ListItem>
                                                                        <asp:ListItem value="06">6</asp:ListItem>
                                                                        <asp:ListItem value="07">7</asp:ListItem>
                                                                        <asp:ListItem value="08">8</asp:ListItem>
                                                                        <asp:ListItem value="09">9</asp:ListItem>
                                                                        <asp:ListItem value="10">10</asp:ListItem>
                                                                        <asp:ListItem value="11">11</asp:ListItem>
                                                                        <asp:ListItem value="12">12</asp:ListItem>
                                                                        <asp:ListItem value="13">13</asp:ListItem>
                                                                        <asp:ListItem value="14">14</asp:ListItem>
                                                                        <asp:ListItem value="15">15</asp:ListItem>
                                                                        <asp:ListItem value="16">16</asp:ListItem>
                                                                        <asp:ListItem value="17">17</asp:ListItem>
                                                                        <asp:ListItem value="18">18</asp:ListItem>
                                                                        <asp:ListItem value="19">19</asp:ListItem>
                                                                        <asp:ListItem value="20">20</asp:ListItem>
                                                                        <asp:ListItem value="21">21</asp:ListItem>
                                                                        <asp:ListItem value="22">22</asp:ListItem>
                                                                        <asp:ListItem value="23">23</asp:ListItem>
                                                                        <asp:ListItem value="24">24</asp:ListItem>
                                                                        <asp:ListItem value="25">25</asp:ListItem>
                                                                        <asp:ListItem value="26">26</asp:ListItem>
                                                                        <asp:ListItem value="27">27</asp:ListItem>
                                                                        <asp:ListItem value="28">28</asp:ListItem>
                                                                        <asp:ListItem value="29">29</asp:ListItem>
                                                                        <asp:ListItem value="30">30</asp:ListItem>
                                                                        <asp:ListItem value="31">31</asp:ListItem>
                                                                      </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server"
                                                                            ControlToValidate="ddlDOBD" Display="Dynamic" ErrorMessage="*"
                                                                            InitialValue="Day" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                   -
                                                                   <asp:DropDownList ID="ddlDOBY" runat="server" TabIndex="8">
                                                                        <asp:ListItem value="Year">Year</asp:ListItem>
                                                                <asp:ListItem value="1996">1996</asp:ListItem>
                                                                <asp:ListItem value="1995">1995</asp:ListItem>
                                                                        <asp:ListItem value="1994">1994</asp:ListItem>
                                                                        <asp:ListItem value="1993">1993</asp:ListItem>
                                                                        <asp:ListItem value="1992">1992</asp:ListItem>
                                                                        <asp:ListItem value="1991">1991</asp:ListItem>
                                                                        <asp:ListItem value="1990">1990</asp:ListItem>
                                                                        <asp:ListItem value="1989">1989</asp:ListItem>
                                                                        <asp:ListItem value="1988">1988</asp:ListItem>
                                                                        <asp:ListItem value="1987">1987</asp:ListItem>
                                                                        <asp:ListItem value="1986">1986</asp:ListItem>
                                                                        <asp:ListItem value="1985">1985</asp:ListItem>
                                                                        <asp:ListItem value="1984">1984</asp:ListItem>
                                                                        <asp:ListItem value="1983">1983</asp:ListItem>
                                                                        <asp:ListItem value="1982">1982</asp:ListItem>
                                                                        <asp:ListItem value="1981">1981</asp:ListItem>
                                                                        <asp:ListItem value="1980">1980</asp:ListItem>
                                                                        <asp:ListItem value="1979">1979</asp:ListItem>
                                                                        <asp:ListItem value="1978">1978</asp:ListItem>
                                                                        <asp:ListItem value="1977">1977</asp:ListItem>
                                                                        <asp:ListItem value="1976">1976</asp:ListItem>
                                                                        <asp:ListItem value="1975">1975</asp:ListItem>
                                                                        <asp:ListItem value="1974">1974</asp:ListItem>
                                                                        <asp:ListItem value="1973">1973</asp:ListItem>
                                                                        <asp:ListItem value="1972">1972</asp:ListItem>
                                                                        <asp:ListItem value="1971">1971</asp:ListItem>
                                                                        <asp:ListItem value="1970">1970</asp:ListItem>
                                                                        <asp:ListItem value="1969">1969</asp:ListItem>
                                                                        <asp:ListItem value="1968">1968</asp:ListItem>
                                                                        <asp:ListItem value="1967">1967</asp:ListItem>
                                                                        <asp:ListItem value="1966">1966</asp:ListItem>
                                                                        <asp:ListItem value="1965">1965</asp:ListItem>
                                                                        <asp:ListItem value="1964">1964</asp:ListItem>
                                                                        <asp:ListItem value="1963">1963</asp:ListItem>
                                                                        <asp:ListItem value="1962">1962</asp:ListItem>
                                                                        <asp:ListItem value="1961">1961</asp:ListItem>
                                                                        <asp:ListItem value="1960">1960</asp:ListItem>
                                                                        <asp:ListItem value="1959">1959</asp:ListItem>
                                                                        <asp:ListItem value="1958">1958</asp:ListItem>
                                                                        <asp:ListItem value="1957">1957</asp:ListItem>
                                                                        <asp:ListItem value="1956">1956</asp:ListItem>
                                                                        <asp:ListItem value="1955">1955</asp:ListItem>
                                                                        <asp:ListItem value="1954">1954</asp:ListItem>
                                                                        <asp:ListItem value="1953">1953</asp:ListItem>
                                                                        <asp:ListItem value="1952">1952</asp:ListItem>
                                                                        <asp:ListItem value="1951">1951</asp:ListItem>
                                                                        <asp:ListItem value="1950">1950</asp:ListItem>
                                                                        <asp:ListItem value="1949">1949</asp:ListItem>
                                                                        <asp:ListItem value="1948">1948</asp:ListItem>
                                                                        <asp:ListItem value="1947">1947</asp:ListItem>
                                                                        <asp:ListItem value="1946">1946</asp:ListItem>
                                                                        <asp:ListItem value="1945">1945</asp:ListItem>
                                                                        <asp:ListItem value="1944">1944</asp:ListItem>
                                                                        <asp:ListItem value="1943">1943</asp:ListItem>
                                                                        <asp:ListItem value="1942">1942</asp:ListItem>
                                                                        <asp:ListItem value="1941">1941</asp:ListItem>
                                                                        <asp:ListItem value="1940">1940</asp:ListItem>
                                                                        <asp:ListItem value="1939">1939</asp:ListItem>
                                                                        <asp:ListItem value="1938">1938</asp:ListItem>
                                                                        <asp:ListItem value="1937">1937</asp:ListItem>
                                                                        <asp:ListItem value="1936">1936</asp:ListItem>
                                                                        <asp:ListItem value="1935">1935</asp:ListItem>
                                                                        <asp:ListItem value="1934">1934</asp:ListItem>
                                                                        <asp:ListItem value="1933">1933</asp:ListItem>
                                                                        <asp:ListItem value="1932">1932</asp:ListItem>
                                                                        <asp:ListItem value="1931">1931</asp:ListItem>
                                                                        <asp:ListItem value="1930">1930</asp:ListItem>
                                                                        <asp:ListItem value="1929">1929</asp:ListItem>
                                                                        <asp:ListItem value="1928">1928</asp:ListItem>
                                                                        <asp:ListItem value="1927">1927</asp:ListItem>
                                                                        <asp:ListItem value="1926">1926</asp:ListItem>
                                                                        <asp:ListItem value="1925">1925</asp:ListItem>
                                                                        <asp:ListItem value="1924">1924</asp:ListItem>
                                                                        <asp:ListItem value="1923">1923</asp:ListItem>
                                                                        <asp:ListItem value="1922">1922</asp:ListItem>
                                                                        <asp:ListItem value="1921">1921</asp:ListItem>
                                                                        <asp:ListItem value="1920">1920</asp:ListItem>
                                                                        <asp:ListItem value="1919">1919</asp:ListItem>
                                                                        <asp:ListItem value="1918">1918</asp:ListItem>
                                                                        <asp:ListItem value="1917">1917</asp:ListItem>
                                                                        <asp:ListItem value="1916">1916</asp:ListItem>
                                                                        <asp:ListItem value="1915">1915</asp:ListItem>
                                                                        <asp:ListItem value="1914">1914</asp:ListItem>
                                                                        <asp:ListItem value="1913">1913</asp:ListItem>
                                                                        <asp:ListItem value="1912">1912</asp:ListItem>
                                                                        <asp:ListItem value="1911">1911</asp:ListItem>
                                                                        <asp:ListItem value="1910">1910</asp:ListItem>
                                                                        <asp:ListItem value="1909">1909</asp:ListItem>
                                                                        <asp:ListItem value="1908">1908</asp:ListItem>
                                                                        <asp:ListItem value="1907">1907</asp:ListItem>
                                                                        <asp:ListItem value="1906">1906</asp:ListItem>
                                                                        <asp:ListItem value="1905">1905</asp:ListItem>
                                                                        <asp:ListItem value="1904">1904</asp:ListItem>
                                                                        <asp:ListItem value="1903">1903</asp:ListItem>
                                                                        <asp:ListItem value="1902">1902</asp:ListItem>
                                                                        <asp:ListItem value="1901">1901</asp:ListItem>
                                                                        <asp:ListItem value="1900">1900</asp:ListItem>
                                                                      </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server"
                                                                            ControlToValidate="ddlDOBY" Display="Dynamic" ErrorMessage="*"
                                                                            InitialValue="Year" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        // end of new form


                                                        <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">

                                                          <tr>
                                                            <td class="main"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                              <tr>
                                                                <td valign="top">
                                                                      
                                                                  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                      <td >
                                                                      <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                                                          <tr>
                                                                            <td height="25" colspan="2"><strong><img src="images/bullet.gif" width="7" height="5" vspace="3" />&nbsp;&nbsp;PERSONAL INFORMATION</strong></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td height="10" colspan="2"></td>
                                                                          </tr>

                                                                          <tr>
                                                                            <td class="label">Last Name:</td>
                                                                            <td>
                                                                                  </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td class="label">Date of Birth:</td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlDOBM" runat="server" TabIndex="6" CssClass="help">
                                                                                        <asp:ListItem value="Month">Month</asp:ListItem>
                                                                                        <asp:ListItem value="01">January</asp:ListItem>
                                                                                        <asp:ListItem value="02">February</asp:ListItem>
                                                                                        <asp:ListItem value="03">March</asp:ListItem>
                                                                                        <asp:ListItem value="04">April</asp:ListItem>
                                                                                        <asp:ListItem value="05">May</asp:ListItem>
                                                                                        <asp:ListItem value="06">June</asp:ListItem>
                                                                                        <asp:ListItem value="07">July</asp:ListItem>
                                                                                        <asp:ListItem value="08">August</asp:ListItem>
                                                                                        <asp:ListItem value="09">September</asp:ListItem>
                                                                                        <asp:ListItem value="10">October</asp:ListItem>
                                                                                        <asp:ListItem value="11">November</asp:ListItem>
                                                                                        <asp:ListItem value="12">December</asp:ListItem>
                                                                                      </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                                                                                            ControlToValidate="ddlDOBM" Display="Dynamic" ErrorMessage="*" 
                                                                                            InitialValue="Month" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                -&nbsp;<asp:DropDownList ID="ddlDOBD" runat="server" TabIndex="7">
                                                                                        <asp:ListItem value="Day">Day</asp:ListItem>
                                                                                        <asp:ListItem value="01">1</asp:ListItem>
                                                                                        <asp:ListItem value="02">2</asp:ListItem>
                                                                                        <asp:ListItem value="03">3</asp:ListItem>
                                                                                        <asp:ListItem value="04">4</asp:ListItem>
                                                                                        <asp:ListItem value="05">5</asp:ListItem>
                                                                                        <asp:ListItem value="06">6</asp:ListItem>
                                                                                        <asp:ListItem value="07">7</asp:ListItem>
                                                                                        <asp:ListItem value="08">8</asp:ListItem>
                                                                                        <asp:ListItem value="09">9</asp:ListItem>
                                                                                        <asp:ListItem value="10">10</asp:ListItem>
                                                                                        <asp:ListItem value="11">11</asp:ListItem>
                                                                                        <asp:ListItem value="12">12</asp:ListItem>
                                                                                        <asp:ListItem value="13">13</asp:ListItem>
                                                                                        <asp:ListItem value="14">14</asp:ListItem>
                                                                                        <asp:ListItem value="15">15</asp:ListItem>
                                                                                        <asp:ListItem value="16">16</asp:ListItem>
                                                                                        <asp:ListItem value="17">17</asp:ListItem>
                                                                                        <asp:ListItem value="18">18</asp:ListItem>
                                                                                        <asp:ListItem value="19">19</asp:ListItem>
                                                                                        <asp:ListItem value="20">20</asp:ListItem>
                                                                                        <asp:ListItem value="21">21</asp:ListItem>
                                                                                        <asp:ListItem value="22">22</asp:ListItem>
                                                                                        <asp:ListItem value="23">23</asp:ListItem>
                                                                                        <asp:ListItem value="24">24</asp:ListItem>
                                                                                        <asp:ListItem value="25">25</asp:ListItem>
                                                                                        <asp:ListItem value="26">26</asp:ListItem>
                                                                                        <asp:ListItem value="27">27</asp:ListItem>
                                                                                        <asp:ListItem value="28">28</asp:ListItem>
                                                                                        <asp:ListItem value="29">29</asp:ListItem>
                                                                                        <asp:ListItem value="30">30</asp:ListItem>
                                                                                        <asp:ListItem value="31">31</asp:ListItem>
                                                                                      </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" 
                                                                                            ControlToValidate="ddlDOBD" Display="Dynamic" ErrorMessage="*" 
                                                                                            InitialValue="Day" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                -
                                                                                <asp:DropDownList ID="ddlDOBY" runat="server" TabIndex="8">
                                                                                        <asp:ListItem value="Year">Year</asp:ListItem>
                                                        				        <asp:ListItem value="1996">1996</asp:ListItem>
                                                        				        <asp:ListItem value="1995">1995</asp:ListItem>
                                                                                        <asp:ListItem value="1994">1994</asp:ListItem>
                                                                                        <asp:ListItem value="1993">1993</asp:ListItem>
                                                                                        <asp:ListItem value="1992">1992</asp:ListItem>
                                                                                        <asp:ListItem value="1991">1991</asp:ListItem>
                                                                                        <asp:ListItem value="1990">1990</asp:ListItem>
                                                                                        <asp:ListItem value="1989">1989</asp:ListItem>
                                                                                        <asp:ListItem value="1988">1988</asp:ListItem>
                                                                                        <asp:ListItem value="1987">1987</asp:ListItem>
                                                                                        <asp:ListItem value="1986">1986</asp:ListItem>
                                                                                        <asp:ListItem value="1985">1985</asp:ListItem>
                                                                                        <asp:ListItem value="1984">1984</asp:ListItem>
                                                                                        <asp:ListItem value="1983">1983</asp:ListItem>
                                                                                        <asp:ListItem value="1982">1982</asp:ListItem>
                                                                                        <asp:ListItem value="1981">1981</asp:ListItem>
                                                                                        <asp:ListItem value="1980">1980</asp:ListItem>
                                                                                        <asp:ListItem value="1979">1979</asp:ListItem>
                                                                                        <asp:ListItem value="1978">1978</asp:ListItem>
                                                                                        <asp:ListItem value="1977">1977</asp:ListItem>
                                                                                        <asp:ListItem value="1976">1976</asp:ListItem>
                                                                                        <asp:ListItem value="1975">1975</asp:ListItem>
                                                                                        <asp:ListItem value="1974">1974</asp:ListItem>
                                                                                        <asp:ListItem value="1973">1973</asp:ListItem>
                                                                                        <asp:ListItem value="1972">1972</asp:ListItem>
                                                                                        <asp:ListItem value="1971">1971</asp:ListItem>
                                                                                        <asp:ListItem value="1970">1970</asp:ListItem>
                                                                                        <asp:ListItem value="1969">1969</asp:ListItem>
                                                                                        <asp:ListItem value="1968">1968</asp:ListItem>
                                                                                        <asp:ListItem value="1967">1967</asp:ListItem>
                                                                                        <asp:ListItem value="1966">1966</asp:ListItem>
                                                                                        <asp:ListItem value="1965">1965</asp:ListItem>
                                                                                        <asp:ListItem value="1964">1964</asp:ListItem>
                                                                                        <asp:ListItem value="1963">1963</asp:ListItem>
                                                                                        <asp:ListItem value="1962">1962</asp:ListItem>
                                                                                        <asp:ListItem value="1961">1961</asp:ListItem>
                                                                                        <asp:ListItem value="1960">1960</asp:ListItem>
                                                                                        <asp:ListItem value="1959">1959</asp:ListItem>
                                                                                        <asp:ListItem value="1958">1958</asp:ListItem>
                                                                                        <asp:ListItem value="1957">1957</asp:ListItem>
                                                                                        <asp:ListItem value="1956">1956</asp:ListItem>
                                                                                        <asp:ListItem value="1955">1955</asp:ListItem>
                                                                                        <asp:ListItem value="1954">1954</asp:ListItem>
                                                                                        <asp:ListItem value="1953">1953</asp:ListItem>
                                                                                        <asp:ListItem value="1952">1952</asp:ListItem>
                                                                                        <asp:ListItem value="1951">1951</asp:ListItem>
                                                                                        <asp:ListItem value="1950">1950</asp:ListItem>
                                                                                        <asp:ListItem value="1949">1949</asp:ListItem>
                                                                                        <asp:ListItem value="1948">1948</asp:ListItem>
                                                                                        <asp:ListItem value="1947">1947</asp:ListItem>
                                                                                        <asp:ListItem value="1946">1946</asp:ListItem>
                                                                                        <asp:ListItem value="1945">1945</asp:ListItem>
                                                                                        <asp:ListItem value="1944">1944</asp:ListItem>
                                                                                        <asp:ListItem value="1943">1943</asp:ListItem>
                                                                                        <asp:ListItem value="1942">1942</asp:ListItem>
                                                                                        <asp:ListItem value="1941">1941</asp:ListItem>
                                                                                        <asp:ListItem value="1940">1940</asp:ListItem>
                                                                                        <asp:ListItem value="1939">1939</asp:ListItem>
                                                                                        <asp:ListItem value="1938">1938</asp:ListItem>
                                                                                        <asp:ListItem value="1937">1937</asp:ListItem>
                                                                                        <asp:ListItem value="1936">1936</asp:ListItem>
                                                                                        <asp:ListItem value="1935">1935</asp:ListItem>
                                                                                        <asp:ListItem value="1934">1934</asp:ListItem>
                                                                                        <asp:ListItem value="1933">1933</asp:ListItem>
                                                                                        <asp:ListItem value="1932">1932</asp:ListItem>
                                                                                        <asp:ListItem value="1931">1931</asp:ListItem>
                                                                                        <asp:ListItem value="1930">1930</asp:ListItem>
                                                                                        <asp:ListItem value="1929">1929</asp:ListItem>
                                                                                        <asp:ListItem value="1928">1928</asp:ListItem>
                                                                                        <asp:ListItem value="1927">1927</asp:ListItem>
                                                                                        <asp:ListItem value="1926">1926</asp:ListItem>
                                                                                        <asp:ListItem value="1925">1925</asp:ListItem>
                                                                                        <asp:ListItem value="1924">1924</asp:ListItem>
                                                                                        <asp:ListItem value="1923">1923</asp:ListItem>
                                                                                        <asp:ListItem value="1922">1922</asp:ListItem>
                                                                                        <asp:ListItem value="1921">1921</asp:ListItem>
                                                                                        <asp:ListItem value="1920">1920</asp:ListItem>
                                                                                        <asp:ListItem value="1919">1919</asp:ListItem>
                                                                                        <asp:ListItem value="1918">1918</asp:ListItem>
                                                                                        <asp:ListItem value="1917">1917</asp:ListItem>
                                                                                        <asp:ListItem value="1916">1916</asp:ListItem>
                                                                                        <asp:ListItem value="1915">1915</asp:ListItem>
                                                                                        <asp:ListItem value="1914">1914</asp:ListItem>
                                                                                        <asp:ListItem value="1913">1913</asp:ListItem>
                                                                                        <asp:ListItem value="1912">1912</asp:ListItem>
                                                                                        <asp:ListItem value="1911">1911</asp:ListItem>
                                                                                        <asp:ListItem value="1910">1910</asp:ListItem>
                                                                                        <asp:ListItem value="1909">1909</asp:ListItem>
                                                                                        <asp:ListItem value="1908">1908</asp:ListItem>
                                                                                        <asp:ListItem value="1907">1907</asp:ListItem>
                                                                                        <asp:ListItem value="1906">1906</asp:ListItem>
                                                                                        <asp:ListItem value="1905">1905</asp:ListItem>
                                                                                        <asp:ListItem value="1904">1904</asp:ListItem>
                                                                                        <asp:ListItem value="1903">1903</asp:ListItem>
                                                                                        <asp:ListItem value="1902">1902</asp:ListItem>
                                                                                        <asp:ListItem value="1901">1901</asp:ListItem>
                                                                                        <asp:ListItem value="1900">1900</asp:ListItem>
                                                                                      </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                                                                                            ControlToValidate="ddlDOBY" Display="Dynamic" ErrorMessage="*" 
                                                                                            InitialValue="Year" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td class="label">SSN:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSSN1" runat="server" Width="50px" MaxLength="3" onKeyUp="return autoTab(this, 3, event);" TabIndex="9"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                                                                                      ControlToValidate="txtSSN1" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                -
                                                                                <asp:TextBox ID="txtSSN2" runat="server" Width="50px" MaxLength="2" onKeyUp="return autoTab(this, 2, event);" TabIndex="10" ></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                                                                                      ControlToValidate="txtSSN2" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                -
                                                                                <asp:TextBox ID="txtSSN3" runat="server" Width="50px" MaxLength="4" onKeyUp="return autoTab(this, 4, event);" TabIndex="11" ></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                                                                                      ControlToValidate="txtSSN3" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td class="label">Re-Enter SSN:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSSNConfirm1" runat="server" Width="50px" MaxLength="3" onKeyUp="return autoTab(this, 3, event);" TabIndex="12" ></asp:TextBox><asp:RequiredFieldValidator
                                                                                      ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtSSNConfirm1"
                                                                                      Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtSSN1" 
                                                                                      ControlToValidate="txtSSNConfirm1" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:CompareValidator>
                                                                                -
                                                                                    <asp:TextBox ID="txtSSNConfirm2" runat="server" Width="50px" MaxLength="2" onKeyUp="return autoTab(this, 2, event);" TabIndex="13" ></asp:TextBox><asp:RequiredFieldValidator
                                                                                        ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtSSNConfirm2"
                                                                                        Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator2" runat="server" 
                                                                                      ControlToCompare="txtSSN2" ControlToValidate="txtSSNConfirm2" Display="Dynamic" 
                                                                                      ErrorMessage="*" SetFocusOnError="True"></asp:CompareValidator>
                                                                                -
                                                                                <asp:TextBox ID="txtSSNConfirm3" runat="server" Width="50px" MaxLength="4" onKeyUp="return autoTab(this, 4, event);" TabIndex="14" ></asp:TextBox><asp:RequiredFieldValidator
                                                                                    ID="RequiredFieldValidator23" runat="server" ControlToValidate="txtSSNConfirm3"
                                                                                    Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator3" runat="server" 
                                                                                      ControlToCompare="txtSSN3" ControlToValidate="txtSSNConfirm3" Display="Dynamic" 
                                                                                      ErrorMessage="*" SetFocusOnError="True"></asp:CompareValidator>
                                                                            </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td class="label">Email Address:</td>
                                                                            <td>
                                                                                  <asp:TextBox ID="txtEmail" runat="server" TabIndex="15"></asp:TextBox><asp:RequiredFieldValidator
                                                                                          ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtEmail" Display="Dynamic"
                                                                                          ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td class="label">Street Address:</td>
                                                                            <td>
                                                                                      <asp:TextBox ID="txtStreet" runat="server" TabIndex="16"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                                                      ControlToValidate="txtStreet" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator><span
                                                                                          class="style1">(No P.O. Box Numbers Please)</span>
                                                                            </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td class="label">Apartment #:</td>
                                                                            <td>
                                                                                  <asp:TextBox ID="txtAptNo" runat="server" TabIndex="17"></asp:TextBox></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td class="label">City:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtCity" runat="server" TabIndex="18"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                                                      ControlToValidate="txtCity" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                &nbsp;&nbsp;
                                                                                <asp:DropDownList ID="ddlState" runat="server" Width="103px" TabIndex="19">
                                                                                    <asp:ListItem value="State">State</asp:ListItem>
                                                                                    <asp:ListItem value="AL">Alabama</asp:ListItem>
                                                                                    <asp:ListItem value="AK">Alaska</asp:ListItem>
                                                                                    <asp:ListItem value="AZ">Arizona</asp:ListItem>
                                                                                    <asp:ListItem value="AR">Arkansas</asp:ListItem>
                                                                                    <asp:ListItem value="CA">California</asp:ListItem>
                                                                                    <asp:ListItem value="CO">Colorado</asp:ListItem>
                                                                                    <asp:ListItem value="CT">Connecticut</asp:ListItem>
                                                                                    <asp:ListItem value="DE">Delaware</asp:ListItem>
                                                                                    <asp:ListItem value="DC">District Of Columbia</asp:ListItem>
                                                                                    <asp:ListItem value="FL">Florida</asp:ListItem>
                                                                                    <asp:ListItem value="GA">Georgia</asp:ListItem>
                                                                                    <asp:ListItem value="HI">Hawaii</asp:ListItem>
                                                                                    <asp:ListItem value="ID">Idaho</asp:ListItem>
                                                                                    <asp:ListItem value="IL">Illinois</asp:ListItem>
                                                                                    <asp:ListItem value="IN">Indiana</asp:ListItem>
                                                                                    <asp:ListItem value="IA">Iowa</asp:ListItem>
                                                                                    <asp:ListItem value="KS">Kansas</asp:ListItem>
                                                                                    <asp:ListItem value="KY">Kentucky</asp:ListItem>
                                                                                    <asp:ListItem value="LA">Louisiana</asp:ListItem>
                                                                                    <asp:ListItem value="ME">Maine</asp:ListItem>
                                                                                    <asp:ListItem value="MD">Maryland</asp:ListItem>
                                                                                    <asp:ListItem value="MA">Massachusetts</asp:ListItem>
                                                                                    <asp:ListItem value="MI">Michigan</asp:ListItem>
                                                                                    <asp:ListItem value="MN">Minnesota</asp:ListItem>
                                                                                    <asp:ListItem value="MS">Mississippi</asp:ListItem>
                                                                                    <asp:ListItem value="MO">Missouri</asp:ListItem>
                                                                                    <asp:ListItem value="MT">Montana</asp:ListItem>
                                                                                    <asp:ListItem value="NE">Nebraska</asp:ListItem>
                                                                                    <asp:ListItem value="NV">Nevada</asp:ListItem>
                                                                                    <asp:ListItem value="NH">New Hampshire</asp:ListItem>
                                                                                    <asp:ListItem value="NJ">New Jersey</asp:ListItem>
                                                                                    <asp:ListItem value="NM">New Mexico</asp:ListItem>
                                                                                    <asp:ListItem value="NY">New York</asp:ListItem>
                                                                                    <asp:ListItem value="NC">North Carolina</asp:ListItem>
                                                                                    <asp:ListItem value="ND">North Dakota</asp:ListItem>
                                                                                    <asp:ListItem value="OH">Ohio</asp:ListItem>
                                                                                    <asp:ListItem value="OK">Oklahoma</asp:ListItem>
                                                                                    <asp:ListItem value="OR">Oregon</asp:ListItem>
                                                                                    <asp:ListItem value="PA">Pennsylvania</asp:ListItem>
                                                                                    <asp:ListItem value="RI">Rhode Island</asp:ListItem>
                                                                                    <asp:ListItem value="SC">South Carolina</asp:ListItem>
                                                                                    <asp:ListItem value="SD">South Dakota</asp:ListItem>
                                                                                    <asp:ListItem value="TN">Tennessee</asp:ListItem>
                                                                                    <asp:ListItem value="TX">Texas</asp:ListItem>
                                                                                    <asp:ListItem value="UT">Utah</asp:ListItem>
                                                                                    <asp:ListItem value="VT">Vermont</asp:ListItem>
                                                                                    <asp:ListItem value="VA">Virginia</asp:ListItem>
                                                                                    <asp:ListItem value="WA">Washington</asp:ListItem>
                                                                                    <asp:ListItem value="WV">West Virginia</asp:ListItem>
                                                                                    <asp:ListItem value="WI">Wisconsin</asp:ListItem>
                                                                                    <asp:ListItem value="WY">Wyoming</asp:ListItem>
                                                                                </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                                                                      ControlToValidate="ddlState" Display="Dynamic" ErrorMessage="*" 
                                                                                      InitialValue="State" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td class="label">Zip Code:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtZip1" runat="server" Width="80px" MaxLength="5" TabIndex="20"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                                                      ControlToValidate="txtZip1" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td class="label">Home Phone:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtHome1" runat="server" Width="35px" MaxLength="3" onKeyUp="return autoTab(this, 3, event);" TabIndex="21"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                                                                      ControlToValidate="txtHome1" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                -
                                                                                <asp:TextBox ID="txtHome2" runat="server" Width="35px" MaxLength="3" onKeyUp="return autoTab(this, 3, event);" TabIndex="22"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                                                                      ControlToValidate="txtHome2" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                -
                                                                                <asp:TextBox ID="txtHome3" runat="server" Width="35px" MaxLength="4" onKeyUp="return autoTab(this, 4, event);" TabIndex="23"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                                                                                      ControlToValidate="txtHome3" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td height="20">&nbsp;</td>
                                                                          </tr>
                                                                      </table>
                                                                  </td>
                                                                      <td width="18" background="images/form_right.jpg" style="background-repeat:no-repeat; background-position:top;">&nbsp;</td>
                                                                    </tr>
                                                                  </table>
                                                                  
                                                                  
                                                                  <!-- End Design Information -->   
                                                                         
                                                                  <!-- Fee Information -->
                                                                  <div id="plFees" runat="server">
                                                                    <asp:RadioButtonList ID="rlistFee" runat="server" DataTextField="title" DataValueField="productId" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                                                  </div>
                                                                  <!-- End Fee Information -->
                                                                    
                                                                  <!-- Payment Information -->
                                                                  <div id="pnlPaymentInfo" runat="server">
                                                                  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                      <td width="17" background="images/form_left.jpg" style="background-repeat:no-repeat; background-position:top;">
                                                        				&nbsp;</td>
                                                                      <td valign="top" background="images/form_bg.jpg" style="background-repeat:repeat-x; background-position:top;">
                                                                      <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                                                          <tr>
                                                                            <td height="25" colspan="4"><strong><img src="images/bullet.gif" width="7" height="5" vspace="3" />&nbsp;&nbsp;PAYMENT 
                                                        					INFORMATION</strong></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td height="10" colspan="4"><label></label>
                                                                              <label></label></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td colspan="4"><label><img src="images/cheque_sample.gif" width="363" height="190" /></label></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td height="1" colspan="4" background="images/line.gif"></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td height="10" colspan="4" class="label"></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td><p>Routing #<br />
                                                                            </p></td>
                                                                            <td width="37%">
                                                                                <asp:TextBox ID="txtRoutingNr" runat="server" TabIndex="2"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtRoutingNr"
                                                                                    Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                                                        ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtRoutingNr"
                                                                                        Display="Dynamic" ErrorMessage="must be 9 digits" ValidationExpression="\d{9}"></asp:RegularExpressionValidator></td>
                                                                            <td width="13%">Re-Enter Routing #</td>
                                                                            <td width="37%">
                                                                                <asp:TextBox ID="txtConfirmRoutingNr" runat="server" TabIndex="2"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtConfirmRoutingNr"
                                                                                    Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:CompareValidator
                                                                                        ID="CompareValidator4" runat="server" ControlToCompare="txtRoutingNr" ControlToValidate="txtConfirmRoutingNr"
                                                                                        Display="Dynamic" ErrorMessage="does not match" SetFocusOnError="True"></asp:CompareValidator></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td></td>
                                                                            <td colspan="3"><span class="small">(The first 9 digits in 
                                                        					the lower left hand corner of your check)</span></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td><p>Account #<br />
                                                                            </p></td>
                                                                            <td width="37%">
                                                                                <asp:TextBox ID="txtAccountNr" runat="server" TabIndex="2"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtAccountNr"
                                                                                    Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                                                        ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAccountNr"
                                                                                        Display="Dynamic" ErrorMessage="must be within 15 digits" ValidationExpression="\d{1,15}"></asp:RegularExpressionValidator></td>
                                                                            <td width="13%">Re-Enter Account #</td>
                                                                            <td width="37%">
                                                                                <asp:TextBox ID="txtConfirmAccountNr" runat="server" TabIndex="2"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtConfirmAccountNr"
                                                                                    Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:CompareValidator
                                                                                        ID="CompareValidator5" runat="server" ControlToCompare="txtAccountNr" ControlToValidate="txtConfirmAccountNr"
                                                                                        Display="Dynamic" ErrorMessage="does not match" SetFocusOnError="True"></asp:CompareValidator></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td></td>
                                                                            <td height="25" colspan="3" valign="top">
                                                                              <span class="small">(The proceeding digits after your 
                                                        						Routing #) </span></td>
                                                                          </tr>
                                                                          
                                                                          <tr>
                                                                            <td colspan="4" bgcolor="#FFFFEA" style="border: #efefe2 solid 1px; padding: 5px 5px 5px 5px; font-size:11px; text-align:justify;"><strong>
                                                        					By submitting this application YOU HEREBY AUTHORIZE 
                                                                                <asp:Label ID="lblCompany" runat="server"></asp:Label>, an authorized distributor of the 
                                                        					CashPass Prepaid MasterCard, to debit the bank account provided 
                                                        					above for a one-time fee of $29.95. I also acknowledge that 
                                                        					I am subject to a $25.00 reject fee if the payment is 
                                                        					returned for insufficient funds.</strong></td>
                                                                            </tr>
                                                                          <tr>
                                                                            <td colspan="4">&nbsp;</td>
                                                                          </tr>
                                                                      </table></td>
                                                                      <td width="18" background="images/form_right.jpg" style="background-repeat:no-repeat; background-position:top;">
                                                        				&nbsp;</td>
                                                                    </tr>
                                                                  </table>
                                                                  </div>
                                                                  <!-- End Payment Information -->
                                                                  <!-- Disclosure Information -->
                                                                  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                      <td width="17" height="300" background="images/form_left.jpg" style="background-repeat:no-repeat; background-position:top;">&nbsp;</td>
                                                                      <td valign="top" background="images/form_bg.jpg" style="background-repeat:repeat-x; background-position:top;"><table width="100%" border="0" cellspacing="0" cellpadding="2">
                                                                          <tr>
                                                                            <td height="25" colspan="2"><strong><img src="images/bullet.gif" width="7" height="5" vspace="3" />&nbsp;&nbsp;DISCLOSURE</strong></td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td colspan="2">&nbsp;</td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkIdVerify" runat="server" /></td>
                                                                            <td width="100%" class="style1">I have read and understand 
                                                        					that my identity will be verified when I click &#39;Submit&#39;</td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkTermsVerify" runat="server" /></td>
                                                                            <td class="style1">I have read, understand, and agree to the
                                                        					<a target="_blank" href="../terms.pdf"><span style="text-decoration: underline">Terms and Conditions.</span></a></td>
                                                                          </tr>
                                                                          <tr>
                                                                              <td>
                                                                              </td>
                                                                              <td class="style1">
                                                                              </td>
                                                                          </tr>
                                                                          <tr>
                                                                              <td>
                                                                              </td>
                                                                              <td class="style1">
                                                                                  Insert the code shown below</td>
                                                                          </tr>
                                                                          <tr>
                                                                              <td>
                                                                              </td>
                                                                              <td class="style1">
                                                                                <img src="gencaptcha.aspx" alt="security code" />
                                                                              </td>
                                                                          </tr>
                                                                          <tr>
                                                                              <td>
                                                                              </td>
                                                                              <td class="style1">
                                                                                  <asp:TextBox ID="txtCaptcha" runat="server" TabIndex="2"></asp:TextBox></td>
                                                                          </tr>
                                                                          <tr>
                                                                              <td>
                                                                              </td>
                                                                              <td class="style1">
                                                                                (Note: If you cannot read the numbers in the above<br>
                                                        					image, reset the form to generate a new one)
                                                                              </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td height="50" colspan="2"><div align="center">
                                                                                <asp:LinkButton ID="LinkButton2" runat="server" Text="Submit" OnClick="lnkSubmit_Click" ><img src="images/btn_Submit.gif" width="90" height="22" /></asp:LinkButton>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <a href="index.aspx"><img alt="Reset" src="images/btn_Reset.gif" /></a>
                                                                                </div></td>
                                                                            </tr>
                                                                      </table></td>
                                                                      <td width="18" background="images/form_right.jpg" style="background-repeat:no-repeat; background-position:top;">&nbsp;</td>
                                                                    </tr>
                                                                  </table>
                                                                  <!-- End Disclosure Information -->          </td>
                                                                
                                                              </tr>
                                                            </table>    </td>
                                                          </tr>
                                                        </table>
                                                </form>
                                                </div>
                                        </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fusion-footer">


                <footer class="fusion-footer-widget-area fusion-widget-area">
                    <div class="fusion-row">
                        <div class="fusion-columns fusion-columns-1 fusion-widget-area">

                            <div class="fusion-column fusion-column-last col-lg-12 col-md-12 col-sm-12">
                                <div id="text-3" class="fusion-footer-widget-column widget widget_text">
                                    <div class="textwidget">
                                        <div class="footer_first_widget">
                                            <h2>CashPass</h2>
                                            <p>@2016-2017.</p>
                                            <p><a href="http://staging111.com/cashpass/disclosures/">Disclosures</a></p>
                                            <p>CashPass Prepaid Visa Card.</p>
                                            <p>CashPass Network</p>
                                            <p><span>?</span> CashPass Help Center</p>
                                            <div class="footer_visa">
                                                <img src="images/2017/01/footer_icon_1.png" alt="Footer Logo">
                                            </div>
                                        </div>
                                        <div class="footer_second_widget">
                                            <ul>
                                                <li>
                                                    * This card issued by Metropolitan Commercial Bank, Member FDIC, pursuant to
                                                    a license from Visa USA,<br/>
                                                    Inc. “Metropolitan Commercial Bank” and “Metropolitan” are registered
                                                    trademarks of Metropolitan<br/>
                                                    Commercial Bank copyright 2014. Use of the Card is subject to the terms and
                                                    conditions of the applicable<br/>
                                                    Cardholder Agreement and fee schedule, if any.
                                                </li>
                                                <li>
                                                    * The USA PATRIOT Act is a Federal law that requires all financial
                                                    institutions to obtain, verify, and record<br/>
                                                    information that identifies each person who opens an account. You will be
                                                    asked to provide your name,<br/>
                                                    address, date of birth, and other information that will allow us to identify
                                                    you. You will be asked to<br/>
                                                    provide your name, address, date of birth, and Social Security Number which
                                                    will allow us to identify you.<br/>
                                                    “100% Approval” and “No one is Turned Down” is contingent upon successfully
                                                    passing this mandatory<br/>
                                                    identification confirmation.</p>
                                                <li>
                                                <li>
                                                    ** Faster access to funds is based on comparison of traditional banking
                                                    policies and deposit of paper<br/>
                                                    checks from employers and government agencies versus deposits made
                                                    electronically.</p>
                                                <li>
                                            </ul>
                                        </div>
                                        <div class="third_footer_widget">
                                            <img src="images/2017/01/footer_icon.png" alt="footer Logo">
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>

                            <div class="fusion-clearfix"></div>
                        </div> <!-- fusion-columns -->
                    </div> <!-- fusion-row -->
                </footer> <!-- fusion-footer-widget-area -->


                <footer id="footer" class="fusion-footer-copyright-area">
                    <div class="fusion-row">
                        <div class="fusion-copyright-content">

                            <div class="fusion-copyright-notice">
                                <div></div>
                            </div>

                        </div> <!-- fusion-fusion-copyright-content -->
                    </div> <!-- fusion-row -->
                </footer> <!-- #footer -->
        </div> <!-- fusion-footer --> 
    </div> 
    <a class="fusion-one-page-text-link fusion-page-load-link"></a>

        <script type="text/javascript" src="revslider/js/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="revslider/js/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="revslider/js/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="revslider/js/revolution.extension.navigation.min.js"></script>

        <!-- W3TC-include-js-head -->

        <script type="text/javascript">
                                                        function revslider_showDoubleJqueryError(sliderID) {
                                                            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                                                            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                                                            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                                                            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                                                            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
                                                            jQuery(sliderID).show().html(errorMessage);
                                                        }
        </script>
        <script type="text/javascript">
            var gfRecaptchaPoller = setInterval(function () {
                if (!window.grecaptcha) {
                    return;
                }
                renderRecaptcha();
                clearInterval(gfRecaptchaPoller);
            }, 100);
        </script>

        <script type='text/javascript' src='js/comment-reply.mine100.js?ver=4.7.2'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var toTopscreenReaderText = {"label": "Go to Top"};
            var avadaVars = {"admin_ajax": "http:\/\/staging111.com\/cashpass\/wp-admin\/admin-ajax.php", "admin_ajax_nonce": "1ccf1d1135", "protocol": "", "theme_url": "http:\/\/staging111.com\/cashpass\/wp-content\/themes\/Avada", "dropdown_goto": "Go to...", "mobile_nav_cart": "Shopping Cart", "page_smoothHeight": "false", "flex_smoothHeight": "false", "language_flag": "", "infinite_blog_finished_msg": "<em>All posts displayed.<\/em>", "infinite_finished_msg": "<em>All items displayed.<\/em>", "infinite_blog_text": "<em>Loading the next set of posts...<\/em>", "portfolio_loading_text": "<em>Loading Portfolio Items...<\/em>", "faqs_loading_text": "<em>Loading FAQ Items...<\/em>", "order_actions": "Details", "avada_rev_styles": "1", "avada_styles_dropdowns": "1", "blog_grid_column_spacing": "40", "blog_pagination_type": "Pagination", "carousel_speed": "2500", "counter_box_speed": "1000", "content_break_point": "800", "disable_mobile_animate_css": "0", "disable_mobile_image_hovers": "1", "portfolio_pagination_type": "Infinite Scroll", "form_bg_color": "#ffffff", "header_transparency": "0", "header_padding_bottom": "0px", "header_padding_top": "0px", "header_position": "Top", "header_sticky": "1", "header_sticky_tablet": "0", "header_sticky_mobile": "0", "header_sticky_type2_layout": "menu_only", "sticky_header_shrinkage": "0", "is_responsive": "1", "is_ssl": "false", "isotope_type": "masonry", "layout_mode": "wide", "lightbox_animation_speed": "Fast", "lightbox_arrows": "1", "lightbox_autoplay": "0", "lightbox_behavior": "all", "lightbox_desc": "0", "lightbox_deeplinking": "1", "lightbox_gallery": "1", "lightbox_opacity": "0.97", "lightbox_path": "horizontal", "lightbox_post_images": "0", "lightbox_skin": "metro-white", "lightbox_slideshow_speed": "5000", "lightbox_social": "1", "lightbox_title": "0", "lightbox_video_height": "720", "lightbox_video_width": "1280", "logo_alignment": "Left", "logo_margin_bottom": "13px", "logo_margin_top": "13px", "megamenu_max_width": "1100", "mobile_menu_design": "modern", "nav_height": "83", "nav_highlight_border": "3", "page_title_fading": "1", "pagination_video_slide": "0", "related_posts_speed": "2500", "submenu_slideout": "1", "side_header_break_point": "1100", "sidenav_behavior": "Hover", "site_width": "960px", "slider_position": "below", "slideshow_autoplay": "1", "slideshow_speed": "7000", "smooth_scrolling": "0", "status_lightbox": "1", "status_totop_mobile": "1", "status_vimeo": "1", "status_yt": "1", "testimonials_speed": "4000", "tfes_animation": "sides", "tfes_autoplay": "1", "tfes_interval": "3000", "tfes_speed": "800", "tfes_width": "200", "title_style_type": "double", "title_margin_top": "0px", "title_margin_bottom": "30px", "typography_responsive": "1", "typography_sensitivity": "0.60", "typography_factor": "1.50", "woocommerce_shop_page_columns": "", "woocommerce_checkout_error": "Not all fields have been filled in correctly.", "side_header_width": "0"};
            /* ]]> */
        </script>
        <script type='text/javascript' src='Avada/assets/js/main.min066b.js?ver=5.0.6' async ></script> 
        <!--[if IE 9]>
        <script type='text/javascript' src='js/avada-ie9.js?ver=5.0.6'></script>
        <![endif]-->
        <!--[if lt IE 9]>
        <script type='text/javascript' src='js/respond.js?ver=5.0.6'></script>
        <![endif]-->
        <script type='text/javascript' src='js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
        <script type='text/javascript' src='js/jquery/ui/datepicker.mine899.js?ver=1.11.4'></script>
        <script type='text/javascript'>
            jQuery(document).ready(function (jQuery) {
                jQuery.datepicker.setDefaults({"closeText": "Close", "currentText": "Today", "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], "nextText": "Next", "prevText": "Previous", "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"], "dateFormat": "MM d, yy", "firstDay": 1, "isRTL": false});
            });
        </script>
        <script type='text/javascript' src='gravityforms/js/datepicker.min4c71.js?ver=2.1.3'></script>
        <script type='text/javascript' src='js/wp-embed.mine100.js?ver=4.7.2'></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <script type="text/javascript">

        </script>
        <script>
            (function ($, window) {
                var adjustAnchor = function () {

                    var $anchor = jQuery(':target'),
                            fixedElementHeight = 150;

                    if ($anchor.length > 0) {

                        jQuery('html, body')
                                .stop()
                                .animate({
                                    scrollTop: $anchor.offset().top - fixedElementHeight
                                }, 200);

                    }

                };

                jQuery(window).on('hashchange load', function () {
                    adjustAnchor();
                });

            })(jQuery, window);
        </script>
        <script type="text/javascript">
            jQuery('#cashpasscard_1').click(function (event) {
                event.preventDefault();
                jQuery('#rlistProducts_0').prop('checked', true);
                jQuery('#cashpasscard_1 .fusion-button-text').text("Selected");
                jQuery('#cashpasscard_2 .fusion-button-text').text("Select Card");
            });

            jQuery('#cashpasscard_2').click(function (event) {
                event.preventDefault();
                jQuery('#rlistProducts_1').prop('checked', true);
                jQuery('#cashpasscard_1 .fusion-button-text').text("Select Card");
                jQuery('#cashpasscard_2 .fusion-button-text').text("Selected");
            });
        </script>                                 
</body>
</html>
